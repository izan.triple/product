-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2022 at 07:37 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homestay`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `booking_id` varchar(50) NOT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_phone` int(11) DEFAULT NULL,
  `customer_email` varchar(50) DEFAULT NULL,
  `title` varchar(50) NOT NULL DEFAULT 'Booked',
  `start` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `ownerid` int(11) NOT NULL,
  `homestayid` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `amount` float DEFAULT NULL,
  `day` int(11) NOT NULL,
  `total` float DEFAULT NULL,
  `current_payment` float DEFAULT 0,
  `balance_payment` float DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_phone` int(11) NOT NULL,
  `ownerid` int(11) NOT NULL,
  `creted_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `status` int(11) NOT NULL DEFAULT 2,
  `remarks` varchar(255) NOT NULL,
  `homestayid` int(11) NOT NULL,
  `ownerid` int(11) NOT NULL,
  `creted_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `date`, `description`, `amount`, `status`, `remarks`, `homestayid`, `ownerid`, `creted_date`) VALUES
(5, '2022-05-08', 'Queen bed room set', 759, 2, 'Zone Home Furniture #for master bedroom ', 2, 1, '2022-05-12 14:12:22'),
(6, '2022-05-08', 'Gas cabinet walnut', 376, 2, 'Kabinet dapur (220508MVQ34XRQ)', 2, 1, '2022-05-12 14:15:45'),
(7, '2022-05-06', 'khind ceiling fan 60\"', 692.74, 2, '6 Kipas ceiling', 2, 1, '2022-05-12 14:27:46'),
(8, '2022-05-09', 'Comfort mattress ', 1068, 2, '2 x 8\'\' mattress Single , 2 x 8\" Mattress queen \r\n(220509P54JKY7V,  220509P4QT8VQR, 220509P55S9VYK, 220509P4ECR4FS)', 2, 1, '2022-05-12 16:33:44'),
(9, '2022-05-10', 'Queen bed room set', 758.98, 2, '(SHOPPEE ORDERID: 220510S6B547N0) ', 2, 1, '2022-05-12 16:36:30'),
(10, '2022-05-08', 'Almari baju', 200.2, 2, 'Almari queen room (Shoppee ID : 220508mx9177uk)', 2, 1, '2022-05-12 16:51:02'),
(11, '2022-05-08', 'Kabinet tv', 310.9, 2, 'TV cabinet(shoppee ID: 220508mvx6tb4x)', 2, 1, '2022-05-12 16:53:23'),
(12, '2022-05-08', 'katil single (double decker)', 428.99, 2, 'single room (shoppee ID : 220508MX9177UJ)', 2, 1, '2022-05-12 17:01:55'),
(13, '2022-05-08', 'Kitchen cabinet', 318, 2, '(shoppee ID: 220508MVQ34XRR)', 2, 1, '2022-05-12 17:02:57'),
(16, '2022-05-10', 'Cadar & Bantal	', 639.8, 5, '6 set queen, 3 set single, 11x bantal (SHOPPEE ID : 220510SSCCAH63)	', 2, 1, '2022-05-12 17:11:24'),
(17, '2022-05-13', 'Non stick cookware', 60, 2, 'cookware', 2, 1, '2022-05-13 11:01:48'),
(18, '2022-05-13', 'Cutlery tray', 11.5, 2, '', 2, 1, '2022-05-13 11:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `homestay`
--

CREATE TABLE `homestay` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postcode` int(5) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `phone` int(12) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `datecreated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `homestay`
--

INSERT INTO `homestay` (`id`, `name`, `address1`, `address2`, `city`, `postcode`, `pic`, `phone`, `status`, `datecreated`) VALUES
(1, 'Homestay johor', 'iskandar putri', '', 'iskandar putri', 12345, 'Nor aizan Awang', 132331431, 0, '2022-04-04 03:53:28'),
(2, 'Homestay perak', 'batu gajah', 'Batu gajah', 'perak', 3445, 'Nor aizan Awang', 132331431, 0, '2022-04-04 05:36:12');

-- --------------------------------------------------------

--
-- Table structure for table `homestay_user`
--

CREATE TABLE `homestay_user` (
  `id` int(11) NOT NULL,
  `homestayid` int(11) NOT NULL,
  `ownerid` int(11) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `homestay_user`
--

INSERT INTO `homestay_user` (`id`, `homestayid`, `ownerid`, `userid`) VALUES
(1, 1, 1, 2),
(2, 1, 1, 16),
(3, 2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `maintenance`
--

CREATE TABLE `maintenance` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `status` int(11) NOT NULL DEFAULT 2,
  `remarks` varchar(255) NOT NULL,
  `homestayid` int(11) NOT NULL,
  `ownerid` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE `owner` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contactno` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `createddate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`id`, `name`, `contactno`, `status`, `createddate`) VALUES
(1, 'Nor aizan', '0132331431', 0, '2022-04-04 03:51:17');

-- --------------------------------------------------------

--
-- Table structure for table `owner_homestay`
--

CREATE TABLE `owner_homestay` (
  `ownerid` int(11) NOT NULL,
  `homestayid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `owner_homestay`
--

INSERT INTO `owner_homestay` (`ownerid`, `homestayid`) VALUES
(3, 1),
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `bookingid` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `amount` float NOT NULL,
  `status` int(11) NOT NULL DEFAULT 2,
  `homestayid` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `reportsave`
--

CREATE TABLE `reportsave` (
  `id` int(11) NOT NULL,
  `reportname` varchar(100) NOT NULL,
  `query` varchar(3000) NOT NULL,
  `condition` varchar(1000) NOT NULL,
  `conditionvalue` varchar(50) NOT NULL,
  `header` varchar(3000) NOT NULL,
  `recordedby` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reportsave`
--

INSERT INTO `reportsave` (`id`, `reportname`, `query`, `condition`, `conditionvalue`, `header`, `recordedby`) VALUES
(3, 'Revenue', 'StaffCode as StaffCode, TradeName as TradeName, Bank as Bank, AccountNo as AccountNo, Rental as EDCRental, MIDCredit as MID_Credit, DateInstall as InstallDate,', ' b.id = \'20\' ', '20', 'StaffCode, TradeName, Bank, AccountNo, EDCRental, MID_Credit, InstallDate,', 'sa'),
(5, 'Termination', 'RefNo as ReferenceNo, StaffCode as StaffCode, RegName as RegisteredName, MIDCredit as MID_Credit, DateInstall as InstallDate, DateTerminated as TerminationDate, DateNotice as NoticeDate, TerminateReason as TerminationReason, DPSRental as DPSRental, b.name as Status,', ' ', 'all', 'ReferenceNo, StaffCode, RegisteredName, MID_Credit, InstallDate, TerminationDate, NoticeDate, TerminationReason, DPSRental, Status,', 'izan'),
(6, 'Installation', 'StaffCode as StaffCode, TradeName as TradeName, MIDCredit as MID_Credit, DateInstall as InstallDate,', ' ', 'all', 'StaffCode, TradeName, MID_Credit, InstallDate,', 'izan'),
(7, 'SI LIST', 'RefNo as ReferenceNo, StaffCode as StaffCode, RegName as RegisteredName, TradeName as TradeName, IC as IC, DPSRental as DPSRental, DateRental as RentalStartDate, Deposit as Deposit, DirecDebit as DirectDebit, DPSProcess as ProcessingFee, ReceivedChqTotal as ChequeAmount, DDRemark as ChargesRemark, DDEPICDate as SI_to_EON_Date, b.name as Status,', ' ', 'all', 'ReferenceNo, StaffCode, RegisteredName, TradeName, IC, DPSRental, RentalStartDate, Deposit, DirectDebit, ProcessingFee, ChequeAmount, ChargesRemark, SI_to_EON_Date, Status,', 'izan'),
(8, 'COMMISSION REPORT', 'RefNo as ReferenceNo, StaffCode as StaffCode, RegName as RegisteredName, RegState as State, TradeState as TradeState, MIDCredit as MID_Credit, TerminalID as TerminalID, DateOpenCall as OpenCallDate, DateInstall as InstallDate, DateTerminated as TerminationDate, DPSRental as DPSRental, Deposit as Deposit, DPSProcess as ProcessingFee, ReceivedChqTotal as ChequeAmount, DDRemark as ChargesRemark, DateReceivedStatus as ReceivedStatusDate, b.name as Status,', ' ', 'all', 'ReferenceNo, StaffCode, RegisteredName, State, TradeState, MID_Credit, TerminalID, OpenCallDate, InstallDate, TerminationDate, DPSRental, Deposit, ProcessingFee, ChequeAmount, ChargesRemark, ReceivedStatusDate, Status,', 'izan'),
(9, 'Merchant account list', 'RefNo as ReferenceNo, StaffCode as StaffCode, RegName as RegisteredName, RegNo as CompanyNo, RegAddress1 as MailingAddress1, RegPostCode as PostCode, RegAddress2 as MailingAddress2, RegCity as City, RegState as State, RegTel as Tel, TradeName as TradeName, TradeAddress1 as TradeAddress1, TradePostCode as TradePostCode, TradeAddress2 as TradeAddress2, TradeCity as TradeCity, TradeState as TradeState, TradeTel as TradeTel, ContactPerson as ContactPerson, TradeHP1 as HP1, TradeHP2 as HP2, MIDCredit as MID_Credit, DateInstall as InstallDate, DateTerminated as TerminationDate, DPSRental as DPSRental, Deposit as Deposit, b.name as Status,', ' ', 'all', 'ReferenceNo, StaffCode, RegisteredName, CompanyNo, MailingAddress1, PostCode, MailingAddress2, City, State, Tel, TradeName, TradeAddress1, TradePostCode, TradeAddress2, TradeCity, TradeState, TradeTel, ContactPerson, HP1, HP2, MID_Credit, InstallDate, TerminationDate, DPSRental, Deposit, Status,', 'izan'),
(10, 'INACTIVE CHECKING', 'RefNo as ReferenceNo, RegName as RegisteredName, TradeName as TradeName, TradeState as TradeState, TradeTel as TradeTel, ContactPerson as ContactPerson, TradeHP1 as HP1, TradeHP2 as HP2, MIDCredit as MID_Credit, MIDDebit as MID_Debit, MIDZIIP as MID_ZIIP, TIDDebit as TerminalDebit, TerminalID as TerminalID, TerminalIDZIIP as TerminalIDZIIP, DateInstall as InstallDate, DateTerminated as TerminationDate, b.name as Status,', ' ', 'all', 'ReferenceNo, RegisteredName, TradeName, TradeState, TradeTel, ContactPerson, HP1, HP2, MID_Credit, MID_Debit, MID_ZIIP, TerminalDebit, TerminalID, TerminalIDZIIP, InstallDate, TerminationDate, Status,', 'izan'),
(11, 'MONTHLY REPORT', 'RefNo as ReferenceNo, StaffCode as StaffCode, RegState as State, MIDCredit as MID_Credit,', ' ', 'all', 'ReferenceNo, StaffCode, State, MID_Credit,', 'izan'),
(12, 'Master Report', 'RefNo as ReferenceNo, StaffCode as StaffCode, RegName as RegisteredName, RegState as State, TradeName as TradeName, TradeState as TradeState, MIDCredit as MID_Credit, TerminalID as TerminalID, DateOpenCall as OpenCallDate, EDCSerialNo as EDCSerialNo, SiteID as SiteID, SAMSerialNo as SAMSerialNo, DateInstall as InstallDate, DateTerminated as TerminationDate, TerminateRemark as TerminationRemark, DPSRental as DPSRental, Deposit as Deposit, DPSProcess as ProcessingFee, ReceivedChqNo as ChequeNo, ReceivedChqTotal as ChequeAmount, DDRemark as ChargesRemark, DateSubmit as SubmittedDate, b.name as Status,', ' ', 'all', 'ReferenceNo, StaffCode, RegisteredName, State, TradeName, TradeState, MID_Credit, TerminalID, OpenCallDate, EDCSerialNo, SiteID, SAMSerialNo, InstallDate, TerminationDate, TerminationRemark, DPSRental, Deposit, ProcessingFee, ChequeNo, ChequeAmount, ChargesRemark, SubmittedDate, Status,', 'izan');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `statename` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `statename`) VALUES
(1, 'KUALA LUMPUR'),
(2, 'PUTRAJAYA'),
(3, 'NEGERI SEMBILAN'),
(4, 'SELANGOR'),
(5, 'MELAKA'),
(6, 'JOHOR'),
(7, 'PAHANG'),
(8, 'PERAK'),
(9, 'KEDAH'),
(10, 'PERLIS'),
(11, 'PULAU PINANG'),
(12, 'KELANTAN'),
(13, 'TERENGGANU'),
(14, 'SABAH'),
(15, 'SARAWAK'),
(16, 'LABUAN');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(2) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ind` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`, `ind`) VALUES
(0, 'New', 'ba'),
(1, 'Pay Deposit', 'ba'),
(2, 'Paid', 'ba'),
(3, 'Unpaid', 'ba'),
(4, 'Cancelled', 'ba'),
(5, 'Installment', 'ba');

-- --------------------------------------------------------

--
-- Table structure for table `table 18`
--

CREATE TABLE `table 18` (
  `COL 1` int(4) DEFAULT NULL,
  `COL 2` int(6) DEFAULT NULL,
  `COL 3` varchar(10) DEFAULT NULL,
  `COL 4` varchar(10) DEFAULT NULL,
  `COL 5` varchar(3) DEFAULT NULL,
  `COL 6` varchar(37) DEFAULT NULL,
  `COL 7` varchar(37) DEFAULT NULL,
  `COL 8` varchar(12) DEFAULT NULL,
  `COL 9` varchar(34) DEFAULT NULL,
  `COL 10` varchar(44) DEFAULT NULL,
  `COL 11` int(5) DEFAULT NULL,
  `COL 12` varchar(13) DEFAULT NULL,
  `COL 13` varchar(15) DEFAULT NULL,
  `COL 14` varchar(17) DEFAULT NULL,
  `COL 15` varchar(11) DEFAULT NULL,
  `COL 16` varchar(31) DEFAULT NULL,
  `COL 17` varchar(34) DEFAULT NULL,
  `COL 18` varchar(44) DEFAULT NULL,
  `COL 19` int(5) DEFAULT NULL,
  `COL 20` varchar(13) DEFAULT NULL,
  `COL 21` varchar(15) DEFAULT NULL,
  `COL 22` varchar(11) DEFAULT NULL,
  `COL 23` varchar(11) DEFAULT NULL,
  `COL 24` varchar(14) DEFAULT NULL,
  `COL 25` varchar(11) DEFAULT NULL,
  `COL 26` varchar(17) DEFAULT NULL,
  `COL 27` varchar(21) DEFAULT NULL,
  `COL 28` varchar(11) DEFAULT NULL,
  `COL 29` varchar(19) DEFAULT NULL,
  `COL 30` varchar(10) DEFAULT NULL,
  `COL 31` varchar(4) DEFAULT NULL,
  `COL 32` varchar(4) DEFAULT NULL,
  `COL 33` varchar(4) DEFAULT NULL,
  `COL 34` varchar(26) DEFAULT NULL,
  `COL 35` varchar(10) DEFAULT NULL,
  `COL 36` varchar(10) DEFAULT NULL,
  `COL 37` varchar(10) DEFAULT NULL,
  `COL 38` varchar(10) DEFAULT NULL,
  `COL 39` varchar(13) DEFAULT NULL,
  `COL 40` varchar(217) DEFAULT NULL,
  `COL 41` varchar(10) DEFAULT NULL,
  `COL 42` varchar(10) DEFAULT NULL,
  `COL 43` varchar(10) DEFAULT NULL,
  `COL 44` varchar(10) DEFAULT NULL,
  `COL 45` varchar(10) DEFAULT NULL,
  `COL 46` varchar(10) DEFAULT NULL,
  `COL 47` varchar(10) DEFAULT NULL,
  `COL 48` varchar(27) DEFAULT NULL,
  `COL 49` varchar(4) DEFAULT NULL,
  `COL 50` varchar(10) DEFAULT NULL,
  `COL 51` bigint(12) DEFAULT NULL,
  `COL 52` varchar(89) DEFAULT NULL,
  `COL 53` varchar(10) DEFAULT NULL,
  `COL 54` varchar(10) DEFAULT NULL,
  `COL 55` varchar(10) DEFAULT NULL,
  `COL 56` varchar(10) DEFAULT NULL,
  `COL 57` varchar(10) DEFAULT NULL,
  `COL 58` varchar(10) DEFAULT NULL,
  `COL 59` varchar(10) DEFAULT NULL,
  `COL 60` varchar(10) DEFAULT NULL,
  `COL 61` varchar(10) DEFAULT NULL,
  `COL 62` int(2) DEFAULT NULL,
  `COL 63` varchar(143) DEFAULT NULL,
  `COL 64` bigint(15) DEFAULT NULL,
  `COL 65` bigint(15) DEFAULT NULL,
  `COL 66` bigint(15) DEFAULT NULL,
  `COL 67` int(8) DEFAULT NULL,
  `COL 68` varchar(8) DEFAULT NULL,
  `COL 69` int(8) DEFAULT NULL,
  `COL 70` varchar(10) DEFAULT NULL,
  `COL 71` decimal(3,2) DEFAULT NULL,
  `COL 72` decimal(3,2) DEFAULT NULL,
  `COL 73` varchar(327) DEFAULT NULL,
  `COL 74` int(4) DEFAULT NULL,
  `COL 75` varchar(17) DEFAULT NULL,
  `COL 76` varchar(11) DEFAULT NULL,
  `COL 77` varchar(16) DEFAULT NULL,
  `COL 78` varchar(10) DEFAULT NULL,
  `COL 79` varchar(9) DEFAULT NULL,
  `COL 80` varchar(10) DEFAULT NULL,
  `COL 81` varchar(9) DEFAULT NULL,
  `COL 82` varchar(10) DEFAULT NULL,
  `COL 83` varchar(10) DEFAULT NULL,
  `COL 84` varchar(10) DEFAULT NULL,
  `COL 85` varchar(10) DEFAULT NULL,
  `COL 86` varchar(10) DEFAULT NULL,
  `COL 87` varchar(10) DEFAULT NULL,
  `COL 88` varchar(10) DEFAULT NULL,
  `COL 89` varchar(10) DEFAULT NULL,
  `COL 90` varchar(10) DEFAULT NULL,
  `COL 91` decimal(5,2) DEFAULT NULL,
  `COL 92` int(3) DEFAULT NULL,
  `COL 93` varchar(10) DEFAULT NULL,
  `COL 94` varchar(10) DEFAULT NULL,
  `COL 95` varchar(10) DEFAULT NULL,
  `COL 96` varchar(17) DEFAULT NULL,
  `COL 97` decimal(6,2) DEFAULT NULL,
  `COL 98` varchar(10) DEFAULT NULL,
  `COL 99` varchar(10) DEFAULT NULL,
  `COL 100` varchar(10) DEFAULT NULL,
  `COL 101` varchar(10) DEFAULT NULL,
  `COL 102` varchar(10) DEFAULT NULL,
  `COL 103` varchar(80) DEFAULT NULL,
  `COL 104` varchar(10) DEFAULT NULL,
  `COL 105` varchar(10) DEFAULT NULL,
  `COL 106` varchar(10) DEFAULT NULL,
  `COL 107` decimal(3,2) DEFAULT NULL,
  `COL 108` varchar(10) DEFAULT NULL,
  `COL 109` decimal(3,2) DEFAULT NULL,
  `COL 110` varchar(10) DEFAULT NULL,
  `COL 111` decimal(3,2) DEFAULT NULL,
  `COL 112` varchar(10) DEFAULT NULL,
  `COL 113` decimal(3,2) DEFAULT NULL,
  `COL 114` varchar(10) DEFAULT NULL,
  `COL 115` decimal(4,2) DEFAULT NULL,
  `COL 116` varchar(10) DEFAULT NULL,
  `COL 117` varchar(15) DEFAULT NULL,
  `COL 118` varchar(8) DEFAULT NULL,
  `COL 119` decimal(5,2) DEFAULT NULL,
  `COL 120` varchar(2) DEFAULT NULL,
  `COL 121` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_list`
--

CREATE TABLE `tbl_list` (
  `listid` int(11) NOT NULL,
  `listname` varchar(100) NOT NULL,
  `listcat` varchar(100) NOT NULL,
  `listurl` varchar(100) NOT NULL,
  `listmemberfk` int(11) NOT NULL,
  `listcode` varchar(100) NOT NULL,
  `listref` varchar(100) NOT NULL,
  `listaddifo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_list`
--

INSERT INTO `tbl_list` (`listid`, `listname`, `listcat`, `listurl`, `listmemberfk`, `listcode`, `listref`, `listaddifo`) VALUES
(1, 'Package A', 'PKG', '', 1, '1', '', ''),
(2, 'Package B', 'PKG', '', 0, '2', '', ''),
(3, 'Package C', 'PKG', '', 0, '3', '', ''),
(4, 'Malaysia', 'COUNTRY', '', 0, '2', '', ''),
(5, 'Selangor', 'STATE', '', 4, 'SEL', '', ''),
(6, 'Kuala Lumpur', 'STATE', '', 4, 'KUL', '', ''),
(11, 'Out', 'ACCTYPE', '', 0, '1', '', ''),
(12, 'In', 'ACCTYPE', '', 0, '2', '', ''),
(13, 'Assets', 'bssheet', '', 0, 'main', '', ''),
(14, 'Liabilities', 'bssheet', '', 0, 'main', '', ''),
(15, 'Owner\'s Equity', 'bssheet', '', 0, 'main', '', ''),
(16, 'Current Assets', 'bssheet', '', 13, 'sub', '', ''),
(17, '	Property Equipment', 'bssheet', '', 13, 'sub', '', ''),
(18, 'Goodwill', 'bssheet', '', 13, 'sub', '', ''),
(19, 'Current Liabilities', 'bssheet', '', 14, 'sub', '', ''),
(20, 'Long-term debt', 'bssheet', '', 14, 'sub', '', ''),
(21, 'Other long-term liabilities', 'bssheet', '', 14, 'sub', '', ''),
(24, 'Equity Capital', 'bssheet', '', 15, 'sub', '', ''),
(25, 'Retained Earnings', 'bssheet', '', 15, 'sub', '', ''),
(40, 'Cash', 'bssheet', '', 16, 'item', '', ''),
(41, 'Inventory', 'bssheet', '', 16, 'item', '', ''),
(42, 'Accrued Expenses', 'bssheet', '', 19, 'item', '', ''),
(43, 'Account Payable', 'bssheet', '', 19, 'item', '', ''),
(44, 'Johor', 'STATE', '', 4, 'JHR', '', ''),
(45, 'Kedah', 'STATE', '', 4, 'KDH', '', ''),
(46, 'Kelantan', 'STATE', '', 4, 'KLTN', '', ''),
(47, 'Melaka', 'STATE', '', 4, 'MLK', '', ''),
(48, 'Penang', 'STATE', '', 4, 'PENANG', '', ''),
(49, 'Perak', 'STATE', '', 4, 'PRK', '', ''),
(50, 'Perlis', 'STATE', '', 4, 'PLS', '', ''),
(51, 'Negeri Sembilan', 'STATE', '', 4, 'NS', '', ''),
(52, 'Terengganu', 'STATE', '', 4, 'TRG', '', ''),
(53, 'Sarawak', 'STATE', '', 4, 'SRW', '', ''),
(54, 'Sabah', 'STATE', '', 4, 'SBH', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `menuid` int(11) NOT NULL,
  `menuclass` varchar(100) NOT NULL,
  `menuTitle` varchar(100) NOT NULL,
  `menuname` varchar(100) NOT NULL,
  `menuurl` varchar(100) NOT NULL,
  `menuStatus` varchar(10) NOT NULL,
  `menuclassstatus` varchar(100) NOT NULL,
  `menutype` int(11) NOT NULL,
  `menuquick` varchar(11) NOT NULL,
  `menustate` varchar(50) NOT NULL DEFAULT 'Active',
  `menuicon` varchar(50) NOT NULL,
  `menugroup` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menuid`, `menuclass`, `menuTitle`, `menuname`, `menuurl`, `menuStatus`, `menuclassstatus`, `menutype`, `menuquick`, `menustate`, `menuicon`, `menugroup`) VALUES
(1, 'fa fa-dashboard', 'Dashboard', 'Dashboard', 'dashboard', '', '', 0, '', 'Active', 'dashboard', 'D'),
(2, 'fa fa-dashboard', '', 'Admin Management', 'admin', 'P', 'active', 0, '', 'Active', 'supervisor_account', 'M'),
(4, 'fa fa-file', '', 'Menu', 'menu', '', '', 1, '', 'Active', '', ''),
(5, 'fa fa-device', 'Configuration', 'Configuration', 'setting', 'P', '', 0, '', 'Active', 'supervisor_account', 'M'),
(12, 'fa fa-device', 'Merchant', 'Merchant', 'merchant', '', '', 0, '', 'Inactive', 'supervisor_account', 'M'),
(14, 'fa fa-device', 'Owner', 'Owner', 'owner', '', '', 0, '', 'Active', 'supervisor_account', 'M'),
(20, 'fa fa-report', 'Report', 'Report', 'report', '', '', 0, '', 'Active', 'insights', 'M'),
(22, 'fa fa-device', 'Homestay', 'Homestay', 'mhomestay', '', '', 0, '', 'Active', 'supervisor_account', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_month`
--

CREATE TABLE `tbl_month` (
  `mid` int(11) NOT NULL,
  `mname` varchar(100) NOT NULL,
  `mvalue` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_month`
--

INSERT INTO `tbl_month` (`mid`, `mname`, `mvalue`) VALUES
(1, 'January', '1'),
(2, 'February', '2'),
(3, 'March', '3'),
(4, 'April', '4'),
(5, 'May', '5'),
(6, 'June', '6'),
(7, 'July', '7'),
(8, 'August', '8'),
(9, 'September', '9'),
(10, 'October', '10'),
(11, 'November', '11'),
(12, 'December', '12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_passwordhistory`
--

CREATE TABLE `tbl_passwordhistory` (
  `username` varchar(60) DEFAULT NULL,
  `user_password` varchar(40) DEFAULT NULL,
  `datehistory` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_passwordhistory`
--

INSERT INTO `tbl_passwordhistory` (`username`, `user_password`, `datehistory`) VALUES
('sa', '7fe9dd825ad2cdfff3300bed95978f07', '2020-09-16 06:01:42'),
('izan', '7fe9dd825ad2cdfff3300bed95978f07', '2020-09-16 06:01:42'),
('pika', '25fffa0dd918af0c4ac47d8e8c6de9d4', '2020-09-16 06:01:42'),
('admin', '7fe9dd825ad2cdfff3300bed95978f07', '2020-09-16 06:01:42'),
('yana', '9d80ecc4cf8abc9503027c004db1f43e', '2020-09-16 06:01:42'),
('damon', '2a38af9db9ac56046bba0f13e5523003', '2020-09-16 06:01:42'),
('nabila', '2a38af9db9ac56046bba0f13e5523003', '2020-09-16 06:01:42'),
('yuen', '09b41b93bb33afdd2a1e01b736d6023b', '2020-09-16 06:01:42'),
('chien', '2a38af9db9ac56046bba0f13e5523003', '2020-09-16 06:01:42'),
('elie', '3074bbdcf7f08e3e71fbb8e51907bb35', '2020-09-16 06:01:42'),
('huilu', '672941c662206abda5ca237b8ae43481', '2020-09-16 06:01:42'),
('izzati', '2a38af9db9ac56046bba0f13e5523003', '2020-09-16 06:01:42'),
('Norhaya', '2a38af9db9ac56046bba0f13e5523003', '2020-09-16 06:01:42'),
('test', '89ffddefca379cea04b8860ab9de52fa', '2020-09-16 06:01:42'),
('izan', 'd2305dbccc807b7876556f27b061929d', '2021-08-14 11:16:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE `tbl_role` (
  `roleid` int(11) NOT NULL,
  `rolename` varchar(50) NOT NULL,
  `roledesc` varchar(1000) NOT NULL,
  `landingpage` varchar(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `createdby` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`roleid`, `rolename`, `roledesc`, `landingpage`, `dateCreated`, `createdby`) VALUES
(1, 'sa', 'sa', '', '2020-01-18 04:28:40', 'izan'),
(2, 'Administrator', 'Administrator', '', '2020-01-18 04:29:01', 'izan'),
(3, 'Viewer', 'Viewer role', '', '2020-09-17 07:22:23', 'sa'),
(6, 'sa', 'sa', '', '2022-04-06 01:21:40', 'izan'),
(7, 'Administrator', 'Administrator', '', '2022-04-06 01:21:59', 'izan'),
(8, 'Viewer', 'Viewer role', '', '2022-04-06 02:19:20', 'izan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role_access`
--

CREATE TABLE `tbl_role_access` (
  `accessid` int(11) NOT NULL,
  `access` varchar(1) NOT NULL,
  `fk_menuid` int(11) NOT NULL,
  `fk_submenuid` int(11) NOT NULL,
  `fk_roleid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role_access`
--

INSERT INTO `tbl_role_access` (`accessid`, `access`, `fk_menuid`, `fk_submenuid`, `fk_roleid`) VALUES
(10, 'F', 1, 11, 2),
(11, 'F', 1, 12, 2),
(12, 'F', 1, 0, 2),
(17, 'F', 2, 0, 1),
(18, 'F', 2, 11, 1),
(19, 'F', 2, 12, 1),
(30, 'F', 10, 0, 1),
(35, 'F', 1, 0, 1),
(37, 'F', 13, 0, 2),
(38, 'F', 13, 120, 2),
(39, 'F', 13, 121, 2),
(40, 'F', 13, 121, 1),
(41, 'F', 13, 120, 1),
(42, 'F', 13, 0, 1),
(44, 'F', 2, 13, 1),
(45, 'F', 2, 14, 1),
(48, 'F', 2, 10, 1),
(50, 'V', 13, 121, 3),
(54, 'V', 13, 0, 3),
(57, 'F', 20, 0, 1),
(58, 'F', 14, 0, 1),
(60, 'F', 22, 0, 3),
(61, 'F', 22, 0, 1),
(62, 'F', 22, 0, 2),
(63, 'F', 14, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sitesetting`
--

CREATE TABLE `tbl_sitesetting` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `iconsize` int(15) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `minlength` int(11) NOT NULL,
  `inactiveday` int(11) NOT NULL,
  `passexpirymin` int(11) NOT NULL,
  `passexpiryday` int(11) NOT NULL,
  `passexpirywarn` int(11) NOT NULL,
  `passhistory` int(11) NOT NULL,
  `concurrenlogin` int(11) NOT NULL,
  `lockduration` int(11) NOT NULL,
  `locattempt` int(11) NOT NULL,
  `smtpuser` varchar(100) NOT NULL,
  `smtphost` varchar(100) NOT NULL,
  `smtpport` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sitesetting`
--

INSERT INTO `tbl_sitesetting` (`id`, `title`, `iconsize`, `domain`, `ip`, `minlength`, `inactiveday`, `passexpirymin`, `passexpiryday`, `passexpirywarn`, `passhistory`, `concurrenlogin`, `lockduration`, `locattempt`, `smtpuser`, `smtphost`, `smtpport`) VALUES
(1, 'Master Management', 40, '0', '', 8, 100, 3, 90, 14, 24, 1, 50, 3, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submenu`
--

CREATE TABLE `tbl_submenu` (
  `submenuid` int(11) NOT NULL,
  `submenuname` varchar(100) NOT NULL,
  `submenuurl` varchar(100) NOT NULL,
  `menuid` int(11) NOT NULL,
  `menuquick` varchar(11) NOT NULL,
  `submenustatus` varchar(50) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_submenu`
--

INSERT INTO `tbl_submenu` (`submenuid`, `submenuname`, `submenuurl`, `menuid`, `menuquick`, `submenustatus`) VALUES
(10, 'General Setting', 'gsetting', 2, '', 'Active'),
(11, 'Role', 'role', 2, '', 'Active'),
(12, 'User', 'user', 2, '', 'Active'),
(13, 'Menu', 'menu', 2, '', 'Inactive'),
(14, 'Maintenance', 'maintenance', 2, '', 'Active'),
(51, 'Salesman', 'salesman', 5, '', 'Active'),
(120, 'Generate Report', 'greport', 13, '', 'Active'),
(121, 'Saved Report', 'sreport', 13, '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(60) DEFAULT NULL,
  `user_password` varchar(40) DEFAULT NULL,
  `fk_roleid` int(11) NOT NULL,
  `bank` int(11) NOT NULL,
  `userstatus` varchar(10) NOT NULL DEFAULT 'Active',
  `dateCreated` date NOT NULL,
  `passchangedate` date NOT NULL,
  `createdby` varchar(50) NOT NULL,
  `lastLogin` datetime NOT NULL DEFAULT current_timestamp(),
  `loginsession` int(11) NOT NULL,
  `user_attempt` int(11) NOT NULL,
  `user_log` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `name`, `username`, `user_password`, `fk_roleid`, `bank`, `userstatus`, `dateCreated`, `passchangedate`, `createdby`, `lastLogin`, `loginsession`, `user_attempt`, `user_log`) VALUES
(1, 'System Admin', 'sa', '7fe9dd825ad2cdfff3300bed95978f07', 1, 0, 'Active', '2020-06-01', '2020-09-16', '', '2021-11-15 04:54:43', 0, 1, 0),
(2, 'Nor Aizan Awang', 'izan', 'd2305dbccc807b7876556f27b061929d', 1, 0, 'Active', '2020-06-01', '2021-08-14', '', '2022-05-13 10:49:15', 1, 0, 0),
(16, 'suriyati', 'suriyati', '0815f2f50a1ff8ac2b820f71f66303d4', 3, 0, 'Active', '2022-04-06', '0000-00-00', 'izan', '2022-04-06 16:07:45', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_year`
--

CREATE TABLE `tbl_year` (
  `yearid` int(11) NOT NULL,
  `yearname` int(11) NOT NULL,
  `yearvalue` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_year`
--

INSERT INTO `tbl_year` (`yearid`, `yearname`, `yearvalue`) VALUES
(10, 2015, '2015'),
(11, 2016, '2016'),
(12, 2017, '2017'),
(13, 2018, '2018'),
(14, 2019, '2019'),
(25, 2020, '2020'),
(26, 2021, '2021'),
(27, 2022, '2022');

-- --------------------------------------------------------

--
-- Table structure for table `trails`
--

CREATE TABLE `trails` (
  `ID` int(11) NOT NULL,
  `mID` varchar(100) DEFAULT NULL,
  `aDate` datetime(6) DEFAULT NULL,
  `aUser` varchar(20) DEFAULT NULL,
  `aRemark` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE `user_log` (
  `id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `activity` varchar(1000) NOT NULL,
  `createdby` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id`, `datetime`, `activity`, `createdby`) VALUES
(1, '2020-05-15 16:24:45', 'Create user Yana', 'sa'),
(2, '2020-05-17 10:24:46', 'Create user sabila', 'sa'),
(3, '2020-05-17 10:30:12', 'Create user damon', 'sa'),
(4, '2020-09-07 08:50:03', 'Create user test', 'admin'),
(5, '2020-06-08 12:05:02', 'Update inactive user chien', 'System'),
(6, '2020-06-08 15:32:25', 'Update inactive user user test', 'System'),
(7, '2020-09-17 05:39:39', 'Login attempt failed . 1', 'yana'),
(8, '2020-09-17 05:46:03', 'Login attempt failed . 1', 'Nabila'),
(9, '2020-09-17 05:59:40', 'Login attempt failed . 1', 'sa'),
(10, '2020-09-17 08:38:10', 'Login attempt failed . 1', 'yuen'),
(11, '2020-09-08 12:01:02', 'Delete inactive user chien after 90 days', 'System');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homestay`
--
ALTER TABLE `homestay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homestay_user`
--
ALTER TABLE `homestay_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maintenance`
--
ALTER TABLE `maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reportsave`
--
ALTER TABLE `reportsave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `tbl_list`
--
ALTER TABLE `tbl_list`
  ADD PRIMARY KEY (`listid`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`menuid`);

--
-- Indexes for table `tbl_month`
--
ALTER TABLE `tbl_month`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `tbl_role`
--
ALTER TABLE `tbl_role`
  ADD PRIMARY KEY (`roleid`);

--
-- Indexes for table `tbl_role_access`
--
ALTER TABLE `tbl_role_access`
  ADD PRIMARY KEY (`accessid`);

--
-- Indexes for table `tbl_sitesetting`
--
ALTER TABLE `tbl_sitesetting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  ADD PRIMARY KEY (`submenuid`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_year`
--
ALTER TABLE `tbl_year`
  ADD PRIMARY KEY (`yearid`);

--
-- Indexes for table `trails`
--
ALTER TABLE `trails`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `homestay`
--
ALTER TABLE `homestay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `homestay_user`
--
ALTER TABLE `homestay_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `maintenance`
--
ALTER TABLE `maintenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reportsave`
--
ALTER TABLE `reportsave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_list`
--
ALTER TABLE `tbl_list`
  MODIFY `listid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `menuid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_month`
--
ALTER TABLE `tbl_month`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_role`
--
ALTER TABLE `tbl_role`
  MODIFY `roleid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_role_access`
--
ALTER TABLE `tbl_role_access`
  MODIFY `accessid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `tbl_sitesetting`
--
ALTER TABLE `tbl_sitesetting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_year`
--
ALTER TABLE `tbl_year`
  MODIFY `yearid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `trails`
--
ALTER TABLE `trails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
