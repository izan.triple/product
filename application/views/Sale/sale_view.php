<!-- Exportable Table -->
<div class="row clearfix">

    <div class="body">

        <form method="POST" id="mform" action='<?php echo base_url().'submitsale/'; ?>'>
            <input type="hidden" name="rndid" value="<?php echo $rndid; ?>">
            <input type="hidden" name="prodid" value="<?php echo $prodid; ?>">
            <div class="modal-body">
                <div class="form-group">
                    <button type="button"
                        onclick="window.location.href='<?php echo base_url().'rnditemdetail/'.$prodid.'/'.$rndid; ?>'"
                        style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn'
                        value="saveuser"><i class="fa fa-arrow-left"></i> <b>Back</b></button>
                    <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                        name='btn' value="savesale"><i class="fa fa-save"></i> <b>Save</b></button>
                </div>
                <div class="form-group">
                    <label>Date</label>
                    <input name="date" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
                </div>

                <div class="form-group">
                    <label>Customer Name</label>
                    <input name="custname" type="text" class="form-control" placeholder="Customer name" required>
                </div>
                <div class="form-group">
                    <label>Customer Address</label>
                    <textarea name="custaddress" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label>Customer Poscode</label>
                        <input name="custposcode" type="text" class="form-control" placeholder="Customer poscode">
                    </div>
                    <div class="col-md-4">
                        <label>Customer City</label>
                        <input name="custcity" type="text" class="form-control" placeholder="Customer city">
                    </div>
                    <div class="col-md-4">
                        <label>State</label>
                        <input name="state" type="text" class="form-control">
                    </div>
                    <div class="col-md-4">
                        <label>Customer Phone</label>
                        <input name="custphone" type="text" class="form-control" placeholder="Customer phone">
                    </div>
                    <div class="col-md-4">
                        <label>Customer Email</label>
                        <input name="custemail" type="text" class="form-control" placeholder="Customer email">
                    </div>
                </div>
            </div>
            <hr>

        </form>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">

                </div>
            </div>
        </div>

    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">
                    <table id="myTable" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Date </th>
                                <th>Sale ID </th>
                                <th>Contact Info </th>
                                <th>Phone</th>
                                
                                <th>Total (RM) </th>
                                <th>Paid (RM) </th>
                                <th>Balance (RM)</th>
                                <th>Status </th>
                                <th width='10%'>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($saleinfo as $key => $value) {
                                    echo "<tr>";
                                    echo "<td>". $value['date'] ."</td>";
                                    echo "<td><a href='".base_url()."editowner/". $value['id'] ."'>". $value['sale_id'] ."<br></a></td>";
                                    echo "<td>". $value['customer_name'] ."</td>";
                                    echo "<td align='right'>". $value['customer_phone'] ."</td>";
                                    echo "<td align='right'>". number_format($value['total'],2) ."</td>";
                                    echo "<td align='right'>". number_format($value['current_payment'],2) ."</td>";
                                    echo "<td align='right'>". number_format($value['balance_payment'],2) ."</td>";
                                    
                                    if($value['status'] == 2){
                                        echo "<td bgcolor='green' class='text-center text-white '><span class='label success'>". $value['name'] ."</span></td>";
                                    }else  if($value['status'] == 0){
                                        echo "<td bgcolor='red' class='text-center '><span class='label danger'>". $value['name'] ."</span></td>";
                                    }else{
                                        echo "<td bgcolor='orange' class='text-center'><span class='label info'>". $value['name'] ."</span></td>";
                                    }
                                    echo "<td width='10%'>
                                    <a href='".base_url()."listpayment/". $value['id'] ."/".$prodid."'><i class='fa fa-money'></i></a>";
                                    if(GAMMA::getSession('role') != 3){
                                        echo "  <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a>";
                                        echo "  <a onclick='cancel(". $value['id'] .")' href=''><i class='fa fa-ban'></i></a>";
                                    }else if($value['status'] == 2 && GAMMA::getSession('role') != 3){
                                        echo "  <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a>";
                                        echo "  <a onclick='cancel(". $value['id'] .")' href=''><i class='fa fa-ban'></i></a>";
                                    }else if($value['status'] != 2 && GAMMA::getSession('role') == 3){
                                        echo "  <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a>";
                                        echo "  <a onclick='cancel(". $value['id'] .")' href=''><i class='fa fa-ban'></i></a>";
                                    }else{
                                        
                                    }
                                    echo "</td></tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- #END# Exportable Table -->

<!-- ******************************* Modal Source Keyg*********************************** -->
<div class="modal fade" id="sourceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><label id="ttitle"></label></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="mform" action='<?php echo base_url().'stockaction/'; ?>'>
                    <input type="hidden" name="ownerid" value="<?php echo $ownerid; ?>">
                    <input type="hidden" name="homestayid" value="<?php echo $homestayid; ?>">
                    <input type='hidden' name='id' id='id' class="form-control">
                    <table width="100%">
                        <tr>
                            <td>Date</td>
                            <td>:</td>
                            <td><input type='date' name='edate' id='edate' class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td>:</td>
                            <td><input type='text' name='edescription' id='edescription' class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Unit Price (RM)</td>
                            <td>:</td>
                            <td><input type='text' name='eunitprice' id='eunitprice' class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Quantity</td>
                            <td>:</td>
                            <td><input type='text' name='equantity' id='equantity' class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Amount</td>
                            <td>:</td>
                            <td><input type='text' name='eamount' id='eamount' class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Unit</td>
                            <td>:</td>
                            <td><input type='text' name='eunit' id='eunit' class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Remarks</td>
                            <td>:</td>
                            <td><textarea name='eremark' id='eremark' class="form-control"></textarea></td>
                        </tr>
                    </table>

            </div>
            <div class="modal-footer">
                <input type="submit" class='btn btn-success btn-bg' id='Save' name='btn' value="edit">
            </div>
            </form>
        </div>
    </div>
</div>
<!-- ****************************************************************** -->





</div>

<script>
function edit(id) {
    url = '<?php echo base_url();?>RndStock/edititm/' + id;
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": "GET",
        "headers": {
            "cache-control": "no-cache"
        }
    }

    $.ajax(settings).done(function(response) {
        console.log(response);
        const obj = JSON.parse(response);
        // alert(obj.description);
        $('#id').val(obj.id);
        $('#edate').val(obj.date);
        $('#edescription').val(obj.description);
        $('#eunitprice').val(obj.unit_price);
        $('#equantity').val(obj.quantity);
        $('#eunit').val(obj.unit);
        $('#estatus').val(obj.status);
        $('#eremark').val(obj.remarks);
    });
}

$('#myTable').DataTable();



function delitm(id) {
    var del = confirm("Are you sure you want to delete this record?");
    if (del == true) {

        url = '<?php echo base_url();?>RndStock/deleteitm/' + id;

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "headers": {
                "cache-control": "no-cache"
            }
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
            alert(response);
        });

    }
}
</script>


<!-- <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script> -->