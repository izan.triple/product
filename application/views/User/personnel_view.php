
<style>

#dtable_filter{
  display:none;
}

.btn-group, .fixed-table-loading{
  display: none;
}

.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

<div>
 <form  method="POST" action='<?php echo base_url().'User/useraction/'; ?>' >
   <h4>Personnel Infomation </h4><hr><br>

<table width="100%">
  <tr><td colspan="2" >&nbsp;&nbsp;
    <button  name="btn" type="submit"  class="btn btn-sm btn-success" style="width: 150px;" value="add_personnel"><i class="fa fa-plus" > Add </i></button>
  </td></tr>
</table></form><br>
<div class="loader"></div>
<table border="0" id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="true" data-toolbar="#toolbar">
    <thead>
        <tr>
            <th>Name</th>
            <th>IC No</th>
            <th>Email</th>
            <th>Tel No.</th>
            <th>Status</th>
            <th>Action </th>
        </tr>
    </thead>
     <tbody id="txn">
       <?php
       foreach ($listpersonnel as $p) {
         echo "<tr>";
         echo "<td>$p->personnelname</td>";
         echo "<td>$p->personnelic</td>";
         echo "<td>$p->personnelemail</td>";
         echo "<td>$p->personneltel1</td>";
         echo "<td>";
         if($p->personnelstatus == 'Active'){
           echo "<span class='label success'>$p->personnelstatus</span>";
         }else{
           echo "<span class='label warning'>$p->personnelstatus</span>";
         }
         echo "</td>";
         echo "<td><a href='personnelinfo/$p->personnelid'><i class='fa fa-edit'></i></a></td>";
         echo "</tr>";
       }
       ?>
     </tbody>

</table>

</div>


<script type="text/javascript">

$( ".loader").hide();
</script>
