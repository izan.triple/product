

<!-- Exportable Table -->
<form  action="<?php echo base_url().'User/changePasswd/'.$id?>" method="post"><br>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card"><br>
        <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value="saveuser" target=_blank><i class="fa fa-save" ></i> <b>Save </b></button>

            <div class="body" style="height:300px;overflow:auto;background:#fff;">
                <div class="table-responsive">
				<input style="width:70%;" type='hidden' name='euid' value="<?php echo $id ?>">
                <b><span style='background-color:red;color:white' class='badge badge-success'><?php echo $this->session->flashdata('msg');?></span></b>
					<table width='80%'>
					<tr><td>Username</td><td>: <input disabled style="width:50%;" type='text' name='eusername' value="<?php echo $userinfo[0]->username ?>"></td></tr>
					<tr><td>Old Password</td><td>: <input style="width:50%;" type='password' name='passwordold' required></td></tr>
                    <tr><td>New Password</td><td>: <input style="width:50%;" type='password' name='passwordnew' minlength="8" required></td></tr>
                    <tr><td>Re-enter New Password</td><td>: <input style="width:50%;" type='password' name='passwordnew2' minlength="8" required><br>
                    <div  style="font-size:10px;color:red;">*MUST contain at least 8 characters (12+ recommended)<br>
                    *MUST contain at least 1 digit and 1 character<br>
                    </div>
                    </td></tr>
						<tr><td colspan="2"><br></td></tr>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->




</div>

</form>


