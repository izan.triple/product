
<style>

.form-control {
  height: 25px;
}

.fixed-table-loading{
  display:none;
}
</style>
<div class="body">
  <form  method="POST" action='<?php echo base_url().'User/useraction'; ?>' >
    <table width="100%">
      
      <tr><td colspan="2"><br>
        <button type="submit" style="width:130px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value="updatesetting"><i class="fa fa-save" ></i> <b>Update Setting</b></button>
      </td>
    </table>


</div>
<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div class="table-responsive">
                    <table width="100%">
                    <tr>
                    <td>
                    <h5>General Settings</h5>
                    <table width="100%">
                      <tr><td width="30%">Site Title</td><td>: <input type="text" name="site" style="width:70%;" value="<?php if($setting[0]->title){echo $setting[0]->title;} ?>"></td><tr>
                      <tr><td>Site Code</td><td>:  <input type="text" value="DPS" name="code" style="width:70%;"></td><tr>
                      <tr><td>Site Local IP</td><td>:  <input type="text" name="ip" value="<?php if($setting[0]->title){echo $setting[0]->ip;} ?>" style="width:70%;"></td><tr>
                      <tr><td>Site Domain</td><td>:  <input type="text" name="domain" value="<?php if($setting[0]->title){echo $setting[0]->domain;} ?>" style="width:70%;"></td><tr>
                      <tr><td>API URL</td><td>:  <input type="text" name="url" value="<?php if($setting[0]->url){echo $setting[0]->url;} ?>" style="width:70%;"></td><tr>
                    </table>
                    </td>
                    </tr>


                    <tr>
                    <td><hr>
                    <h5>Account & Password Settings</h5>
                    <table width="100%">
                      <tr><td width="30%">Minimum Password Length</td><td>: <input type="number" name="minlength" value="<?php if($setting[0]->minlength){echo $setting[0]->minlength;} ?>" size="10px"> </td><tr>
                      <tr><td width="20%">Inactive User Login (Day)</td><td>: <input type="number" name="inactiveday" value="<?php if($setting[0]->inactiveday){echo $setting[0]->inactiveday;} ?>" size="10px"> days</td><tr>
                      <tr><td width="20%">Concurrent Login Session</td><td>: <input type="number" name="concurrentday" value="<?php if($setting[0]->concurrenlogin){echo $setting[0]->concurrenlogin;} ?>" size="10px"> </td><tr>
                      <tr><td width="20%">Minimum Password Age (Day)</td><td>: <input type="number" name="minexpiryday" value="<?php if($setting[0]->passexpirymin){echo $setting[0]->passexpirymin;} ?>" size="10px"> days</td><tr>
                      <tr><td width="20%">Maximum Password Age (Day)</td><td>: <input type="number" name="expiryday" value="<?php if($setting[0]->passexpiryday){echo $setting[0]->passexpiryday;} ?>" size="10px"> days</td><tr>
                      <tr><td width="30%">Password Expiration Warning Message (Day)</td><td>: <input type="number" name="expirymsg" value="<?php if($setting[0]->passexpirywarn){echo $setting[0]->passexpirywarn;} ?>" size="10px"> days</td><tr>
                      <tr><td width="30%">Password History Length (Day)</td><td>: <input type="number" name="passhis" value="<?php if($setting[0]->passhistory){echo $setting[0]->passhistory;} ?>" size="10px"> days</td><tr>
                      <tr><td width="30%">Account Lockout Duration (Mins)</td><td>: <input type="number" name="lockduration" value="<?php if($setting[0]->lockduration){echo $setting[0]->lockduration;} ?>" size="10px"> </td><tr>
                      <tr><td width="30%">Account Lockout Threshold (Attemps)</td><td>: <input type="number" name="locattempt" value="<?php if($setting[0]->locattempt){echo $setting[0]->locattempt;} ?>" size="10px"> </td><tr>
                      
                    </table>
                    </td>
                    </tr>

                    <tr>
                    <td><hr>
                    <h5>Theme</h5>
                    <table width="100%">
                      <tr><td width="30%">Icon Size</td><td>: <input type="number" name="iconsize" value="<?php if($setting[0]->iconsize){echo $setting[0]->iconsize;} ?>" size="10px"> </td><tr>
                      <tr><td width="30%">Navbar Color</td><td>: <input type="text" name="navbar" value="<?php if($setting[0]->navbarcolor){echo $setting[0]->navbarcolor;} ?>" size="10px"> </td><tr>
                      <tr><td width="30%">Logo Path</td><td>: <input type="text" name="logopath" value="<?php if($setting[0]->logo_path){echo $setting[0]->logo_path;} ?>" size="20px"> </td><tr>
                    </table>
                    </td>
                    </tr>


                    <tr>
                    <td><hr>
                    <h5>Mail Settings</h5>
                    <table width="100%">
                      
                      <tr><td width="30%">SMTP Host</td><td>: <input type="text" name="smtphost" value="<?php if($setting[0]->smtphost){echo $setting[0]->smtphost;} ?>" style="width:50%;"> </td><tr>
                      <tr><td width="20%">SMTP Port</td><td>: <input type="number" name="smtpport" value="<?php if($setting[0]->smtpport){echo $setting[0]->smtpport;} ?>" style="width:50%;"> </td><tr>
                      <tr><td width="20%">SMTP User</td><td>: <input type="text" name="smtpuser" value="<?php if($setting[0]->smtpuser){echo $setting[0]->smtpuser;} ?>" style="width:50%;"> </td><tr>
                    </table>
                    </td>
                    </tr>
                      
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->
</form>

<script type="text/javascript">

$( ".loader").hide();
</script>
