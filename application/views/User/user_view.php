
<style>

.form-control {
  height: 25px;
}

.fixed-table-loading{
  display:none;
}
</style>
<div class="body">
  <form  method="POST" action='<?php echo base_url().'User/useraction'; ?>' >
    <table width="100%">
      
      <tr><td colspan="2"><br>
        <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value="add_user"><i class="fa fa-plus" ></i> <b>Add User</b></button>
        <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value="trails"><i class="fa fa-eye" ></i> <b>User Logs</b></button>

      </td>
    </table>


</form></div>
<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>User role</th>
                            <th>Last Login</th>
                            <th>Status</th>
                            <th>Action </th>
                        </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>User role</th>
                            <th>Last Login</th>
                            <th>Status</th>
                            <th>Action </th>
                            </tr>
                        </tfoot>
                        <tbody id="txn">
                          <?php
                          foreach ($listuser as $usr) {
                            echo "<tr>";
                            echo "<td>$usr->name</td>";
                            echo "<td>$usr->username</td>";
                            echo "<td>$usr->rolename</td>";
                            echo "<td>$usr->lastLogin</td>";
                            if($usr->userstatus == 'Active'){
                              echo "<td><center><label style='background-color:green;' class='badge badge-success'>$usr->userstatus</label></td>";
                            }else{
                              echo "<td><center><span style='background-color:red;' class='badge badge-success'>$usr->userstatus</span></td>";
                            }
                            echo "<td>";
                            if($usr->userstatus == 'Active'){
                              echo "<a  href='".base_url()."userinfo/".$usr->user_id."'><i class='fa fa-edit'></i></a>";
                            }else{
                              echo "<a  href='".base_url()."userinfo/".$usr->user_id."'><i class='fa fa-edit'></i></a>";
                              // echo "<a onlick='deleteuser() href='' '><i class='fa fa-trash'></i></a>";
                              ?>
                               <a onclick="deleteuser(<?php echo $usr->user_id; ?>,'<?php echo $usr->username; ?>')" href=''><i class='fa fa-trash'></i></a></td>
                              <?php
                            }
                            
                            
                            
                            echo "</td>";
                            echo "</tr>";
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->


<script>
function deleteuser(id,name){
  var urlbase = "https://dps01.dp.com.my/";

  var del=confirm("Are you sure you want to delete this record?");
  if (del==true){
    url = urlbase+'User/deleteitm/'+id+'/'+name;
    
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": url,
      "method": "GET",
      "headers": {
        "cache-control": "no-cache"
      }
    }

      $.ajax(settings).done(function (response) {
        console.log(response);
        alert(response);
      });

  }
}


$(document).ready( function () {
  
    $('#myTable').DataTable();
} );
</script>
