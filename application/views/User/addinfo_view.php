<script src="<?php echo base_url() ?>Assets/js/vendor/jquery-1.11.3.min.js"></script>

<div class="row clearfix">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="card">
<div class="body">
<form id="roleForm">
<table width='100%'>
  <tr><td width='20%'>Role Name</td><td>: <input style="width:30%;" type='text' name='rname' value=""><br></td></tr>
  <tr><td>Role Description</td><td>: <input style="width:50%;" type='text' name='rdesc' value=""></td></tr>
  <tr><td colspan="2"><br>
  <a class="btn btn-success btn-bg" href="<?php echo base_url().'role' ?>"  style="width:100px;"><i class="fa fa-left"></i><span> Back </span></a>
  <a id="update_role" class="btn btn-success btn-bg" href="" style="width:130px;"><i class="fa fa-save"></i><span> Save Role </span></a>

  </td></tr>

</table>
</form>

</div>
</div>
</div>
</div>

<hr>

<script type="text/javascript">

$( "#deleteInfo").click(function(event) {

  var r = confirm("Are you sure to delete the item?");
  if (r == true) {
    var id = $( "#roleid").val();
    event.preventDefault();
    url = '<?php echo base_url();?>User/deleteRole/'+id;
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": url,
      "method": "GET",
      "headers": {
        "cache-control": "no-cache"
      }
    }

     $.ajax(settings).done(function (response) {
       console.log(response);
       if(response == 0){
         alert("Successfully Delete !");
         location.replace("<?php echo base_url();?>role");
       }else{
         alert("Unable to delete role. Role belong to another user !");
       }
     });
  }
})
//delete-------------

$( ".loader").hide();

$( "#update_role").click(function(event) {
   event.preventDefault();
   url = '<?php echo base_url();?>User/saveinfo/';
  //  alert(url);
    var form = $('#roleForm')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {

            console.log("SUCCESS : ", response);
            if(response > 0){
              if(response == "001"){
                alert("User already exist !");
              }else{
                alert("Successfully Add !");
                var redirect = "<?php echo base_url().'roleinfo/' ?>";
                window.location.replace(redirect+response);
              }

            }else{
              alert("Fail added !");
            }


        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert("Err");

        }
    });
});

$('.access').change(function() {
  //alert(this.id);
  var id = this.id;
  var string_parts = id.split("_");
  var rid = $('#roleid').val();
  var mid = string_parts[string_parts.length - 1];
  var sid = string_parts[string_parts.length - 2];
  var maccess = string_parts[string_parts.length - 3];
   event.preventDefault();
   url = '<?php echo base_url();?>User/updateaccess/'+rid+'/'+mid+'/'+maccess+'/'+sid;
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": url,
    "method": "GET",
    "headers": {
      "cache-control": "no-cache"
    }
  }

   $.ajax(settings).done(function (response) {
     console.log(response);
       //alert('Update successful !');
   });
});
</script>
