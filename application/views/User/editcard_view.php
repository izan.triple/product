  <h4>Change Card Information</h4><hr>
  <div id="content2">
  <form id="editcardForm">
    <table width="70%">
    <?php
    foreach ($cinfo as $ci) {
      ?>
      <tr><td width="15%" colspan="2"><b>Owner Information:</b></td></tr>
      <tr><td width="15%">IC No.</td><td>: <?php echo $ci->personnelic; ?></td></tr>
      <tr><td width="15%">Owner Name</td><td>: <?php echo $ci->personnelname; ?></td></tr>
      <tr><td width="15%">Owner Phone</td><td>: <?php echo $ci->personneltel1; ?></td></tr>
      <tr><td width="15%">Owner Email</td><td>: <?php echo $ci->personnelemail; ?></td></tr>
      <tr><td width="15%" colspan="2"><br><b>Card Information:</b></td></tr>
      <tr><td width="15%">Card No.</td><td>: <?php echo $ci->cardno; ?></td></tr>
      <tr><td width="15%">Card Expiry Date </td><td>: <?php echo date("d/m/Y", strtotime($ci->cardexpiry)); ?></td></tr>
      <tr><td width="15%">Area Name </td><td>: <?php echo $ci->areaname; ?></td></tr>
      <tr><td width="15%">Card Status </td><td>: <select style="width:30%">
        <?php
        foreach ($cstatus as $cs) {
          echo "<option value='$cs->statusid'>$cs->statusname</option>";
        }

        ?>
      </select>
      </td></tr>
      <tr><td width="15%" colspan="2"><br><a class="btn btn-success btn-bg" href="javascript:history.go(-1)" ><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a></td></tr>

      <?php
      }
     ?>

  </table>
  <br><hr>
  </form>
  </div>

<script>


$( "#btnEditpersonnel").click(function(event) {
   event.preventDefault();
   var id = "personnel";
   url = '<?php echo base_url();?>User/editinfo/'+id;
    var form = $('#editForm')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            console.log("SUCCESS : ", response);
              if(response == 1){
                alert("Successfully Update !");
              }
            location.reload();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert("Err");

        }
    });
});


$( "#saveSP").click(function(event) {
  var id = $( "#saveSP").val();
  event.preventDefault();
  url = '<?php echo base_url();?>User/saveinfo2/'+id;
   var form = $('#addspForm')[0];
   var data = new FormData(form);

   $.ajax({
       type: "POST",
       enctype: 'multipart/form-data',
       url: url,
       data: data,
       processData: false,
       contentType: false,
       cache: false,
       timeout: 600000,
       success: function (response) {
           console.log("SUCCESS : ", response);
           if(response > 0){
             if(response == "001"){
               alert("User already exist !");
             }else{
               alert("Successfully added !");
             }
             history.back();

           }else{
             alert("Successfully update !");
           }

           location.reload();
       },
       error: function (e) {
           console.log("ERROR : ", e);
           alert("Err");

       }
   });
});

$( "#saveSP2").click(function(event) {
  var id = $( "#saveSP2").val();
  event.preventDefault();
  url = '<?php echo base_url();?>User/saveinfo2/'+id;
   var form = $('#addcpForm')[0];
   var data = new FormData(form);

   $.ajax({
       type: "POST",
       enctype: 'multipart/form-data',
       url: url,
       data: data,
       processData: false,
       contentType: false,
       cache: false,
       timeout: 600000,
       success: function (response) {
           console.log("SUCCESS : ", response);
           if(response > 0){
             if(response == "001"){
               alert("User already exist !");
             }else{
               alert("Successfully added !");
             }
             history.back();

           }else{
             alert("Successfully update !");
           }

           location.reload();
       },
       error: function (e) {
           console.log("ERROR : ", e);
           alert("Err");

       }
   });
});

</script>
