
<style>

.form-control {
  height: 25px;
}

.fixed-table-loading{
  display:none;
}
</style>
<div class="body">
  <form  method="POST" action='<?php echo base_url().'User/useraction'; ?>' >
    <table width="100%">
      
      <tr><td colspan="2"><br>
      <a href="<?php echo base_url().'user'  ?>" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value="back"><i class="fa fa-arrow-left" ></i> <b>Back</b></a>
        <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value="trails"><i class="fa fa-eye" ></i> <b> User Logs</b></button>

      </td>
    </table>


</form></div>
<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div class="table-responsive">
                    <table id="mytable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th width="20%">Date</th>
                            <th>Activities</th>
                            <th width="10%">By</th>
                        </tr>
                        </thead>
                        
                        <tbody id="txn">
                          <?php
                          foreach ($trails as $t) {
                            echo "<tr>";
                            echo "<td>$t->datetime</td>";
                            echo "<td>$t->activity</td>";
                            echo "<td>$t->createdby</td>";
                          
                            echo "</tr>";
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->


<script type="text/javascript">

$( ".loader").hide();

$(document).ready( function () {
    $('#mytable').DataTable();
} );


</script>
