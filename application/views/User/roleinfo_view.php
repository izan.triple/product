<script src="<?php echo base_url() ?>Assets/js/vendor/jquery-1.11.3.min.js"></script>
<style>

</style>
<hr>
<form id="roleForm">
  <input type='hidden' value='<?php echo $rid; ?>' name='roleid' id='roleid'>
<table width='100%'>
  <tr><td width='20%'>Role Name</td><td>: <input style="width:80%;" type='text' name='rname' value="<?php if($roleinfo){echo $roleinfo[0]->rolename;} ?>"><br></td></tr>
  <tr><td>Role Description</td><td>: <input style="width:80%;" type='text' name='rdesc' value="<?php if($roleinfo){echo $roleinfo[0]->roledesc; }?>"></td></tr>
  <tr><td colspan="2"><br>
  <a id="update_role"  class="btn bg-blue btn-lg"  href='' ><span class="glyphicon glyphicon-plus"></span> Add Role</a>
  <?php
    if($access == 'F'){
      ?>
      <a id="deleteInfo"  class="btn bg-blue btn-lg"  href='' ><span class="glyphicon glyphicon-remove"></span> Delete Role</a>
<?php
    }
   ?>

  </td></tr>
  <tr><td colspan="2"><br><h5>List Menu<h5>
    <table border='1' width="98.5%" >
      <tr><td>Menu Name</td><td width="20%" align='center'>Full Access</td><td width="20%" align='center'>View</td><td width="20%" align='center'>None</td></tr>
    </table>
    <div style="width:100%; height:320px; overflow:auto;">
    <table border='1' width="100%" >
      <tr><td></td><td width="20%" align='center'></td><td width="20%" align='center'></td><td width="20%" align='center'></td></tr>

      <tbody style="width:95%; height:320px; overflow:auto;">
  <?php
  // print_r($listsubaccess);
      foreach ($listmenu as $m){
          if($m->menuStatus == 'P'){
            echo "<tr><td colspan='4' style='background-color:#addde6;' >$m->menuname</td></tr>";
            foreach ($submenu as $sm){
              if($sm->menuid == $m->menuid){
                $ind2=0;
                $acc2;
                foreach ($listsubaccess as $accm){
                  if($accm->fk_submenuid == $sm->submenuid ){
                    $ind2=1;
                    $acc2 = $accm->access;
                  }
                }
                if($ind2 == 1){
                  if($acc2 == 'F'){
                    echo "<tr><td >$sm->submenuname</td><td align='center'><input checked name='radio_$sm->submenuid' id='S_F_$sm->submenuid".'_'.$sm->menuid."' class='access' type='radio' value='S_F_$sm->submenuid".'_'.$sm->menuid."'></td><td align='center'><input name='radio_$sm->submenuid' id='S_V_$sm->submenuid".'_'.$sm->menuid."' class='access' type='radio' value='S_V_$sm->submenuid".'_'.$sm->menuid."'></td><td align='center'><input name='radio_$sm->submenuid' id='S_N_$sm->submenuid".'_'.$sm->menuid."' type='radio' class='access' value='S_N_$sm->submenuid".'_'.$sm->menuid."'></td></tr>";
                  }else if($acc2 == 'V'){
                    echo "<tr><td >$sm->submenuname</td><td align='center'><input name='radio_$sm->submenuid' id='S_F_$sm->submenuid' class='access' type='radio' value='S_F_$sm->submenuid'></td><td align='center'><input checked name='radio_$sm->submenuid' id='S_V_$sm->submenuid' class='access' type='radio' value='S_V_$sm->submenuid'></td><td align='center'><input name='radio_$sm->submenuid' id='S_N_$sm->submenuid' type='radio' class='access' value='S_N_$sm->submenuid'></td></tr>";
                  }else{

                  }
                }else{
                  echo "<tr><td >$sm->submenuname</td><td align='center'><input name='radio_$sm->submenuid' id='S_F_$sm->submenuid".'_'.$sm->menuid."' class='access' type='radio' value='S_F_$sm->submenuid".'_'.$sm->menuid."'></td><td align='center'><input name='radio_$sm->submenuid' id='S_V_$sm->submenuid".'_'.$sm->menuid."' class='access' type='radio' value='S_V_$sm->submenuid".'_'.$sm->menuid."'></td><td align='center'><input name='radio_$sm->submenuid' id='S_N_$sm->submenuid".'_'.$sm->menuid."' type='radio' class='access' value='S_N_$sm->submenuid".'_'.$sm->menuid."'></td></tr>";
                }
              }


            }

          }else{
            $ind = 0;
            $acc;
            foreach ($listsubaccess as $accm){
              if($m->menuid == $accm->fk_menuid){
                $ind = 1;
                $acc = $accm->access;
              }
            }

            if($ind == 1){
              if($acc == 'F'){
                echo "<tr><td >$m->menuname</td><td align='center'><input checked name='radio_$m->menuid' id='M_F_0_$m->menuid' class='access' type='radio' value='M_F_0_$m->menuid'></td><td align='center'><input name='radio_$m->menuid' id='M_V_0_$m->menuid' class='access' type='radio' value='M_V_0_$m->menuid'></td><td align='center'><input name='radio_$m->menuid' id='M_N_0_$m->menuid' type='radio' class='access' value='M_N_0_$m->menuid'></td></tr>";
              }else if($acc == 'V'){
                echo "<tr><td >$m->menuname</td><td align='center'><input  name='radio_$m->menuid' id='M_F_0_$m->menuid' class='access' type='radio' value='M_F_0_$m->menuid'></td><td align='center'><input checked name='radio_$m->menuid' id='M_V_0_$m->menuid' class='access' type='radio' value='M_V_0_$m->menuid'></td><td align='center'><input name='radio_$m->menuid' id='M_N_0_$m->menuid' type='radio' class='access' value='M_N_0_$m->menuid'></td></tr>";
              }
            }else{
             echo "<tr><td >$m->menuname</td><td align='center'><input name='radio_$m->menuid' id='M_F_0_$m->menuid' class='access' type='radio' value='M_F_0_$m->menuid'></td><td align='center'><input name='radio_$m->menuid' id='M_V_0_$m->menuid' class='access' type='radio' value='M_V_0_$m->menuid'></td><td align='center'><input name='radio_$m->menuid' id='M_N_0_$m->menuid' type='radio' class='access' value='M_N_0_$m->menuid'></td></tr>";

            }

          }

    }


 ?>
 </table>
</div>
    </td></tr>
<?php //print_r($listaccess);

?>
</table>
</form>

<hr>

<script type="text/javascript">

$( "#deleteInfo").click(function(event) {

  var r = confirm("Are you sure to delete the item?");
  if (r == true) {
    var id = $( "#roleid").val();
    event.preventDefault();
    url = '<?php echo base_url();?>User/deleteRole/'+id;
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": url,
      "method": "GET",
      "headers": {
        "cache-control": "no-cache"
      }
    }

     $.ajax(settings).done(function (response) {
       console.log(response);
       if(response == 0){
         alert("Successfully Delete !");
         var redirect = "<?php echo base_url().'role/' ?>";
         window.location.replace(redirect);
       }else{
         alert("Unable to delete role. Role belong to another user !");
       }
     });
  }
})
//delete-------------

$( ".loader").hide();

$( "#update_role").click(function(event) {
   event.preventDefault();
   var id = "role";
   url = '<?php echo base_url();?>User/saveinfo/'+id;
    var form = $('#roleForm')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            console.log("SUCCESS : ", response);
            if(response > 0){
              if(response == "001"){
                alert("User already exist !");
              }else{
                alert("Successfully update !");
              }
              history.back();

            }else{
              alert("Fail added !");
            }

            location.reload();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert("Err");

        }
    });
});

$('.access').change(function() {
  //alert(this.id);
  var id = this.id;
  var string_parts = id.split("_");
  var rid = $('#roleid').val();
  var mid = string_parts[string_parts.length - 1];
  var sid = string_parts[string_parts.length - 2];
  var maccess = string_parts[string_parts.length - 3];
   event.preventDefault();
   url = '<?php echo base_url();?>User/updateaccess/'+rid+'/'+mid+'/'+maccess+'/'+sid;
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": url,
    "method": "GET",
    "headers": {
      "cache-control": "no-cache"
    }
  }

   $.ajax(settings).done(function (response) {
     console.log(response);
       //alert('Update successful !');
   });
});
</script>
