<style>
#fheader {
  position: fixed;
  top: 20%;
  width: 100%;
}

#content {
  width: 100%;
  height: 500px;
  overflow: auto;
position: relative;
}

</style>
<?php

if($form == 'user'){
  ?>
  <form  action="<?php echo base_url() ?>submituser" method="post"><br>
  <p align="right"><button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value="add"><i class="fa fa-save" ></i> <b>Add User</b></button><br>

  <table width='80%'>
    <tr><td width='20%'>Name</td><td>: <input style="width:70%;" type='text' name='uname'><br></td></tr>
    <tr><td>Username</td><td>: <input style="width:50%;" type='text' name='username'></td></tr>
    <tr><td>Password</td><td>: <input style="width:50%;" type='password' name='upassword' minlength="8" required>
    <br>
    <div  style="font-size:10px;color:red;">*MUST contain at least 8 characters (12+ recommended)<br>
    *MUST contain at least 1 digit and 1 character<br>
    </div>
    </td></tr>
    <tr><td>Role</td><td>:
      <select style="width:50%;" name='urole'>
        <?php
          foreach ($listrole as $r) {
            echo "<option value='$r->roleid'>$r->rolename</option>";
          }
         ?>
      </select>
    </td></tr>
    <tr><td>Log Activity</td><td>: <input type="checkbox" name="log" value="1">
    <tr><td colspan="2"><br></td></tr>
  </table>
  <hr>
  </form>
  <?php
}else if($form == 'euser'){
  ?>

  <h4>Change User Information</h4><hr>
  <form id="<?php echo $form ?>Form">
    <input style="width:70%;" type='hidden' name='euid' value="<?php echo $userinfo[0]->user_id ?>">
  <table width='80%'>
    <tr><td width='20%'>Name</td><td>: <input style="width:70%;" type='text' name='euname' value="<?php echo $userinfo[0]->name ?>"><br></td></tr>
    <tr><td>Username</td><td>: <input disabled style="width:50%;" type='text' name='eusername' value="<?php echo $userinfo[0]->username ?>"></td></tr>
    <tr><td>Role</td><td>:
      <select style="width:50%;" name='eurole'>
        <?php
          foreach ($listrole as $r) {
            if($userinfo[0]->fk_roleid == $r->roleid){
              echo "<option value='$r->roleid' selected>$r->rolename</option>";
            }else{
              echo "<option value='$r->roleid'>$r->rolename</option>";
            }

          }
         ?>
      </select>
    </td></tr>
    <tr><td>Area / Site</td><td>:
      <select style="width:50%;" name='euarea' id='uarea'>
        <?php
          echo "<option value=''>- Please Select -</option>";
          if($userinfo[0]->fk_areaid == 0){
            echo "<option value=0 selected>All</option>";
          }else{
            foreach ($listarea as $a) {
              if($userinfo[0]->fk_areaid == $a->areaid){
                echo "<option value='$a->areaid' selected>$a->areaname</option>";
              }
            }
          }

          foreach ($listarea as $a) {
            if($userinfo[0]->fk_areaid != $a->areaid){
            echo "<option value='$a->areaid'>$a->areaname</option>";
            }
          }
         ?>
      </select>
    </td></tr>
    <tr><td>Status</td><td>:
      <select style="width:50%;" name='estatus'>
        <?php

          if($userinfo[0]->userstatus == 'Active'){
            echo "<option value='Active' selected>".$userinfo[0]->userstatus."</option>";
            echo "<option value='Inactive' >Inactive</option>";
          }else{
            echo "<option value='Active'>Active</option>";
            echo "<option value='Inactive' selected>".$userinfo[0]->userstatus."</option>";

          }

         ?>
      </select>
    </td></tr>
    <tr><td colspan="2"><br></td></tr>
  </table>
  <hr>
  <a class="btn btn-success btn-bg" href="<?php echo base_url().'user'; ?>"  style="width:100px;"><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a>
  <a id="btnEdit<?php echo $form;?>" class="btn btn-success btn-bg" href="" style="width:150px;"><i class="fa fa-save" style='width:30px;'></i>Update User</a>
  </form>
  <?php
}else if($form == 'personnel'){
  ?>
  <h4>Add Personnel</h4><hr>
  <form id="<?php echo $form ?>Form">
    <input style="width:30%;" type='hidden' name='tid' value="<?php echo $tid; ?>">
  <table width='80%'>
    <tr><td width='20%'>IC Number</td><td>: <input style="width:30%;" type='number' name='uic'><br></td></tr>
    <tr><td width='20%'>Name</td><td>: <input style="width:80%;" type='text' name='uname'><br></td></tr>
    <tr><td>Address Line 1</td><td>: <input style="width:80%;" type='text' name='uline1'></td></tr>
    <tr><td>Address Line 2</td><td>: <input style="width:80%;" type='text' name='uline2'></td></tr>
    <tr><td>City</td><td>: <input style="width:30%;" type='text' name='ucity'></td></tr>
    <tr><td>Postcode</td><td>: <input style="width:10%;" type='number' name='upostcode'></td></tr>
    <tr><td>State</td><td>:
      <!-- <input style="width:30%;" type='text' name='ustate'> -->
      <select style="width:30%;" name='ustate'>
        <?php
        foreach ($liststate as $st) {
          echo "<option value='$st->stateId'>$st->stateName</option>";
        }

         ?>
      </select>
    </td></tr>
    <tr><td>Phone No.</td><td>: <input style="width:30%;" type='number' name='uphone'></td></tr>
    <tr><td>Email</td><td>: <input style="width:30%;" type='email' name='uemail'></td></tr>
    <tr id="addsite"><td colspan="2"><br><b>Assign Site Profile</b><br>
      [<a data-toggle="modal" data-target="#addModal" href="">Add Site Profile</a>]
      <table width="100%">
        <tr bgcolor="#ADD8E6"><td><b>Area</td><td><b>Expiry</td><td><b>Action</td></tr>
        <?php
        foreach ($psinfo as $sp) {
          echo "<tr><td>$sp->areaname</td><td>".date("d/m/Y", strtotime($sp->siteexpiry))."</td>
          <td><a href=''><i class='fa fa-trash' aria-hidden='true'></i></a></td></tr>";
        }
         ?>
      </table>
    </td></tr>
    <tr><td colspan="2"><br></td></tr>
  </table>
  <hr>
  <a class="btn btn-success btn-bg" href="<?php echo base_url().'membermngt'; ?>"  style="width:100px;"><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a>
  <a id="btn<?php echo $form;?>" class="btn btn-success btn-bg" href="" style="width:150px;"><i class="fa fa-plus" style='width:30px;'></i>Add Personnel</a>
  </form>
  <?php
}else if($form == 'personneledit'){
  ?>
  <h4>Change Personnel Information</h4><hr>
  <div id="content">
  <form id="<?php echo $form ?>Form">

    <input style="width:30%;" type='hidden' name='pid2' value="<?php echo $pinfo[0]->personnelid; ?> ">
  <table width='80%'>
    <tr><td width='20%'>IC Number</td><td>: <input style="width:30%;" type='text' name='uic2' value="<?php echo $pinfo[0]->personnelic; ?> "><br></td></tr>
    <tr><td width='20%'>Name</td><td>: <input style="width:80%;" type='text' name='uname2' value="<?php echo $pinfo[0]->personnelname; ?> "><br></td></tr>
    <tr><td>Address Line 1</td><td>: <input style="width:80%;" type='text' name='uline12' value="<?php echo $pinfo[0]->personneladdr1; ?> "></td></tr>
    <tr><td>Address Line 2</td><td>: <input style="width:80%;" type='text' name='uline22' value="<?php echo $pinfo[0]->personneladdr2; ?> "></td></tr>
    <tr><td>City</td><td>: <input style="width:30%;" type='text' name='ucity2' value="<?php echo $pinfo[0]->personnelcity; ?> "></td></tr>
    <tr><td>Postcode</td><td>: <input style="width:10%;" type='text' name='upostcode2' value="<?php echo $pinfo[0]->personnelpostcode; ?> "></td></tr>
    <tr><td>State</td><td>:
      <select style="width:30%;" name='ustate2'>
        <?php
        foreach ($liststate as $st) {
          if($st->stateId == $pinfo[0]->fk_stateId){
            echo "<option selected value='$st->stateId'>$st->stateName</option>";
          }else{
            echo "<option value='$st->stateId'>$st->stateName</option>";
          }

        }

         ?>
      </select>
    </td></tr>
    <tr><td>Phone No.</td><td>: <input style="width:30%;" type='text' name='uphone2' value="<?php echo $pinfo[0]->personneltel1; ?> "></td></tr>
    <tr><td>Email</td><td>: <input style="width:30%;" type='text' name='uemail2' value="<?php echo $pinfo[0]->personnelemail; ?> "></td></tr>
    <tr><td colspan="2"><br><hr></td></tr>

    <tr><td colspan="2"><b>Assign Site Profile2</b><br>
      [<a data-toggle="modal" data-target="#addModal" href="">Add Site Profile</a>]

      <!-- <button type="button" class="btn btn-success btn-bg" data-toggle="modal" data-target="#addModal" id="execbtn" value="1"><span class="fa fa-plus"></span> Main Category</button> -->
      <table width="100%">
        <tr bgcolor="#ADD8E6"><td><b>Area</td><td><b>Expiry</td><td><b>Action</td></tr>
        <?php
        foreach ($psinfo as $sp) {
          echo "<tr><td>$sp->areaname</td><td>".date("d/m/Y", strtotime($sp->siteexpiry))."</td>
          <td><button onclick='test(dsds)'><i class='fa fa-trash' aria-hidden='true'></i></button></td></tr>";
        }
         ?>
      </table>
    </td></tr>

    <tr><td colspan="2"><br><b>Assign Card</b><br>
      [<a data-toggle="modal" data-target="#addcardModal" href="">Add Card</a>]
      <table width="100%">
        <tr bgcolor="#ADD8E6"><td><b>Card No</td></tr>
          <?php
          foreach ($cpinfo as $cp) {
            echo "<tr><td>$cp->cardno</td><td></td><td></td></tr>";
          }
           ?>

      </table>
    </td></tr>
  </table>
  <br><hr>
</div>
  <a class="btn btn-success btn-bg" href="<?php echo base_url().'membermngt'; ?>"  style="width:100px;"><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a>
  <a id="btnEdit<?php echo $form;?>" class="btn btn-success btn-bg" href="" style="width:170px;"><i class="fa fa-save" style='width:30px;'></i> Update Personnel</a>

  </form>
  <?php
}else if($form == 'sprofile'){
  ?>
  <h4>Add Site Profile</h4><hr>
  <form id="<?php echo $form ?>Form">
  <table width='80%'>
    <tr><td width='20%'>Profile Name</td><td>: <input style="width:60%;" type='text' name='spname'><br></td></tr>
    <tr><td width='20%'>Profile Description</td><td>: <input style="width:80%;" type='text' name='spdesc'><br></td></tr>
    <tr><td colspan="2"><hr>Area / Site:<br>
        <?php
          foreach ($listarea as $a) {
            echo "<input type= checkbox name=sparea[] value='".$a->areaid."'>$a->areaname</br>";
          }
         ?>
    </td></tr>
    <tr><td colspan="2"><br></td></tr>
  </table>

  <a class="btn btn-success btn-bg" href="<?php echo base_url().'sprofile'; ?>"  style="width:100px;"><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a>
  <a id="btn<?php echo $form;?>" class="btn btn-success btn-bg" href="" style="width:130px;"><i class="fa fa-plus" style='width:30px;'></i>Add Profile</a>
  </form>
  <?php
}else if($form == 'tprofile'){
  ?>
  <h4>Add Tanent Profile</h4><hr>
  <form id="<?php echo $form ?>Form">
<table>
  <tr><td width='20%'>Tenant Area</td><td>:<select style="width:60%;"  name='tparea'>
      <?php
        foreach ($listarea as $a) {
          echo "<option value='".$a->areaid."'>$a->areaname</option>";
        }
       ?>
    </select>
    <br></td></tr>
      <tr><td>Tenant Name</td><td>:<input type='text' style="width:60%;" name='tenantname' id='tenantname'></td></tr>
      <tr><td>Tenant Address Line 1</td><td>:<input style="width:60%;" type='text' name='tenantaddr1' id='tenantaddr1'></td></tr>
      <tr><td>Tenant Address Line 2</td><td>:<input style="width:60%;" type='text' name='tenantaddr2' id='tenantaddr2'></td></tr>
      <tr><td>Tenant Postcode</td><td>:<input type='text' name='tenantpostcode' id='tenantpostcode'></td></tr>
      <tr><td>Tenant City</td><td>:<input type='text' name='tenantcity' id='tenantcity'></td></tr>
      <tr><td>Tenant State</td><td>:<input type='text' name='fk_stateId' id='fk_stateId'></td></tr>
      <tr><td>Tel 1</td><td>:<input type='text' name='tenanttel1' id='tenanttel1'></td></tr>
      <tr><td>Tel 2</td><td>:<input type='text' name='tenanttel2' id='tenanttel2'></td></tr>
      <tr><td>Fax 1</td><td>:<input type='text' name='tenantfax1' id='tenantfax1'></td></tr>
      <tr><td>Fax 2</td><td>:<input type='text' name='tenantfax2' id='tenantfax2'></td></tr>
      <tr><td>Tenant Email</td><td>:<input type='text' name='tenantemail' id='tenantemail'></td></tr>
      <tr><td>Tenant Expiry Date</td><td>:<input type='date' name='tenantxpiry' id='tenantxpiry' value="<?php echo date('Y-m-d') ?>"></td></tr>
    </table><br>
      <a class="btn btn-success btn-bg" href="<?php echo base_url().'tprofile'; ?>"  style="width:100px;"><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a>
      <a id="btn<?php echo $form;?>" class="btn btn-success btn-bg" href="" style="width:130px;"><i class="fa fa-plus" style='width:30px;'></i>Add Tenant</a>
      </form>

  <?php
}
?>

<!-- ******************************* Modal Source Keyg*********************************** -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-info" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Site Profile</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="addspForm">

          <input type='hidden' name='pid' value="<?php echo $pid; ?>">
          <table width="80%">
            <tr bgcolor="#ADD8E6"><td>Area</td><td>Expiry Date</td></tr>
          <?php
          foreach ($allarea as $all) {
            $ind=0;
            $date;
            foreach ($listarea as $si) {
              if($si->fk_areaid == $all->areaid){
                $ind=1;
                $date=$si->siteexpiry;
              }
              }

            if($ind == 1){
              echo "<tr><td><input checked type= checkbox name=sparea[] value='".$all->areaid."'>$all->areaname</td><td><input name=areaxpiry_$all->areaid type='date' value='".$date."'></td></tr>";
            }else{
              echo "<tr><td><input type= checkbox name=sparea[] value='".$all->areaid."'>$all->areaname</td><td><input name=areaxpiry_$all->areaid type='date' value='".date('Y-m-d')."'></td></tr>";

            }

          }

           ?>
         </table>
      </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-bg" id="saveSP" value="1"><span class="fa fa-save"></span> Save</button>
      </div>
    </div>
  </div>
</div>
<!-- ****************************************************************** -->

<!-- ******************************* Modal Source Keyg*********************************** -->
<div class="modal fade" id="addcardModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-info" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Card</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="addcpForm">
          <input type='hidden' name='pid2' value="<?php echo $pid; ?>">
          Select Card : <select style="width:30%" name="card">
            <?php
            foreach ($cinfo as $ci) {
              echo "<option value='$ci->cardid'>$ci->cardno</option>";
            }
             ?>
          </select>

      </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-bg" id="saveSP2" value="2"><span class="fa fa-plus"></span> Add</button>
      </div>
    </div>
  </div>
</div>
<!-- ****************************************************************** -->
<script>

$('#addsite').hide();

function test(dsds){
  alert("sdf");
}

$('.access').change(function() {
  //alert(this.id);
  var id = this.id;
  var string_parts = id.split("_");
  var rid = $('#roleid').val();
  var mid = string_parts[string_parts.length - 1];
  var sid = string_parts[string_parts.length - 2];
  var maccess = string_parts[string_parts.length - 3];

   event.preventDefault();
   url = '<?php echo base_url();?>User/updateaccess/'+rid+'/'+mid+'/'+maccess+'/'+sid;

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": url,
    "method": "GET",
    "headers": {
      "cache-control": "no-cache"
    }
  }

   $.ajax(settings).done(function (response) {
     console.log(response);
       //alert('Update successful !');
   });
});

$( "#btn<?php echo $form;?>").click(function(event) {
   event.preventDefault();
   var id = "<?php echo $form;?>";
   url = '<?php echo base_url();?>User/saveinfo/'+id;
   alert(url);return;
    var form = $('#<?php echo $form;?>Form')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            console.log("SUCCESS : ", response);
            if(response > 0){
              if(response == "001"){
                alert("User already exist !");
              }else{
                if(id == "personnel"){
                  $( "#personnelid").val(response);
                  var redirect = "<?php echo base_url().'userdetail/' ?>";
                   window.location.replace(redirect+response);
                }
                alert("Successfully added !");
              }
              // history.back();

            }else{
              alert("Fail added !");
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert("Err");

        }
    });
});

$( "#btnEdit<?php echo $form;?>").click(function(event) {
   event.preventDefault();
   var id = "<?php echo $form;?>";
   url = '<?php echo base_url();?>User/editinfo/'+id;
    var form = $('#<?php echo $form;?>Form')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            console.log("SUCCESS : ", response);
              if(response == 1){
                alert("Successfully Update !");
              }
            location.reload();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert("Err");

        }
    });
});

$( "#update_role").click(function(event) {
   event.preventDefault();
   var id = "<?php echo $form;?>";
   url = '<?php echo base_url();?>User/saveinfo/'+id;
    var form = $('#<?php echo $form;?>Form')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            console.log("SUCCESS : ", response);
            alert(response);
            if(response > 0){
              if(response == "001"){
                alert("User already exist !");
              }else{
                alert("Successfully added !");
              }
              history.back();

            }else{
              alert("Fail added !");
            }

            location.reload();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert("Err");

        }
    });
});

$('#urole').change(function() {
  id = $('#urole').val();
   event.preventDefault();
   url = '<?php echo base_url();?>User/getsite/'+id;
   alert(url);
  //
  // var settings = {
  //   "async": true,
  //   "crossDomain": true,
  //   "url": url,
  //   "method": "GET",
  //   "headers": {
  //     "cache-control": "no-cache"
  //   }
  // }
  //
  //  $.ajax(settings).done(function (response) {
  //    console.log(response);
  //      //alert('Update successful !');
  //  });
});


$( "#saveSP").click(function(event) {
  var id = $( "#saveSP").val();
  event.preventDefault();
  url = '<?php echo base_url();?>User/saveinfo2/'+id;
   var form = $('#addspForm')[0];
   var data = new FormData(form);

   $.ajax({
       type: "POST",
       enctype: 'multipart/form-data',
       url: url,
       data: data,
       processData: false,
       contentType: false,
       cache: false,
       timeout: 600000,
       success: function (response) {
           console.log("SUCCESS : ", response);
           if(response > 0){
             if(response == "001"){
               alert("User already exist !");
             }else{
               alert("Successfully added !");
             }
             history.back();

           }else{
             alert("Successfully update !");
           }

           location.reload();
       },
       error: function (e) {
           console.log("ERROR : ", e);
           alert("Err");

       }
   });
});

$( "#saveSP2").click(function(event) {
  var id = $( "#saveSP2").val();
  event.preventDefault();
  url = '<?php echo base_url();?>User/saveinfo2/'+id;
   var form = $('#addcpForm')[0];
   var data = new FormData(form);

   $.ajax({
       type: "POST",
       enctype: 'multipart/form-data',
       url: url,
       data: data,
       processData: false,
       contentType: false,
       cache: false,
       timeout: 600000,
       success: function (response) {
           console.log("SUCCESS : ", response);
           if(response > 0){
             if(response == "001"){
               alert("User already exist !");
             }else{
               alert("Successfully added !");
             }
             history.back();

           }else{
             alert("Successfully update !");
           }

           location.reload();
       },
       error: function (e) {
           console.log("ERROR : ", e);
           alert("Err");

       }
   });
});

</script>
