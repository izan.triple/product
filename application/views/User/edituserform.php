  <h4>Change Personnel Information</h4><hr>
  <div id="content2">
  <form id="editForm">

    <input style="width:30%;" type='hidden' name='pid2' value="<?php echo $pinfo[0]->personnelid; ?> ">
  <table width='80%'>
    <tr><td width='20%'>IC Number</td><td>: <input style="width:30%;" type='text' name='uic2' value="<?php echo $pinfo[0]->personnelic; ?> "><br></td></tr>
    <tr><td width='20%'>Name</td><td>: <input style="width:80%;" type='text' name='uname2' value="<?php echo $pinfo[0]->personnelname; ?> "><br></td></tr>
    <tr><td>Address Line 1</td><td>: <input style="width:80%;" type='text' name='uline12' value="<?php echo $pinfo[0]->personneladdr1; ?> "></td></tr>
    <tr><td>Address Line 2</td><td>: <input style="width:80%;" type='text' name='uline22' value="<?php echo $pinfo[0]->personneladdr2; ?> "></td></tr>
    <tr><td>City</td><td>: <input style="width:30%;" type='text' name='ucity2' value="<?php echo $pinfo[0]->personnelcity; ?> "></td></tr>
    <tr><td>Postcode</td><td>: <input style="width:10%;" type='text' name='upostcode2' value="<?php echo $pinfo[0]->personnelpostcode; ?> "></td></tr>
    <tr><td>State</td><td>:
      <select style="width:30%;" name='ustate2'>
        <?php
        foreach ($liststate as $st) {
          if($st->stateId == $pinfo[0]->fk_stateId){
            echo "<option selected value='$st->stateId'>$st->stateName</option>";
          }else{
            echo "<option value='$st->stateId'>$st->stateName</option>";
          }

        }

         ?>
      </select>
    </td></tr>
    <tr><td>Phone No.</td><td>: <input style="width:30%;" type='text' name='uphone2' value="<?php echo $pinfo[0]->personneltel1; ?> "></td></tr>
    <tr><td>Email</td><td>: <input style="width:30%;" type='text' name='uemail2' value="<?php echo $pinfo[0]->personnelemail; ?> "></td></tr>
    <tr><td colspan="2"><br>
      <a class="btn btn-success btn-bg" href="<?php echo base_url().'pprofile'; ?>"  style="width:100px;"><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a>
    <a id="btnEditpersonnel" class="btn btn-success btn-bg" href="" style="width:170px;"><i class="fa fa-save" style='width:30px;'></i> Update Personnel</a>
</td></tr>
    <tr><td colspan="2"><br><hr></td></tr>

    <tr><td colspan="2"><b>Assign Site Profile</b><br>
      [<a data-toggle="modal" data-target="#addModal" href="">Add Site Profile</a>]

      <!-- <button type="button" class="btn btn-success btn-bg" data-toggle="modal" data-target="#addModal" id="execbtn" value="1"><span class="fa fa-plus"></span> Main Category</button> -->
      <table width="100%">
        <tr bgcolor="#ADD8E6" ><td><b>Area</td><td ><b>Expiry Date</td><td><b>Action</td></tr>
        <?php
        foreach ($psinfo as $sp) {
          echo "<tr>
          <td width='30%'>$sp->areaname</td>
          <td>".date("d/m/Y", strtotime($sp->siteexpiry))."</td>
          <td width='10%'><button  id='pstrash' value='$sp->pspid' ><i class='fa fa-trash' aria-hidden='true'></i></button></td>
          </tr>";
        }
         ?>
      </table>
    </td></tr>

    <tr><td colspan="2"><br><b>Assign Card</b><br>
      [<a data-toggle="modal" data-target="#addcardModal" href="">Add Card</a>]
      <table width="100%" id="table" >
        <tr bgcolor="#ADD8E6"><td ><b>Card No</td><td ><b>Expiry Date</td><td ><b>Area</td><td ><b>Status</td><td  width='10%'><b>Action</td></tr>
          <?php

          foreach ($cpinfo as $cp) {
            echo "<tr>
            <td width='30%'>$cp->cardno</td>
            <td>".date("d/m/Y", strtotime($cp->cardexpiry))."</td>
            <td width='30%'>$cp->areaname</td>
            <td >$cp->statusname</td>
            <td width='10%'><a href=".base_url().'cardinfo/'.$cp->csid." id='carsstatus' value='$cp->csid' ><i class='fa fa-edit' aria-hidden='true'></i></a></td>
            </tr>";
          }
           ?>

      </table>
    </td></tr>
  </table>
  <br><hr>
</div>

  </form>

<!-- ******************************* Modal Source Keyg*********************************** -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-info" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Site Profile</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="addspForm">

          <input type='hidden' name='pid' value="<?php echo $pid; ?>">
          <table width="80%">
            <tr bgcolor="#ADD8E6"><td>Area</td><td>Expiry Date</td></tr>
          <?php
          foreach ($allarea as $all) {
            $ind=0;
            $date;
            foreach ($listarea as $si) {
              if($si->fk_areaid == $all->areaid){
                $ind=1;
                $date=$si->siteexpiry;
              }
              }

            if($ind == 1){
              echo "<tr><td>$all->areaname</td><td>".date("d/m/Y", strtotime($date))."</td></tr>";
            }else{
              if($tid > 0){
              }else{
                echo "<tr><td><input type= checkbox name=sparea[] value='".$all->areaid."'>$all->areaname</td><td><input name=areaxpiry_$all->areaid type='date' value='".date('Y-m-d')."'></td></tr>";
              }

            }

          }

           ?>
         </table>
      </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-bg" id="saveSP" value="1"><span class="fa fa-save"></span> Save</button>
      </div>
    </div>
  </div>
</div>
<!-- ****************************************************************** -->

<!-- ******************************* Modal Source Keyg*********************************** -->
<div class="modal fade" id="addcardModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-info" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Card</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="addcpForm">
          <table width="100%">
            <tr><td width=15%>ID/Card No</td><td>: <input type = "text" style="width:30%" name="card"></td></tr>
            <tr><td width=15%>ID/Card Description</td><td>: <input type = "text" style="width:60%" name="cdesc"></td></tr>
            <tr><td>Card Media </td><td>:
              <select name='mediaid' style="width:20%;">
                <?php
                foreach ($listmedia as $media) {
                echo "<option value=".$media->mediaid.">$media->medianame</option>";
                }
                ?>
              </select>
             </td></tr>
            <tr><td>Area</td><td>: <select name="acard">
              <?php
              if($tid > 0){      
              }else{
                echo "<option value='0'>- All -</option>";
              }

              foreach ($allarea as $all2) {
                $ind2=0;
                $date;
                foreach ($listarea as $si2) {
                  if($si2->fk_areaid == $all2->areaid){
                    $ind2=1;
                    $date=$si2->siteexpiry;
                  }
                  }

                if($ind2 == 1){
                  echo "<option value='$all2->areaid'>$all2->areaname</option>";
                  // echo "<tr><td>$all->areaname</td><td>".date("d/m/Y", strtotime($date))."</td></tr>";
                }else{
                  if($tid > 0){
                  }else{
                    echo "<option value='$all2->areaid'>$all2->areaname</option>";
                    // echo "<tr><td><input type= checkbox name=sparea[] value='".$all->areaid."'>$all->areaname</td><td><input name=areaxpiry_$all->areaid type='date' value='".date('Y-m-d')."'></td></tr>";
                  }

                }

              }

               ?>

              <?php

                // foreach ($allarea as $all) {
                //   if($si->fk_areaid == $all->areaid){
                //     $ind2=0;
                //     $date=$si->siteexpiry;
                //   }
                //   echo "<option value='$all->areaid'>$all->areaname</option>";
                // }
               ?>
             </select>

          </table>
          <input type='hidden' name='pid2' value="<?php echo $pid; ?>">
      </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-bg" id="saveSP2" value="2"><span class="fa fa-plus"></span> Add</button>
      </div>
    </div>
  </div>
</div>
<!-- ****************************************************************** -->
<script>

function test(id){
  alert(id);
}

$( "#pstrash").click(function(event) {
  alert($( "#pstrash").val());
})

$( "#btnEditpersonnel").click(function(event) {
   event.preventDefault();
   var id = "personnel";
   url = '<?php echo base_url();?>User/editinfo/'+id;
    var form = $('#editForm')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            console.log("SUCCESS : ", response);
              if(response == 1){
                alert("Successfully Update !");
              }
            location.reload();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert("Err");

        }
    });
});


$( "#saveSP").click(function(event) {
  var id = $( "#saveSP").val();
  event.preventDefault();
  url = '<?php echo base_url();?>User/saveinfo2/'+id;
   var form = $('#addspForm')[0];
   var data = new FormData(form);

   $.ajax({
       type: "POST",
       enctype: 'multipart/form-data',
       url: url,
       data: data,
       processData: false,
       contentType: false,
       cache: false,
       timeout: 600000,
       success: function (response) {
           console.log("SUCCESS : ", response);
           if(response > 0){
             if(response == "001"){
               alert("User already exist !");
             }else{
               alert("Successfully added !");
             }
             history.back();

           }else{
             alert("Successfully update !");
           }

           location.reload();
       },
       error: function (e) {
           console.log("ERROR : ", e);
           alert("Err");

       }
   });
});

$( "#saveSP2").click(function(event) {
  var id = $( "#saveSP2").val();
  event.preventDefault();
  url = '<?php echo base_url();?>User/saveinfo2/'+id;
   var form = $('#addcpForm')[0];
   var data = new FormData(form);

   $.ajax({
       type: "POST",
       enctype: 'multipart/form-data',
       url: url,
       data: data,
       processData: false,
       contentType: false,
       cache: false,
       timeout: 600000,
       success: function (response) {
           console.log("SUCCESS : ", response);
           if(response > 0){
             if(response == "001"){
               alert("User already exist !");
             }else{
               alert("Successfully added !");
             }
             history.back();

           }else{
             alert("Successfully update !");
           }

           location.reload();
       },
       error: function (e) {
           console.log("ERROR : ", e);
           alert("Err");

       }
   });
});

</script>
