<!-- Exportable Table -->
<link href='<?php echo base_url(); ?>assets/fullcalendar/packages/core/main.css' rel='stylesheet' />
<link href='<?php echo base_url(); ?>assets/fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
<div class="row clearfix">
    <div class="body">
        
        <form method="POST" id="mform" action='<?php echo base_url().'homestayuser/'.$homestayid.'/'.$ownerid; ?>'>
            <input type="hidden" name="oid" value="<?php echo $ownerid; ?>">
            <input type="hidden" name="hid" value="<?php echo $homestayid; ?>">
            <table width="100%">
                <tr>
                    <td colspan="2"><br>
                        <?php if(GAMMA::getSession('role') == 3){ ?>
                            <button type="button" name="btn"
                            onclick="window.location.href='<?php echo base_url().'homestay'; ?>'"
                            style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect "><i
                                class="fa fa-arrow-left"></i> Back</button>
                        <?php }else{?>
                            <button type="button" name="btn"
                            onclick="window.location.href='<?php echo base_url().'listhomestay/'.$ownerid; ?>'"
                            style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect "><i
                                class="fa fa-arrow-left"></i> Back</button>
                        <?php } ?>
                            <button type="button" name="btn" value="booking" data-toggle="modal" data-target="#newModal"
                            style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect "><i
                                class="fa fa-book"></i> Booking</button>
                        <?php if(GAMMA::getSession('role') != 3){ ?>
                            <button type="button" name="btn"
                            onclick="window.location.href='<?php echo base_url().'homestayuser/'.$homestayid.'/'.$ownerid; ?>'"
                            style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect "><i
                                class="fa fa-user"></i> User</button>
                            <button type="button" name="btn" value="maintenance"  onclick="window.location.href='<?php echo base_url().'homestaymaintenance/'.$homestayid.'/'.$ownerid; ?>'"
                            style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect "><i
                                class="fa fa-gear"></i> Maintenance</button>
                            <button type="button" name="btn" value="expense"  onclick="window.location.href='<?php echo base_url().'homestayexpense/'.$homestayid.'/'.$ownerid; ?>'"
                            style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect "><i
                                class="fa fa-money"></i> Expense</button>
                        <?php } ?>
                    </td>
            </table>
        </form>
    </div>
    

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">
                    <div class="row">
                        <?php foreach ($homestay as $key => $value) { ?>
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-body">
                                    <i class="fa fa-home fa-6" aria-hidden="true"
                                        style="font-size:<?php if($setting[0]['iconsize']){echo $setting[0]['iconsize'];}?>px"></i>
                                    <h5 class="card-title"><?php echo $value['name'] ?></h5>
                                    <p class="card-text">
                                        <?php echo $value['address1'].','.$value['postcode'].','.$value['city']; ?></p>
                                    <?php echo $value['pic'].'('.$value['phone'].')'; ?>

                                </div>

                                
                                <hr>
                                <?php
                                    if(GAMMA::getSession('role') != 3){
                                        echo "<a href=''><i class='fa fa-edit'></i></a>";
                                    }
                                
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="card">
                                <div >
                                <i class="fa fa-bar-chart" aria-hidden="true"
                                        style="font-size:<?php if($setting[0]['iconsize']){echo $setting[0]['iconsize'];}?>px"></i>
                                    <h5> Total Booking : <?php echo $tbook; ?></h5>
                                    <h5> Total Payment : RM <?php echo number_format($tpayment,2); ?></h5>
                                    <h5> Total Maintenance : RM <?php echo number_format($tmaintenance,2); ?></h5>
                                    <h5> Total Expenses : RM <?php echo number_format($texpense,2); ?></h5>
                                    <h5> Total Users</h5>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="row">
                        <?php foreach ($homestay as $key => $value) { ?>
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-header">Calendar</h4>
                                    <div id='calendar-container'>
                                        <div id='calendar'></div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>

                        <div class="row">
                            <?php foreach ($homestay as $key => $value) { ?>
                            <div class="col-sm-12">
                                <div class="body">
                                    <div class="card-body">
                                        <h4 class="card-header">Booking Information</h4>
                                        <table id="myTable" style="font-size:12px;"
                                            class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                    <th>Booking ID </th>
                                                    <th>Contact Info </th>
                                                    <th>Start Date </th>
                                                    <th>End Date </th>
                                                    <th>Amount(Day) (RM)</th>
                                                    <th>Status </th>
                                                    <th>Total (RM) </th>
                                                    <th>Paid (RM) </th>
                                                    <th>Balance (RM)</th>
                                                    <th>Booking By</th>
                                                    <th width='10%'>Action </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                            foreach ($bookinginfo as $key => $value) {
                                                echo "<tr>";
                                                echo "<td><a href='".base_url()."editowner/". $value['id'] ."'>". $value['booking_id'] ."<br></a></td>";
                                                echo "<td>". $value['customer_name'] ."(". $value['customer_phone'] .")</td>";
                                                echo "<td>". date("d/m/Y", strtotime($value['start'])) ."</td>";
                                                echo "<td>". date("d/m/Y", strtotime($value['end_date'])) ."</td>";
                                                echo "<td align='right'>". $value['amount'] ."(". $value['day'] .")</td>";
                                                if($value['status'] == 2){
                                                    echo "<td bgcolor='green' class='text-center text-white '><span class='label success'>". $value['name'] ."</span></td>";
                                                }else  if($value['status'] == 0){
                                                    echo "<td bgcolor='red' class='text-center '><span class='label danger'>". $value['name'] ."</span></td>";
                                                }else  if($value['status'] == 1){
                                                    echo "<td bgcolor='orange' class='text-center'><span class='label warning'>". $value['name'] ."</span></td>";
                                                }else{
                                                    echo "<td bgcolor='orange' class='text-center'><span class='label info'>". $value['name'] ."</span></td>";
                                                }
                                                
                                                echo "<td align='right'>". number_format($value['total'],2) ."</td>";
                                                echo "<td align='right'>". number_format($value['current_payment'],2) ."</td>";
                                                echo "<td align='right'>". number_format($value['balance_payment'],2) ."</td>";
                                                echo "<td>". $value['username'] ."</td>";
                                                echo "<td width='10%'>
                                                <a href='".base_url()."listpayment/". $value['id'] ."/".$homestayid."'><i class='fa fa-money'></i></a>";
                                                if(GAMMA::getSession('role') != 3){
                                                    echo "  <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a>";
                                                    echo "  <a onclick='cancel(". $value['id'] .")' href=''><i class='fa fa-ban'></i></a>";
                                                }else if($value['status'] == 2 && GAMMA::getSession('role') != 3){
                                                    echo "  <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a>";
                                                    echo "  <a onclick='cancel(". $value['id'] .")' href=''><i class='fa fa-ban'></i></a>";
                                                }else if($value['status'] != 2 && GAMMA::getSession('role') == 3){
                                                    echo "  <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a>";
                                                    echo "  <a onclick='cancel(". $value['id'] .")' href=''><i class='fa fa-ban'></i></a>";
                                                }else{
                                                    
                                                }
                                                
                                                echo "</td></tr>";
                                            }
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="editModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLongTitle">Homestay Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <hr>
                <form method="POST" id="mform" action='<?php echo base_url().'submitbooking/'; ?>'>
                    <input type="hidden" name="ownerid" value="<?php echo $ownerid; ?>">
                    <input type="hidden" name="homestayid" value="<?php echo $homestayid; ?>">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Customer Name</label>
                            <input name="custname" type="text" class="form-control" placeholder="Customer name" required>
                        </div>
                        <div class="form-group">
                            <label>Customer Phone</label>
                            <input name="custphone" type="text" class="form-control" placeholder="Customer phone"  required>
                        </div>
                        <div class="form-group">
                            <label>Start Date</label>
                            <input name="startdate" type="date" value='<?php echo date('Y-m-d'); ?>'
                                class="form-control" min='<?php echo date('Y-m-d'); ?>' required>
                        </div>

                        <div class="form-group">
                            <label>Total Day</label>
                            <input name="day" type="text" class="form-control" placeholder="Day" required>
                        </div>

                        <div class="form-group">
                            <label>Amount/day</label>
                            <input name="amount" type="text" class="form-control" placeholder="RM" required>
                        </div>

                        <div class="form-group">
                            <label>Total Deposit</label>
                            <input name="deposit" type="text" class="form-control" value="0" placeholder="RM" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" name="btn" value="book" style="width:120px;"
                                class="btn bg-blue btn-lg  btn-sm waves-effect "><i class="fa fa-arrow-save"></i>
                                Save</button>
                        </div>



                </form>
            </div>
        </div>
    </div>



    <!-- #END# Exportable Table -->


    <script src='<?php echo base_url(); ?>assets/fullcalendar/packages/core/main.js'></script>
    <script src='<?php echo base_url(); ?>assets/fullcalendar/packages/interaction/main.js'></script>
    <script src='<?php echo base_url(); ?>assets/fullcalendar/packages/daygrid/main.js'></script>
    <script src='<?php echo base_url(); ?>assets/fullcalendar/packages/timegrid/main.js'></script>
    <script src='<?php echo base_url(); ?>assets/fullcalendar/packages/list/main.js'></script>



    <script>
    $('#myTable').DataTable();
    var obj = <?php echo $booking; ?>;
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    const now = new Date();
    var today = now.getFullYear() + '-';
    if (now.getMonth() < 10)
        today += '0' + (now.getMonth() + 1) + '-';
    else
        today += (now.getMonth() + 1) + '-';
    if (now.getDay() < 10)
        today += '0' + now.getDay();
    else
        today += now.getDay();

    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
            displayTimezone: 'local',
            height: 'parent',
            title: 'title',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },
            popover: true,
            defaultView: 'dayGridMonth',
            defaultDate: today,
            navLinks: true, // can click day/week names to navigate views
            editable: false,
            eventLimit: true, // allow "more" link when too many events
            events: obj,

        });

        calendar.render();
    });




    function delitm(id) {
        var del = confirm("Are you sure you want to delete this record?");
        if (del == true) {

            url = '<?php echo base_url();?>Homestay/deleteitm/' + id;

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": url,
                "method": "GET",
                "headers": {
                    "cache-control": "no-cache"
                }
            }

            $.ajax(settings).done(function(response) {
                console.log(response);
                alert(response);
            });

        }
    }

    function cancel(id) {
        var del = confirm("Are you sure you want to cancel this record?");
        if (del == true) {

            url = '<?php echo base_url();?>Homestay/updateitm/' + id;

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": url,
                "method": "GET",
                "headers": {
                    "cache-control": "no-cache"
                }
            }

            $.ajax(settings).done(function(response) {
                console.log(response);
                alert(response);
            });

        }
    }
</script>


    <!-- <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script> -->