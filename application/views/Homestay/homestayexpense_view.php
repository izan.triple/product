<!-- Exportable Table -->
<div class="row clearfix">

    <div class="body">
    
        <form method="POST" id="mform" action='<?php echo base_url().'expenseaction/'; ?>'>
            <input type="hidden" name="ownerid" value="<?php echo $ownerid; ?>"> 
            <input type="hidden" name="homestayid" value="<?php echo $homestayid; ?>">
            <br>
            <div class="form-group">
                <label >Date:</label>
                <input type="date" class="form-control" id="date" name="date">
            </div>
            <div class="form-group">
                <label >Description:</label>
                <input type="text" class="form-control" id="desc" name="desc" placeholder="description">
            </div>
            <div class="form-group">
                <label >Amount: RM </label>
                <input type="text" class="form-control" id="amount" name="amount" placeholder="amount">
            </div>
            <div class="form-group">
                <label >Status:</label>
                <select class="form-control" id="status" name="status" placeholder="Select User">
                    <option value="2">Paid</option>
                    <option value="5">Installment</option>
                </select>
            </div><br>
            <div class="form-group">
                <label >Remarks:</label>
                <textarea class="form-control" id="remarks" name="remarks" placeholder="remarks"></textarea>
            </div><br>
            <p align="right">
            <button type="button" onclick="window.location.href='<?php echo base_url().'homestaydetail/'.$homestayid.'/'.$ownerid; ?>'"  style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                    name='btn' value="saveuser"><i class="fa fa-arrow-left"></i> <b>Back</b></button>   
            <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                    name='btn' value="saveuser"><i class="fa fa-save"></i> <b>Save</b></button>
        </form>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">

                </div>
            </div>
        </div>

    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">
                    <table id="myTable" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Remarks</th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($homestayexpense as $key => $value) {
                                    echo "<tr>";
                                    echo "<td>". date("d/m/Y", strtotime($value['date'])) ."</td>";
                                    echo "<td>". $value['description'] ."</td>";
                                    echo "<td align='right'>". number_format($value['amount'],2) ."</td>";
                                    echo "<td>". $value['name'] ."</td>";
                                    echo "<td>". $value['remarks'] ."</td>";
                                    echo "<td width='1%'>
                                    <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a>";
                                    echo "<a data-toggle='modal' data-target='#sourceModal' href='' onclick='edit(". $value['id'] .")' > <span class='fa fa-edit'></span></a></td>";
                                    echo "</tr>";
                                
                            ?>
                           
                            <?php

                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- #END# Exportable Table -->

 <!-- ******************************* Modal Source Keyg*********************************** -->
 <div class="modal fade" id="sourceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-info" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><label id="ttitle"></label></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
        <form method="POST" id="mform" action='<?php echo base_url().'expenseaction/'; ?>'>
            <input type="hidden" name="ownerid" value="<?php echo $ownerid; ?>"> 
            <input type="hidden" name="homestayid" value="<?php echo $homestayid; ?>">
            <input type='hidden' name='id' id='id' class="form-control">
            <table width="100%">
            <tr><td>Date</td><td>:</td><td><input type='date' name='edate' id='edate' class="form-control"></td></tr>
            <tr><td>Description</td><td>:</td><td><input type='text' name='edescription' id='edescription' class="form-control"></td></tr>
            <tr><td>Amount</td><td>:</td><td><input type='text' name='eamount' id='eamount' class="form-control"></td></tr>
            <tr><td>Status</td><td>:</td><td>
                <select class="sel" id="estatus" name="estatus" >
                    <option value="2">Paid</option>
                    <option value="5">Installment</option>
                </select>
                
            </td></tr>
            <tr><td>Remarks</td><td>:</td><td><textarea name='eremark' id='eremark' class="form-control"></textarea></td></tr>
            </table>

        </div>
        <div class="modal-footer">
            <input type="submit" class='btn btn-success btn-bg' id='Save' name='btn' value="edit">
        </div>
        </form>
        </div>
    </div>
</div>
 <!-- ****************************************************************** -->





</div>

<script>

function edit(id) {
    url = '<?php echo base_url();?>HomestayExpense/edititm/' + id;

var settings = {
    "async": true,
    "crossDomain": true,
    "url": url,
    "method": "GET",
    "headers": {
        "cache-control": "no-cache"
    }
}

$.ajax(settings).done(function(response) {
    console.log(response);
    const obj = JSON.parse(response);
    // alert(obj.description);
    $('#id').val(obj.id);
    $('#edate').val(obj.date);
    $('#edescription').val(obj.description);
    $('#eamount').val(obj.amount);
    $('#estatus').val(obj.status);
    $('#eremark').val(obj.remarks);
});
}

$('#myTable').DataTable();



function delitm(id) {
    var del = confirm("Are you sure you want to delete this record?");
    if (del == true) {

        url = '<?php echo base_url();?>HomestayExpense/deleteitm/' + id;

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "headers": {
                "cache-control": "no-cache"
            }
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
            alert(response);
        });

    }
}
</script>


<!-- <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script> -->