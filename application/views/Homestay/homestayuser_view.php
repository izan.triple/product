<!-- Exportable Table -->
<div class="row clearfix">

    <div class="body">
    
        <form method="POST" id="mform" action='<?php echo base_url().'submithuser/'; ?>'>
            <input type="hidden" name="ownerid" value="<?php echo $ownerid; ?>"> 
            <input type="hidden" name="homestayid" value="<?php echo $homestayid; ?>">
            <br>
            <div class="form-group">
                <label >Select User:</label>
                <select class="form-control" id="sel1" name="userid" placeholder="Select User">
                    <?php
                         foreach ($user as $key => $value) {
                             echo "<option value='". $value['user_id'] ."'>". $value['username'] ."</option>";
                         }
                    ?>
                </select>
            </div><br>
            <p align="right">
            <button type="button" onclick="window.location.href='<?php echo base_url().'homestaydetail/'.$homestayid.'/'.$ownerid; ?>'"  style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                    name='btn' value="saveuser"><i class="fa fa-arrow-left"></i> <b>Back</b></button>   
            <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                    name='btn' value="saveuser"><i class="fa fa-save"></i> <b>Save</b></button>
        </form>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">

                </div>
            </div>
        </div>

    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">
                    <table id="myTable" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Report Name </th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                          foreach ($homestayuser as $key => $value) {
                            echo "<tr>";
                            echo "<td>". $value['name'] ."</td>";
                            echo "<td width='1%'>
                            <a href='".base_url()."listhomestay/". $value['userid'] ."'><i class='fa fa-list'></i></a>
                            <a href='".base_url()."editowner/". $value['userid'] ."'><i class='fa fa-edit'></i></a>
                            <a onclick='delitm(". $value['userid'] .")' href=''><i class='fa fa-trash'></i></a></td>";
                            echo "</tr>";
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- #END# Exportable Table -->




</div>

<script>
$('#myTable').DataTable();



function delitm(id) {
    var del = confirm("Are you sure you want to delete this record?");
    if (del == true) {

        url = '<?php echo base_url();?>Report/deleteitm/' + id;

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "headers": {
                "cache-control": "no-cache"
            }
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
            alert(response);
        });

    }
}
</script>


<!-- <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script> -->