<form method="POST" id="mform" action='<?php echo base_url().'homestayaction/'.$ownerid; ?>'>    <p align="right"><button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
            name='btn' value="submit"><i class="fa fa-save"></i> <b>Submit</b></button><br>
    <div class="body" style="height:auto;overflow:auto;background:#fff;font-size:12px;">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" aria-describedby="Name" placeholder="Enter name">
            </div>
            <div class="form-group">
                <label for="address1">Address 1</label>
                <input type="text" class="form-control" name="address1" placeholder="Address 1">
            </div>
            <div class="form-group">
                <label for="address2">Address 2</label>
                <input type="text" class="form-control" name="address2" placeholder="Address 2">
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control" name="city" placeholder="City">
            </div>
            <div class="form-group">
                <label for="postcode">Postcode</label>
                <input type="text" class="form-control" name="postcode" placeholder="Postcode">
            </div>
            <hr>
            <div class="form-group">
                <label for="contactname">Contact Name</label>
                <input type="text" class="form-control" name="contactname" placeholder="Contact Name">
            </div>
            <div class="form-group">
                <label for="contactphone">Contact No.</label>
                <input type="text" class="form-control" name="contactphone" placeholder="Contact No">
            </div>
        </div>
    </div>
</form>


<!-- #END# Exportable Table -->

<script>
var statid = "<?php echo $statid; ?>";
$('#statid').val(statid);
</script>