<div class="container" style="margin-top:6%;font-size:12px;">
    <div class="container">
        <b>Thank you for placing an order. We will contact you soon.</b><br><br>
        Your order detail as below :<br><br>
        DATE : <?php echo date('Y/m/d H:m:s'); ?><br><br>
        <?php echo strtoupper($saleinfo['customer_name']); ?><br>
        <?php echo strtoupper($saleinfo['customer_address']); ?><br>
        <?php echo strtoupper($saleinfo['customer_poscode']).", ".strtoupper($saleinfo['customer_city']).",".strtoupper($saleinfo['state']); ?><br><br><br>

        
        Order ID : <b><?php echo $orderinfo['orderid']; ?></b><br>
        Total : <b>RM <?php echo number_format($orderinfo['total']+$siteSetting[0]->postage,2); ?>
        <i> (Include RM<?php echo $siteSetting[0]->postage ?> postage)</i></b>
        <hr><br><br>
        <b>Order Items:</b><br><br>
        <table width="100%" style="font-size:12px;">
            <tr>
                <td>Item</td>
                <td align="center"><b>Quantity</b></td>
                <td align="right"><b>Unit Price (RM)</b></td>
                <td align="right"><b>Total (RM)</b></td>
            </tr>
            <tr><td colspan=4><hr></td></tr>
            <?php 
                foreach($orderitminfo as $oi){
                ?>
            <tr>
                <td><?php echo $oi['name'] ?></td>
                <td align="center"><?php echo $oi['quantity'] ?></td>
                <td align="right"><?php echo number_format($oi['unit_price'],2); ?></td>
                <td align="right"><?php echo number_format($oi['total'],2); ?></td>
            </tr>
            <?php
            }
            ?>
        </table><br><br>
        


    </div>
</div>