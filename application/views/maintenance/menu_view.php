
<!-- Exportable Table -->
<div class="row clearfix">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>Function Name</th>
                            <th>Primary Name</th>
                            <th>Action </th>
                        </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Function Name</th>
                                <th>Primary Name</th>
                                <th>Action </th>
                            </tr>
                        </tfoot>
                        <tbody>
                              <tr>
                                <?php foreach ($qmenu as $m){
                                  ?>
                                  <td><?php echo $m->menuname;  ?> </td>
                                  <td><?php echo $m->menuurl;  ?> </td>
                                  <td><a id="stmt" title="Set Function" href='<?php echo base_url().'Page/menuitmIndex/'.$m->menuid;?>'  ><span class="fa fa-gear"></span></a>
                                  </tr>
                                  <?php
                                } ?>
                          </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->


