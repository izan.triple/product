<table width="100%">
<tr><td>Reset All Data</td><td width="10%">[ <a href="<?php base_url() ?>Page/Resetdata/all">Reset</a> ]</td></tr>
<tr><td>Reset Booking</td><td width="10%">[ <a href="<?php base_url() ?>Page/Resetdata/booking">Reset</a> ] || [ <a href="<?php base_url() ?>Page/Restoredata/booking">Restore</a> ]</td></tr>
<tr><td>Reset Payment</td><td width="10%">[ <a href="<?php base_url() ?>Page/Resetdata/payment">Reset</a> ] || [ <a href="<?php base_url() ?>Page/Restoredata/payment">Restore</a> ]</td></tr>
<tr><td>Reset Customer</td><td width="10%">[ <a href="<?php base_url() ?>Page/Resetdata/customer">Reset</a> ]</td></tr>
<tr><td>Reset Homestay</td><td width="10%">[ <a href="<?php base_url() ?>Page/Resetdata/homestay">Reset</a> ]</td></tr>
<tr><td>Reset Owner</td><td width="10%">[ <a href="<?php base_url() ?>Page/Resetdata/owner">Reset</a> ]</td></tr>
<tr><td>Reset Expenses</td><td width="10%">[ <a href="<?php base_url() ?>Page/Resetdata/expense">Reset</a> ] || [ <a href="<?php base_url() ?>Page/Restoredata/expense">Restore</a> ]</td></tr>
<tr><td>Reset Maintenance</td><td width="10%">[ <a href="<?php base_url() ?>Page/Resetdata/maintenance">Reset</a> ] || [ <a href="<?php base_url() ?>Page/Restoredata/maintenance">Restore</a> ]</td></tr>
</table>
