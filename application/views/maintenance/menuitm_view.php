
<script src="<?php echo base_url() ?>Assets/js/vendor/jquery-1.11.3.min.js"></script>

<!-- Exportable Table -->
<div class="row clearfix">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                          <th>Submenu Name</th>
                          <th>Submenu URL</th>
                          <th>Action </th>
                        </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                              <?php foreach ($qmenu as $m){
                                ?>
                                <td><?php echo $m->submenuname;  ?> </td>
                                <td><?php echo $m->submenuurl;  ?> </td>
                                <td><a id="stmt" title="Set Function" href='<?php echo base_url().'Page/menuitmIndex/'.$m->submenuid;?>'  ><span class="fa fa-file"></span></a>
                                <a id="stmt" title="Remove" href='<?php echo base_url().'Page/delete_function/'.$m->submenuid;?>'  ><span class="fa fa-remove"></span></a></td>
                                </tr>
                                <?php
                              } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->


<center><h5 class="form-signin-heading">Menu Information</h5></center>
<form >
<table width=80% >
  <tr><td colspan="2">
    <a  href='<?php echo base_url(); ?>menu' class="btn btn-success btn-bg" > <span class="fa fa-back"></span> Back</a>
    <a data-toggle="modal" data-target="#sourceModal" href='' class="btn btn-success btn-bg" > <span class="fa fa-plus"></span> Add SubMenu</a></td></tr>
</table>
</form>
<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Submenu Name</th>
                <th>Submenu URL</th>
                <th>Action </th>
            </tr>
        </thead>
        <tbody>
            <tr>
              <?php foreach ($qmenu as $m){
                ?>
                <td><?php echo $m->submenuname;  ?> </td>
                <td><?php echo $m->submenuurl;  ?> </td>
                <td><a id="stmt" title="Set Function" href='<?php echo base_url().'Page/menuitmIndex/'.$m->submenuid;?>'  ><span class="fa fa-file"></span></a>
                <a id="stmt" title="Remove" href='<?php echo base_url().'Page/delete_function/'.$m->submenuid;?>'  ><span class="fa fa-remove"></span></a></td>
                </tr>
                <?php
              } ?>
        </tbody>

    </table>


    <!-- ******************************* Modal Source Keyg*********************************** -->
    <div class="modal fade" id="sourceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-info" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><label id="ttitle"></label></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id='submenuform'>
              <table>
              <tr><td>Submenu ID</td><td>:<input type='text' name='submenuid' id='submenuid'></td></tr>
              <tr><td>Submenu Name</td><td>:<input type='text' name='submenuname' id='submenuname'></td></tr>
              <tr><td>Submenu URL</td><td>:<input type='text' name='submenuurl' id='submenuurl'></td></tr>
              <tr><td>Menu ID</td><td>:<input type='text' name='menuid' id='menuid' value=<?php echo $menuid?>></td></tr>
              </table>

          </div>
          <div class="modal-footer">
            <button class='btn btn-success btn-bg' id='Save'><span class='fa fa-save'></span> Save </button>
          </div>
        </form>
        </div>
      </div>
    </div>
    <!-- ****************************************************************** -->


    <script>

    $( '#Save').click(function(event) {
        event.preventDefault()
        url = '<?php echo base_url();?>Page/addsubmenu'
        var form = $('#submenuform')[0]
        var data = new FormData(form)
        $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            console.log('SUCCESS : ', response)
            if(response > 0){
              alert('Successfully added');
            }
            location.reload()
            },
            error: function (e) {
            console.log('ERROR : ', e);
            alert('Err');
            }
        })
    });

    </script>
