<style>
.fixed-table-loading{display: none;}
</style>
<h4>State Infomation </h4><hr>

<div>
<div class="row clearfix" style="background-color:white;">
<div class="col-lg-13 col-md-13 col-sm-13 col-xs-12">
     <div class="card">
         <div class="body">
           <form  method="POST" action='<?php echo base_url().'addsystem'; ?>' >
           <button type="submit" name="btn" value="state" class="btn bg-green waves-effect"><i class="fa fa-plus"></i> <span>Add State</span></button></div>
         </form>
     </div>
 </div>
</div><br>
     <div class="row clearfix" style="background-color:white;">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="card">
                           <div class="body">
                               <div class="table-responsive">
                                 <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="true" data-show-toggle="false" data-resizable="true" data-cookie="true"
                                   data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="true" data-toolbar="#toolbar">
                                       <thead>
                                         <tr>
                                             <th>State Name</th>
                                             <th>Honorific</th>
                                             <th>Action </th>
                                         </tr>
                                       </thead>
                                       <tbody>
                                         <?php
                                         foreach ($liststate as $st) {
                                           echo "<tr>";
                                           echo "<td>$st->stateName</td>";
                                           echo "<td>$st->honorific</td>";
                                           echo "<td><a href='".base_url()."roleinfo/$st->stateId'><i class='fa fa-edit'></i></a></td>";
                                           echo "</tr>";
                                         }
                                         ?>
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>




</div>

<!-- Jquery Core Js -->
