<style>
.fixed-table-loading{display: none;}

.success {background-color: #4CAF50;} /* Green */
.info {background-color: #2196F3;} /* Blue */
.warning {background-color: #ff9800;} /* Orange */
.danger {background-color: #f44336;} /* Red */
.other {background-color: #e7e7e7; color: black;} /* Gray */
</style>
<h4>Area Infomation </h4><hr>

<div>
<div class="row clearfix" style="background-color:white;">
<div class="col-lg-13 col-md-13 col-sm-13 col-xs-12">
     <div class="card">
         <div class="body">
           <form  method="POST" action='<?php echo base_url().'addsystem'; ?>' >
           <button type="submit" name="btn" value="area" class="btn bg-green waves-effect"><i class="fa fa-plus"></i> <span>Add Area</span></button></div>
         </form>
     </div>
 </div>
</div>
<br>

     <div class="row clearfix" style="background-color:white;">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="card">
                           <div class="body">
                               <div class="table-responsive">
                                 <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="false" data-show-pagination-switch="false" data-show-refresh="false" data-key-events="true" data-show-toggle="false" data-resizable="true" data-cookie="true"
                                   data-cookie-id-table="saveId" data-show-export="false" data-click-to-select="true" data-toolbar="#toolbar">                                       <thead>
                                         <tr>
                                           <th>Building ID</th>
                                           <th>Area Name</th>
                                           <th>Owner Name</th>
                                           <th>Expiry Date</th>
                                           <th>Area Status</th>
                                           <th>Action </th>
                                         </tr>
                                       </thead>
                                       <tbody>
                                         <?php
                                         foreach ($listarea as $a) {
                                           echo "<tr>";
                                           echo "<td>$a->buildingid</td>";
                                           echo "<td>$a->areaname</td>";
                                           echo "<td>$a->ownername</td>";
                                           echo "<td>".date('d/m/Y',strtotime($a->areaxpiry))."</td>";
                                           if($a->areastatus == 'Active'){
                                             echo "<td><span class='label success'>$a->areastatus</span></td>";
                                           }else{
                                             echo "<td><span class='label danger'>$a->areastatus</span></td>";
                                           }

                                           echo "<td><a href='".base_url()."areainfo/$a->areaid'><i class='fa fa-edit'></i></a></td>";
                                           echo "</tr>";
                                         }
                                         ?>
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>




</div>

<!-- Jquery Core Js -->
