
<?php

if($form == 'state'){
  ?>

  <h4>Add State</h4><hr>
  <form id="<?php echo $form ?>Form" >
  <table width='80%'>
    <tr><td>Country Name</td><td>: <select style="width:50%;"  name='statecountry'>

      <?php foreach ($country as $c){
        echo "<option value='$c->countryid'>$c->countryname</option>";
      }

       ?>
      <select>
    </td></tr>
    <tr><td width='20%'>State Name</td><td>: <input style="width:70%;" type='text' name='statename'><br></td></tr>
    <tr><td>Latitude</td><td>: <input style="width:50%;" type='text' name='statelat'></td></tr>
    <tr><td>Logitude</td><td>: <input style="width:50%;" type='text' name='statelong'></td></tr>
    <tr><td>Honorific</td><td>: <input style="width:50%;" type='text' name='honorific'></td></tr>

    <tr><td colspan="2"><br></td></tr>
  </table>
  <hr>
  <a class="btn btn-success btn-bg" href="<?php echo base_url().'state'; ?>"  style="width:100px;"><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a>
  <a id="btn<?php echo $form;?>" class="btn btn-success btn-bg" href="" style="width:130px;"><i class="fa fa-plus" style='width:30px;'></i>Add State</a>
  </form>
  <?php
}else if($form == 'area'){
  ?>

  <h4>Add Area Information</h4><hr>
  <form id="<?php echo $form ?>Form">
  <table width='80%'>
    <tr><td width='10%'>Owner Name</td><td>:<select style="width:30%;" name='fk_ownerid' id='fk_ownerid'>
      <?php foreach ($owner as $o){
        echo "<option value='$o->ownerid'>$o->ownername</option>";
      }
       ?>
    </select></td></tr>
    <tr><td width="20%">Building ID</td><td>:<input type='text' style="width:30%;" name='buildingid' id='buildingid'></td></tr>
    <tr><td>Area/Site Name</td><td>:<input type='text' style="width:30%;" name='areaname' id='areaname'></td></tr>
    <tr><td>Area/Site Address</td><td>:<input type='text' style="width:30%;" name='areaaddr' id='areaaddr'></td></tr>
    <tr><td>Grace Day</td><td>:<input type='text' style="width:10%;" name='areagraceday' id='areagraceday'></td></tr>
    <tr><td>Expiry Date</td><td>:<input type='date' name='areaxpiry' id='areaxpiry' value="<?php echo date('Y-m-d') ?>"></td></tr>
    <tr><td colspan="2"><br></td></tr>
  </table>
  <hr>
  <a class="btn btn-success btn-bg" href="<?php echo base_url().'area'; ?>"  style="width:100px;"><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a>
  <a id="btn<?php echo $form;?>" class="btn btn-success btn-bg" href="" style="width:130px;"><i class="fa fa-plus" style='width:30px;'></i>Add Area</a>
  </form>
  <?php
}else if($form == 'owner'){
  ?>
  <h4>Add Owner</h4><hr>
  <form id="<?php echo $form ?>Form">
<table width="100%">
      <tr><td width="20%">Owner Name</td><td>:<input type='text' style="width:40%;" name='ownername' id='ownername'></td></tr>
      <tr><td>Owner Address Line 1</td><td>:<input style="width:60%;" type='text' name='owneraddr1' id='owneraddr1'></td></tr>
      <tr><td>Owner Address Line 2</td><td>:<input style="width:60%;" type='text' name='owneraddr2' id='owneraddr2'></td></tr>
      <tr><td>Country</td><td>:<select style="width:50%;"  name='fk_countryId'>
        <?php foreach ($country as $c){
          echo "<option value='$c->countryid'>$c->countryname</option>";
        }
         ?>
        <select>
      </td></tr>

      <tr><td>State</td><td>:<select style="width:50%;"  name='fk_stateId'>
        <?php foreach ($state as $s){
          echo "<option value='$s->stateId'>$s->stateName</option>";
        }

         ?>
        <select>
      </td></tr>

      <tr><td>Owner Postcode</td><td>:<input type='text' name='ownerpostcode' id='ownerpostcode'></td></tr>
      <tr><td>Owner City</td><td>:<input type='text' name='ownercity' id='ownercity'></td></tr>
      <tr><td>Tel 1</td><td>:<input type='text' name='ownertel1' id='ownertel1'></td></tr>
      <tr><td>Tel 2</td><td>:<input type='text' name='ownertel2' id='ownertel2'></td></tr>
      <tr><td>Fax 1</td><td>:<input type='text' name='ownerfax1' id='ownerfax1'></td></tr>
      <tr><td>Fax 2</td><td>:<input type='text' name='ownerfax2' id='ownerfax2'></td></tr>
      <tr><td>Owner Email</td><td>:<input type='text' name='owneremail' id='owneremail'></td></tr>
      <tr><td>Owner Expiry Date</td><td>:<input type='date' name='ownerxpiry' id='ownerxpiry' value="<?php echo date('Y-m-d') ?>"></td></tr>
    </table><br>
      <a class="btn btn-success btn-bg" href="<?php echo base_url().'owner'; ?>"  style="width:100px;"><i class="fa fa-chevron-left" style='width:30px;'></i> Back</a>
      <a id="btn<?php echo $form;?>" class="btn btn-success btn-bg" href="" style="width:130px;"><i class="fa fa-plus" style='width:30px;'></i>Add owner</a>
      </form>

  <?php
}
?>


<script>



$( "#btn<?php echo $form;?>").click(function(event) {
   event.preventDefault();
   var id = "<?php echo $form;?>";
   url = '<?php echo base_url();?>system2/saveinfo/'+id;
   alert(url);
    var form = $('#<?php echo $form;?>Form')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            console.log("SUCCESS : ", response);
            if(response > 0){
              if(response == "001"){
                alert("User already exist !");
              }else{
                alert("Successfully added !");
              }
              history.back();

            }else{
              alert("Fail added !");
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert(e);

        }
    });
});

$( "#btnEdit<?php echo $form;?>").click(function(event) {
   event.preventDefault();
   var id = "<?php echo $form;?>";
   url = '<?php echo base_url();?>User/editinfo/'+id;
    var form = $('#<?php echo $form;?>Form')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (response) {
            console.log("SUCCESS : ", response);
              if(response == 1){
                alert("Successfully Update !");
              }
            location.reload();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert("Err");

        }
    });
});



</script>
