<?php 
$submit = 0;
$install = 0;
$terminate = 0;
$pending = 0;
$p1 = 0;
$p2 = 0;
$p3 = 0;
$p4 = 0;

foreach($dashmerchant as $count){
    // echo $count->status. '-';
    // echo $count->name. '-';
    // echo $count->total. '<br>';
    if($count->status == 20){
        $install = $count->total;
    }
    else if($count->status == 0){       
        $submit = $count->total;
    }else if($count->status == 21){       
        $terminate = $count->total;
    }else if($count->status == 11){       
        $p1 = $count->total;
    }else if($count->status == 12){       
        $p2 = $count->total;
    }else if($count->status == 13){       
        $p3 = $count->total;
    }else if($count->status == 14){       
        $p4 = $count->total;
    }
    
    $pending = $p1+$p2+$p3+$p4;
}


?>


<div class="row rowdash"><br>
    <div class="col-xl-3 col-md-3">
        <div class="card bg-success text-white mb-3" style="background:green;color:white;border-radius: 5px;">
            <div class="card-body">
                <h3>
                    <center>Total Merchant
                </h3>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <center>
                    <h3><?php echo $totalmerchant; ?></h3>
                </center>
                <div class="small text-white"><br></div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-3">
        <div class="card bg-success text-white mb-3" style="background:blue;color:white;border-radius: 5px;">
            <div class="card-body">
                <h3>
                    <center>Total Installed
                </h3>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <center>
                    <h3><?php echo $install; ?></h3>
                    <div class="small text-white"><br></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-3">
        <div class="card bg-success text-white mb-3" style="background:grey;color:white;border-radius: 5px;">
            <div class="card-body">
                <h3>
                    <center>Total Submitted
                </h3>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <center>
                    <h3><?php echo $submit; ?></h3>
                    <div class="small text-white"><br></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-3">
        <div class="card bg-success text-white mb-3" style="background:yellow;color:black;border-radius: 5px;">
            <div class="card-body">
                <h3>
                    <center>Total Pending
                </h3>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <center>
                    <h3><?php echo $pending; ?></h3>
                    <div class="small text-white"><br></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-3">
        <div class="card bg-danger text-white mb-3" style="background:red;color:white;border-radius: 5px;">
            <div class="card-body">
                <h3>
                    <center>Total Terminate
                </h3>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <center>
                    <h3><?php echo $terminate; ?></h3>
                    <div class="small text-white"><br></div>
            </div>
        </div>
    </div>
</div>

<!-- Content Row -->