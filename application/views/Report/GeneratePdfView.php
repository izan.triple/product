<!DOCTYPE html>
<html>

<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title><?php if($type != NULL){echo "<i class='fa fa-file-o'></i>  ".ucwords($type) . " Report";} ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css"
        rel="stylesheet" />
    <style>
        table{
            border-collapse: collapse; /* Remove cell spacing */
        /* width: 235px;
        margin: 2px; */
        }
    }
    </style>
</head>

<body>
    <?php 
        $total = 0;

        foreach($info as $key => $value){
            if($type == 'booking'){
                $total +=$value['total'];
            }else{
                $total +=$value['amount'];
            }
            
        }
        $monthNum = $month;
        $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));

        if($type != NULL){echo "<H3><i class='fa fa-file-o'></i>  ".ucwords($type) . " Report for ".$monthName." ".$year."</H3>";} 
    ?><br>
    
    <p>Homestay Name: <?php echo $hs; ?></p>
    <p>Sum Amount: RM <?php echo number_format($total,2); ?></p>
    <p>Report Date : <?php echo date('d/m/Y') ?></p>
    <hr>
    <table id="myTable" style="font-size:12px;"
        class="table table-bordered table-striped table-hover dataTable js-exportable" border=1 width="100%">
        <thead>
            <?php if($type == "booking" && $type !=NULL){?>
            <tr>
                <th>Booking ID </th>
                <th>Contact Info </th>
                <th>Start Date </th>
                <th>End Date </th>
                <th>Amount (RM)</th>
                <th>Day</th>
                <th>Total (RM) </th>
                <th>Paid (RM) </th>
                <th>Balance (RM)</th>
                <th >Homestay </th>
                <th>Status </th>
            </tr>
            <?php }else if($type == "payment" && $type !=NULL){ ?>
            <tr>
                <th>Date </th>
                <th>Description </th>
                <th >Amount(RM) </th>
                <th >Homestay </th>
            </tr>
            <?php }else if($type == "maintenance" && $type !=NULL){ ?>
            <tr>
                <th>Date</th>
                <th>Description</th>
                <th>Amount (RM)</th>
                <th>Homestay </th>
                <th>Remarks</th>
                <th>Status</th>
            </tr>
            <?php }else if($type == "expense" && $type !=NULL){ ?>
            <tr>
                <th>Date</th>
                <th>Description</th>
                <th>Amount (RM)</th>
                <th>Homestay </th>
                <th>Remarks</th>
                <th>Status</th>
            </tr>
            <?php }?>
        </thead>
        <tbody>
            <?php 
                if($type == "booking"  && $type !=NULL){
                    foreach ($info as $key => $value) {
                        echo "<tr>";
                        echo "<td>". $value['booking_id'] ."<br></a></td>";
                        echo "<td>". $value['customer_name'] ."(". $value['customer_phone'] .")</td>";
                        echo "<td>". date("d/m/Y", strtotime($value['start'])) ."</td>";
                        echo "<td>". date("d/m/Y", strtotime($value['end_date'])) ."</td>";
                        echo "<td align='right'>". number_format($value['amount'],2) ."</td>";
                        echo "<td align='right'>". $value['day'] ."</td>";
                        echo "<td align='right'>". number_format($value['total'],2) ."</td>";
                        echo "<td align='right'>". number_format($value['current_payment'],2) ."</td>";
                        echo "<td align='right'>". number_format($value['balance_payment'],2) ."</td>";
                        echo "<td>". $value['homestayname'] ."</td>";
                        if($value['status'] == 2){
                            echo "<td bgcolor='green' class='text-center text-white '><span class='label success'>". $value['name'] ."</span></td>";
                        }else  if($value['status'] == 0){
                            echo "<td bgcolor='red' class='text-center '><span class='label danger'>". $value['name'] ."</span></td>";
                        }else  if($value['status'] == 1){
                            echo "<td bgcolor='orange' class='text-center'><span class='label warning'>". $value['name'] ."</span></td>";
                        }else{
                            echo "<td bgcolor='orange' class='text-center'><span class='label info'>". $value['name'] ."</span></td>";
                        }
                        echo "</tr>";
                    }
                }else if($type == "payment" && $type !=NULL){
                    foreach ($info as $key => $value) {
                        echo "<tr>";
                        echo "<td>". date("d/m/Y", strtotime($value['date'])) ."</td>";
                        echo "<td>". $value['description'] ."</td>";
                        echo "<td>". number_format($value['amount'],2) ."</td>";
                        echo "<td>". $value['homestayname'] ."</td>";
                        echo "</tr>";
                    }
                }else if($type == "maintenance" && $type !=NULL){
                    foreach ($info as $key => $value) {
                        echo "<tr>";
                        echo "<td>". date("d/m/Y", strtotime($value['date'])) ."</td>";
                        echo "<td>". $value['description'] ."</td>";
                        echo "<td>". number_format($value['amount'],2) ."</td>";
                        echo "<td>". $value['homestayname'] ."</td>";
                        echo "<td>". $value['remarks'] ."</td>";
                        echo "<td>". $value['name'] ."</td>";
                        if($value['status'] == 2){
                            echo "<td bgcolor='green' class='text-center text-white'><span class='label success'>". $value['name'] ."</span></td>";
                        }else  if($value['status'] == 0){
                            echo "<td bgcolor='red' class='text-center '><span class='label danger'>". $value['name'] ."</span></td>";
                        }else  if($value['status'] == 1){
                            echo "<td bgcolor='orange' class='text-center'><span class='label warning'>". $value['name'] ."</span></td>";
                        }else{
                            echo "<td bgcolor='orange' class='text-center'><span class='label info'>". $value['name'] ."</span></td>";
                        }
                        echo "</tr>";
                    }
                }else if($type == "expense" && $type !=NULL){
                    foreach ($info as $key => $value) {
                        echo "<tr>";
                        echo "<td>". date("d/m/Y", strtotime($value['date'])) ."</td>";
                        echo "<td>". $value['description'] ."</td>";
                        echo "<td>". number_format($value['amount'],2) ."</td>";
                        echo "<td>". $value['homestayname'] ."</td>";
                        echo "<td>". $value['remarks'] ."</td>";
                        if($value['status'] == 2){
                            echo "<td bgcolor='green' class='text-center text-white'><span class='label success'>". $value['name'] ."</span></td>";
                        }else  if($value['status'] == 0){
                            echo "<td bgcolor='red' class='text-center '><span class='label danger'>". $value['name'] ."</span></td>";
                        }else  if($value['status'] == 1){
                            echo "<td bgcolor='orange' class='text-center'><span class='label warning'>". $value['name'] ."</span></td>";
                        }else{
                            echo "<td bgcolor='orange' class='text-center'><span class='label info'>". $value['name'] ."</span></td>";
                        }
                        echo "</tr>";
                    }
                }
            ?>

        </tbody>
    </table>
</body>

</html>