<!-- Exportable Table -->
<div class="row clearfix">
    <div class="body">
        <form method="POST" id="mform" action='<?php echo base_url().'generate/'; ?>'>
            <br>
            <table style="overflow-x:auto;overflow-y:auto;">
                <tr>
                    <td>
                        <div class="form-group">
                            <label>Year:</label>
                            <select name="year" id="" class="form-control">
                                <?php
                                    foreach ($listyear as $key => $value) {
                                        if($value['yearname'] == $year){
                                            echo "<option value='". $value['yearname'] ."' selected>". $value['yearname'] ."</option>";
                                        }else{
                                            echo "<option value='". $value['yearname'] ."'>". $value['yearname'] ."</option>";
                                        }
                                    }
                                ?>
                                
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label>Month:</label>
                            <select name="month" id="" class="form-control">
                                <option value="01" <?php if($month == "01"){echo "selected";} ?> >Jan</option>
                                <option value="02" <?php if($month == "02"){echo "selected";} ?>>Feb</option>
                                <option value="03" <?php if($month == "03"){echo "selected";} ?>>Mac</option>
                                <option value="04" <?php if($month == "04"){echo "selected";} ?>>Apr</option>
                                <option value="05" <?php if($month == "05"){echo "selected";} ?>>May</option>
                                <option value="06" <?php if($month == "06"){echo "selected";} ?>>Jun</option>
                                <option value="07" <?php if($month == "07"){echo "selected";} ?>>Jul</option>
                                <option value="08" <?php if($month == "08"){echo "selected";} ?>>Aug</option>
                                <option value="09" <?php if($month == "09"){echo "selected";} ?>>Sep</option>
                                <option value="10" <?php if($month == "10"){echo "selected";} ?>>Oct</option>
                                <option value="11" <?php if($month == "11"){echo "selected";} ?>>Nov</option>
                                <option value="12" <?php if($month == "12"){echo "selected";} ?>>Dec</option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label>Report Type:</label>
                            <select name="type" id="" class="form-control">
                                <option value="booking" <?php if($type == "booking"){echo "selected";} ?>>Booking</option>
                                <option value="payment" <?php if($type == "payment"){echo "selected";} ?>>Payment</option>
                                <option value="maintenance" <?php if($type == "maintenance"){echo "selected";} ?>>Maintenance</option>
                                <option value="expense" <?php if($type == "expense"){echo "selected";} ?>>Expense</option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label>Homestay:</label>
                            <select name="homestay" id="" class="form-control">
                                <?php
                                    foreach ($homestay as $key => $value) {
                                        if($value['id'] == $hstay){
                                            echo "<option value='". $value['id'] ."' selected >". $value['name'] ."</option>";
                                        }else{
                                            echo "<option value='". $value['id'] ."'>". $value['name'] ."</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </td>
                </tr>
                

                <tr>
                    <td colspan="3"><br>
                        <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                            name='btn' value="search"><i class="fa fa-search"></i> <b>Search</b></button>
                        <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                            name='btn' value="pdf"><i class="fa fa-download"></i> <b>PDF</b></button>
                    </td>
                </tr>
            </table>
        </form>
    </div>

     

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">
                    <H4> <?php if($type != NULL){echo "<i class='fa fa-file-o'></i>  ".ucwords($type) . " Report";} ?></H4><br>
                    <table id="myTable" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <?php if($type == "booking" && $type !=NULL){?>
                                <tr>
                                    <th>Booking ID </th>
                                    <th>Contact Info </th>
                                    <th>Start Date </th>
                                    <th>End Date </th>
                                    <th>Amount (RM)</th>
                                    <th>Day</th>
                                    <th>Total (RM) </th>
                                    <th>Paid (RM) </th>
                                    <th>Balance (RM)</th>
                                    <th width='10%'>Homestay </th>
                                    <th>Status </th>
                                    <th>Action </th>
                                </tr>
                            <?php }else if($type == "payment" && $type !=NULL){ ?>
                                <tr>
                                <th>Date </th>
                                    <th>Description </th>
                                    <th width='20%'>Amount(RM) </th>
                                    <th width='10%'>Homestay </th>
                                </tr>
                            <?php }else if($type == "maintenance" && $type !=NULL){ ?>
                                <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th>Homestay </th>
                                    <th>Remarks</th>
                                    <th>Status</th>
                                </tr>
                            <?php }else if($type == "expense" && $type !=NULL){ ?>
                                <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th>Homestay </th>
                                    <th>Remarks</th>
                                    <th>Status</th>
                                </tr>
                            <?php }?>
                        </thead>
                        <tbody>
                            <?php 
                                if($type == "booking"  && $type !=NULL){
                                    foreach ($info as $key => $value) {
                                        echo "<tr>";
                                        echo "<td>". $value['booking_id'] ."<br></a></td>";
                                        echo "<td>". $value['customer_name'] ."(". $value['customer_phone'] .")</td>";
                                        echo "<td>". date("d/m/Y", strtotime($value['start'])) ."</td>";
                                        echo "<td>". date("d/m/Y", strtotime($value['end_date'])) ."</td>";
                                        echo "<td align='right'>". number_format($value['amount'],2) ."</td>";
                                        echo "<td align='right'>". $value['day'] ."</td>";
                                        echo "<td align='right'>". number_format($value['total'],2) ."</td>";
                                        echo "<td align='right'>". number_format($value['current_payment'],2) ."</td>";
                                        echo "<td align='right'>". number_format($value['balance_payment'],2) ."</td>";
                                        echo "<td>". $value['homestayname'] ."</td>";
                                        
                                        if($value['status'] == 2){
                                            echo "<td bgcolor='green' class='text-center text-white '><span class='label success'>". $value['name'] ."</span></td>";
                                        }else  if($value['status'] == 0){
                                            echo "<td bgcolor='red' class='text-center '><span class='label danger'>". $value['name'] ."</span></td>";
                                        }else  if($value['status'] == 1){
                                            echo "<td bgcolor='orange' class='text-center'><span class='label warning'>". $value['name'] ."</span></td>";
                                        }else{
                                            echo "<td bgcolor='orange' class='text-center'><span class='label info'>". $value['name'] ."</span></td>";
                                        }
                                        echo "</tr>";
                                    }
                                }else if($type == "payment" && $type !=NULL){
                                    foreach ($info as $key => $value) {
                                        echo "<tr>";
                                        echo "<td>". date("d/m/Y", strtotime($value['date'])) ."</td>";
                                        echo "<td>". $value['description'] ."</td>";
                                        echo "<td>". number_format($value['amount'],2) ."</td>";
                                        echo "<td>". $value['homestayname'] ."</td>";
                                        echo "</tr>";
                                    }
                                }else if($type == "maintenance" && $type !=NULL){
                                    foreach ($info as $key => $value) {
                                        echo "<tr>";
                                        echo "<td>". date("d/m/Y", strtotime($value['date'])) ."</td>";
                                        echo "<td>". $value['description'] ."</td>";
                                        echo "<td>". number_format($value['amount'],2) ."</td>";
                                        echo "<td>". $value['homestayname'] ."</td>";
                                        echo "<td>". $value['remarks'] ."</td>";
                                        if($value['status'] == 2){
                                            echo "<td bgcolor='green' class='text-center text-white'><span class='label success'>". $value['name'] ."</span></td>";
                                        }else  if($value['status'] == 0){
                                            echo "<td bgcolor='red' class='text-center '><span class='label danger'>". $value['name'] ."</span></td>";
                                        }else  if($value['status'] == 1){
                                            echo "<td bgcolor='orange' class='text-center'><span class='label warning'>". $value['name'] ."</span></td>";
                                        }else{
                                            echo "<td bgcolor='orange' class='text-center'><span class='label info'>". $value['name'] ."</span></td>";
                                        }
                                        
                                        echo "</tr>";
                                    }
                                }else if($type == "expense" && $type !=NULL){
                                    foreach ($info as $key => $value) {
                                        echo "<tr>";
                                        echo "<td>". date("d/m/Y", strtotime($value['date'])) ."</td>";
                                        echo "<td>". $value['description'] ."</td>";
                                        echo "<td>". number_format($value['amount'],2) ."</td>";
                                        echo "<td>". $value['homestayname'] ."</td>";
                                        echo "<td>". $value['remarks'] ."</td>";
                                        if($value['status'] == 2){
                                            echo "<td bgcolor='green' class='text-center text-white '><span class='label success'>". $value['name'] ."</span></td>";
                                        }else  if($value['status'] == 0){
                                            echo "<td bgcolor='red' class='text-center '><span class='label danger'>". $value['name'] ."</span></td>";
                                        }else  if($value['status'] == 1){
                                            echo "<td bgcolor='orange' class='text-center'><span class='label warning'>". $value['name'] ."</span></td>";
                                        }else{
                                            echo "<td bgcolor='orange' class='text-center'><span class='label info'>". $value['name'] ."</span></td>";
                                        }
                                        
                                        echo "</tr>";
                                    }
                                }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->




</div>

<script>
$('#myTable').DataTable();
</script>