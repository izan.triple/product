<html>
    <head>
        <style>
            /** Define the margins of your page **/
            @page {
                margin: 100px 25px;
            }

            header {
                position: fixed;
                top: -80px;

            }

            footer {
                position: fixed;
                bottom: -20px;

            }

            table {
                border-collapse: collapse;
            }


        </style>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
        <!-- <header> -->
          <!-- <h3>DATAPREP HOLDINGS BHD </h3>
          <h3><center>Declaration List</center></h3> -->

        <!-- </header>

        <footer> -->
            <!-- <center>Thanks for your business!</center> -->
        <!-- </footer> -->

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
          Date:<?php echo date('d/m/Y') ?><br>
          Total Data:<?php echo count($listsaleman) ?><br>
          Printed by:<?php echo $this->session->userdata('username'); ?><br>

          <?php
                    if($listsaleman != 0) {
                      ?>
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                              <th width='15%'>Salesman Code	</th>
                              <th width='20%'>Salesman Name </th>
                              <th width='15%'>Contact No </th>
                              <th width='15%'>Status </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          foreach ($listsaleman as $sale) {
                            echo "<tr>";
                            echo "<td>$sale->CN</td>";
                            echo "<td>$sale->Name</td>";
                            echo "<td>$sale->Contact</td>";
                            echo "<td>";
                            if($sale->status == "0"){echo "Active";}else{echo "Inactive";}
                            echo "</td>";
                            echo "</tr>";
                          }
                          ?>
                        </tbody>
                    </table>
                        <?php } ?>
        </main>
    </body>
</html>
