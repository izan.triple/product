

<div class="body">
  <form  method="POST" action='<?php echo base_url().'salesmaninfo'; ?>' >
    <table width="100%">
      <tr><td  width='10%'>Status</td><td width='40%'>: 
      <?php if($bank == 'hlbb' || $bank == 'ghl'){?>
      <select name="statid" class="intext" id="statid">
              <option value="all">All</option>
              <option value="0">Active</option>
							<option value="1">Inactive</option>
						
          </select>
       <?php 
         
         }
          ?>
      
      <br></td>
      <td  width='40%'> <input type='text' name='search' size="50" placeholder='Salesman code / Salesman name' value=<?php if($search){echo $search;} ?>><br></td>
      <tr><td colspan="2"><br>
      <!-- <button   type="submit"  class="btn btn-sm btn-success" style="width: 150px;" name='searchButton' value="search"><i class="fa fa-search" > Search </i></button> -->

        <button type="submit" style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value="add"><i class="fa fa-plus" ></i> <b>Add Salesman</b></button>
        <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value='search'><i class="fa fa-search" ></i> <b>Search</b></button>
        <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value='csv'><i class="fa fa-download" ></i> <b>CSV</b></button>
        <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect" name='btn' value='pdf'><i class="fa fa-download" ></i> <b>PDF</b></button>
      </td>
    </table>


</form></div>

<!-- Exportable Table -->
<div class="row clearfix" >
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:450px;overflow:auto;background:#fff;font-size:12px;">
                <div class="table-responsive">
                <?php
                    if(count($listsaleman) > 0) {
                      ?>
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                              <th width='15%'>Salesman Code	</th>
                              <th width='20%'>Salesman Name </th>
                              <th width='15%'>Contact No </th>
                              <th width='15%'>Status </th>
                              <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th width='15%'>Salesman Code	</th>
                              <th width='20%'>Salesman Name </th>
                              <th width='15%'>Contact No </th>
                              <th width='15%'>Status </th>
                              <th width='1%'>Action </th>
                            </tr>
                        </tfoot>
                        <tbody>
                          <?php
                          foreach ($listsaleman as $sale) {
                            echo "<tr>";
                            echo "<td>$sale->CN</td>";
                            echo "<td>$sale->Name</td>";
                            echo "<td>$sale->Contact</td>";
                            echo "<td>";
                            if($sale->status == "0"){echo "Active";}else{echo "Inactive";}
                            echo "</td>";
                            echo "<td width='1%'><a  href='".base_url()."salesmanupdate/$sale->ID'><i class='fa fa-edit'></i></a>  </td>";
                            echo "</tr>";
                          }
                          ?>
                        </tbody>
                    </table>
                        <?php }else{
                          echo "No Data";
                        } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->


</div>
<script>
  var statid = "<?php echo $statid; ?>";
  $('#statid').val(statid);

  </script>


