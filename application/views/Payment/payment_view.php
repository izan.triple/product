<!-- Exportable Table -->
<div class="row clearfix">
    <div class="body">

        <form method="POST" id="mform" action='<?php echo base_url().'submitpayment/'; ?>'>
            <input type="hidden" name="bookid" value="<?php echo $bookingid; ?>">
            <input type="hidden" name="homestayid" value="<?php echo $homestayid; ?>">
            <div class="modal-body">
                <div class="form-group">
                    <?php if($booking[0]['balance_payment'] > 0){?>
                    <h4 style="color:red;">REMAINING PAYMENT : RM
                        <?php echo number_format($booking[0]['balance_payment'],2);?></h4>
                    <?php } ?>
                    <p><b>TOTAL PAYMENT :</b> RM <?php echo number_format($booking[0]['total'],2);?></p>
                    <p><b>CURRENT PAYMENT :</b> RM <?php echo number_format($booking[0]['current_payment'],2);?></p>
                    <p><b>STATUS :</b>
                        <?php if($paystatus['name'] == "Paid"){ ?>
                    <div class="bg-success text-white font-weight-bold"><?php echo $paystatus['name'];?></div>
                    <?php }else{ ?>
                    <div class="bg-primary text-white font-weight-bold"><?php echo $paystatus['name'];?></div>
                    <?php } ?>
                    </p>
                    <hr>
                </div>
                <?php if($booking[0]['current_payment'] != $booking[0]['total']){?>
                <div class="form-group">
                    <label>Date</label>
                    <input name="paydate" type="date" value='<?php echo date('Y-m-d'); ?>' class="form-control"
                        min='<?php echo date('Y-m-d'); ?>' required>
                </div>

                <div class="form-group">
                    <label>Description</label>
                        <select name="paydescrtiption" id="" class="form-control">
                            <option value="Payment">Payment</option>
                            <option value="Refund">Refund</option>
                        </select>
                </div><br><br>

                <div class="form-group">
                    <label>Amount</label>
                    <input name="payamount" type="text" class="form-control" placeholder="RM" required>
                </div>

                <div class="form-group">
                    <button type="button" name="btn"
                        onclick="window.location.href='<?php echo base_url().'homestaydetail/'.$booking[0]['homestayid'].'/'.$booking[0]['ownerid']; ?>'"
                        style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect "><i
                            class="fa fa-arrow-left"></i> Back</button>
                    <button type="submit" name="btn" value="payment" style="width:120px;"
                        class="btn bg-blue btn-lg  btn-sm waves-effect "><i class="fa fa-save"></i> Save</button>
                </div>
                <?php }else{?>
                    <button type="button" name="btn"
                        onclick="window.location.href='<?php echo base_url().'homestaydetail/'.$booking[0]['homestayid'].'/'.$booking[0]['ownerid']; ?>'"
                        style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect "><i
                            class="fa fa-arrow-left"></i> Back</button>
                            <button type="submit" name="btn" value="refund" style="width:120px;"
                        class="btn bg-red btn-lg  btn-sm waves-effect "><i class="fa fa-money"></i> Refund</button>
                <?php } ?>
        </form>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">
                    <table id="myTable" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Date </th>
                                <th>Description </th>
                                <th width='20%'>Amount(RM) </th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                          foreach ($payment as $key => $value) {
                            echo "<tr>";
                            echo "<td>". date("d/m/Y", strtotime($value['date'])) ."</td>";
                            echo "<td>". $value['description'] ."</td>";
                            echo "<td>". number_format($value['amount'],2) ."</td>";
                            echo "<td width='1%'>
                            <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a></td>";
                            echo "</tr>";
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->




</div>

<script>
$('#myTable').DataTable();



function delitm(id) {
    var del = confirm("Are you sure you want to delete this record?");
    if (del == true) {

        url = "<?php echo base_url();?>Owner/deleteitm/" + id;
        alert(url);

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "headers": {
                "cache-control": "no-cache"
            }
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
            alert(response);
        });

    }
}
</script>


<!-- <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script> -->