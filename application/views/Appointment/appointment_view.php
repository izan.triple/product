<!-- Exportable Table -->
<div class="row clearfix">
    <div class="body">
        <form method="POST" id="mform" action='<?php echo base_url().'homestayaction/'.$ownerid; ?>'>
            <table width="100%">
                <tr>
                    <td colspan="2"><br>
                        
                        <?php if(GAMMA::getSession('role') !=3){ ?>
                            <button type="submit" style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                            name='btn' value="back"><i class="fa fa-arrow-left"></i> <b>Back</b></button>

                            <button type="submit" style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                            name='btn' value="add"><i class="fa fa-plus"></i> <b>Data Baru</b></button>
                        <?php }?>
                    </td>
                </tr>
                </tr>
                    <td colspan="2"><br>
                        <?php
                          foreach ($country as $key => $value) {
                            echo "<button type='submit' style='width:120px;' class='btn bg-blue btn-lg  btn-sm waves-effect'
                            name='btn' value='add'><i class='fa fa-search'></i> <b>". $value['country'] ."</b></button>  ";
                          }
                        ?>
                        <br>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive"><br><br>
                    Status : <span class="label label-default">All</span>
                    <table id="myTable0" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Country </th>
                                <th>Area </th>
                                <th>Phone </th>
                                <th>Name </th>
                                <th>Status </th>
                                <th>Rawatan </th>
                                <th>Remark </th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php
                          foreach ($appointment as $key => $value) {
                                echo "<tr>";
                                echo "<td>". $value['country'] ."</td>";
                                echo "<td>". $value['area'] ."</td>";
                                echo "<td>". $value['phone'] ."</td>";
                                echo "<td>". $value['name'] ."</td>";
                                echo "<td>". $value['status'] ."</td>";
                                echo "<td width='30%'>". $value['treatment'] ."</td>";
                                echo "<td>". $value['remark'] ."</td>";
                                echo "<td width='1%'>
                                <a href='".base_url()."listhomestay/". $value['id'] ."'><i class='fa fa-list'></i></a>
                                <a href='".base_url()."editowner/". $value['id'] ."'><i class='fa fa-edit'></i></a>
                                <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a></td>";
                                echo "</tr>";
                                    
                            
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive"><br><br>
                    Status : <span class="label label-warning">Not confirm</span>
                    <table id="myTable" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Country </th>
                                <th>Area </th>
                                <th>Phone </th>
                                <th>Name </th>
                                <th>Status </th>
                                <th>Rawatan </th>
                                <th>Remark </th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php
                          foreach ($appointment as $key => $value) {
                            if($value['status'] == "Not confirm"){
                                echo "<tr>";
                                echo "<td>". $value['country'] ."</td>";
                                echo "<td>". $value['area'] ."</td>";
                                echo "<td>". $value['phone'] ."</td>";
                                echo "<td>". $value['name'] ."</td>";
                                echo "<td>". $value['status'] ."</td>";
                                echo "<td width='30%'>". $value['treatment'] ."</td>";
                                echo "<td>". $value['remark'] ."</td>";
                                echo "<td width='1%'>
                                <a href='".base_url()."listhomestay/". $value['id'] ."'><i class='fa fa-list'></i></a>
                                <a href='".base_url()."editowner/". $value['id'] ."'><i class='fa fa-edit'></i></a>
                                <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a></td>";
                                echo "</tr>";
                                    
                            }
                            
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive"><br><br>
                    Status : <span class="label label-primary">Appointment</span>
                    <table id="myTable2" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Country </th>
                                <th>Area </th>
                                <th>Phone </th>
                                <th>Name </th>
                                <th>Status </th>
                                <th>Rawatan </th>
                                <th>Remark </th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php
                          foreach ($appointment as $key => $value) {
                            if($value['status'] == "Appointment"){
                                echo "<tr>";
                                echo "<td>". $value['country'] ."</td>";
                                echo "<td>". $value['area'] ."</td>";
                                echo "<td>". $value['phone'] ."</td>";
                                echo "<td>". $value['name'] ."</td>";
                                echo "<td>". $value['status'] ."</td>";
                                echo "<td>". $value['treatment'] ."</td>";
                                echo "<td>". $value['remark'] ."</td>";
                                echo "<td width='1%'>
                                <a href='".base_url()."listhomestay/". $value['id'] ."'><i class='fa fa-list'></i></a>
                                <a href='".base_url()."editowner/". $value['id'] ."'><i class='fa fa-edit'></i></a>
                                <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a></td>";
                                echo "</tr>";
                                    
                            }
                            
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive"><br><br>
                    Status : <span class="label label-success">Completed</span>
                    <table id="myTable3" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Country </th>
                                <th>Area </th>
                                <th>Phone </th>
                                <th>Name </th>
                                <th>Status </th>
                                <th>Rawatan </th>
                                <th>Remark </th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php
                          foreach ($appointment as $key => $value) {
                            if($value['status'] == "Completed"){
                                echo "<tr>";
                                echo "<td>". $value['country'] ."</td>";
                                echo "<td>". $value['area'] ."</td>";
                                echo "<td>". $value['phone'] ."</td>";
                                echo "<td>". $value['name'] ."</td>";
                                echo "<td>". $value['status'] ."</td>";
                                echo "<td>". $value['treatment'] ."</td>";
                                echo "<td>". $value['remark'] ."</td>";
                                echo "<td width='1%'>
                                <a href='".base_url()."listhomestay/". $value['id'] ."'><i class='fa fa-list'></i></a>
                                <a href='".base_url()."editowner/". $value['id'] ."'><i class='fa fa-edit'></i></a>
                                <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a></td>";
                                echo "</tr>";
                                    
                            }
                            
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive"><br><br>
                    Status : <span class="label label-info">Hold</span>
                    <table id="myTable4" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Country </th>
                                <th>Area </th>
                                <th>Phone </th>
                                <th>Name </th>
                                <th>Status </th>
                                <th>Rawatan </th>
                                <th>Remark </th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php
                          foreach ($appointment as $key => $value) {
                            if($value['status'] == "Hold"){
                                echo "<tr>";
                                echo "<td>". $value['country'] ."</td>";
                                echo "<td>". $value['area'] ."</td>";
                                echo "<td>". $value['phone'] ."</td>";
                                echo "<td>". $value['name'] ."</td>";
                                echo "<td>". $value['status'] ."</td>";
                                echo "<td>". $value['treatment'] ."</td>";
                                echo "<td>". $value['remark'] ."</td>";
                                echo "<td width='1%'>
                                <a href='".base_url()."listhomestay/". $value['id'] ."'><i class='fa fa-list'></i></a>
                                <a href='".base_url()."editowner/". $value['id'] ."'><i class='fa fa-edit'></i></a>
                                <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a></td>";
                                echo "</tr>";
                                    
                            }
                            
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive"><br><br>
                    Status : <span class="label label-danger">Closed</span>
                    <table id="myTable5" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Country </th>
                                <th>Area </th>
                                <th>Phone </th>
                                <th>Name </th>
                                <th>Status </th>
                                <th>Rawatan </th>
                                <th>Remark </th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php
                          foreach ($appointment as $key => $value) {
                            if($value['status'] == "Closed"){
                                echo "<tr>";
                                echo "<td>". $value['country'] ."</td>";
                                echo "<td>". $value['area'] ."</td>";
                                echo "<td>". $value['phone'] ."</td>";
                                echo "<td>". $value['name'] ."</td>";
                                echo "<td>". $value['status'] ."</td>";
                                echo "<td>". $value['treatment'] ."</td>";
                                echo "<td>". $value['remark'] ."</td>";
                                echo "<td width='1%'>
                                <a href='".base_url()."listhomestay/". $value['id'] ."'><i class='fa fa-list'></i></a>
                                <a href='".base_url()."editowner/". $value['id'] ."'><i class='fa fa-edit'></i></a>
                                <a onclick='delitm(". $value['id'] .")' href=''><i class='fa fa-trash'></i></a></td>";
                                echo "</tr>";
                                    
                            }
                            
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- #END# Exportable Table -->




</div>

<script>
$('#myTable').DataTable();
$('#myTable0').DataTable();
$('#myTable2').DataTable();
$('#myTable3').DataTable();
$('#myTable4').DataTable();
$('#myTable5').DataTable();


function delitm(id) {
    var del = confirm("Are you sure you want to delete this record?");
    if (del == true) {

        url = '<?php echo base_url();?>Report/deleteitm/' + id;

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "headers": {
                "cache-control": "no-cache"
            }
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
            alert(response);
        });

    }
}
</script>


<!-- <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script> -->