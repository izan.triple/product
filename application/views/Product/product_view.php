<!-- Exportable Table -->
<div class="row clearfix">
<div class="body">
    <form method="POST" id="mform" action='<?php echo base_url().'rndaction'; ?>'>
          <table width="100%">
            <tr>
                <td colspan="2"><br>
                    <!-- <button   type="submit"  class="btn btn-sm btn-success" style="width: 150px;" name='searchButton' value="search"><i class="fa fa-search" > Search </i></button> -->
                    <?php if($this->session->userdata('role') != 3){?>
                    <button type="submit" style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                        name='btn' value="add"><i class="fa fa-plus"></i> <b>Add Rnd</b></button> <?php } ?>
                    <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                        name='btn' value='csv'><i class="fa fa-download"></i> <b>CSV</b></button>
                    <button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                        name='btn' value='pdf'><i class="fa fa-download"></i> <b>PDF</b></button>
                </td>
        </table>


    </form>
</div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">
                    <table id="myTable" style="font-size:12px;"
                        class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Name  </th>
                                <th>Price  </th>
                                <th>Stock  </th>
                                <th width='1%'>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                          foreach ($proddisp as $key => $value) {
                            echo "<tr>";
                            echo "<td>". $value['name'] ."</td>";
                            echo "<td>". number_format($value['price'],2)."</td>";
                            echo "<td>". $value['stock'] ."</td>";
                                                        
                            echo "<td width='1%'>
                            <a href='".base_url()."editprod/". $value['id'] ."'>
                            <i class='fa fa-edit'></i></a></td>";
                            echo "</tr>";
                          }
                          ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->




</div>

<script>
$('#myTable').DataTable();



function delitm(id) {
    var del = confirm("Are you sure you want to delete this record?");
    if (del == true) {
        
        url = "<?php echo base_url();?>Owner/deleteitm/" + id;
        alert(url);

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "headers": {
                "cache-control": "no-cache"
            }
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
            alert(response);
        });

    }
}
</script>


<!-- <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script> -->