<form action="<?php echo base_url(); ?>submitaction" method="post"><br><br>
    <button type="submit" name="btn" value="back" style="width:120px;"
                        class="btn bg-primary btn-lg  btn-sm waves-effect "><i class="fa fa-arrow-left"></i> Back</button>
    <button type="submit" name="btn" value="save" style="width:120px;"
                        class="btn bg-primary btn-lg  btn-sm waves-effect "><i class="fa fa-save"></i> Save</button><br>
    
    
    <input type="hidden" name="id" value="<?php echo $id; ?>"><br>
    <label for="menu">Menu Name :</label>
    <input type="text" class="form-control" name="menu" value="<?php echo $content['menu']; ?>"><br>
    <label for="long_desc">Content :</label>
    <textarea id='makeMeSummernote' class="form-control" name='long_desc' class="form-control" ><?php echo $content['content']; ?></textarea>
    
</form>
<script>
$('#makeMeSummernote').summernote({
    placeholder: 'Hello stand alone ui',
    tabsize: 2,
    height: 500,
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
    ]
});
// $('#abc').summernote({
//     placeholder: "{!!$policy->policy_details!!}",
//     tabsize: 2,
//     height: 100
// }).summernote('code', `{!!$policy->policy_details!!}`);

// $('#makeMeSummernote').summernote({
//     height:200,
// });
</script>