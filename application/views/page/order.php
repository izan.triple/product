<table width="80%">
    <tr bgcolor="litegrey">
        <th>No.</th>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Total</th>
    </tr>
    <?php
        $index = 0;
        foreach($product as $p){ 
            $index+= 1;
            ?>
        
    <tr>
        <td></td>
        <td align="left"><?php echo $p['name']; ?></td>
        <td>
            <select name="qtt[]" id="comboA" class="comboA" >
                <option value="1_<?php echo $p['id'];?>">1</option>
                <option value="2_<?php echo $p['id'];?>">2</option>
                <option value="3_<?php echo $p['id'];?>">3</option>
                <option value="4_<?php echo $p['id'];?>">4</option>
                <option value="5_<?php echo $p['id'];?>">5</option>
                <option value="6_<?php echo $p['id'];?>">6</option>
            </select>

        </td>
        <td><?php echo $p['price']; ?><input type="hidden" id="p_<?php echo $p['id'];?>" value="<?php echo $p['price']; ?>"></td>
        <td><input type="text" name="total[]" id="t_<?php echo $p['id'];?>" class="gtotalall"></td>
        
    </tr>
    
    <?php }
    
    ?>
    <tr><td colspan="4">Total (RM) </td><td><input type="text"  id="gtotal"></td></tr>
</table>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script >
    $('.comboA').on('change', function() {
        var text = this.value;
        var myArray = text.split("_");
        var total = myArray[0]*$('#p_'+myArray[1]).val();
        $('#t_'+myArray[1]).val(total);

        (function( $ ){
            $.fn.sum=function () {
                var sum=0;
                    $(this).each(function(index, element){
                        if($(element).val()!="")
                        sum += parseFloat($(element).val());
                    });
                return sum;
                }; 
            })( jQuery );
            $('#gtotal').val(($('.gtotalall').sum()));
    });
</script>

