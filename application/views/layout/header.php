<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="<?php echo (GAMMA::getSession('lang')); ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->lang->line('pageTitle'); ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png">

    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700"
        rel="stylesheet">
    <link type="text/css" rel="stylesheet"
        href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet"
        href="<?php echo base_url(); ?>assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css">
    <link type="text/css" rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" integrity="sha512-Velp0ebMKjcd9RiCoaHhLXkR1sFoCCWXNp6w4zj1hfMifYB5441C+sKeBl/T/Ka6NjBiRfBBQRaQq65ekYz3UQ==" crossorigin="anonymous" /> -->
    <link type="text/css" rel="stylesheet"
        href="https://bootstraplily.com/demo/gallery-ui/gallery-one/css/light-box.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/gammastyles.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>

<body>
    <header>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <!-- <div class="navbar-form navbar-left">
						<div class="input-group">
							<input id="txtsearch" name="txtsearch" type="text" class="form-control" placeholder="Search" >
							<div class="input-group-btn">
								<button id="btnsearch" class="btn btn-primary" type="button">
									<i class="glyphicon glyphicon-search"></i>
								</button>
							</div>
						</div>
					</div> -->
                <!-- <ul class="header-links pull-left">
						<li><a href="#"><i class="fa fa-phone"></i> +021-95-51-84</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>						
					</ul> -->
                <ul class="header-links pull-right">
                    <li>
                        <!-- <span id="en" class="lang <?php echo (GAMMA::getSession('lang') == 'en' ? 'active' : ''); ?>">BI</span>&nbsp;
							<span class="text-white">|</span>&nbsp; -->
                        <span id="ms"
                            class="lang <?php echo (GAMMA::getSession('lang') == 'ms' ? 'active' : ''); ?>">BM</span>
                    </li>
                    
                </ul>
            </div>
        </nav>
        <!-- MAIN HEADER -->
        <div id="header">
            <!-- container -->
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- LOGO -->
                    <div class="col-md-3">
                        <div class="header-logo">
                            <a href="/" class="logo">
                                <img src="<?php echo base_url('assets/img/icons/Logo.png'); ?>" alt="">
                            </a>
                        </div>
                    </div>
                    <!-- /LOGO -->
                    <!-- SEARCH BAR -->
                    <div class="col-md-6">
                        <div class="header-search">
                            <h3>PUSAT RAWATAN KEMPETERI NURFADIYAH</h3>
                            <!-- <form id="searchcat"> 
									<select id="category" name="category[]" class="input-select selectpicker" data-live-search="true" multiple data-selected-text-format="count > 1">
										<option value="" selected disabled><?php echo $this->lang->line('category_text') ?></option>
									</select>
									<input id="txtsearch" name="txtsearch" class="input" placeholder="<?php echo $this->lang->line('search_text') ?>">
									<button id="btnsearch" type="button" class="search-btn"><?php echo $this->lang->line('search_btn') ?></button>
								</form> -->
                        </div>
                    </div>
                    <!-- /SEARCH BAR -->

                    <!-- ACCOUNT -->
                    <div class="col-md-3 clearfix">
                        <div class="header-ctn">
                            <div class="row">
                                <div class="col-md-4 col-sm-6">
                                    <a href="/faq"
                                        class="<?php echo ($this->uri->uri_string == "faq" ? 'active' : ''); ?>">
                                        <i class="fa fa-question-circle"></i>
                                        <span><?php echo $this->lang->line('faq_title'); ?></span>
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <a href="/aboutus"
                                        class="<?php echo ($this->uri->uri_string == "aboutus" ? 'active' : ''); ?>">
                                        <i class="fa fa-info-circle"></i>
                                        <span><?php echo $this->lang->line('about_title'); ?></span>
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <a href="/contactus"
                                        class="<?php echo ($this->uri->uri_string == "contactus" ? 'active' : ''); ?>">
                                        <i class="fa fa-phone-square"></i>
                                        <span><?php echo $this->lang->line('contactus_title'); ?></span>
                                    </a>
                                </div>
                                <div class="col-sm-6 menu-toggle">
                                    <a href="#">
                                        <i class="fa fa-bars"></i>
                                        <span>Menu</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /ACCOUNT -->
                </div>
                <!-- row -->
            </div>
            <!-- container -->
            <hr>

        </div>
        <!-- /MAIN HEADER -->

    </header>
    <nav id="navigation">
        <div class="container">
            <div id="responsive-nav">
                <ul class="main-nav nav navbar-nav">
                    <li class="<?php echo ($this->uri->uri_string == "" ? 'active' : ''); ?>"><a
                            href="/"><?php echo $this->lang->line('home'); ?></a></li>

                    <!-- <li >
							<a href="" id="catmenu"  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->lang->line('category_text') ?> <span class="caret"></span></a>
							<ul id="cat" class="dropdown-menu"></ul>
						</li> -->
                    <li class="dropdown">
                        <a href="" id="platmenu" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false"><?php echo $this->lang->line('platform_text') ?>
                            <span class="caret"></span></a>
                        <ul id="plat" class="dropdown-menu">
                            <li class="navplat active"><a id="all"
                                    href="profil"><?php echo $this->lang->line('platform_text') ?></a></li>
                            <li class=""><a id="2" href="aboutus">Tentang Kami</a></li>
                            <li class="navplat "><a id="1" href="misi">Visi & Misi</a></li>
                            <li class="navplat "><a id="1" href="medic">Falsafah Perubatan</a></li>
                        </ul>
                    </li>
                    <!-- <li><a href="/#featuredApps"><?php echo $this->lang->line('feature_text'); ?></a></li> -->
                    <li><a
                            href="<?php echo base_url(); ?>activity"><?php echo $this->lang->line('top_text'); ?></a>
                    </li>
                    <li><a
                            href="<?php echo base_url(); ?>news"><?php echo $this->lang->line('recent_text'); ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>