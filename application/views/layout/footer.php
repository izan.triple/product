	<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

		<footer id="footer">
			<div class="section">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-3 col-xs-12">
							<div class="footer">
								<h3 class="footer-title"><?php echo $this->lang->line('link_title'); ?></h3>
								<ul class="footer-links">
									<li class="<?php echo ($this->uri->uri_string == "aboutus" ? 'active' : ''); ?>"><a href="/aboutus"><?php echo $this->lang->line('about_title'); ?></a></li>
									<!-- <li class="<?php echo ($this->uri->uri_string == "privacy" ? 'active' : ''); ?>"><a href="/privacy"><?php echo $this->lang->line('privacy_title'); ?></a></li>
									<li class="<?php echo ($this->uri->uri_string == "terms" ? 'active' : ''); ?>"><a href="/terms"><?php echo $this->lang->line('policy_title'); ?></a></li>
									<li class="<?php echo ($this->uri->uri_string == "copyright" ? 'active' : ''); ?>"><a href="/copyright"><?php echo $this->lang->line('copyright_title'); ?></a></li>
									<li class="<?php echo ($this->uri->uri_string == "disclaimer" ? 'active' : ''); ?>"><a href="/disclaimer"><?php echo $this->lang->line('disclaimer_title'); ?></a></li> -->
								</ul>
							</div>
						</div>					
						<div class="col-md-6 col-xs-12">
							<div class="footer">
								<!-- <div class="panel with-nav-tabs panel-primary">
									<div class="panel-heading">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#tab1primary" data-toggle="tab"><i class="fa fa-twitter"></i> Twitter</a></li>
											<li><a href="#tab2primary" data-toggle="tab"><i class="fa fa-facebook"></i> Facebook</a></li>
										</ul>
									</div>
									<div class="panel-body">
										<div class="tab-content">
											<div class="tab-pane fade in active" id="tab1primary">
												<script charset="utf-8" src="https://platform.twitter.com/widgets.js"></script>
												<div class="twitter-block">
													<a
														class="twitter-timeline"
														data-height="350"
														data-chrome="nofooter noscrollbar noheader"
														href="https://twitter.com/GAMMA_MY?ref_src=twsrc%5Etfw">
													</a>
												</div>
											</div>
											<div class="tab-pane fade" id="tab2primary">
												<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0"></script>
												<div 
													class="fb-page" 
													data-href="https://www.facebook.com/GAMMA.my" 
													data-tabs="timeline" 
													data-width="500" 
													data-height="350" 
													data-small-header="true" 
													data-adapt-container-width="true" 
													data-hide-cover="true" 
													data-show-facepile="true">
													<blockquote 
														cite="https://www.facebook.com/GAMMA.my" 
														class="fb-xfbml-parse-ignore">
														<a href="https://www.facebook.com/GAMMA.my">GAMMA</a>
													</blockquote>
												</div>
											</div>											
										</div>
									</div>
								</div> -->
								<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0"></script>
								<div 
									class="fb-page" 
									data-href="https://www.facebook.com/prknurfadiyah" 
									data-tabs="timeline" 
									data-width="900" 
									data-height="350" 
									data-small-header="true" 
									data-adapt-container-width="true" 
									data-hide-cover="true" 
									data-show-facepile="true">
									<blockquote 
										cite="https://www.facebook.com/prknurfadiyah" 
										class="fb-xfbml-parse-ignore">
										<a href="https://www.facebook.com/prknurfadiyah">PRKNurfadiyah</a>
									</blockquote>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-xs-12">
							<div class="footer">
								<h3 class="footer-title"><?php echo $this->lang->line('contactus_title'); ?></h3>								
								<div id="address"></div>
								<ul class="footer-links">
									<li id="phoneno"><i class="fa fa-phone"></i></li>
									<!-- <li id="email"><i class="fa fa-envelope-o"></i></li> -->
								</ul>
								<!-- <div class="social-links">
									<a href="https://www.twitter.com/GAMMA_my" target="_blank" class="twitter">
										<i class="fa fa-twitter"></i>
									</a>
									<a href="https://www.facebook.com/GAMMA.my" target="_blank" class="facebook">
										<i class="fa fa-facebook"></i>
									</a>
									
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="bottom-footer" class="section">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<span class="copyright">
								&copy; <?php echo $this->lang->line('copyright_text'); ?>
								<?php echo date('Y'); ?> 
								<?php echo $this->lang->line('gov_text'); ?><br>
								<?php echo $this->lang->line('browse_text'); ?>
							</span>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>

		<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script> -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js" integrity="sha512-Y2IiVZeaBwXG1wSV7f13plqlmFOx8MdjuHyYFVoYzhyRr3nH/NMDjTBSswijzADdNzMyWNetbLMfOpIPl6Cv9g==" crossorigin="anonymous"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.1/dist/sweetalert2.all.min.js"></script>
		<!-- <script type="text/javascript" src="assets/js/slick.min.js"></script> 
		<script type="text/javascript" src="assets/js/nouislider.min.js"></script> 
		<script type="text/javascript" src="assets/js/jquery.zoom.min.js"></script> -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>javascripts/pages/header.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>javascripts/require.js" data-main="/javascripts/controller.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>javascripts/pages/footer.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>javascripts/twitter.js"></script>
		
	</body>
</html>
