<?php 
    defined('BASEPATH') OR exit('No direct script access allowed'); 

    $data['title']   = isset($title) ? $title : '';
    $data['result']  = isset($result) ? $result : '';
    $data['content'] = isset($content) ? $content : 'blank_view'; 
    
    $this->load->view('layout/header', $data, FALSE);
    echo $content['content'];
    $this->load->view('layout/footer', $data, FALSE); 
?>