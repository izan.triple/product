<form action="../submitrnd" method="post"><br>
<input type="hidden" name="rndid" value="<?php echo $rndid; ?>">
    <p align="right"><button type="submit" style="width:110px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
            name='btn' value="submitprod"><i class="fa fa-save"></i> <b>Submit</b></button><br>
    <div class="body" style="height:auto;overflow:auto;background:#fff;font-size:12px;">
        <div style='height: auto;
   margin: 0em;
   overflow-y: auto;'>
            <div class="form-group">
                <label for="name">Name</label>
                    <select name="name" id="name" class="form-control" >
                    <?php
                        foreach($proddata as $p){
                            echo "<option value='P".$p["id"]."_".$p["name"]."'>".$p["name"]."</option>";
                        }
                    ?>
                    </select><br><br>
                <!-- <input type="text" class="form-control" name="name" aria-describedby="Name" placeholder="Enter name"> -->
            </div>
            <div class="form-group">
                <label for="contact">Description</label>
                <textarea id='makeMeSummernote' class="form-control" name='long_desc' class="form-control" ></textarea>
            </div>
        </div>
    </div>
</form>


<!-- #END# Exportable Table -->

<script>
var statid = "<?php echo $statid; ?>";
$('#statid').val(statid);

$('#makeMeSummernote').summernote({
    placeholder: 'Hello stand alone ui',
    tabsize: 2,
    height: 500,
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
    ]
});

</script>