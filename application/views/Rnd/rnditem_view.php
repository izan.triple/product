<!-- Exportable Table -->
<div class="row clearfix">
    <div class="body">
        <form method="POST" id="mform" action='<?php echo base_url().'rndaction/'; ?>'>
            <input type="hidden" name="rndid" value="<?php echo $rndid; ?>">
            <table width="100%">
                <tr>
                    <td colspan="2"><br>
                        
                        <?php if(GAMMA::getSession('role') !=3){ ?>
                            <button type="submit" style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                            name='btn' value="back"><i class="fa fa-arrow-left"></i> <b>Back</b></button>

                            <button type="submit" style="width:120px;" class="btn bg-blue btn-lg  btn-sm waves-effect"
                            name='btn' value="addprod"><i class="fa fa-plus"></i> <b>Add product</b></button>
                        <?php }?>
                    </td>
            </table>
        </form>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body" style="height:auto;overflow:auto;background:#fff;">
                <div class="table-responsive">
                    <div class="row">
                        <?php foreach ($homestay as $key => $value) { ?>
                            <div class="col-sm-3" onclick="location.href='<?php echo base_url().'rnditemdetail/'.$value['id'].'/'.$rndid; ?>';">
                                <div class="card">
                                    <div class="card-body">
                                    <i class="fa fa-home fa-6" aria-hidden="true" style="font-size:<?php if($setting[0]['iconsize']){echo $setting[0]['iconsize'];}?>px"></i>
                                    <h5 class="card-title"><?php echo $value['name'] ?></h5>
                                    <?php echo $value['description']; ?>
                                    <hr>
                                    <?php
                                        if(GAMMA::getSession('role') != 3){
                                            echo "<button onclick='location.href='".base_url()."'' value=edit name=btn><i class='fa fa-edit'></i></button>";
                                        }
                                    
                                    ?>
                                    
                                    
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                          
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->




</div>

<script>
$('#myTable').DataTable();



function delitm(id) {
    var del = confirm("Are you sure you want to delete this record?");
    if (del == true) {

        url = '<?php echo base_url();?>Report/deleteitm/' + id;

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "headers": {
                "cache-control": "no-cache"
            }
        }

        $.ajax(settings).done(function(response) {
            console.log(response);
            alert(response);
        });

    }
}
</script>


<!-- <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script> -->