﻿<!DOCTYPE html>
<html>
<?php
$setting = $this->session->userdata('setting');
$lock = ($setting[0]->lockduration)*60;
?>
<head>
    <meta http-equiv="refresh" content="<?php echo $lock; ?>;url=<?php echo base_url(); ?>login/logout2" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>DP - Master Merchant</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <link href="<?php echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Morris Chart Css-->
    <link href="<?php echo base_url(); ?>assets/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url(); ?>assets/css/themes/all-themes.css" rel="stylesheet" />


    <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/morrisjs/morris.js"></script>

    

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>assets/js/admin.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="<?php echo base_url(); ?>assets/js/demo.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>  -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>  

    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <style>
        .navbar{
        backgroud:white;
        }
</style>
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">

    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar" style="background-color:<?php if($setting[0]->navbarcolor){echo $setting[0]->navbarcolor;} ?>;">
        <div class="container-fluid">
            <div class="navbar-header">
            <?php
              $bank2 = $this->session->userdata('bank');
            ?>
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                </a><a href="javascript:void(0);" class="bars"></a>
                <a><img height="40" style='margin-top:15px;' src="<?php 
                   echo base_url().'images/'.$setting[0]->logo_path;  ?>"></img></a><br>
            </div>
            
            <h2 class="navbar-brand"><?php if($setting[0]->title){echo strtoupper($setting[0]->title);} ?></h2>
        </div>
        
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->

            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu mt-2" >
                <ul class="list">
                <li class="header"><i class="fa fa-circle" style="font-size:20px;color:green"></i> <label class="clabel-name"><?php echo $this->session->userdata('name'); ?></label>
                <br> <a href='<?php echo base_url().'userchangepwd' ?>'>[ Profile ]</a></li>
                
                <li ><div class="divider"><hr></div></li>
                  <!-- <li class="header">PAYMENT SOLUTION MANAGEMENT</li> -->
                  <?php
                  $menu = $this->session->userdata('menu');
                  $submenu = $this->session->userdata('submenu');

                  foreach ($menu as $m){
                    if($m->menuStatus=="P" && $m->menugroup=="D"){
                      ?>
                      <li>
                          <a href="javascript:void(0);" class="menu-toggle">
                              <i class="material-icons"><?php echo $m->menuicon; ?></i>
                              <span><?php echo $m->menuname; ?></span>
                          </a>
                        <ul class="ml-menu">
                          <?php
                          foreach($submenu as $sm){
                            if($sm->menuid == $mid){
                              ?>
                              <li><a href="<?php echo base_url().$sm->submenuurl; ?>"><?php echo $sm->submenuname; ?></a></li>
                              <?php

                            }

                          }
                          ?>

                        </ul>
                      </li>
                        <?php
                    }else{
                      if($m->menugroup=="D"){
                      ?>
                      <li>
                          <a href="<?php echo base_url().$m->menuurl; ?>">
                          
                          <i class="material-icons"><?php echo $m->menuicon; ?></i>
                              <span><?php echo $m->menuname; ?></span>
                          </a>
                      </li>
                      <?php
                    }
                  }
                  }
                   ?>

                    <?php
                    foreach ($menu as $m){
                      if($m->menuStatus=="P" && $m->menugroup=="M"){
                        $mid=$m->menuid;
                        ?>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle main<?php echo $m->menuid; ?>">
                            <i class="material-icons"><?php echo $m->menuicon; ?></i>
                                <span><?php echo $m->menuname; ?></span>
                            </a>
                          <ul class="ml-menu menu<?php echo $mid; ?>">
                            <?php
                            foreach($submenu as $sm){
                              if($sm->menuid == $mid){
                                ?>
                                <li><a href="<?php echo base_url().$sm->submenuurl; ?>"><i class="fa fa-bullseye fa-xs"  aria-hidden="true"></i>&nbsp;&nbsp;<?php echo $sm->submenuname; ?></a></li>
                                <?php

                              }

                            }
                            ?>

                          </ul>
                        </li>
                          <?php
                      }else{
                        if($m->menugroup=="M"){
                        ?>
                        <li>
                            <a href="<?php echo base_url().$m->menuurl; ?>">
                            <i class="material-icons"><?php echo $m->menuicon; ?></i>
                                <span><?php echo $m->menuname; ?></span>
                            </a>
                        </li>
                        <?php
                      }
                    }
                    }
                      ?>
                      
                    <li>
                        <a href="<?php echo base_url().'login/logout';?>">
                            <i class="material-icons col-red">logout</i>
                            <span>Logout</span>
                        </a>
                    </li>

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                Copyright &copy;<?php echo date('Y'); ?> | <?php echo COPYRIGHT; ?>
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>

        </aside>
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
        <i class="fa fa-dedent fa-2x" aria-hidden="true">  <label class="clabel-menu"><?php echo $menuname ?></label></i>
          <div class="row clearfix">
              <div class="col-xs-13 col-sm-13 col-md-13 col-lg-13">
                  <div class="card">
                  <?php 
                    date_default_timezone_set("Asia/Kuala_Lumpur");
                    echo '<div class="pull-right pt-10"><label style="right:10px;">'.date('Y-m-d H:m:s'). '</label></div>';?>
                          <?php
                            $this->load->view($main_content);
                           ?>

                  </div>
              </div>
          </div>


        </div>
    </section>

    <!-- Jquery Core Js -->
    
    <!-- <script src="<?php echo base_url(); ?>assets/js/pages/tables/jquery-datatable.js"></script> -->
</body>

<script>


if(key){
    
    // $(".main<?php echo $menuid ?>").addClass("toggled");
    $(".menu<?php echo $menuid ?>").css("display","block");
    // alert('<?php echo $menuid ?>');
}
</script>

</html>
