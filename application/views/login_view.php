<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>DPS - Merchant</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/default-css.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/responsive.css">
    <link rel="icon" href="<?php echo base_url(); ?>logo/dp.png" type="image/gif" sizes="16x16">

</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area login-s2">
        <div class="container">
            <label style="display:block; width:x; height:y; text-align:right;margin-top: 5%;;margin-right: 5%;"><b><?php echo date('Y-m-d H:m:s'); ?></b></label>
            <div class="login-box ptb--60">
                <form action="<?php echo base_url().'auth'; ?>" method="post">
                    <div class="login-form-head">
                        <img src="<?php echo base_url().'images/logo-DPS.png'; ?>"></img><br><br>
                        <b><span style='background-color:red;color:white' class='badge badge-success'><?php echo $this->session->flashdata('msg');?></span></b>
                        <!-- <b><label style="color:red;"><?php //echo $this->session->flashdata('msg');?></label><b> -->
                    </div>
                    <div class="login-form-body">
                        <div class="form-gp">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" id="email" name="email" required>
                            <i class="ti-email"></i>
                            <div class="text-danger"></div>
                        </div>
                        <div class="form-gp">
                            <label for="exampleInputPassword1">Kata Laluan</label>
                            <input type="password" id="password" name="password" required>
                            <i class="ti-lock"></i>
                            <div class="text-danger"></div>
                        </div>
                        <div class="row mb-4 rmber-area">
                            <div class="col-6">

                            </div>
                            <!-- <div class="col-6 text-right">
                                <a href="#">Forgot Password?</a>
                            </div> -->
                        </div>
                        <div class="submit-btn-area">
                            <button id="form_submit" type="submit">Log Masuk <i class="ti-arrow-right"></i></button>
                        </div>
                        <center>Copyright &copy;<?php echo YEAR; ?> | <?php echo COPYRIGHT; ?><br><br>
                        <label style="color:blue;">This site is best viewed in Google Chrome, Firefox and Internet Explorer (version 8 and above)</label>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- login area end -->

    <!-- jquery latest version -->
    <script src="<?php echo base_url() ?>assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <!-- <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script> -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
    <!-- <script src="<?php //echo base_url() ?>assets/js/metisMenu.min.js"></script> -->
    <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.slicknav.min.js"></script>

    <!-- others plugins -->
    <script src="<?php echo base_url() ?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url() ?>assets/js/scripts.js"></script>
</body>

</html>
