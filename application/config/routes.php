<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']    = 'Login';
$route['home']           = 'Login';
$route['alogin']         = 'Login/login';
$route['index.php/auth']        = 'Login/auth';
$route['auth']          = 'Login/auth';
$route['404_override']  = '';
$route['translate_uri_dashes']  = FALSE;
$route['dashboard']     = 'page';
$route['feedback']     = 'page/feedback';
$route['menu']          = 'page/menuIndex';
$route['resetsession']  = 'merchantInfoIndexLogin/resetsession';
$route['maintenance']   = 'page/maintenanceIndex';

//content
$route['content']           = 'Content';
$route['submitaction']      = 'Content/content';
$route['editcontent/(:num)']= 'Content/editcontent/$1';
$route['page/(:any)']       = 'Content/page/$1';
$route['contentaction']     = 'Content/contentaction';
$route['submitcontent']     = 'Content/contentaction';

//rnd
$route['rnd']                   = 'Rnd';
$route['rndaction']             = 'Rnd/rndaction';
$route['submitrnd']             = 'Rnd/rndaction';
$route['submitproduct']         = 'Rnd/rndaction';
$route['listrnd/(:num)']        = 'Rnd/listrnd/$1';
$route['rnditemdetail/(:num)/(:num)']  = 'Rnd/rnditemdetail/$1/$2';

//order
$route['order']                 = 'Order';
$route['complete/(:any)']       = 'order/completeorder/$1';

//product display
$route['proddisp']              = 'product';
$route['editprod/(:num)']       = 'product/updateprod/$1';
$route['prodaction']            = 'product/prodaction/$1';

//r&d stock
$route['rndstock/(:num)/(:num)']  = 'RndStock/listrndstock/$1/$2';
$route['stockaction']             = 'RndStock/stockaction';
$route['updaterndstock']          = 'RndStock/stockaction';

//r&d cost
$route['rndcost/(:num)/(:num)']   = 'RndCost/listrndcost/$1/$2';
$route['costaction']              = 'RndCost/costaction';


//r&d sale
$route['sale/(:num)/(:num)']      = 'Sale/listsale/$1/$2';
$route['submitsale']              = 'Sale/saleaction';


//set password

$route['setpass']           = 'Login/setpass';
$route['changepwd/(:num)']  = 'User/changeIndex/$1';
$route['userchangepwd']     = 'User/changeIndex/0';

//User route--------------
$route['role']              = 'User/roleIndex';
$route['roleinfo']          = 'User/roleInfoIndex';
$route['roleinfo/(:num)']   = 'User/roleEditIndex/$1';
$route['user']          = 'User/userIndex';
$route['gsetting']      = 'User/SettingIndex';
$route['userinfo/(:num)']   = 'User/userEditIndex/$1';
$route['editinfo/(:num)']   = 'User/userEditIndex/$1';
$route['personnelinfo/(:num)'] = 'User/userDetailIndex/$1';
$route['membermngt']    = 'User/personnelIndex';
$route['submituser']    = 'User/userdata';


//homestay-------
$route['homestay']                      = 'Homestay';
$route['mhomestay']                     = 'Homestay';
$route['listhomestay/(:num)']           = 'Homestay/listhomestay/$1';
$route['homestayaction/(:num)']         = 'Homestay/homestayaction/$1';
$route['homestaydetail/(:num)/(:num)']  = 'Homestay/homestaydetail/$1/$2';


//appointment------
$route['appointment']                   = 'Appointment';
$route['listappment/(:any)']            = 'Appointment/listappment/$1';


//homestay_user-------
$route['homestayuser/(:num)/(:num)']   = 'HomestayUser/listhomestayuser/$1/$2';
$route['submithuser']                  = 'HomestayUser/submithuser';


//homestay_maintenance-------
$route['homestaymaintenance/(:num)/(:num)']  = 'HomestayMaintenance/listhomestaymaintenance/$1/$2';
$route['maintenanceaction']                  = 'HomestayMaintenance/maintenanceaction';


//homestay_expense-------
$route['homestayexpense/(:num)/(:num)']  = 'HomestayExpense/listhomestayexpense/$1/$2';
$route['expenseaction']                  = 'HomestayExpense/expenseaction';


//owner-------
$route['owner']             = 'Owner';
$route['owneraction']       = 'Owner/owneraction';
$route['submitowner']       = 'Owner/owneraction';
$route['editowner/(:num)']  = 'Owner/editowner/$1';



//booking-----
$route['submitbooking']     = 'Booking/bookingaction';

//payment-----
$route['listpayment/(:num)/(:num)'] = 'Payment/listpayment/$1/$2';
$route['submitpayment']             = 'Payment/addpayment';

//maintenance-----
$route['grabdata/(:any)']           = 'Api/dataimport/$1';
$route['updatedata/(:any)/(:num)']  = 'Api/updatedata/$1/$2';


//report-------
$route['report']    = 'Report';
$route['generate']  = 'Report/generate';


//Cronjob-------
$route['inactiveuser'] = 'Api/inactiveuser';


?>
