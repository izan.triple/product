<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter PDF Library
 *
 * Generate PDF in CodeIgniter applications.
 *
 * @package            CodeIgniter
 * @subpackage        Libraries
 * @category        Libraries
 * @author            CodexWorld
 * @license            https://www.codexworld.com/license/
 * @link            https://www.codexworld.com
 */

// reference the Dompdf namespace
use Dompdf\Dompdf;

class Pdf
{
    public function __construct(){

        // include autoloader
        require_once 'assets2/dompdf/autoload.inc.php';

        // instantiate and use the dompdf class
        // $pdf = new DOMPDF();
        $pdf = new Dompdf(['isHtml5ParserEnabled' => true]);

        $CI =& get_instance();
        $CI->dompdf = $pdf;

    }
}
?>
