<?php defined('BASEPATH') or die('Unauthorized Access');
class GAMMA {

	public static $CI;
	public static $my;
	public static $language;
	public static $user;
	public static $browser;
	public static $path;
	public static $ip;

	function __construct(){
		date_default_timezone_set('Asia/Kuala_Lumpur');
		GAMMA::$CI 		=& get_instance();

	}

	// public static function loadLanguage(){
	// 	// Default language
	// 	$lang = GAMMA::getSession( 'lang' );
	// 	if( $lang == 'en' ){
	// 		GAMMA::$language 	= 'en';
	// 		self::$CI->lang->load('en_lang', 'english');
	// 	} else {
	// 		self::$CI->lang->load('bm_lang', 'bahasa');
	// 	}
	// }

	
	public static function getTemplate( $templatePath = '', $param = array(), $return = false )	{
		$template = self::$CI->load->view( $templatePath, $param, true );
		if($return)	{
			return $template;
		}
	}

	public static function loadTemplate( $templatePath = '', $param = array(), $return = false ){
		$htmlPath 		= GAMMA::$path.'/html';
		$headPath 		= GAMMA::$path.'/head';
		$dependencyPath = GAMMA::$path.'/dependency';
		$bodyPath 		= GAMMA::$path.'/body';
		$footerPath 	= GAMMA::$path.'/footer';
		
		// Prepare data for view template
		$viewName	= explode('/', $templatePath);
		$viewName	= array_pop( $viewName );

		// Check css file exist
		$layoutCssPath		= 'css/'.GAMMA::$path.'/'.$param['layoutName'].'.css';
		$layoutCssFile		= file_exists( $layoutCssPath );
		$param['layoutCss']	= '';
		if( $layoutCssFile ){
			$param['layoutCss'] = site_url( $layoutCssPath );
		}

		// Load the css at head
		$param['view']	= $viewName;
		$head			= self::$CI->load->view( $headPath, $param, true );

		// $viewHelper			= GAMMA::getHelpers('view');
		// $data				= $viewHelper->$viewName();
		// $param[$viewName]	= $data;

		// Template load inside body.
		$template = self::$CI->load->view( $templatePath, $param, true );

		// Javascript load right before </body> tag.
		$dependency = self::$CI->load->view( $dependencyPath, $param, true );

		$bodyData = array();
		$bodyData['template'] 	= $template;
		$bodyData['dependency'] = $dependency;

		$body 		= self::$CI->load->view( $bodyPath, $bodyData, true );
		$footer 	= self::$CI->load->view( $footerPath, '', true );

		// Load everything in html.
		$html = self::$CI->load->view( $htmlPath, array( 'head' => $head, 'body' => $body, 'footer' => $footer ), $return );

		if($return){
			return $html;
		}
	}

	public static function getModel( $name = '' ){
		$name = $name . '_model';
		self::$CI->load->model( $name );
		return new $name;
	}

	public static function getHelper( $name = '' )	{
		$name = $name . '_helper';
		self::$CI->load->helper( $name );
		return new $name;
	}

	public static function getLibrary( $name = '' )	{
		self::$CI->load->library( $name );
		return new $name;
	}

	public static function setSession( $name = '', $value = array() ){
		if( empty($name) && !empty($value) ){
			foreach( $value as $key => $data ){
				self::$CI->session->set_userdata( $key, $data );
			}
		} else if( !empty($name) && !empty($value) ){
			self::$CI->session->set_userdata( $name, $value );
		}
	}

	public static function getSession( $name = '' )	{
		if( empty($name) ){
			return self::$CI->session->all_userdata();
		}
		return self::$CI->session->userdata( $name );
	}

	public static function unsetSession( $name = '' ){
		if( !empty($name) ){
			self::$CI->session->unset_userdata( $name );
			return true;
		}

		// Unset all
		self::$CI->session->unset_userdata( 'user' );
		self::$CI->session->unset_userdata( 'usergroup' );
		self::$CI->session->unset_userdata( 'timezone' );
		self::$CI->session->sess_destroy();
		return true;
	}

	public static function destorySession( ){
		self::$CI->session->sess_destroy();
		return true;
	}
}
