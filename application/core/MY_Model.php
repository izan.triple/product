<?php defined('BASEPATH') or die('Unauthorized Access');

class MY_Model extends CI_Model {

	protected static $tablename = '';
	
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function add( $data = array(), $returnId = false ){
		
		if( !empty($data) ){
			$this->db->insert( $this::$tablename, $data );
			return $this->db->insert_id();
		}
		return false;
	}

	public function update( $data = array(), $pointer = array() ){
		if( empty($data) || empty($pointer) ){
			return false;
		}

		$this->db->update( $this::$tablename, $data, $pointer );
		return true;
	}

	public function deleteOne( $data = array() )	{
		return $this->db->delete( $this::$tablename, $data );
	}


	public function delete( $pointer = array() )	{
		if( empty($pointer) )		{
			return false;
		}
		$this->db->delete( $this::$tablename, $pointer );
		return true;
	}

	public function selectOne( $data = array() ){
		if( !empty($data) ){
			$this->db->where($data);
			return $this->db->get($this::$tablename)->row_array();
		}
	}	

	public function selectAll( $data = array() ){
		if( !empty($data) ){
			$this->db->where($data);
			// if( !empty($order) ){
			// 	$this->db->order_by($order, $sort);
			// }
			return $this->db->get($this::$tablename)->result_array();
		}
	}

	public function all(){
			return $this->db->get($this::$tablename)->result_array();
	}

	
}
