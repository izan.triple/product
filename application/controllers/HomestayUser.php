<?php
class HomestayUser extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function listhomestayuser($hid,$oid){
    $homestayuserModel  = GAMMA::getModel('homestayuser');
    $userModel          = GAMMA::getModel('user');
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');
    
    $key                ="report";
    $data['homestayid'] = $hid;
    $data['ownerid']    = $oid;
    $data['setting']    = $siteModel->all();
    $roleid             =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'User Homestay Information';
    $data['user']       = $userModel->all();
    $data['homestayuser'] = $homestayuserModel->listuserhomestay($hid);
    $data['main_content'] = 'Homestay/homestayuser_view';
    $this->load->view('mainPage',$data);
  }

  public function submithuser(){
    $homestayuserModel    = GAMMA::getModel('homestayuser');
    $oid                  = $this->input->post('ownerid');
    $hid                  = $this->input->post('homestayid');
    $uid                  = $this->input->post('userid');
    $checkuser            = $homestayuserModel->selectUser($hid,$oid,$uid);
    
    if(empty($checkuser)){
      
      $datadb['homestayid'] = $hid;
      $datadb['ownerid']    = $oid;
      $datadb['userid']     = $uid;
      $result               = $homestayuserModel->add($datadb);   
      if($result > 0){
        redirect('homestayuser/'.$hid.'/'.$oid);
      }
    }else{
      echo "user exist";
    }
    
      
  }
  





}
