<?php
class System2 extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('menu_model');
    $this->load->model('dashboard_model');
    $this->load->model('system_model');
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }

//1. INDEX DATA'-------------------------------------------------------------------------
public function areaIndex()
{
	$key="system";
  $mid = $this->menu_model->getid($key);
  $data['setting'] = $this->menu_model->getsitesetting();
  $roleid= $this->session->userdata('role');
  $areaid= $this->session->userdata('area');
  $data['menuid'] = $mid;
  $data['access'] = $this->menu_model->get_access($roleid,$mid);
  $data['menu'] = $this->menu_model->getMenu($roleid);
  $data['menutitle'] = 'Role Information';
  $data['submenu'] = $this->menu_model->getSubMenu($roleid);
  $data['siteinfo'] = $this->menu_model->getsitesetting();

	$data['listarea'] = $this->system_model->listarea($areaid);
	$data['main_content'] = 'System/area_view';
	$this->load->view('mainPage',$data);
}


public function ownerIndex()
{
	$key="system";
  $mid = $this->menu_model->getid($key);
  $data['setting'] = $this->menu_model->getsitesetting();
  $roleid= $this->session->userdata('role');
  $data['menuid'] = $mid;
  $data['access'] = $this->menu_model->get_access($roleid,$mid);
  $data['menu'] = $this->menu_model->getMenu($roleid);
  $data['menutitle'] = 'Role Information';
  $data['submenu'] = $this->menu_model->getSubMenu($roleid);
  $data['siteinfo'] = $this->menu_model->getsitesetting();

	$data['listowner'] = $this->system_model->listowner();
	$data['main_content'] = 'System/owner_view';
	$this->load->view('mainPage',$data);
}


public function stateIndex()
{
	$key="system";
  $mid = $this->menu_model->getid($key);
  $data['setting'] = $this->menu_model->getsitesetting();
  $roleid= $this->session->userdata('role');
  $data['menuid'] = $mid;
  $data['access'] = $this->menu_model->get_access($roleid,$mid);
  $data['menu'] = $this->menu_model->getMenu($roleid);
  $data['menutitle'] = 'Role Information';
  $data['submenu'] = $this->menu_model->getSubMenu($roleid);
  $data['siteinfo'] = $this->menu_model->getsitesetting();

	$data['liststate'] = $this->system_model->liststate();
	$data['main_content'] = 'System/state_view';
	$this->load->view('mainPage',$data);
}


public function systemaction()
	{
	$btn = $this->input->post('btn');
  $key="system";
  $mid = $this->menu_model->getid($key);
  $data['setting'] = $this->menu_model->getsitesetting();
  $roleid= $this->session->userdata('role');
  $areaid= $this->session->userdata('area');
  $data['menuid'] = $mid;
  $data['areaid'] = $areaid;
  $data['access'] = $this->menu_model->get_access($roleid,$mid);
  $data['menu'] = $this->menu_model->getMenu($roleid);
  $data['menutitle'] = 'Add System Information';
  $data['submenu'] = $this->menu_model->getSubMenu($roleid);
  $data['siteinfo'] = $this->menu_model->getsitesetting();
  $data['form'] = $btn;
  $data['country'] = $this->system_model->listcountry();
  $data['state'] = $this->system_model->liststate();
  $data['owner'] = $this->system_model->listowner();
  $data['main_content'] = 'System/addsystemform';
	$this->load->view('mainPage',$data);
}

//2. GET DATA'-------------------------------------------------------------------------

//3. SAVE DATA'-------------------------------------------------------------------------

public function saveinfo($ind)
{
if($ind == 'state'){
	$datadb['stateName'] = $this->input->post('statename');
	$datadb['lat'] = $this->input->post('statelat');
	$datadb['long'] = $this->input->post('statelong');
	$datadb['honorific'] = $this->input->post('honorific');
	$datadb['createdon'] = date('Y-m-d h:i:s');
  $datadb['fk_countryid'] = $this->input->post('statecountry');
	$result = $this->system_model->insert_state($datadb);
	echo $result;
}
else if($ind == 'area'){
  $datadb['fk_ownerid'] = $this->input->post('fk_ownerid');
  $datadb['buildingid'] = $this->input->post('buildingid');
  $datadb['areaname'] = $this->input->post('areaname');
  $datadb['areaaddr'] = $this->input->post('areaaddr');
  $datadb['areagraceday'] = $this->input->post('areagraceday');
  $datadb['areaxpiry'] = $this->input->post('areaxpiry');
  $datadb3['dateCreated'] = date('Y-m-d h:i:s');
	$datadb3['createdby'] = $this->session->userdata('username');
	$result = $this->system_model->insert_area($datadb);
	echo $result;
}
else if($ind == 'owner'){
	$datadb3['ownername'] = $this->input->post('ownername');
  $datadb3['owneraddr1'] = $this->input->post('owneraddr1');
  $datadb3['owneraddr2'] = $this->input->post('owneraddr2');
  $datadb3['ownerpostcode'] = $this->input->post('ownerpostcode');
  $datadb3['ownercity'] = $this->input->post('ownercity');
  $datadb3['fk_stateId'] = $this->input->post('fk_stateId');
  $datadb3['fk_countryId'] = $this->input->post('fk_countryId');
  $datadb3['ownertel1'] = $this->input->post('ownertel1');
  $datadb3['ownertel2'] = $this->input->post('ownertel2');
  $datadb3['ownerfax1'] = $this->input->post('ownerfax1');
  $datadb3['ownerfax2'] = $this->input->post('ownerfax2');
  $datadb3['owneremail'] = $this->input->post('owneremail');
	$datadb3['dateCreated'] = date('Y-m-d h:i:s');
	$datadb3['createdby'] = $this->session->userdata('username');
	$result = $this->system_model->insert_owner($datadb3);
	if($result){
		echo $result;
		return;
	}

}else{
	$data['main_content'] = 'User/role_view';
}
}

//4. DELETE DATA--------------------------------------------------------


}
