<?php
class Api extends CI_Controller
{
	 /**
	  * __construct function.
	  *
	  * @access public
	  * @return void
	  */
	 public function __construct() {
	 	parent::__construct();
		$this->load->model('setting_model');
		$this->load->model('menu_model');
		$this->load->model('user_model');
		$this->load->model('login_model');
	 }

	 public function inactiveuser()
	{
		$setting = $this->menu_model->getsitesetting();
		// echo $setting[0]->inactiveday;exit;
		$dataincative = $this->setting_model->inactive($setting[0]->inactiveday);

		foreach($dataincative as $d){
			$datadb['userstatus'] = 'Inactive';
			$result = $this->login_model->update_ustatus($datadb,$d->user_id);
			$total=0;
			if($result){
				$total ++;
				$datadb2['datetime'] = date('Y-m-d h:i:s');
				$datadb2['activity'] = 'Update inactive user user '.$d->username;
				$datadb2['createdby'] = 'System';	
				$this->user_model->user_trail($datadb2);
			}
		}
		
		$data = new stdClass();
		if(count($dataincative) > 0){
			$data->code = 0;
			$data->msg = $total . ' updated.';
		}else{
			$data->code = 1;
			$data->msg = "0 updated";
		}
		echo json_encode($data);
		// $this->output->set_content_type('application/json');
		// $data = new stdClass();
		// if(count($dataincative) >= 0){
		// 	$data->code = 0;
		// 	$data->data = $dataincative;
		// }else{
		// 	$data->code = 1;
		// 	$data->msg = "Unable to display data.";
		// }
		// echo json_encode($data);
	}


	public function dataimport($ind)
	{
		if($ind == "expense"){
			$homestayexpenseModel  = GAMMA::getModel('homestayexpense');
			$datas	= $homestayexpenseModel->grepdata();
		}else if($ind == "maintenance"){
			$homestaymaintenanceModel  = GAMMA::getModel('homestaymaintenance');
			$datas	= $homestaymaintenanceModel->grepdata();
		}else if($ind == "booking"){
			$bookingModel     = GAMMA::getModel('booking'); 
			$datas	= $bookingModel->grepdata();
		}else if($ind == "payment"){
			$paymentModel     = GAMMA::getModel('payment'); 
			$datas	= $paymentModel->grepdata();
		}
		
		
		$data = new stdClass();
		if(count($datas) > 0){
			$data->code = 0;
			$data->data = $datas;
		}else{
			$data->code = 1;
			$data->msg = "0 updated";
		}
		echo json_encode($data);
		// $this->output->set_content_type('application/json');
		// $data = new stdClass();
		// if(count($dataincative) >= 0){
		// 	$data->code = 0;
		// 	$data->data = $dataincative;
		// }else{
		// 	$data->code = 1;
		// 	$data->msg = "Unable to display data.";
		// }
		// echo json_encode($data);
	}

	public function updatedata($ind,$id)
	{
		// echo "here";exit;
		if($ind == "expense"){
			$homestayexpenseModel  	= GAMMA::getModel('homestayexpense');
			$datadb2['sync']    	= 0;
			$result               	= $homestayexpenseModel->update($datadb2,array('id' => $id));
		}else if($ind == "maintenance"){
			$homestaymaintenanceModel  = GAMMA::getModel('homestaymaintenance');
			$datadb2['sync']    	= 0;
			$result               	= $homestaymaintenanceModel->update($datadb2,array('id' => $id));
		}else if($ind == "booking"){
			$bookingModel  			= GAMMA::getModel('booking');
			$datadb2['sync']    	= 0;
			$result               	= $bookingModel->update($datadb2,array('id' => $id));
		}else if($ind == "payment"){
			$paymentModel  			= GAMMA::getModel('payment');
			$datadb2['sync']    	= 0;
			$result               	= $paymentModel->update($datadb2,array('id' => $id));
		}
		
		
		$data = new stdClass();
		if(count($result) > 0){
			$data->code = 0;
			$data->data = $result;
		}else{
			$data->code = 1;
			$data->msg = "0 updated";
		}
		echo json_encode($data);
		// $this->output->set_content_type('application/json');
		// $data = new stdClass();
		// if(count($dataincative) >= 0){
		// 	$data->code = 0;
		// 	$data->data = $dataincative;
		// }else{
		// 	$data->code = 1;
		// 	$data->msg = "Unable to display data.";
		// }
		// echo json_encode($data);
	}


	




}
