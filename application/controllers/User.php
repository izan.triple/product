<?php
class User extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('menu_model');
    $this->load->model('dashboard_model');
	$this->load->model('user_model');
	$this->load->model('login_model');
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }

  public function userDetailIndex($id)
  {
    $key="personnel";
    $mid = $this->menu_model->getid($key);
    $data['setting'] = $this->menu_model->getsitesetting();
    $roleid= $this->session->userdata('role');
    $areaid= $this->session->userdata('area');
    $data['menuid'] = $mid;
    $data['access'] = $this->menu_model->get_access($roleid,$mid);
    $data['menu'] = $this->menu_model->getMenu($roleid);
    $data['menutitle'] = 'Role Information';
    $data['submenu'] = $this->menu_model->getSubMenu($roleid);
    $data['siteinfo'] = $this->menu_model->getsitesetting();



    $pinfo = $this->user_model->getpinfo($id);
    $tid = $pinfo[0]->fk_tenantid;
    $data['pinfo'] = $pinfo;
    $data['tid'] = $tid;
    $data['allarea'] = $this->user_model->getarea($areaid,$tid);
    $data['listarea'] = $this->user_model->spgetarea($id,$areaid);
    $data['liststate'] = $this->user_model->getstate();
    $data['psinfo'] = $this->user_model->getpsinfo($id,$areaid,$tid);


    $data['cpinfo'] = $this->user_model->getcpinfo2($id);
    $data['listtenant'] = $this->user_model->get_tenant($areaid);
    // $data['sinfo'] = $this->user_model->getspinfo();
    $data['listmedia'] = $this->user_model->getmedia();
    // $data['cinfo'] = $this->user_model->getcpinfo($areaid);
    $data['pid'] = $id;
    $data['main_content'] = 'User/edituserform';
    $this->load->view('mainPage',$data);
  }

public function roleIndex()
{
	$key="admin";
	$mid = $this->menu_model->getid($key);
	$data['setting'] = $this->menu_model->getsitesetting();
	$roleid= $this->session->userdata('role');
	$data['menuid'] = $mid;
	$data['key'] = $key;
	$data['menuname'] = 'Role Information';
	$data['access'] = $this->menu_model->get_access($roleid,$mid);
	$data['menu'] = $this->menu_model->getMenu($roleid);
	$data['menutitle'] = 'Role Information';
	$data['submenu'] = $this->menu_model->getSubMenu($roleid);
	$data['siteinfo'] = $this->menu_model->getsitesetting();
	$data['listrole'] = $this->user_model->getrole();
	$data['main_content'] = 'User/role_view';
	$this->load->view('mainPage',$data);
}


  public function roleInfoIndex()
{
	$key="admin";
	$mid = $this->menu_model->getid($key);
	$data['setting'] = $this->menu_model->getsitesetting();
	$roleid= $this->session->userdata('role');
	$data['rid'] = $roleid;
	$data['menuid'] = $mid;
	$data['menuname'] = "Role";
	$data['access'] = $this->menu_model->get_access($roleid,$mid);
	$data['menu'] = $this->menu_model->getMenu($roleid);
	$data['menutitle'] = 'Change Role';
	$data['submenu'] = $this->menu_model->getSubMenu($roleid);
	$data['submenu'] = $this->menu_model->get_submenu2();
	$data['listmenu'] = $this->menu_model->get_allmenu();
	$data['main_content'] = 'User/addinfo_view';
	$this->load->view('mainPage',$data);
}


public function  roleEditIndex($id){
	$key="admin";
	$mid = $this->menu_model->getid($key);
	$data['setting'] = $this->menu_model->getsitesetting();
	$roleid= $this->session->userdata('role');
	$data['form'] = 'role';
	$data['rid'] = $id;
	$data['menuname'] = 'Change Role';
	$data['menuid'] = $mid;
	$data['access'] = $this->menu_model->get_access($roleid,$mid);
	$data['menu'] = $this->menu_model->getMenu($roleid);
	$data['menutitle'] = 'Change Role';
	$data['submenu'] = $this->menu_model->getSubMenu($roleid);
	$data['roleinfo'] = $this->user_model->getroleinfo($id);
	$data['listaccess'] = $this->user_model->getroleinfo2($id,1);
	$data['listsubaccess'] = $this->user_model->getroleinfo2($id,2);

	$data['submenu'] = $this->menu_model->get_submenu2();
	$data['listmenu'] = $this->menu_model->get_allmenu();

	$data['main_content'] = 'User/roleinfo_view';
	$this->load->view('mainPage',$data);
}


public function userIndex()
{
	$key='admin';
	$mid = $this->menu_model->getid($key);
	$roleid= $this->session->userdata('role');
	$areaid= $this->session->userdata('area');
	$data['rid'] = $roleid;
	$data['menuid'] = $mid;
	$data['key'] = $key;
	$data['menuname'] = 'User';
	$data['access'] = $this->menu_model->get_access($roleid,$mid);
	$data['menutitle'] = 'Change Role';
	$data['listuser'] = $this->user_model->getuser($areaid);
	$data['main_content'] = 'User/user_view';
	$this->load->view('mainPage',$data);
}

public function SettingIndex()
{
	$key='admin';
	$mid = $this->menu_model->getid($key);
	$roleid= $this->session->userdata('role');
	$areaid= $this->session->userdata('area');
	$setting= $this->login_model->listsetting();
	// print_r($setting);exit;
	$data['rid'] = $roleid;
	$data['menuid'] = $mid;
	$data['key'] = $key;
	$data['menuname'] = 'General & Account Setting';
	$data['setting'] = $setting;
	$data['access'] = $this->menu_model->get_access($roleid,$mid);
	$data['menutitle'] = 'Change Role';
	$data['listuser'] = $this->user_model->getuser($areaid);
	$data['main_content'] = 'User/setting_view';
	$this->load->view('mainPage',$data);
}


public function userEditIndex($id)
{
	$key="personnel";
	$mid = $this->menu_model->getid($key);
	$roleid= $this->session->userdata('role');
	$areaid= $this->session->userdata('area');
	$data['menuname'] = 'User';
	$data['rid'] = $roleid;
	$data['menuid'] = $mid;
	$data['access'] = $this->menu_model->get_access($roleid,$mid);
	$data['menutitle'] = 'Change Role';
	$data['userinfo'] = $this->user_model->getuserinfo($areaid,$id);
	$data['listrole'] = $this->user_model->getrole();
	$data['form'] = 'euser';
	$data['main_content'] = 'User/edituser_view';
	$this->load->view('mainPage',$data);
}

public function changePasswd($id)
{

	$btn = $this->input->post('btn');

	if($btn == 'back'){
		redirect('userinfo/'.$id);
	}else if($btn == 'saveuser'){
		$pwdori = $this->input->post('passwordold');
		$pwdnewori =$this->input->post('passwordnew');

		$pwdold = md5($this->input->post('passwordold').KEY);
		$pwdnew = md5($this->input->post('passwordnew').KEY);
		$pwdnew2 = md5($this->input->post('passwordnew2').KEY);
		$uid= $this->session->userdata('id');
		$areaid = 0;
		$username= $this->session->userdata('username');
		$usrinfo = $this->user_model->getuserinfo($areaid,$uid);
		$passchangedate= $this->session->userdata('passchangedate');
		$setting= $this->session->userdata('setting');
		$passexpirymin= $setting[0]->passexpirymin;
		$limit= $setting[0]->passhistory;

		$date1=date_create(date('Y-m-d'));
		$date2=date_create($passchangedate);
		$diff=date_diff($date1,$date2);
		$daydiff =  $diff->format("%a");

		

		if( $pwdnew == $pwdnew2){
			
			if(preg_match("#[0-9]+#",$pwdnewori) == 0) {
				$msg = "Sorry, your Password Must combine At Least 1 Number or 1 Character! !";
				if($pwdori == "xxx"){
					echo $this->session->set_flashdata('msg2',$msg);
					redirect('setpass');
				}else{
					echo $this->session->set_flashdata('msg',$msg);
					redirect('userchangepwd');
				}
				exit;
			}

			if (preg_match('#[A-Za-z]+#',$pwdnewori) == 0) {
				$msg = "Sorry, your password MUST combine at least 1 Number or 1 Letter! !";
				if($pwdori == "xxx"){
					echo $this->session->set_flashdata('msg2',$msg);
					redirect('setpass');
				}else{
					echo $this->session->set_flashdata('msg',$msg);
					redirect('userchangepwd');
				}
				exit;
			}
			

			if($passexpirymin > $daydiff){
				$msg = 'You are not allowed to change the password due to the policy.Please Contact Administrator !';
				if($pwdori == "xxx"){
					echo $this->session->set_flashdata('msg2',$msg);
					redirect('setpass');
				}else{
					echo $this->session->set_flashdata('msg',$msg);
					redirect('userchangepwd');
				}
				exit;
			}

			if($pwdori == "xxx"){
				$datadb['user_password'] = $pwdnew;
				$datadb['passchangedate'] = date('Y-m-d');
				$result = $this->user_model->update_user($datadb,$id);
				if($result){
					$datadb2['datetime'] = date('Y-m-d h:i:s');
					$datadb2['activity'] = 'Change password by '.$username;
					$datadb2['createdby'] = $this->session->userdata('username');	
					$ulog = $userattemp[0]->user_log;
					if($ulog == 1){
						$this->user_model->user_trail($datadb2);
					}
					echo "<script type='text/javascript'>alert('Update Succesfully !');window.location.href = '".base_url().'dashboard/'."';</script>";
				}
				exit;

			}

			if($usrinfo[0]->user_password == $pwdold){
			$datadb['user_password'] = $pwdnew;
			$resultcheck = $this->user_model->check_user($pwdnew,$username,$limit);

			$ind=0;
			if($resultcheck){
				foreach($resultcheck as $s){
					if($s->user_password == $pwdnew){
						$ind=1;
					}
				}
			}

			if($ind==1){
				$msg = "Sorry, you are not allowed to use the previous password. Please change another password !";
				if($pwdori == "xxx"){
					echo $this->session->set_flashdata('msg2',$msg);
				redirect('setpass');
				}else{
					echo $this->session->set_flashdata('msg',$msg);
					redirect('userchangepwd');
				}
				
			}else{
				$result = $this->user_model->update_user($datadb,$id);
				if($result){
					$datadb2['datetime'] = date('Y-m-d h:i:s');
					$datadb2['activity'] = 'Change password by '.$username;
					$datadb2['createdby'] = $this->session->userdata('username');	
					$ulog = $userattemp[0]->user_log;
					if($ulog == 1){
						$this->user_model->user_trail($datadb2);
					}

					$datadb3['user_password'] = $pwdnew;
					$datadb3['datehistory'] = date('Y-m-d h:i:s');
					$datadb3['username'] = $this->session->userdata('username');	
					$this->user_model->password_history($datadb3);
					echo "<script type='text/javascript'>alert('Update Succesfully !');window.location.href = '".base_url().'dashboard/'."';</script>";
				}
			}
			
			}else{
				echo $this->session->set_flashdata('msg','Invalid Old Password !');
				redirect('userchangepwd');
				
			}
			
		}else{
			
			if($pwdori == "xxx"){
				echo $this->session->set_flashdata('msg2','Password Not Match !');
				redirect('setpass');
			}else{
				echo $this->session->set_flashdata('msg','Password Not Match !');
				redirect('userchangepwd');
			}
			
			// echo "<script type='text/javascript'>alert('Password Not Match !');window.location.href = '".base_url().'userchangepwd/'."';</script>";

		}
		

	}else{
		$datadb['user_password'] = md5($this->input->post('password').KEY);
		$result = $this->user_model->update_user($datadb,$id);
		echo "<script type='text/javascript'>alert('Update Succesfully !');window.location.href = '".base_url().'userinfo/'.$id ."';</script>";

	}
}


public function changeIndex($id)
{
	$key="user";
	$mid = $this->menu_model->getid($key);
	$roleid= $this->session->userdata('role');
	$areaid= $this->session->userdata('area');
	$uid= $this->session->userdata('id');
	

	$data['menuname'] = 'Change Password';
	$data['rid'] = $roleid;
	$data['menuid'] = $mid;
	$data['access'] = $this->menu_model->get_access($roleid,$mid);
	
	if($id == 0){
		$data['id'] = $uid;
		$data['userinfo'] = $this->user_model->getuserinfo($areaid,$uid);
		$data['main_content'] = 'User/userchangepasswd';
	}else{
		$data['id'] = $id;
		$data['userinfo'] = $this->user_model->getuserinfo($areaid,$id);
		$data['main_content'] = 'User/changepasswd';
	}
	
	$this->load->view('mainPage',$data);
}



public function useraction()
{
	$btn = $this->input->post('btn');

	$key="user";
	$mid = $this->menu_model->getid($key);
	$data['setting'] = $this->menu_model->getsitesetting();
	$roleid= $this->session->userdata('role');
	$areaid= $this->session->userdata('area');
	$data['rid'] = $roleid;
	$data['menuid'] = $mid;
	$data['access'] = $this->menu_model->get_access($roleid,$mid);
	$data['menu'] = $this->menu_model->getMenu($roleid);
	$data['menutitle'] = 'Change Role';
	$data['submenu'] = $this->menu_model->getSubMenu($roleid);
	$data['listrole'] = $this->user_model->getrole();
	if($btn == 'add_user'){
		$data['form'] = 'user';
		$data['areaid'] = $areaid;
		$data['menuname'] = 'Add User';
		$data['listmenu'] = $this->menu_model->get_allmenu();
		$data['main_content'] = 'User/adduserform';
	}else if($btn == 'updatesetting'){
		// echo $this->input->post('site');exit;
		$datadb['title'] = $this->input->post('site');
		$datadb['ip'] = $this->input->post('ip');
		$datadb['iconsize'] = $this->input->post('iconsize');
		$datadb['domain'] = $this->input->post('domain');
		$datadb['inactiveday'] = $this->input->post('inactiveday');
		$datadb['passexpirymin'] = $this->input->post('minexpiryday');
		$datadb['passexpiryday'] = $this->input->post('expiryday');
		$datadb['passexpirywarn'] = $this->input->post('expirymsg');
		$datadb['passhistory'] = $this->input->post('passhis');
		$datadb['concurrenlogin'] = $this->input->post('concurrentday');
		$datadb['lockduration'] = $this->input->post('lockduration');
		$datadb['locattempt'] = $this->input->post('locattempt');
		$datadb['minlength'] = $this->input->post('minlength');
		$datadb['smtphost'] = $this->input->post('smtphost');
		$datadb['smtpport'] = $this->input->post('smtpport');
		$datadb['smtpuser'] = $this->input->post('smtpuser');
		$datadb['url'] = $this->input->post('url');
		$datadb['navbarcolor'] = $this->input->post('navbar');
		$datadb['logo_path'] = $this->input->post('logopath');
		$result = $this->login_model->update_setting($datadb);
		if($result){
			echo "<script type='text/javascript'>alert('Succesfully updated  !'); window.location.href = '".base_url().'gsetting/'."';</script>";
			}else{
			echo "<script type='text/javascript'>alert('Succesfully updated  !'); window.location.href = '".base_url().'gsetting/'."';</script>";
			}
	} else if($btn == 'trails'){
		$data['form'] = 'user';
		$data['areaid'] = $areaid;
		$data['menuname'] = 'User Logs';
		$data['listmenu'] = $this->menu_model->get_allmenu();
		$data['trails'] = $this->user_model->get_trails();
		$data['main_content'] = 'User/user_trail';
	}else if($btn == 'add_personnel'){
		$data['form'] = 'personnel';
		$id = $this->input->post('tid');
		if($id){
			$data['tid'] = $id;
		}else{
			$data['tid'] = '';
		}

		$data['listmenu'] = $this->menu_model->get_allmenu();
		$data['liststate'] = $this->user_model->getstate();
		$data['listtenant'] = $this->user_model->get_tenant($areaid);
		$data['main_content'] = 'User/adduserform';
	}else if($btn == 'add_sprofile'){
		$data['form'] = 'sprofile';
		$data['listarea'] = $this->manage_model->listarea();
		$data['listmenu'] = $this->menu_model->get_allmenu();
		$data['main_content'] = 'User/adduserform';
	}else if($btn == 'add_tprofile'){
		$data['form'] = 'tprofile';
		$data['listarea'] = $this->manage_model->getarea($areaid);
		$data['listmenu'] = $this->menu_model->get_allmenu();
		$data['main_content'] = 'User/adduserform';
	}else{
		exit;
		$data['main_content'] = 'User/role_viewdd';
	}
	$this->load->view('mainPage',$data);
}

//2. SAVE DATA'-------------------------------------------------------------------------
public function  saveinfo(){
	$datadb['rolename'] = $this->input->post('rname');
	$datadb['roledesc'] = $this->input->post('rdesc');
	$datadb['dateCreated'] = date('Y-m-d h:i:s');
	$datadb['createdby'] = $this->session->userdata('username');;
	$roleid = $this->user_model->insert_tblrole($datadb);
	echo $roleid;
	return;
}


public function  userdata(){
	$username = $this->input->post('username');
	$datadb['name'] = $this->input->post('uname');
	$datadb['username'] = $username;
	$datadb['user_password'] =  md5($this->input->post('upassword',TRUE).KEY);
	$datadb['fk_roleid'] = $this->input->post('urole');
	$datadb['user_log'] = $this->input->post('log');
	$datadb['dateCreated'] = date('Y-m-d h:i:s');
	$datadb['createdby'] = $this->session->userdata('username');;
	$result = $this->user_model->insert_user($datadb);
	if($result){
		$datadb2['datetime'] = date('Y-m-d h:i:s');
		$datadb2['activity'] = 'Create user '.$username;
		$datadb2['createdby'] = $this->session->userdata('username');	
		$ulog = $userattemp[0]->user_log;
		if($ulog == 1){
			$this->user_model->user_trail($datadb2);
		}
	echo "<script type='text/javascript'>alert('Succesfully added  !'); window.location.href = '".base_url().'user/'."';</script>";
	}else{
	echo "<script type='text/javascript'>alert('Failed !'); window.history.back(-1);</script>";
	}

}


public function updateaccess($rid,$mid,$maccess,$sid)
{

	if($sid == 0){
		$access = $this->user_model->getaccess($rid,$mid,2);
	}else{
		echo 5;
		$access = $this->user_model->getaccess($rid,$sid,1);
	}

	if($access){
		$datadb['access'] = $maccess;
		if($maccess == 'N'){
			$result = $this->user_model->del_access($access);
			echo "1";
		}else{
			if($sid == 0){
			$result = $this->user_model->update_access($datadb,$rid,$mid,2);
			}else{
			$result = $this->user_model->update_access($datadb,$rid,$sid,1);
			}
	 	  echo "2";
		}
	}else{
	 $datadb['fk_roleid'] = $rid;
	 $datadb['access'] = $maccess;
	 $datadb['fk_menuid'] = $mid;
	 $datadb['fk_submenuid'] = $sid;

   $datadb2['fk_roleid'] = $rid;
	 $datadb2['access'] = $maccess;
	 $datadb2['fk_menuid'] = $mid;
	 $datadb2['fk_submenuid'] = 0;

	 if($maccess != 'N'){
     $check = $this->user_model->checkmenuid($mid);
     if($check){}else{$this->user_model->insert_access($datadb2);}
		 $result = $this->user_model->insert_access($datadb);
	 }

	 echo "3";
	}

}


//3. DELETE DATA--------------------------------------------------------
function deleteRole($id=null)
{
	$result = $this->user_model->checkroleinfo($id);
	if($result){
		echo 1;
	}else{
		$this->user_model->del_role($id);
		echo 0;
	}
}


public function deleteitm($id,$name){
	$result = $this->user_model->del_user($id);
	if($result){
		$datadb2['datetime'] = date('Y-m-d h:i:s');
		$datadb2['activity'] = 'Delete user '.$name;
		$datadb2['createdby'] = $this->session->userdata('username');	
		$ulog = $userattemp[0]->user_log;
		if($ulog == 1){
			$this->user_model->user_trail($datadb2);
		}
		echo "Successfully deleted !";
	}else{
		echo "Failed !";
	}
  }

//4. Update data-------------------------------------------------------------------------
public function editinfo($ind)
{	 
	$euid = $this->input->post('euid');
	$datadb['name'] = $this->input->post('euname');
	$datadb['fk_roleid'] = $this->input->post('eurole');
	$datadb['userstatus'] = $this->input->post('estatus');
	$result = $this->user_model->update_user($datadb,$euid);
	echo "<script type='text/javascript'>alert('Update Succesfully !');window.location.href = '".base_url().'userinfo/'.$euid ."';</script>";
}

}
