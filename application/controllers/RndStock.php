<?php
class RndStock extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function listrndstock($hid,$oid){
    $rndstockModel      = GAMMA::getModel('rndstock');
    $userModel          = GAMMA::getModel('user');
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');
    
    $key                ="report";
    $data['prodid']     = $hid;
    $data['rndid']      = $oid;
    $data['setting']    = $siteModel->all();
    $roleid             =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'RnD Stock Information';
    $data['homestayexpense'] = $rndstockModel->all();
    $data['main_content'] = 'Rnd/rndstock_view';
    $this->load->view('mainPage',$data);
  }

  public function stockaction(){
    $rndstockModel        = GAMMA::getModel('rndstock');
    $rnditemModel         = GAMMA::getModel('rnditem');
    $btn                  = $this->input->post('btn');

    $oid                  = $this->input->post('rndid');
    $hid                  = $this->input->post('prodid');
    $datadb['date']       = $this->input->post('date');
    $datadb['description']= $this->input->post('desc');
    $datadb['unit_price'] = $this->input->post('unitprice');
    $datadb['quantity']   = $this->input->post('quantity');
    $datadb['unit']       = $this->input->post('unit');
    $datadb['amount']     = $this->input->post('amount');
    $datadb['balance']    = $this->input->post('amount')*$this->input->post('quantity');
    $datadb['total']      = $datadb['unit_price']*$datadb['quantity'];
    $datadb['status']     = 1;
    $datadb['remarks']    = $this->input->post('remarks');
    if (strpos($_SERVER['HTTP_HOST'], 'mastersentral.com') !== false) {
      $datadb['sync']     = 1;
    }

    if($btn == "edit"){
      $id                   = $this->input->post('id');
      $datadb2['date']       = $this->input->post('edate');
      $datadb2['description']= $this->input->post('edescription');
      $datadb2['unit_price'] = $this->input->post('eunitprice');
      $datadb2['quantity']   = $this->input->post('equantity');
      $datadb2['unit']       = $this->input->post('eunit');
      $datadb2['amount']     = $this->input->post('eamount');
      $datadb2['total']      = $datadb2['unit_price']*$datadb2['quantity'];
      $datadb2['status']     = $this->input->post('estatus');
      $datadb2['remarks']    = $this->input->post('eremark');
      $result                = $rndstockModel->update($datadb2,array('id' => $id));
    }if($btn == "savestock"){
      $rndid                  = $this->input->post('rndid');
      $prodid                 = $this->input->post('prodid');
      $datadb3['name']        = $this->input->post('uname');
      $datadb3['description'] = $this->input->post('udesc');
      $datadb3['releasedate'] = $this->input->post('daterelease');
      $datadb3['expirydate']  = $this->input->post('dateexpiry');
      $datadb3['totalstock']  = $this->input->post('totalstock');
      $datadb3['suggestprice'] = $this->input->post('suggestprice');
      $result                = $rnditemModel->update($datadb3,array('id' => $prodid));
      redirect('rnditemdetail/'.$hid.'/'.$oid);
    }else{
      $result               = $rndstockModel->add($datadb);
    }
    
    

    if($result > 0){
      redirect('rndstock/'.$hid.'/'.$oid);
    }
      
  }

  public function edititm($id){
    $rndstockModel  = GAMMA::getModel('rndstock');
    $result         = $rndstockModel->selectOne(array('id' => $id));
    
    echo json_encode($result);
  }

  public function deleteitm($id){
      $rndstockModel  = GAMMA::getModel('rndstock');
      $result = $rndstockModel->delete(array('id' => $id));
      if($result){
        echo "Successfully deleted !";
      }else{
        echo "Failed !";
      }
  }
      
}
