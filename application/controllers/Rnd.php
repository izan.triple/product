<?php
class Rnd extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function index(){
    $rndModel       = GAMMA::getModel('rnd');
    $siteModel      = GAMMA::getModel('site');
    $menuModel      = GAMMA::getModel('menu');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'Rnd Information';
    $data['homestay']   = $rndModel->all();
    $data['main_content'] = 'Rnd/rnd_view';
    $this->load->view('mainPage',$data);
  }

  public function listrnd($id){
    $rnditemModel        = GAMMA::getModel('rnditem');
    $siteModel           = GAMMA::getModel('site');
    $menuModel           = GAMMA::getModel('menu');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['gensetting'] =  GAMMA::getSession('setting');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['rndid']      = $id;
    $data['menuname']   = 'RND Item Information';
    $data['homestay']   = $rnditemModel->listrnditem($id);
    $data['main_content'] = 'Rnd/rnditem_view';
    $this->load->view('mainPage',$data);
  }

  public function rnditemdetail($id,$oid){
    $rnditemModel        = GAMMA::getModel('rnditem');
    $rndModel            = GAMMA::getModel('rnd');
    $rndcostModel        = GAMMA::getModel('rndcost');
    $siteModel           = GAMMA::getModel('site');
    $menuModel           = GAMMA::getModel('menu');
    $SaleModel           = GAMMA::getModel('sale');
    $PaymentModel        = GAMMA::getModel('payment');
    $expenseModel        = GAMMA::getModel('homestayexpense');
    $maintenanceModel    = GAMMA::getModel('homestaymaintenance');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['gensetting'] =  GAMMA::getSession('setting');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['homestayid'] = $id;
    $data['prodid']     = $id;
    $data['ownerid']    = $oid;
    $data['menuname']   = 'RnD Item Details';
    $data['homestay']   = $rnditemModel->selectAll(array('id' => $id));
    $data['rndinfo']    = $rndModel->selectOne(array('id' => $oid));
    $data['saleinfo']   = $SaleModel->listsalenew($id);
    // $book               = $BookingModel->bookinfo($id);
    // $data['booking']    = json_encode($book);
    
    $data['tcost']      = $rndcostModel->totalcost($id);
    $data['tpayment']   = $PaymentModel->totalpayment($id);
    $data['texpense']   = $expenseModel->totalexpense($id);
    $data['tmaintenance'] = $maintenanceModel->totalmaintenance($id);

    // echo  $id;exit;
    
    $data['main_content'] = 'Rnd/rnditemdetail_view';
    $this->load->view('mainPage',$data);
  }

  //end rnd

  public function rndaction($btn=NULL,$statid=NULL,$searchname=NULL)
  {
    
    $rndModel            = GAMMA::getModel('rnd');
    $rnditemModel        = GAMMA::getModel('rnditem');
    $productModel        = GAMMA::getModel('product');

    if($statid == NULL){
      $btn= $this->input->post('btn');
      $statid= $this->input->post('statid');
    }

    if($searchname == NULL){
      $searchname= $this->input->post('search');
    }else if($searchname == 'null'){
      $searchname= '';
    }
  
      $data['menuid'] = 12;
      $btn= $this->input->post('btn');


      if($btn == "add"){
        $data['menuname']     = 'New Rnd';
        $data['main_content'] = 'Rnd/addrnd_view';
        $this->load->view('mainPage',$data);

      }else if($btn == "addprod"){
        $data['menuname']     = 'New Product';
        $data['proddata']     = $productModel->all();
        // print_r($data['proddata']);exit;
        $data['rndid']        = $this->input->post('rndid');
        $data['main_content'] = 'Rnd/addproduct_view';
        $this->load->view('mainPage',$data);

      }else if($btn == "submit"){
        $datadb['name']         = $this->input->post('name');
        // $datadb['description']  = $this->input->post('long_desc');
        $result                 = $rndModel->add($datadb);
        
        if($result > 0){
          redirect('rnd');
        }else{
          redirect('rnd');
        }

      }else if($btn == "submitrnd"){
        $datadb['name']         = $this->input->post('name');
        $datadb['indicator']    = $this->input->post('ind');
        $result                 = $rndModel->add($datadb);
        
        if($result > 0){
          redirect('rnd');
        }else{
          redirect('rnd');
        }

      }else if($btn == "submitprod"){
        $prod                   = explode('_',$this->input->post('name'));
        $datadb['name']         = $prod[1];
        $datadb['description']  = $this->input->post('long_desc');
        $datadb['rndid']        = $this->input->post('rndid');
        $result                 = $rnditemModel->add($datadb);
        $rdata                  = $rndModel->selectOne(array('id' => $datadb['rndid']));

        // print_r($rdata['ind'].$this->input->post('rndid')."P".$result);exit;

        $datadb['batchno']      = $rdata['ind'].$this->input->post('rndid');
        $datadb['prodno']       = $rdata['ind'].$this->input->post('rndid').$prod[0].'-'.$result;
        $rnditemModel->update($datadb,array('id' => $result));

        if($result > 0){
          redirect('listrnd/'.$datadb['rndid']);
        }else{
          redirect('listrnd/'.$datadb['rndid']);
        }

      }else if($btn == "csv"){
        // output headers so that the file is downloaded rather than displayed
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename= mechantreport'.date('Ymd').".csv");

        // do not cache the file
        header('Pragma: no-cache');
        header('Expires: 0');

        // create a file pointer connected to the output stream
        $file = fopen('php://output', 'w');

        // send the column headers
        fputcsv($file, array('Submission Date', 'Merchant Name', 'Location', 'Status'));

        // Sample data. This can be fetched from mysql too
        $data['menuname'] = 'Merchant Information';
        $query = 'DateSubmit,RegName,RegState,b.name';

        if($bank == "hlbb"){
          $data = $this->Merchant_model->getmerchantgroup($query,$statid,$searchname);
        }else if($bank == "rhb"){
          $data = $this->Merchant_model->getmerchantgrouprhb($query,$statid,$searchname);
        }else if($bank == "ambank"){
          $data = $this->Merchant_model->getmerchantgroupambank($query,$statid,$searchname);
        }

        // output each row of the data
        foreach ($data as $row)
        {
          if( is_object($row) )
          $row = (array) $row;
          fputcsv($file, $row);

        }

        exit();

      }else if($btn == "pdf"){
        $data['menuname'] = 'Merchant Information';
        $query = 'DateSubmit,RegName,RegState,b.name';
        $data['liststatus'] = $this->Merchant_model->getstatus();

        if($bank == "hlbb"){
          $data['listmerchant'] = $this->Merchant_model->getmerchantgroup($query,$statid,$searchname);
        }else if($bank == "rhb"){
          $data['listmerchant'] = $this->Merchant_model->getmerchantgrouprhb($query,$statid,$searchname);
        }else if($bank == "ambank"){
          $data['listmerchant'] = $this->Merchant_model->getmerchantgroupambank($query,$statid,$searchname);
        }

        $loaded_html = $this->load->view('Merchant/merchanttemplate_view', $data, true);
        $filename='mechantreport'.date('Ymd').'.pdf';
        $this->load->library('pdf'); 
        $this->dompdf->loadHtml($loaded_html);
        $this->dompdf->setPaper('A4', 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream($filename);

      }else{
        redirect('rnd');
      }
      
} 

public function editowner($id)
{
  $ownerModel     = GAMMA::getModel('owner');
  $siteModel      = GAMMA::getModel('site');
  $menuModel      = GAMMA::getModel('menu');
  
  $key             ="merchant";
  $data['setting'] = $siteModel->all();
  $roleid          =  GAMMA::getSession('role');
  $data['menuid']     = $menuModel->getid($key);
  $data['key']        = $key;
  $data['menuname']   = 'Owner Information';
  $data['ownerdata']  = $ownerModel->selectOne(array('id' => $id));

  print_r($data['ownerdata']);exit;
  $data['main_content'] = 'Owner/editmerchant_view';

  $this->load->view('mainPage',$data);
}

public function deleteitm($id){

  $ownerModel     = GAMMA::getModel('owner');
  $result         = $ownerModel->delete(array('id' => $id));
  
	if($result){
    echo "Successfully deleted !";
  }else{
    echo "Failed !";
  }
}





}
