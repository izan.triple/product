<?php
class Appointment extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function index(){
    $appointmentModel   = GAMMA::getModel('appointment');
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');

    $key             ="appoinment";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $uid                = GAMMA::getSession('id');
    $data['menuname']   = 'Appointment Information';
    $data['appointment']  = $appointmentModel->listappmt('all');
    $data['country']      = $appointmentModel->listcountry();

    // print_r($data['country']);exit;
    $data['main_content'] = 'Appointment/appointment_view';
    $this->load->view('mainPage',$data);
  }




  public function listappment($ind){
    $appointmentModel   = GAMMA::getModel('appointment');
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');

    $key             ="appoinment";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $uid                = GAMMA::getSession('id');
    $data['menuname']   = 'Appointment Information';
    // $data['appointment']  = $appointmentModel->all();
    $data['appointment']  = $appointmentModel->listappmt($ind);
    $data['country']      = $appointmentModel->listcountry();

    // print_r($data['country']);exit;
    $data['main_content'] = 'Appointment/appointment_view';
    $this->load->view('mainPage',$data);
  }



  public function homestaydetail($id,$oid){
    $homestayModel       = GAMMA::getModel('homestay');
    $siteModel           = GAMMA::getModel('site');
    $menuModel           = GAMMA::getModel('menu');
    $BookingModel        = GAMMA::getModel('booking');
    $PaymentModel        = GAMMA::getModel('payment');
    $expenseModel        = GAMMA::getModel('homestayexpense');
    $maintenanceModel    = GAMMA::getModel('homestaymaintenance');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['gensetting'] =  GAMMA::getSession('setting');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['homestayid'] = $id;
    $data['ownerid']    = $oid;
    $data['menuname']   = 'Homestay Details';
    $data['homestay']   = $homestayModel->selectAll(array('id' => $id));
    $data['bookinginfo']= $BookingModel->listbook($id);
    $book               = $BookingModel->bookinfo($id);
    $data['booking']    = json_encode($book);
    $data['tbook']      = $BookingModel->totalbook($id);
    $data['tpayment']   = $PaymentModel->totalpayment($id);
    $data['texpense']   = $expenseModel->totalexpense($id);
    $data['tmaintenance'] = $maintenanceModel->totalmaintenance($id);

    // echo  $id;exit;
    
    $data['main_content'] = 'Homestay/homestaydetail_view';
    $this->load->view('mainPage',$data);
  }

  public function homestayaction($oid){
    $homestayModel       = GAMMA::getModel('homestay');
    $ownerhomestayModel  = GAMMA::getModel('ownerhomestay');
    $siteModel           = GAMMA::getModel('site');
    $menuModel           = GAMMA::getModel('menu');
    $btn                 = $this->input->post('btn');
    
    $key             ="homestay";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'Homestay Information';

    if($btn == "add"){
      $data['ownerid']      = $oid;
      $data['main_content'] = 'Homestay/addhomestay_view';
      $this->load->view('mainPage',$data);
    }else if($btn == "back"){
      redirect('owner');
    }else if($btn == "submit"){
      $datadb['name']        = $this->input->post('name');
      $datadb['address1']    = $this->input->post('address1');
      $datadb['address2']    = $this->input->post('address2');
      $datadb['city']        = $this->input->post('city');
      $datadb['postcode']    = $this->input->post('postcode');
      $datadb['pic']         = $this->input->post('contactname');
      $datadb['phone']       = $this->input->post('contactphone');
      $result                = $homestayModel->add($datadb);

      if($result > 0){
        $datadb2['homestayid']   = $result;
        $datadb2['ownerid ']     = $oid;
        $ownerhomestayModel->add($datadb2);

        redirect('listhomestay/'.$oid);
      }
      
    }

  }


  public function deleteitm($id){
    $bookingModel   = GAMMA::getModel('booking');
    $result         = $bookingModel->delete(array('id' => $id));
    
    if($result){
      echo "Successfully deleted !";
    }else{
      echo "Failed !";
    }
  }

  public function updateitm($id){
    $bookingModel   = GAMMA::getModel('booking');
    $paymentModel   = GAMMA::getModel('payment');

    $data['status'] = 4;
    $result         = $bookingModel->update($data, array('id' => $id));
    
    if($result){
      echo "Successfully update !";
    }else{
      echo "Failed !";
    }
  }





}
