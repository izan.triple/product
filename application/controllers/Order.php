<?php
class Order extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function index(){
    $orderModel     = GAMMA::getModel('sale');
    $siteModel      = GAMMA::getModel('site');
    $menuModel      = GAMMA::getModel('menu');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'Order Information';
    $data['order']      = $orderModel->all();
    $data['main_content'] = 'Order/order_view';
    $this->load->view('mainPage',$data);
  }

  function completeorder($oid){

    $saleModel  	        = GAMMA::getModel('sale');
    $productorderModel  	= GAMMA::getModel('productorder');
    $productdisplayModel  = GAMMA::getModel('productdisplay');
    $productorderitemModel= GAMMA::getModel('productorderitem');
    $settingModel   	    = GAMMA::getModel('login');
    $siteModel            = GAMMA::getModel('site');
    $menuModel            = GAMMA::getModel('menu');

    $saleinfo   = $saleModel->selectOne(array('sale_id' => $oid));
    $orderinfo  = $productorderitemModel->selectAll(array('orderid' => $oid));
    $productorderinfo = $productorderModel->selectOne(array('orderid' => $oid));
    
    $data['saleinfo']     = $saleinfo;
    $data['orderinfo']    = $productorderinfo;
    $data['orderitminfo'] = $orderinfo;
    $data['siteSetting'] 	= $settingModel->listsetting();
  
    $key    =$this->uri->segment(1);
    $mid    = $menuModel->getid($key);
    $roleid = $this->session->userdata('role');
    $data['orderid']    = $oid;
    $data['menuname']   = 'Order Information';
    $data['access']     = $this->menu_model->get_access($roleid,$mid);
    
    $data['menutitle']      = 'Order Competed';
    // $data['main_content']   = 'order/orderconfirm_view';
    $this->load->view('Order/orderconfirm_view',$data);

}





}
