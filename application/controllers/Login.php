<?php
class Login extends CI_Controller
{
	 /**
	  * __construct function.
	  *
	  * @access public
	  * @return void
	  */
	 public function __construct() {
	 	parent::__construct();
    	$this->load->model('login_model');
		$this->load->model('setting_model');
		$this->load->model('menu_model');
		$this->load->model('user_model');
	 }

	function index(){

		echo "Under construction";exit;
		$contentModel   = GAMMA::getModel('content');

		$listsetting = $this->login_model->listsetting();
		$data['siteSetting'] 	= $listsetting;
		$data['ind'] 			= "home";
		$data['menu']			= $contentModel->contentAll();
		$data['content']		= $contentModel->selectOne(array('menu' => 'home'));
		$this->load->view('layout/master',$data);
	 }

	function login(){
		$listsetting = $this->login_model->listsetting();
		$data['siteSetting'] = $listsetting;
		$this->load->view('login_view',$data);
	}

	 


  function logout(){
	  $email= $this->session->userdata('username');
	  
	  $this->session->sess_destroy();
	  $datadb['loginsession'] = 0;
	  $this->login_model->updatelastlogin($datadb,$email);
      redirect("");
  }

  
  function logout2(){
    $email= $this->session->userdata('username');
    echo $this->session->set_flashdata('msg','Sorry, your session has expired. Please log in again to continue !');
    $data['bank'] = $bank;
    $this->session->sess_destroy();
    $datadb['loginsession'] = 0;
    $this->login_model->updatelastlogin($datadb,$email);
	$this->load->view('login_view',$data);
}

  function resetsession($email){
	$datadb['loginsession'] = 0;
	$this->login_model->updatelastlogin($datadb,$email);
	echo $this->session->set_flashdata('msg',NULL);
	redirect('login');
  }
  

  function setpass(){
	$data['id']    = $this->session->userdata('id');;
	echo $this->session->set_flashdata('msg','Your password is expired. Please reset the password to continue !');
	$this->load->view('User/setpass',$data);
 }
  


  function auth(){
	$email    = $this->input->post('email',TRUE);
	$bank    = $this->input->post('bank',TRUE);
	$password = md5($this->input->post('password',TRUE).KEY);
	$validate = $this->login_model->validate($email,$password);
	$listsetting = $this->login_model->listsetting();
	$userattemp = $this->login_model->checkattemp($email);	
	
	if($validate->num_rows() > 0){
		$data  = $validate->row_array();
		if($data['userstatus'] == "Active"){
			$name  = $data['username'];
			$id  = $data['user_id'];
			$fullname  = $data['name'];
			$lastLogin = $data['lastLogin'];
			$role = $data['fk_roleid'];
			$datecreated = $data['dateCreated'];
			$passchangedate = $data['passchangedate'];
			$usrlog = $data['user_log'];

			$sesdata = array(
			'id'  => $id,
			'username'  => $name,
			'name'  => $fullname,
			'email'     => $email,
			'role'      =>  $role,
			'lastLogin' =>  $lastLogin,
			'menu' 		=> $this->menu_model->getMenu($role),
			'submenu'	 => $this->menu_model->getSubMenu($role),
			'siteinfo' => $this->menu_model->getsitesetting(),
			'passchangedate' => $passchangedate,
			'datecreate' => $datecreated,
			'setting' => $listsetting,
			'usrlog' => $usrlog,
			'bank' => $bank,
			'logged_in' => TRUE
			);
			$this->session->set_userdata($sesdata);
			//date_default_timezone_set('UTC');
			$session = $data['loginsession'] + 1;
			$datadb['lastLogin'] = date('Y-m-d H:i:s');
			$datadb['loginsession'] = $session;
			$datadb['user_attempt'] = 0;

			$maxday = $listsetting[0]->passexpiryday;
			$inactiveday = $listsetting[0]->inactiveday;
			$warnday = $listsetting[0]->passexpirywarn;
			$minmsg = ($maxday - $warnday)-1;

			$date1=date_create(date('Y-m-d'));
			$date2=date_create($passchangedate);
			$diff=date_diff($date1,$date2);
			$daydiff =  $diff->format("%a");
			// echo $daydiff;exit;
			$day = $maxday - $daydiff;

			// echo $daydiff.'-----'.$day.'------'.$minmsg.'------'.$inactiveday..($maxday + 1);
			// exit;
				if($inactiveday == 999){
					$this->login_model->updatelastlogin($datadb,$email);
					$roleid          =  GAMMA::getSession('role');
					echo $roleid;exit;
					if($roleid == 9){
						echo "<script type='text/javascript'>window.location.href = '".base_url().'appointment/'."';</script>";
					}else{
						echo "<script type='text/javascript'>window.location.href = '".base_url().'dashboard/'."';</script>";
					}
					
				}else{
					//HLBB POLICY
					if($daydiff > ($maxday + 1) && $daydiff < ($inactiveday + 1)){
						redirect('setpass');
					}else{
						if($daydiff > $minmsg && $daydiff < ($inactiveday+1)){
							
							echo "<script type='text/javascript'>alert('Your password will be expired in ".$day." !');</script>";
						}
						$this->login_model->updatelastlogin($datadb,$email);
						if(GAMMA::getSession('role') == 3){
							echo "<script type='text/javascript'>window.location.href = '".base_url().'homestay/'."';</script>";
						}else{
							echo "<script type='text/javascript'>window.location.href = '".base_url().'dashboard/'."';</script>";
						}
						
					}
				}
			

			}else{
			// echo $this->session->set_flashdata('msg','Sorry your account IS NOT ACTIVE ! Please contact your Administrator. ');

			echo $this->session->set_flashdata('msg','Sorry your account INACTIVE ! Please contact your Administrator.');
			redirect('alogin');
			}
		}
		else{
			$userattemp1 = $userattemp[0]->user_attempt;
			$attempt = $listsetting[0]->locattempt;
			$tot = $userattemp1+1;

			$datadb2['datetime'] = date('Y-m-d h:i:s');
			$datadb2['activity'] = 'Login attempt failed . '.$tot;
			$datadb2['createdby'] = $email;	

			$ulog = $userattemp[0]->user_log;
			if($ulog == 1){
				$this->user_model->user_trail($datadb2);
			}
			$remain = $attempt-$tot;
			$datadb['user_attempt'] = $tot;
			if($tot < ($attempt+1)){
				echo $this->session->set_flashdata('msg','Incorrect username or password. Your total attempts are : '.$remain);
			}else{
				$datadb['userstatus'] = 'Inactive';
				echo $this->session->set_flashdata('msg','Your account has been locked. Please contact your administrator');
			}
			$this->login_model->updatelastlogin($datadb,$email);
			
			redirect('alogin');
		}	
 	}


}