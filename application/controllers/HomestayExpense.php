<?php
class HomestayExpense extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function listhomestayexpense($hid,$oid){
    $homestayexpenseModel  = GAMMA::getModel('homestayexpense');
    $userModel          = GAMMA::getModel('user');
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');
    
    $key                ="report";
    $data['homestayid'] = $hid;
    $data['ownerid']    = $oid;
    $data['setting']    = $siteModel->all();
    $roleid             =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'Homestay Expense Information';
    $data['homestayexpense'] = $homestayexpenseModel->listexpensehomestay($hid);
    $data['main_content'] = 'Homestay/homestayexpense_view';
    $this->load->view('mainPage',$data);
  }

  public function expenseaction(){
    $homestayexpenseModel  = GAMMA::getModel('homestayexpense');

    $btn                  = $this->input->post('btn');

    $oid                  = $this->input->post('ownerid');
    $hid                  = $this->input->post('homestayid');
    $datadb['homestayid'] = $hid;
    $datadb['ownerid']    = $oid;
    $datadb['date']       = $this->input->post('date');
    $datadb['description']= $this->input->post('desc');
    $datadb['amount']     = $this->input->post('amount');
    $datadb['status']     = $this->input->post('status');
    $datadb['remarks']    = $this->input->post('remarks');
    if (strpos($_SERVER['HTTP_HOST'], 'mastersentral.com') !== false) {
      $datadb['sync']     = 1;
    }

    if($btn == "edit"){
      $id                   = $this->input->post('id');
      $datadb2['date']       = $this->input->post('edate');
      $datadb2['description']= $this->input->post('edescription');
      $datadb2['amount']     = $this->input->post('eamount');
      $datadb2['status']     = $this->input->post('estatus');
      $datadb2['remarks']    = $this->input->post('eremark');
      $result               = $homestayexpenseModel->update($datadb2,array('id' => $id));
    }else{
      $result               = $homestayexpenseModel->add($datadb);
    }
    
    

    if($result > 0){
      redirect('homestayexpense/'.$hid.'/'.$oid);
    }
      
  }

  public function edititm($id){
    $homestayexpenseModel  = GAMMA::getModel('homestayexpense');
    $result = $homestayexpenseModel->selectOne(array('id' => $id));
    
    // print_r($result);exit;
    echo json_encode($result);
  }

  public function deleteitm($id){
      $homestayexpenseModel  = GAMMA::getModel('homestayexpense');
      $result = $homestayexpenseModel->delete(array('id' => $id));
      if($result){
        echo "Successfully deleted !";
      }else{
        echo "Failed !";
      }
  }
      
}
