<?php
class Page extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('menu_model');
    $this->load->model('dashboard_model');
    $this->load->model('Merchant_model');
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }

  function index(){
        $key="dashboard";
        if($this->session->userdata('logged_in')=='TRUE'){
        $datacount = $data['dashmerchant'] = $this->Merchant_model->get_dashboard();
        
        $mid = $this->menu_model->getid($key);
        $roleid= $this->session->userdata('role');
        $data['menuid'] = $mid;
        $data['menuname'] = 'Dashboard';
        $data['access'] = $this->menu_model->get_access($roleid,$mid);
        $data['dashmerchant'] = $datacount;
        $total= 0;
        foreach($datacount as $c){
          $total += $c->total;
        }

        $data['menutitle'] = 'Role Information';
        $data['totalmerchant'] = $total;
        $data['main_content'] = 'dashboard_view';
        $this->load->view('mainPage',$data);
      }else{
          echo $this->session->userdata('level');
          echo "Access Denied";
          redirect('login');
      }

  }

  function feedback(){
    echo "hereeee";
  }


  function menuIndex(){
    $key=$this->uri->segment(1);
    $mid = $this->menu_model->getid($key);
    $data['setting'] = $this->menu_model->getsitesetting();
    $roleid= $this->session->userdata('role');
    $data['menuname'] = 'Menu';
    $data['menuid'] = $mid;
    $data['access'] = $this->menu_model->get_access($roleid,$mid);
    $data['menutitle'] = 'Role Information';
    $data['qmenu'] = $this->menu_model->displaymenu();

    $data['main_content'] = 'maintenance/menu_view';
    $this->load->view('mainPage',$data);
  }

  function menuitmIndex($id){
    $mid = $id;
    $data['setting'] = $this->menu_model->getsitesetting();
    $roleid= $this->session->userdata('role');
    $data['menuid'] = $mid;
    $data['menuname'] = 'Sub-Menu';
    $data['access'] = $this->menu_model->get_access($roleid,$mid);
    $data['menu'] = $this->menu_model->getMenu($roleid);
    $data['menutitle'] = 'Role Information';
    $data['submenu'] = $this->menu_model->getSubMenu($roleid);
    $data['siteinfo'] = $this->menu_model->getsitesetting();
    $data['qmenu'] = $this->menu_model->displaymenuitms($id);

    $data['main_content'] = 'maintenance/menuitm_view';
    $this->load->view('mainPage',$data);
  }

  function maintenanceIndex(){
    $key=$this->uri->segment(1);
    $mid = $this->menu_model->getid($key);
    $data['setting'] = $this->menu_model->getsitesetting();
    $roleid= $this->session->userdata('role');
    $data['menuid'] = $mid;
    $data['access'] = $this->menu_model->get_access($roleid,$mid);
    $data['menuname'] = 'Maintenance';
    $data['main_content'] = 'maintenance/maintenance_view';
    $this->load->view('mainPage',$data);
  }

  //2. Insert controller
  public function addmenu(){
    $datadb['menuclass'] = $this->input->post('menuclass');
    $datadb['menuTitle'] = $this->input->post('menuTitle');
    $datadb['menuname'] = $this->input->post('menuname');
    $datadb['menuurl'] = $this->input->post('menuurl');
    $datadb['menuicon'] = $this->input->post('menuicon');
    $datadb['menuStatus'] = $this->input->post('menustatus');
    $result = $this->menu_model->insert_addmenu($datadb);
    echo $result;
  }

  public function addsubmenu(){
    $datadb['submenuid'] = $this->input->post('submenuid');
    $datadb['submenuname'] = $this->input->post('submenuname');
    $datadb['submenuurl'] = $this->input->post('submenuurl');
    $datadb['menuid'] = $this->input->post('menuid');
    $result = $this->menu_model->insert_addsubmenu($datadb);
    echo $result;
    }

//3. delete controller
  public function Resetdata($ind)
	{
		$result = $this->dashboard_model->resetsaledata($ind);
		redirect('maintenance');
	}

  public function Restoredata($ind)
	{
    // echo "Here"; exit;
    $siteModel          = GAMMA::getModel('site');
    $setting            = $siteModel->all();

    // print_r($setting[0]->url);
    
    $api_url        = $setting[0]['url'].'grabdata/'.$ind;
    $json_data      = file_get_contents($api_url);
    $response_data  = json_decode($json_data);
    $exp_data       = $response_data->data;

    // print_r($exp_data);
    foreach ($exp_data as $exp) {
      if($ind == 'expense'){
        $dataModel = GAMMA::getModel('homestayexpense');
      }elseif($ind == 'maintenance'){
        $dataModel  = GAMMA::getModel('homestaymaintenance');
      }elseif($ind == 'booking'){
        $dataModel  = GAMMA::getModel('booking');
      }elseif($ind == 'payment'){
        $dataModel  = GAMMA::getModel('payment');
      }
      $result       = $dataModel->selectOne(array('id' => $exp->id));
      
      if($ind == 'expense' || $ind == 'maintenance'){
        $datadb['id']           = $exp->id;
        $datadb['date']         = $exp->date;
        $datadb['description']  = $exp->description;
        $datadb['amount']       = $exp->amount;
        $datadb['status']       = $exp->status;
        $datadb['remarks']      = $exp->remarks;
        $datadb['homestayid']   = $exp->homestayid;
        $datadb['ownerid']      = $exp->ownerid;
        $datadb['sync']         = $exp->sync;
      }elseif($ind == 'booking'){
        $datadb['id']             = $exp->id;
        $datadb['date']           = $exp->date;
        $datadb['booking_id']     = $exp->booking_id;
        $datadb['customer_name']  = $exp->customer_name;
        $datadb['customer_phone'] = $exp->customer_phone;
        $datadb['customer_email'] = $exp->customer_email;
        $datadb['title']          = $exp->title;
        $datadb['start']          = $exp->start;
        $datadb['end_date']       = $exp->end_date;
        $datadb['ownerid']        = $exp->ownerid;
        $datadb['homestayid']     = $exp->homestayid;
        $datadb['status']         = $exp->status;
        $datadb['created_by']     = $exp->created_by;
        $datadb['created_date']   = $exp->created_date;
        $datadb['amount']         = $exp->amount;
        $datadb['day']            = $exp->day;
        $datadb['total']          = $exp->total;
        $datadb['current_payment']= $exp->current_payment;
        $datadb['balance_payment']= $exp->balance_payment;
        $datadb['sync']           = $exp->sync;
      }elseif($ind == 'payment'){
        $datadb['id']             = $exp->id;
        $datadb['description']    = $exp->description;
        $datadb['bookingid']      = $exp->bookingid;
        $datadb['date']           = $exp->date;
        $datadb['amount']         = $exp->amount;
        $datadb['status']         = $exp->status;
        $datadb['homestayid']     = $exp->homestayid;
        $datadb['date_created']   = $exp->date_created;
        $datadb['sync']           = $exp->sync;
      }

      if($result){
        $dataModel->update($datadb,array('id' => $exp->id));
      }else{
        $dataModel->add($datadb);
      }
      $api_url        = $setting[0]['url'].'updatedata/'.$ind.'/'.$exp->id;
      $json_data      = file_get_contents($api_url);
    }

    redirect('maintenance');

	}

}


