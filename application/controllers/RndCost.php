<?php
class RndCost extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function listrndcost($hid,$oid){
    $rndstockModel      = GAMMA::getModel('rndstock');
    $rndcostModel      = GAMMA::getModel('rndcost');
    $userModel          = GAMMA::getModel('user');
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');
    
    $key                ="report";
    $data['prodid']     = $hid;
    $data['rndid']      = $oid;
    $data['setting']    = $siteModel->all();
    $roleid             =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'RnD Cost Information';
    $data['stock']      = $rndstockModel->all();
    $data['cost']       = $rndcostModel->listrndprod($hid);
    $data['main_content'] = 'Rnd/rndcost_view';

    $this->load->view('mainPage',$data);
  }

  public function costaction(){

    $stockname = explode("_",$this->input->post('desc'));

    $rndcostModel         = GAMMA::getModel('rndcost');
    $rndstockModel        = GAMMA::getModel('rndstock');
    $btn                  = $this->input->post('btn');

    $oid                  = $this->input->post('rndid');
    $hid                  = $this->input->post('prodid');
    $datadb['prodid']     = $hid;
    $datadb['rndid']      = $oid;
    $datadb['date']       = date('Y-m-d');
    $datadb['description']= $stockname[0];
    $datadb['amount']     = $this->input->post('amount');

    $stock                = $rndstockModel->selectOne(array('id' => $stockname[1]));

    $datadb3['balance']   = $stock['balance'] - $datadb['amount'];
    $unitprice            = $stock['total']/($stock['quantity']*$stock['amount']);

    // print_r($stock);exit;

    $datadb['unit_price'] = $unitprice*$datadb['amount'];
    $datadb['quantity']   = 1;
    $datadb['unit']       = $stock['unit'];
    $datadb['stock_id']   = $stockname[1];
    $datadb['total']      = $datadb['unit_price']*$datadb['quantity'];
    $datadb['status']     = 1;
    $datadb['remarks']    = $this->input->post('remarks');
    if (strpos($_SERVER['HTTP_HOST'], 'mastersentral.com') !== false) {
      $datadb['sync']     = 1;
    }

    if($btn == "edit"){
      $id                   = $this->input->post('id');
      $datadb2['date']       = $this->input->post('edate');
      $datadb2['description']= $this->input->post('edescription');
      $datadb2['unit_price'] = $this->input->post('eunitprice');
      $datadb2['quantity']   = $this->input->post('equantity');
      $datadb2['unit']       = $this->input->post('eunit');
      $datadb2['amount']     = $this->input->post('eamount');
      $datadb2['total']      = $datadb2['unit_price']*$datadb2['quantity'];
      $datadb2['status']     = $this->input->post('estatus');
      $datadb2['remarks']    = $this->input->post('eremark');
      $result                = $rndcostModel->update($datadb2,array('id' => $id));
    }else{
      $result1               = $rndstockModel->update($datadb3,array('id' => $stockname[1]));
      $result                = $rndcostModel->add($datadb);
    }
    
    
    
    if($result > 0){
      redirect('rndcost/'.$hid.'/'.$oid);
    }
      
  }

  public function edititm($id){
    $rndstockModel  = GAMMA::getModel('rndstock');
    $result         = $rndstockModel->selectOne(array('id' => $id));
    
    echo json_encode($result);
  }

  public function deleteitm($id){
      $rndstockModel  = GAMMA::getModel('rndstock');
      $rndcostModel   = GAMMA::getModel('rndcost');

      $costdata   = $rndcostModel->selectOne(array('id' => $id));
      $stockid    = $costdata['stock_id'];
      $stockdata  = $rndstockModel->selectOne(array('id' => $stockid ));
      // print_r($stockdata['balance']+$costdata['amount']);exit;
      $result = $rndcostModel->delete(array('id' => $id));
      
      if($result){
        $datadb3['balance']   = $stockdata['balance']+$costdata['amount'];
        $rndstockModel->update($datadb3,array('id' => $stockid));
        echo "Successfully deleted !";
      }else{
        echo "Failed !";
      }
  }
      
}
