<?php
class Booking extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function index(){
    $ownerModel     = GAMMA::getModel('owner');
    $siteModel      = GAMMA::getModel('site');
    $menuModel      = GAMMA::getModel('menu');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'Owner Information';
    $data['homestay']   = $ownerModel->all();
    $data['main_content'] = 'Owner/owner_view';
    $this->load->view('mainPage',$data);
  }

  public function bookingaction($btn=NULL,$statid=NULL)
  {
    $bookingModel     = GAMMA::getModel('booking');
    $paymentModel     = GAMMA::getModel('payment');
    $customerModel    = GAMMA::getModel('customer');
    
    if($statid == NULL){
      $btn= $this->input->post('btn');
      $statid= $this->input->post('statid');
    }

      $data['menuid'] = 12;

      if($btn == "book"){
        $datadb['booking_id'] = date('hmsYdm');
        $datadb['title']      = $datadb['booking_id'];
        $datadb['start']      = $this->input->post('startdate');
        $datadb['day']        = $this->input->post('day');
        $day                  = $datadb['day'] - 1;
        $datadb['end_date']   = date('Y-m-d', strtotime($datadb['start']. ' + '.$day.' days'));
        $datadb['ownerid']    = $this->input->post('ownerid');
        $datadb['homestayid'] = $this->input->post('homestayid');
        $datadb['customer_name']  = $this->input->post('custname');
        $datadb['customer_phone'] = $this->input->post('custphone');
        $datadb['created_by'] =  GAMMA::getSession('id');
        $datadb['amount']     = $this->input->post('amount');
        $datadb['total']      = $datadb['amount']*$datadb['day'];
        $datadb['current_payment'] = $this->input->post('deposit');
        if($this->input->post('deposit') > 0 && $this->input->post('deposit') < $datadb['total']){
          $datadb['status']   = 1;
        }else if($this->input->post('deposit') > 0 && $this->input->post('deposit') == $datadb['total']){
          $datadb['status']   = 2;
        }
        $datadb['balance_payment'] = $datadb['total'] - $datadb['current_payment'] ;
       
        
        $checkbooking              = $bookingModel->checkbooking($datadb['homestayid'],$datadb['start'],$datadb['end_date']);
        // print_r($checkbooking);exit;

        if(empty($checkbooking)){
          $result                    = $bookingModel->add($datadb);
        
          if($result > 0){
            $datadb2['bookingid']       = $result;
            $datadb2['date']            = date('Y-m-d');
            $datadb2['description']     = 'Deposit' ;
            $datadb2['homestayid']      = $this->input->post('homestayid');
            $datadb2['amount']          = $this->input->post('deposit');

            $result                    = $paymentModel->add($datadb2);

            $checkcustomer             = $customerModel->selectOne(array('customer_phone' => $datadb['customer_phone']));
            if(empty($checkcustomer)){
              $datadb3['ownerid']        = $datadb['ownerid'];
              $datadb3['customer_name']  = $datadb['customer_name'];
              $datadb3['customer_phone'] = $datadb['customer_phone'];
              $result                    = $customerModel->add($datadb3);
            }
            echo '<script>alert("Successful")</script>';
            $script = "<script>
                window.location = '".base_url()."homestaydetail/".$datadb['homestayid']."/".$datadb['ownerid']."';</script>";
            echo $script;
            // redirect('homestaydetail/'.$datadb['homestayid'].'/'.$datadb['ownerid']);
           
          }else{
            redirect('homestaydetail/'.$datadb['homestayid'].'/'.$datadb['ownerid']);
          }
        }else{
          echo '<script>alert("Sorry date is not available")</script>';
            $script = "<script>
                window.location = '".base_url()."homestaydetail/".$datadb['homestayid']."/".$datadb['ownerid']."';</script>";
          echo $script;

        }
        

      }else{
        echo $btn;
      }

      
} 



}
