<?php
class Sale extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function listsale($prodid,$rndid){
    $saleModel          = GAMMA::getModel('sale');
    $userModel          = GAMMA::getModel('user');
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');
    
    $key                ="report";
    $data['prodid']     = $prodid;
    $data['rndid']      = $rndid;
    $data['setting']    = $siteModel->all();
    $roleid             =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['saleinfo']   = $saleModel->listsale($prodid);    
    $data['menuname']   = 'Sale Information';
    $data['main_content'] = 'Sale/sale_view';

    $this->load->view('mainPage',$data);
  }

  public function saleaction(){

    $saleModel            = GAMMA::getModel('sale');
    $btn                  = $this->input->post('btn');

    $oid                  = $this->input->post('rndid');
    $hid                  = $this->input->post('prodid');
    $datadb['prodid']     = $hid;
    $datadb['rndid']      = $oid;
    $datadb['date']       = $this->input->post('date');
    $datadb['sale_id']    = uniqid();
    $datadb['title']      = $datadb['sale_id'];
    
    $datadb['customer_name']    = $this->input->post('custname');
    $datadb['customer_phone']   = $this->input->post('custphone');
    $datadb['customer_address'] = $this->input->post('custaddress');
    $datadb['customer_city']    = $this->input->post('custcity');
    $datadb['customer_poscode'] = $this->input->post('custposcode');
    $datadb['state']            = $this->input->post('state');

    $datadb['created_by'] =  GAMMA::getSession('id');
    $datadb['status']     = 2;
    
    if (strpos($_SERVER['HTTP_HOST'], 'mastersentral.com') !== false) {
      $datadb['sync']     = 1;
    }

    if($btn == "edit"){
      $id                   = $this->input->post('id');
      $datadb2['date']       = $this->input->post('edate');
      $datadb2['description']= $this->input->post('edescription');
      $datadb2['unit_price'] = $this->input->post('eunitprice');
      $datadb2['quantity']   = $this->input->post('equantity');
      $datadb2['unit']       = $this->input->post('eunit');
      $datadb2['amount']     = $this->input->post('eamount');
      $datadb2['total']      = $datadb2['unit_price']*$datadb2['quantity'];
      $datadb2['status']     = $this->input->post('estatus');
      $datadb2['remarks']    = $this->input->post('eremark');
      $result                = $rndcostModel->update($datadb2,array('id' => $id));
    }else{
      $result                = $saleModel->add($datadb);
    }
    if($result > 0){
      redirect('rndcost/'.$hid.'/'.$oid);
    }
      
  }

  public function edititm($id){
    $rndstockModel  = GAMMA::getModel('rndstock');
    $result         = $rndstockModel->selectOne(array('id' => $id));
    
    echo json_encode($result);
  }

  public function deleteitm($id){
      $rndstockModel  = GAMMA::getModel('rndstock');
      $rndcostModel   = GAMMA::getModel('rndcost');

      $costdata   = $rndcostModel->selectOne(array('id' => $id));
      $stockid    = $costdata['stock_id'];
      $stockdata  = $rndstockModel->selectOne(array('id' => $stockid ));
      // print_r($stockdata['balance']+$costdata['amount']);exit;
      $result = $rndcostModel->delete(array('id' => $id));
      
      if($result){
        $datadb3['balance']   = $stockdata['balance']+$costdata['amount'];
        $rndstockModel->update($datadb3,array('id' => $stockid));
        echo "Successfully deleted !";
      }else{
        echo "Failed !";
      }
  }
      
}
