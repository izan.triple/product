<?php
class Merchant extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('menu_model');
    $this->load->model('dashboard_model');
    $this->load->model('Merchant_model');
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }

  

public function merchantIndex()
{
  $key="personnel";
  $mid = $this->menu_model->getid($key);
  $query = 'a.ID,DateSubmit,RegName,RegState,name';
  $roleid= $this->session->userdata('role');
  $bank= $this->session->userdata('bank');
  $data['search'] = '';
  $data['menuid'] = $mid;
  $data['bank'] = $bank;
  $data['menuname'] = 'Merchant Information';
  $data['access'] = $this->menu_model->get_access($roleid,$mid);
  $data['liststatus'] = $this->Merchant_model->getstatus();
  $data['listmerchant'] = 0;
//   $data['listmerchant'] = $this->Merchant_model->getmerchant($query);
  $data['main_content'] = 'Merchant/merchant_view';
  $this->load->view('mainPage',$data);
}



public function updateview($id)
{
  redirect('merchantupdate/'.$id);
  exit;
}

public function payview($id,$stat,$search)
{

  $key="merchant";
  $mid = $this->menu_model->getid($key);
  $roleid= $this->session->userdata('role');
  $bank= $this->session->userdata('bank');
  $data['menuid'] = $mid;
  $data['stat'] = $stat;
  $data['search'] = $search;
  $data['menuname'] = 'Payment Information';
  $data['access'] = $this->menu_model->get_access($roleid,$mid);
  if($bank == 'hlbb'){

  }else if($bank == 'rhb'){
    $data['listpay'] = $this->Merchant_model->get_paymentrhb($id);
  }
  
  $data['main_content'] = 'Merchant/payment_view';
  $this->load->view('mainPage',$data);
  
}

public function historyview($id,$stat,$search)
{

  $key="merchant";
  $mid = $this->menu_model->getid($key);
  $roleid= $this->session->userdata('role');
  $bank= $this->session->userdata('bank');
  $data['menuid'] = $mid;
  $data['stat'] = $stat;
  $data['search'] = $search;
  $data['menuname'] = 'History Information';
  $data['access'] = $this->menu_model->get_access($roleid,$mid);

  if($bank == 'hlbb'){
    $data['listhistory'] = $this->Merchant_model->get_history($id);
  }else if($bank == 'ghl'){
    $data['listhistory'] = $this->Merchant_model->get_historyghl($id);
  }else if($bank == 'rhb'){
    $data['listhistory'] = $this->Merchant_model->get_historyrhb($id);
  }else if($bank == 'ambank'){
    $data['listhistory'] = $this->Merchant_model->get_historyambank($id);
  }

  $data['main_content'] = 'Merchant/history_view';
  $this->load->view('mainPage',$data);
  
}

public function editMerchantIndex($id,$stat,$search)
{
  $key="merchant";
  $mid = $this->menu_model->getid($key);
  $roleid= $this->session->userdata('role');
  $bank= $this->session->userdata('bank');

  if($search == ''){
    $search = "null";
    $data['search'] = '';
  }else{
    $data['search'] = $search;
  }
 
  
  $data['stat'] = $stat;
  $data['menuid'] = $mid;
  $data['menuname'] = 'Merchant Information';
  $data['access'] = $this->menu_model->get_access($roleid,$mid);
  $data['listmerchant'] = 0;
  $data['state'] = $this->Merchant_model->liststate();
//   $data['listmerchant'] = $this->Merchant_model->getmerchant($query);
  if($bank == 'hlbb'){
    $data['merchantdata'] = $this->Merchant_model->getmerchantinfo($id);
    $data['main_content'] = 'Merchant/editmerchant_view';
  }else if($bank == 'ghl'){
    $data['merchantdata'] = $this->Merchant_model->getmerchantinfoghl($id);
    $data['main_content'] = 'Merchant/editmerchantghl_view';
  }else if($bank == 'rhb'){
    $data['merchantdata'] = $this->Merchant_model->getmerchantinforhb($id);
    $data['main_content'] = 'Merchant/editmerchantrhb_view';
  }else if($bank == 'ambank'){
    $data['merchantdata'] = $this->Merchant_model->getmerchantinfoambank($id);
    $data['main_content'] = 'Merchant/editmerchantambank_view';
  }
  
  $this->load->view('mainPage',$data);
}


// public function merchantInfo()
// {
//   $btn= $this->input->post('btn');
// }


public function merchantInfo($btn=NULL,$statid=NULL,$searchname=NULL)
{

  if($statid == NULL){
    $btn= $this->input->post('btn');
  }

  if($statid == NULL){
    $statid= $this->input->post('statid');
  }

  if($searchname == NULL){
    $searchname= $this->input->post('search');
  }else if($searchname == 'null'){
    $searchname= '';
  }
  
  
  // $searchname= $this->input->post('search');
  $bank= $this->session->userdata('bank');
  $data['search'] = $searchname;
  $data['bank'] = $bank;
  $data['state'] = $this->Merchant_model->liststate();
  $data['menuid'] = 12;

  if($btn == "search" ||  $btn == "back"){
	$data['menuname'] = 'Merchant Information';
	$query = 'a.ID,DateSubmit,RefNo,RegName,RegState,b.name';
	if($statid){
		$data['statid'] = $statid;
	}else{
		$data['statid'] = 0;
  }
  	
  $data['liststatus'] = $this->Merchant_model->getstatus();
  
  if($bank == "hlbb"){
    $data['listmerchant'] = $this->Merchant_model->getmerchantgroup($query,$statid,$searchname);
  }else if($bank == "ghl"){
    $data['listmerchant'] = $this->Merchant_model->getmerchantgroupghl($query,$statid,$searchname);
  }else if($bank == "rhb"){
    $data['listmerchant'] = $this->Merchant_model->getmerchantgrouprhb($query,$statid,$searchname);
  }else if($bank == "ambank"){
    $data['listmerchant'] = $this->Merchant_model->getmerchantgroupambank($query,$statid,$searchname);
  }

	 $data['main_content'] = 'Merchant/merchant_view';
    $this->load->view('mainPage',$data);

  }else if($btn == "add"){
    $data['menuname'] = 'New Merchant';
    
    if($bank == "hlbb"){
      $data['staff'] = $this->Merchant_model->liststaff();
      $data['main_content'] = 'Merchant/addmerchant_view';
    }else if($bank == "ghl"){
      $data['staff'] = $this->Merchant_model->liststaffghl();
      $data['main_content'] = 'Merchant/addmerchantghl_view';
    }else if($bank == "rhb"){
      $data['staff'] = $this->Merchant_model->liststaffrhb();
      $data['main_content'] = 'Merchant/addmerchantrhb_view';
    }else if($bank == "ambank"){
      $data['staff'] = $this->Merchant_model->liststaffambank();
      $data['main_content'] = 'Merchant/addmerchantambank_view';
    }
    
    $this->load->view('mainPage',$data);

  }else if($btn == "csv"){
      // output headers so that the file is downloaded rather than displayed
      header('Content-type: text/csv');
      header('Content-Disposition: attachment; filename= mechantreport'.date('Ymd').".csv");

      // do not cache the file
      header('Pragma: no-cache');
      header('Expires: 0');

      // create a file pointer connected to the output stream
      $file = fopen('php://output', 'w');

      // send the column headers
      fputcsv($file, array('Submission Date', 'Merchant Name', 'Location', 'Status'));

      // Sample data. This can be fetched from mysql too
      $data['menuname'] = 'Merchant Information';
      $query = 'DateSubmit,RegName,RegState,b.name';

      if($bank == "hlbb"){
        $data = $this->Merchant_model->getmerchantgroup($query,$statid,$searchname);
      }else if($bank == "rhb"){
        $data = $this->Merchant_model->getmerchantgrouprhb($query,$statid,$searchname);
      }else if($bank == "ambank"){
        $data = $this->Merchant_model->getmerchantgroupambank($query,$statid,$searchname);
      }

      // output each row of the data
      foreach ($data as $row)
      {
        if( is_object($row) )
        $row = (array) $row;
        fputcsv($file, $row);

      }

      exit();

  }else if($btn == "pdf"){

	$data['menuname'] = 'Merchant Information';
	$query = 'DateSubmit,RegName,RegState,b.name';
  $data['liststatus'] = $this->Merchant_model->getstatus();
  
  if($bank == "hlbb"){
    $data['listmerchant'] = $this->Merchant_model->getmerchantgroup($query,$statid,$searchname);
  }else if($bank == "rhb"){
    $data['listmerchant'] = $this->Merchant_model->getmerchantgrouprhb($query,$statid,$searchname);
  }else if($bank == "ambank"){
    $data['listmerchant'] = $this->Merchant_model->getmerchantgroupambank($query,$statid,$searchname);
  }

  $loaded_html = $this->load->view('Merchant/merchanttemplate_view', $data, true);
	$filename='mechantreport'.date('Ymd').'.pdf';
	$this->load->library('pdf'); 
	$this->dompdf->loadHtml($loaded_html);
	$this->dompdf->setPaper('A4', 'landscape');
	$this->dompdf->render();
	$this->dompdf->stream($filename);

}else{
  echo $btn;
}

}


public function submitmerchant()
{
  $bank= $this->session->userdata('bank');
  
  $data['bank'] = $bank;

  if($bank == 'hlbb'){
    $datadb['RefNo']= $this->input->post('refno');
    $datadb['chain']= $this->input->post('chain');
    $datadb['StaffCode']= $this->input->post('staffcode');

    //
    $datadb['RegName']= $this->input->post('regname');
    $datadb['RegNo']= $this->input->post('regno');
    $datadb['RegAddress1']= $this->input->post('regadd1');
    $datadb['RegPostCode']= $this->input->post('regpostcode');
    $datadb['RegAddress2']= $this->input->post('regadd2');
    $datadb['RegCity']= $this->input->post('regcity');
    $datadb['RegState']= $this->input->post('regstate');
    $datadb['RegTel']= $this->input->post('regtel');
    $datadb['RegEmail']= $this->input->post('regemail');
    $datadb['RegFax']= $this->input->post('regfax');

    //trades info
    $datadb['TradeName']= $this->input->post('tradename');
    $datadb['TradeAddress1']= $this->input->post('tradeadd1');
    $datadb['TradePostCode']= $this->input->post('tradepostcode');
    $datadb['TradeAddress2']= $this->input->post('tradeadd2');
    $datadb['TradeCity']= $this->input->post('tradecity');
    $datadb['TradeState']= $this->input->post('tradestate');
    $datadb['TradeTel']= $this->input->post('tradetel');
    $datadb['TradeFax']= $this->input->post('tradefax');
    $datadb['IC']= $this->input->post('IC');
    $datadb['ContactPerson']= $this->input->post('contactperson');
    $datadb['TradeHP1']= $this->input->post('tradehp1');
    $datadb['Designation']= $this->input->post('designation');
    $datadb['TradeHP2']= $this->input->post('tradehp2');

    //biz info
    $datadb['BizType']= $this->input->post('biztype');
    $datadb['BizYear']= $this->input->post('bizyear');
    $datadb['BizTypeOther']= $this->input->post('biztypeother');
    $datadb['BizHourStart']= $this->input->post('bizstart');
    $datadb['BizHourEnd']= $this->input->post('bizend');
    $datadb['SalesTurnover']= $this->input->post('bizsales');
    $datadb['TotalOutlet']= $this->input->post('bizoutlet');
    $datadb['CardTurnover']= $this->input->post('bizcard');
    $datadb['TotalTerminal']= $this->input->post('bizterminal');
    $datadb['BizNature']= $this->input->post('biznature');
    $datadb['BizHotSpot']= $this->input->post('bizhotspot');
    $datadb['BizRemark']= $this->input->post('bizremark');

    //Payment details
    $datadb['Bank']= $this->input->post('paymethod');
    $datadb['Branch']= $this->input->post('paybranch');
    $datadb['BankOther']= $this->input->post('payotherbank');
    $datadb['AccountNo']= $this->input->post('payaccount');
    $datadb['PayRemark']= $this->input->post('payremark');

    //Recommendation
    $datadb['RecMonth6']= $this->input->post('RecMonth6');
    $datadb['RecMonthRate6']= $this->input->post('RecMonthRate6');
    $datadb['RecMonth12']= $this->input->post('RecMonth12');
    $datadb['RecMonthRate12']= $this->input->post('RecMonthRate12');
    $datadb['RecMonth18']= $this->input->post('RecMonth18');
    $datadb['RecMonthRate18']= $this->input->post('RecMonthRate18');
    $datadb['RecMonth24']= $this->input->post('RecMonth24');
    $datadb['RecMonthRate24']= $this->input->post('RecMonthRate24');
    $datadb['RecMonth36']= $this->input->post('RecMonth36');
    $datadb['RecMonthRate36']= $this->input->post('RecMonthRate36');
    $datadb['RecRemark']= $this->input->post('recRemark');
    $datadb['Rental']= $this->input->post('recrental');
    $datadb['VisaMaster']= $this->input->post('recvisamc');
    $datadb['Debit']= $this->input->post('recdebit');


    //Competitor info
    $datadb['RivalBank']= $this->input->post('combank');
    $datadb['RivalRental']= $this->input->post('comrental');
    $datadb['RivalRemark']= $this->input->post('comremark');
    $datadb['RivalMDR']= $this->input->post('commdr');

    //Bank offer term
    $datadb['MIDCredit']= $this->input->post('bankcredit');
    $datadb['BankVisaMaster']= $this->input->post('bankVisaMaster');
    $datadb['MIDZIIP']= $this->input->post('MIDZIIP');
    $datadb['BankDebit']= $this->input->post('bankmdrdebit');
    $datadb['MIDDebit']= $this->input->post('bankdebit');
    $datadb['Tlimit']= $this->input->post('tlimit');
    $datadb['TerminalID']= $this->input->post('bankterminal');
    $datadb['Mlimit']= $this->input->post('mlimit');
    $datadb['TerminalIDZIIP']= $this->input->post('TerminalIDZIIP');
    $datadb['TIDDebit']= $this->input->post('terminaldebit');
    $datadb['MCC']= $this->input->post('bankMCC');
    $datadb['BankRemark']= $this->input->post('bankremark');


    //Equipment & installation
    $datadb['EDCType']= $this->input->post('edctype');
    $datadb['DateOpenCall']= $this->input->post('opencalldate');
    $datadb['EDCSerialNo']= $this->input->post('edcsno');
    $datadb['SiteID']= $this->input->post('siteid');
    $datadb['SAMSerialNo']= $this->input->post('samsno');
    $datadb['DateInstall']= $this->input->post('installdate');
    $datadb['InstallRemark']= $this->input->post('edcremark');


    //Termination
    $datadb['DateTerminated']= $this->input->post('terminatedate');
    $datadb['Refund']= $this->input->post('refund');
    $datadb['DateNotice']= $this->input->post('noticedate');
    $datadb['HandTo']= $this->input->post('handto');
    $datadb['TerminateReason']= $this->input->post('terminatereason');
    $datadb['ChqNo']= $this->input->post('chqno');
    $datadb['TerminateRemark']= $this->input->post('terminateremark');


    //DPS Charges
    $datadb['DPSRental']= $this->input->post('dpsrental');
    $datadb['DateRental']= $this->input->post('dpsrentaldate');
    $datadb['Deposit']= $this->input->post('dpsdeposit');
    $datadb['Rebate']= $this->input->post('dpsrebate');
    $datadb['dpsProcess']= $this->input->post('dpsprocess');
    $datadb['DirecDebit']= $this->input->post('dpsdd');
    $datadb['ReceivedChqDate']= $this->input->post('dpschqdate');
    $datadb['DDDate']= $this->input->post('dpsdddate');
    $datadb['ReceivedChqNo']= $this->input->post('dpschqno');
    $datadb['CurrentAcNo']= $this->input->post('dpsddaccount');
    $datadb['ReceivedChqTotal']= $this->input->post('dpschqamt');
    $datadb['DDTotal']= $this->input->post('dpsddamt');
    $datadb['DDRemark']= $this->input->post('dpsremark');
    $datadb['DDFreq']= $this->input->post('dpsddfreq');
    $datadb['DDEPICDate']= $this->input->post('dpsddepic');

    //Application Status
    $datadb['DateSubmit']= $this->input->post('appsubmit');
    $datadb['DateDue']= $this->input->post('appdue');
    $datadb['DateReceivedStatus']= $this->input->post('appreceivestatus');
    $datadb['DateAppeal']= $this->input->post('appappeal');
    $datadb['DateReceivedLO']= $this->input->post('appreceivelo');
    $datadb['StatusRemark']= $this->input->post('appremark');
    $datadb['Status ']= $this->input->post('appstatus');


    $result = $this->Merchant_model->insert_merchant($datadb);
    $this->trails($result,"Create New Merchant");
  }else if($bank == 'ghl'){
    $datadb['RefNo']= $this->input->post('refno');
    $datadb['chain']= $this->input->post('chain');
    $datadb['StaffCode']= $this->input->post('staffcode');

    //
    $datadb['RegName']= $this->input->post('regname');
    $datadb['RegNo']= $this->input->post('regno');
    $datadb['RegAddress1']= $this->input->post('regadd1');
    $datadb['RegPostCode']= $this->input->post('regpostcode');
    $datadb['RegAddress2']= $this->input->post('regadd2');
    $datadb['RegCity']= $this->input->post('regcity');
    $datadb['RegState']= $this->input->post('regstate');
    $datadb['RegTel']= $this->input->post('regtel');
    $datadb['RegEmail']= $this->input->post('regemail');
    $datadb['RegFax']= $this->input->post('regfax');

    //trades info
    $datadb['TradeName']= $this->input->post('tradename');
    $datadb['TradeAddress1']= $this->input->post('tradeadd1');
    $datadb['TradePostCode']= $this->input->post('tradepostcode');
    $datadb['TradeAddress2']= $this->input->post('tradeadd2');
    $datadb['TradeCity']= $this->input->post('tradecity');
    $datadb['TradeState']= $this->input->post('tradestate');
    $datadb['TradeTel']= $this->input->post('tradetel');
    $datadb['TradeFax']= $this->input->post('tradefax');
    $datadb['IC']= $this->input->post('IC');
    $datadb['ContactPerson']= $this->input->post('contactperson');
    $datadb['TradeHP1']= $this->input->post('tradehp1');
    $datadb['Designation']= $this->input->post('designation');
    $datadb['TradeHP2']= $this->input->post('tradehp2');

    //biz info
    $datadb['BizType']= $this->input->post('biztype');
    $datadb['BizYear']= $this->input->post('bizyear');
    $datadb['BizTypeOther']= $this->input->post('biztypeother');
    $datadb['BizHourStart']= $this->input->post('bizstart');
    $datadb['BizHourEnd']= $this->input->post('bizend');
    $datadb['SalesTurnover']= $this->input->post('bizsales');
    $datadb['TotalOutlet']= $this->input->post('bizoutlet');
    $datadb['CardTurnover']= $this->input->post('bizcard');
    $datadb['TotalTerminal']= $this->input->post('bizterminal');
    $datadb['BizNature']= $this->input->post('biznature');
    $datadb['BizHotSpot']= $this->input->post('bizhotspot');
    $datadb['BizRemark']= $this->input->post('bizremark');

    //Payment details
    $datadb['Bank']= $this->input->post('paymethod');
    $datadb['Branch']= $this->input->post('paybranch');
    $datadb['BankOther']= $this->input->post('payotherbank');
    $datadb['AccountNo']= $this->input->post('payaccount');
    $datadb['PayRemark']= $this->input->post('payremark');

    //Recommendation
    $datadb['RecMonth6']= $this->input->post('RecMonth6');
    $datadb['RecMonthRate6']= $this->input->post('RecMonthRate6');
    $datadb['RecMonth12']= $this->input->post('RecMonth12');
    $datadb['RecMonthRate12']= $this->input->post('RecMonthRate12');
    $datadb['RecMonth18']= $this->input->post('RecMonth18');
    $datadb['RecMonthRate18']= $this->input->post('RecMonthRate18');
    $datadb['RecMonth24']= $this->input->post('RecMonth24');
    $datadb['RecMonthRate24']= $this->input->post('RecMonthRate24');
    $datadb['RecMonth36']= $this->input->post('RecMonth36');
    $datadb['RecMonthRate36']= $this->input->post('RecMonthRate36');
    $datadb['RecRemark']= $this->input->post('recRemark');
    $datadb['Rental']= $this->input->post('recrental');
    $datadb['VisaMaster']= $this->input->post('recvisamc');
    $datadb['Debit']= $this->input->post('recdebit');


    //Competitor info
    $datadb['RivalBank']= $this->input->post('combank');
    $datadb['RivalRental']= $this->input->post('comrental');
    $datadb['RivalRemark']= $this->input->post('comremark');
    $datadb['RivalMDR']= $this->input->post('commdr');

    //Bank offer term
    $datadb['MIDCredit']= $this->input->post('bankcredit');
    $datadb['BankVisaMaster']= $this->input->post('bankVisaMaster');
    $datadb['MIDZIIP']= $this->input->post('MIDZIIP');
    $datadb['BankDebit']= $this->input->post('bankmdrdebit');
    $datadb['MIDDebit']= $this->input->post('bankdebit');
    $datadb['Tlimit']= $this->input->post('tlimit');
    $datadb['TerminalID']= $this->input->post('bankterminal');
    $datadb['Mlimit']= $this->input->post('mlimit');
    $datadb['TerminalIDZIIP']= $this->input->post('TerminalIDZIIP');
    $datadb['TIDDebit']= $this->input->post('terminaldebit');
    $datadb['MCC']= $this->input->post('bankMCC');
    $datadb['BankRemark']= $this->input->post('bankremark');

    
    //Equipment & installation
    $datadb['EDCType']= $this->input->post('edctype');
    $datadb['DateOpenCall']= $this->input->post('opencalldate');
    $datadb['EDCSerialNo']= $this->input->post('edcsno');
    $datadb['SiteID']= $this->input->post('siteid');
    $datadb['SAMSerialNo']= $this->input->post('samsno');
    $datadb['DateInstall']= $this->input->post('installdate');
    $datadb['InstallRemark']= $this->input->post('edcremark');


    //Termination
    $datadb['DateTerminated']= $this->input->post('terminatedate');
    $datadb['Refund']= $this->input->post('refund');
    $datadb['DateNotice']= $this->input->post('noticedate');
    $datadb['HandTo']= $this->input->post('handto');
    $datadb['TerminateReason']= $this->input->post('terminatereason');
    $datadb['ChqNo']= $this->input->post('chqno');
    $datadb['TerminateRemark']= $this->input->post('terminateremark');


    //DPS Charges
    $datadb['DPSRental']= $this->input->post('dpsrental');
    $datadb['DateRental']= $this->input->post('dpsrentaldate');
    $datadb['Deposit']= $this->input->post('dpsdeposit');
    $datadb['Rebate']= $this->input->post('dpsrebate');
    $datadb['dpsProcess']= $this->input->post('dpsprocess');
    $datadb['DirecDebit']= $this->input->post('dpsdd');
    $datadb['ReceivedChqDate']= $this->input->post('dpschqdate');
    $datadb['DDDate']= $this->input->post('dpsdddate');
    $datadb['ReceivedChqNo']= $this->input->post('dpschqno');
    $datadb['CurrentAcNo']= $this->input->post('dpsddaccount');
    $datadb['ReceivedChqTotal']= $this->input->post('dpschqamt');
    $datadb['DDTotal']= $this->input->post('dpsddamt');
    $datadb['DDRemark']= $this->input->post('dpsremark');
    $datadb['DDFreq']= $this->input->post('dpsddfreq');
    $datadb['DDEPICDate']= $this->input->post('dpsddepic');

    //Application Status
    $datadb['DateSubmit']= $this->input->post('appsubmit');
    $datadb['DateDue']= $this->input->post('appdue');
    $datadb['DateReceivedStatus']= $this->input->post('appreceivestatus');
    $datadb['DateAppeal']= $this->input->post('appappeal');
    $datadb['DateReceivedLO']= $this->input->post('appreceivelo');
    $datadb['StatusRemark']= $this->input->post('appremark');
    $datadb['Status ']= $this->input->post('appstatus');

    
    $result = $this->Merchant_model->insert_merchantghl($datadb);
    $this->trails($result,"Create New Merchant");
  }else if($bank == 'rhb'){

    //application info
    $datadb2['RefNo']= $this->input->post('refno');
    $datadb2['StaffCode']= $this->input->post('staffcode');
    $datadb2['DateSubmit']= $this->input->post('appsubmit');
    $datadb2['DateDue']= $this->input->post('appdue');
    $datadb2['DateAppeal']= $this->input->post('appappeal');
    $datadb2['DateReceivedLO']= $this->input->post('appreceivelo');

    //business info
    $datadb2['RegName']= $this->input->post('regname');
    $datadb2['RegNo']= $this->input->post('regno');
    $datadb2['RegAddress1']= $this->input->post('regadd1');
    $datadb2['BizType']= $this->input->post('biztype');
    $datadb2['RegAddress2']= $this->input->post('regadd2');
    $datadb2['BizNature']= $this->input->post('biznature');
    $datadb2['RegCity']= $this->input->post('regcity');
    $datadb2['MCC']= $this->input->post('bankMCC');
    $datadb2['RegState']= $this->input->post('regstate');
    $datadb2['RegPostCode']= $this->input->post('regpostcode');
    $datadb2['BizYear']= $this->input->post('bizyear');
    $datadb2['RegTel']= $this->input->post('regtel');
    $datadb2['RegFax']= $this->input->post('regfax');
    $datadb2['BizHourStart']= $this->input->post('bizstart');
    $datadb2['BizHourEnd']= $this->input->post('bizend');
    $datadb2['RegEmail']= $this->input->post('regemail');
    $datadb2['ContactPerson']= $this->input->post('contactperson');
    $datadb2['BizTypeOther']= $this->input->post('biztypeother');


    //trade info
    $datadb2['TradeName']= $this->input->post('tradename');
    $datadb2['Designation']= $this->input->post('designation');
    $datadb2['TradeAddress1']= $this->input->post('tradeadd1');
    $datadb2['IC']= $this->input->post('IC');
    $datadb2['TradeAddress2']= $this->input->post('tradeadd2');
    $datadb2['TradeHP1']= $this->input->post('tradehp1');
    $datadb2['TradeCity']= $this->input->post('tradecity');
    $datadb2['TradeState']= $this->input->post('tradestate');
    $datadb2['TradePostCode']= $this->input->post('tradepostcode');
    $datadb2['BizHotSpot']= $this->input->post('bizhotspot');
    $datadb2['TradeTel']= $this->input->post('tradetel');
    $datadb2['TradeHP2']= $this->input->post('tradehp2');
    $datadb2['TradeFax']= $this->input->post('tradefax');
    $datadb2['BizRemark']= $this->input->post('bizremark');


    //trasaction details
    $datadb2['Bank']= $this->input->post('paymethod');
    $datadb2['Branch']= $this->input->post('paybranch');
    $datadb2['AccountNo']= $this->input->post('payaccount');
    $datadb2['PayRemark']= $this->input->post('payremark');


    //Bank facilities
    $datadb2['VisaMaster']= $this->input->post('recvisamc');
    $datadb2['Debit']= $this->input->post('recdebit');
    $datadb2['Rental']= $this->input->post('recrental');
    $datadb2['RecMonthRate3']= $this->input->post('RecMonthRate3');
    $datadb2['RecMonthRate6']= $this->input->post('RecMonthRate6');
    $datadb2['RecMonthRate12']= $this->input->post('RecMonthRate12');
    $datadb2['RecMonthRate18']= $this->input->post('RecMonthRate18');
    $datadb2['RecMonthRate24']= $this->input->post('RecMonthRate24');
    $datadb2['RecMonthRate30']= $this->input->post('RecMonthRate30');
    $datadb2['RecMonthRate36']= $this->input->post('RecMonthRate36');
    $datadb2['RecMonthRate48']= $this->input->post('RecMonthRate48');
    $datadb2['MIDCredit']= $this->input->post('bankcredit');
    $datadb2['TerminalID']= $this->input->post('bankterminal');
    $datadb2['MIDDebit']= $this->input->post('bankdebit');
    $datadb2['TIDDebit']= $this->input->post('terminaldebit');
    $datadb2['EPP3Mid']= $this->input->post('mid3epp');
    $datadb2['EPP6Mid']= $this->input->post('mid6epp');
    $datadb2['EPP12Mid']= $this->input->post('mid12epp');
    $datadb2['EPP18Mid']= $this->input->post('mid18epp');
    $datadb2['EPP24Mid']= $this->input->post('mid24epp');
    $datadb2['EPP30Mid']= $this->input->post('mid30epp');
    $datadb2['EPP36Mid']= $this->input->post('mid36epp');
    $datadb2['EPP48Mid']= $this->input->post('mid48epp');
    $datadb2['EPP3Tid']= $this->input->post('tid3epp');
    $datadb2['EPP6Tid']= $this->input->post('tid6epp');
    $datadb2['EPP12Tid']= $this->input->post('tid12epp');
    $datadb2['EPP18Tid']= $this->input->post('tid18epp');
    $datadb2['EPP24Tid']= $this->input->post('tid24epp');
    $datadb2['EPP30Tid']= $this->input->post('tid30epp');
    $datadb2['EPP36Tid']= $this->input->post('tid36epp');
    $datadb2['EPP48Tid']= $this->input->post('tid48epp');
    $datadb2['Tipadj']= $this->input->post('tipsadj');
    $datadb2['ONthered']= $this->input->post('onthespot');
    $datadb2['CVV2']= $this->input->post('cvv2fun');
    $datadb2['MagS']= $this->input->post('magstripe');
    $datadb2['OnlineReg']= $this->input->post('onlinereg');
    $datadb2['AMEX']= $this->input->post('amex');
    $datadb2['RecRemark']= $this->input->post('recRemark');
    

    //Equipment
    $datadb2['EDCType']= $this->input->post('edctype');
    $datadb2['DateOpenCall']= $this->input->post('opencalldate');
    $datadb2['EDCSerialNo']= $this->input->post('edcsno');
    $datadb2['SiteID']= $this->input->post('siteid');
    $datadb2['SAMSerialNo']= $this->input->post('samsno');
    $datadb2['DateInstall']= $this->input->post('installdate');
    $datadb2['StatusRemark']= $this->input->post('appremark');
    $datadb2['InstallRemark']= $this->input->post('edcremark');


    //Terminal setting
    $datadb2['TerConMode']= $this->input->post('terconmode');
    $datadb2['PANMas']= $this->input->post('panmask');
    $datadb2['ManSettle']= $this->input->post('manialset');
    $datadb2['AutoSett']= $this->input->post('autoset');
    $datadb2['PreAuthF']= $this->input->post('preauth');
    $datadb2['OffFun']= $this->input->post('offlinef');
    $datadb2['ManEnFun']= $this->input->post('manentryf');
    $datadb2['CashAdvF']= $this->input->post('cashadvf');
    $datadb2['TermMode']= $this->input->post('termodel');
    $datadb2['ForceS']= $this->input->post('forceset');
    $datadb2['NACloc']= $this->input->post('nacloc');
    $datadb2['PrNACNo']= $this->input->post('primnac');
    $datadb2['TerSofApp']= $this->input->post('termsofapp');
    $datadb2['SeNACNo']= $this->input->post('secnac');

    //Terminal setting

    $datadb2['SWdate']= $this->input->post('SWdate');
    $datadb2['SAMsn']= $this->input->post('SAMsn');
    $datadb2['EDCsn']= $this->input->post('EDCsn');
    $datadb2['SWjs']= $this->input->post('SWjs');
    $datadb2['SWdate1']= $this->input->post('SWdate1');
    $datadb2['SAMsn1']= $this->input->post('SAMsn1');
    $datadb2['EDCsn1']= $this->input->post('EDCsn1');
    $datadb2['SWjs1']= $this->input->post('SWjs1');
    $datadb2['SWdate2']= $this->input->post('SWdate2');
    $datadb2['SAMsn2']= $this->input->post('SAMsn2');
    $datadb2['EDCsn2']= $this->input->post('EDCsn2');
    $datadb2['SWjs2']= $this->input->post('SWjs2');
    $datadb2['SWdate3']= $this->input->post('SWdate3');
    $datadb2['SAMsn3']= $this->input->post('SAMsn3');
    $datadb2['EDCsn3']= $this->input->post('EDCsn3');
    $datadb2['SWjs3']= $this->input->post('SWjs3');
    $datadb2['SWdate4']= $this->input->post('SWdate4');
    $datadb2['SAMsn4']= $this->input->post('SAMsn4');
    $datadb2['EDCsn4']= $this->input->post('EDCsn4');
    $datadb2['SWjs4']= $this->input->post('SWjs4');
    $datadb2['SWdate5']= $this->input->post('SWdate5');
    $datadb2['SAMsn5']= $this->input->post('SAMsn5');
    $datadb2['EDCsn5']= $this->input->post('EDCsn5');
    $datadb2['SWjs5']= $this->input->post('SWjs5');
    $datadb2['SWdate6']= $this->input->post('SWdate6');
    $datadb2['SAMsn6']= $this->input->post('SAMsn6');
    $datadb2['EDCsn6']= $this->input->post('EDCsn6');
    $datadb2['SWjs6']= $this->input->post('SWjs6');
    $datadb2['SWdate7']= $this->input->post('SWdate7');
    $datadb2['SAMsn7']= $this->input->post('SAMsn7');
    $datadb2['EDCsn7']= $this->input->post('EDCsn7');
    $datadb2['SWjs7']= $this->input->post('SWjs7');
    $datadb2['SWdate8']= $this->input->post('SWdate8');
    $datadb2['SAMsn8']= $this->input->post('SAMsn8');
    $datadb2['EDCsn8']= $this->input->post('EDCsn8');
    $datadb2['SWjs8']= $this->input->post('SWjs8');
    $datadb2['SWdate9']= $this->input->post('SWdate9');
    $datadb2['SAMsn9']= $this->input->post('SAMsn9');
    $datadb2['EDCsn9']= $this->input->post('EDCsn9');
    $datadb2['SWjs9']= $this->input->post('SWjs9');


    //terminal & retrieval
    $datadb2['DateNotice']= $this->input->post('noticedate');
    $datadb2['DateTerminated']= $this->input->post('terminatedate');
    $datadb2['Refund']= $this->input->post('refund');
    $datadb2['RetDate']= $this->input->post('retdate');
    $datadb2['TerminateReason']= $this->input->post('terminatereason');
    $datadb2['HandTo']= $this->input->post('handto');
    $datadb2['BankRemark']= $this->input->post('bankremark');
    $datadb2['ChqNo']= $this->input->post('chqno');
    $datadb2['TerminalIDZIIP']= $this->input->post('TerminalIDZIIP');
    $datadb2['TerminateRemark']= $this->input->post('terminateremark');

    //DPS charges & Terms
    $datadb2['MonRenAmt']= $this->input->post('monrentamt');
    $datadb2['AdvMonths']= $this->input->post('advmts');
    $datadb2['ConPerMths']= $this->input->post('contraper');
    $datadb2['AmPackage']= $this->input->post('packtype');
    $datadb2['DirecDebit']= $this->input->post('dpsdd');
    $datadb2['ReceivedChqDate']= $this->input->post('dpschqdate');
    $datadb2['ReceivedChqTotal']= $this->input->post('dpschqamt');
    $datadb2['ReceivedChqNo']= $this->input->post('dpschqno');
    $datadb2['ReChqDate1']= $this->input->post('dpschqdate1');
    $datadb2['ReChqTotal1']= $this->input->post('dpschqamt1');
    $datadb2['ReChqNo1']= $this->input->post('dpschqno1');
    $datadb2['ReChqDate2']= $this->input->post('dpschqdate2');
    $datadb2['ReChqTotal2']= $this->input->post('dpschqamt2');
    $datadb2['ReChqNo2']= $this->input->post('dpschqno2');
    $datadb2['TotRental']= $this->input->post('totalren');
    $datadb2['Deposit']= $this->input->post('dpsdeposit');
    $datadb2['DPSProcess']= $this->input->post('dpsprocess');
    $datadb2['GrandTot']= $this->input->post('grandtt');
    $datadb2['DDRemark']= $this->input->post('dpsremark');


    $datadb2['Status']= $this->input->post('appstatus');
    $datadb2['DDFreq']= $this->input->post('dpsddfreq');

    $result = $this->Merchant_model->insert_merchantrhb($datadb2);
    $this->trails($result,"Create New Merchant");
    
  }else if($bank == 'ambank'){

    //application info
    $datadb2['RefNo']= $this->input->post('refno');
    $datadb2['StaffCode']= $this->input->post('staffcode');
    $datadb2['DateSubmit']= $this->input->post('appsubmit');
    $datadb2['DateDue']= $this->input->post('appdue');
    $datadb2['DateAppeal']= $this->input->post('appappeal');
    $datadb2['DateReceivedLO']= $this->input->post('appreceivelo');

    //business info
    $datadb2['RegName']= $this->input->post('regname');
    $datadb2['RegNo']= $this->input->post('regno');
    $datadb2['RegAddress1']= $this->input->post('regadd1');
    $datadb2['BizType']= $this->input->post('biztype');
    $datadb2['RegAddress2']= $this->input->post('regadd2');
    $datadb2['BizNature']= $this->input->post('biznature');
    $datadb2['RegCity']= $this->input->post('regcity');
    $datadb2['MCC']= $this->input->post('bankMCC');
    $datadb2['RegState']= $this->input->post('regstate');
    $datadb2['RegPostCode']= $this->input->post('regpostcode');
    $datadb2['BizYear']= $this->input->post('bizyear');
    $datadb2['RegTel']= $this->input->post('regtel');
    $datadb2['RegFax']= $this->input->post('regfax');
    $datadb2['BizHourStart']= $this->input->post('bizstart');
    $datadb2['BizHourEnd']= $this->input->post('bizend');
    $datadb2['RegEmail']= $this->input->post('regemail');
    $datadb2['ContactPerson']= $this->input->post('contactperson');
    $datadb2['BizTypeOther']= $this->input->post('biztypeother');


    //trade info
    $datadb2['TradeName']= $this->input->post('tradename');
    $datadb2['Designation']= $this->input->post('designation');
    $datadb2['TradeAddress1']= $this->input->post('tradeadd1');
    $datadb2['IC']= $this->input->post('IC');
    $datadb2['TradeAddress2']= $this->input->post('tradeadd2');
    $datadb2['TradeHP1']= $this->input->post('tradehp1');
    $datadb2['TradeCity']= $this->input->post('tradecity');
    $datadb2['TradeState']= $this->input->post('tradestate');
    $datadb2['TradePostCode']= $this->input->post('tradepostcode');
    $datadb2['BizHotSpot']= $this->input->post('bizhotspot');
    $datadb2['TradeTel']= $this->input->post('tradetel');
    $datadb2['TradeHP2']= $this->input->post('tradehp2');
    $datadb2['TradeFax']= $this->input->post('tradefax');
    $datadb2['BizRemark']= $this->input->post('bizremark');


    //trasaction details
    $datadb2['Bank']= $this->input->post('paymethod');
    $datadb2['Branch']= $this->input->post('paybranch');
    $datadb2['AccountNo']= $this->input->post('payaccount');
    $datadb2['PayRemark']= $this->input->post('payremark');


    //Bank facilities
    $datadb2['VisaMaster']= $this->input->post('recvisamc');
    $datadb2['Debit']= $this->input->post('recdebit');
    $datadb2['Rental']= $this->input->post('recrental');
    $datadb2['RecMonthRate3']= $this->input->post('RecMonthRate3');
    $datadb2['RecMonthRate6']= $this->input->post('RecMonthRate6');
    $datadb2['RecMonthRate12']= $this->input->post('RecMonthRate12');
    $datadb2['RecMonthRate18']= $this->input->post('RecMonthRate18');
    $datadb2['RecMonthRate24']= $this->input->post('RecMonthRate24');
    $datadb2['RecMonthRate30']= $this->input->post('RecMonthRate30');
    $datadb2['RecMonthRate36']= $this->input->post('RecMonthRate36');
    $datadb2['RecMonthRate48']= $this->input->post('RecMonthRate48');
    $datadb2['MIDCredit']= $this->input->post('bankcredit');
    $datadb2['TerminalID']= $this->input->post('bankterminal');
    $datadb2['MIDDebit']= $this->input->post('bankdebit');
    $datadb2['TIDDebit']= $this->input->post('terminaldebit');
    $datadb2['EPP3Mid']= $this->input->post('mid3epp');
    $datadb2['EPP6Mid']= $this->input->post('mid6epp');
    $datadb2['EPP12Mid']= $this->input->post('mid12epp');
    $datadb2['EPP18Mid']= $this->input->post('mid18epp');
    $datadb2['EPP24Mid']= $this->input->post('mid24epp');
    $datadb2['EPP30Mid']= $this->input->post('mid30epp');
    $datadb2['EPP36Mid']= $this->input->post('mid36epp');
    $datadb2['EPP48Mid']= $this->input->post('mid48epp');
    $datadb2['EPP3Tid']= $this->input->post('tid3epp');
    $datadb2['EPP6Tid']= $this->input->post('tid6epp');
    $datadb2['EPP12Tid']= $this->input->post('tid12epp');
    $datadb2['EPP18Tid']= $this->input->post('tid18epp');
    $datadb2['EPP24Tid']= $this->input->post('tid24epp');
    $datadb2['EPP30Tid']= $this->input->post('tid30epp');
    $datadb2['EPP36Tid']= $this->input->post('tid36epp');
    $datadb2['EPP48Tid']= $this->input->post('tid48epp');
    $datadb2['Tipadj']= $this->input->post('tipsadj');
    $datadb2['ONthered']= $this->input->post('onthespot');
    $datadb2['CVV2']= $this->input->post('cvv2fun');
    $datadb2['MagS']= $this->input->post('magstripe');
    $datadb2['OnlineReg']= $this->input->post('onlinereg');
    $datadb2['AMEX']= $this->input->post('amex');
    $datadb2['RecRemark']= $this->input->post('recRemark');
    

    //Equipment
    $datadb2['EDCType']= $this->input->post('edctype');
    $datadb2['DateOpenCall']= $this->input->post('opencalldate');
    $datadb2['EDCSerialNo']= $this->input->post('edcsno');
    $datadb2['SiteID']= $this->input->post('siteid');
    $datadb2['SAMSerialNo']= $this->input->post('samsno');
    $datadb2['DateInstall']= $this->input->post('installdate');
    $datadb2['StatusRemark']= $this->input->post('appremark');
    $datadb2['InstallRemark']= $this->input->post('edcremark');


    //Terminal setting
    $datadb2['TerConMode']= $this->input->post('terconmode');
    $datadb2['PANMas']= $this->input->post('panmask');
    $datadb2['ManSettle']= $this->input->post('manialset');
    $datadb2['AutoSett']= $this->input->post('autoset');
    $datadb2['PreAuthF']= $this->input->post('preauth');
    $datadb2['OffFun']= $this->input->post('offlinef');
    $datadb2['ManEnFun']= $this->input->post('manentryf');
    $datadb2['CashAdvF']= $this->input->post('cashadvf');
    $datadb2['TermMode']= $this->input->post('termodel');
    $datadb2['ForceS']= $this->input->post('forceset');
    $datadb2['NACloc']= $this->input->post('nacloc');
    $datadb2['PrNACNo']= $this->input->post('primnac');
    $datadb2['TerSofApp']= $this->input->post('termsofapp');
    $datadb2['SeNACNo']= $this->input->post('secnac');

    //Terminal setting

    $datadb2['SWdate']= $this->input->post('SWdate');
    $datadb2['SAMsn']= $this->input->post('SAMsn');
    $datadb2['EDCsn']= $this->input->post('EDCsn');
    $datadb2['SWjs']= $this->input->post('SWjs');
    $datadb2['SWdate1']= $this->input->post('SWdate1');
    $datadb2['SAMsn1']= $this->input->post('SAMsn1');
    $datadb2['EDCsn1']= $this->input->post('EDCsn1');
    $datadb2['SWjs1']= $this->input->post('SWjs1');
    $datadb2['SWdate2']= $this->input->post('SWdate2');
    $datadb2['SAMsn2']= $this->input->post('SAMsn2');
    $datadb2['EDCsn2']= $this->input->post('EDCsn2');
    $datadb2['SWjs2']= $this->input->post('SWjs2');
    $datadb2['SWdate3']= $this->input->post('SWdate3');
    $datadb2['SAMsn3']= $this->input->post('SAMsn3');
    $datadb2['EDCsn3']= $this->input->post('EDCsn3');
    $datadb2['SWjs3']= $this->input->post('SWjs3');
    $datadb2['SWdate4']= $this->input->post('SWdate4');
    $datadb2['SAMsn4']= $this->input->post('SAMsn4');
    $datadb2['EDCsn4']= $this->input->post('EDCsn4');
    $datadb2['SWjs4']= $this->input->post('SWjs4');
    $datadb2['SWdate5']= $this->input->post('SWdate5');
    $datadb2['SAMsn5']= $this->input->post('SAMsn5');
    $datadb2['EDCsn5']= $this->input->post('EDCsn5');
    $datadb2['SWjs5']= $this->input->post('SWjs5');
    $datadb2['SWdate6']= $this->input->post('SWdate6');
    $datadb2['SAMsn6']= $this->input->post('SAMsn6');
    $datadb2['EDCsn6']= $this->input->post('EDCsn6');
    $datadb2['SWjs6']= $this->input->post('SWjs6');
    $datadb2['SWdate7']= $this->input->post('SWdate7');
    $datadb2['SAMsn7']= $this->input->post('SAMsn7');
    $datadb2['EDCsn7']= $this->input->post('EDCsn7');
    $datadb2['SWjs7']= $this->input->post('SWjs7');
    $datadb2['SWdate8']= $this->input->post('SWdate8');
    $datadb2['SAMsn8']= $this->input->post('SAMsn8');
    $datadb2['EDCsn8']= $this->input->post('EDCsn8');
    $datadb2['SWjs8']= $this->input->post('SWjs8');
    $datadb2['SWdate9']= $this->input->post('SWdate9');
    $datadb2['SAMsn9']= $this->input->post('SAMsn9');
    $datadb2['EDCsn9']= $this->input->post('EDCsn9');
    $datadb2['SWjs9']= $this->input->post('SWjs9');


    //terminal & retrieval
    $datadb2['DateNotice']= $this->input->post('noticedate');
    $datadb2['DateTerminated']= $this->input->post('terminatedate');
    $datadb2['Refund']= $this->input->post('refund');
    $datadb2['RetDate']= $this->input->post('retdate');
    $datadb2['TerminateReason']= $this->input->post('terminatereason');
    $datadb2['HandTo']= $this->input->post('handto');
    $datadb2['BankRemark']= $this->input->post('bankremark');
    $datadb2['ChqNo']= $this->input->post('chqno');
    $datadb2['TerminalIDZIIP']= $this->input->post('TerminalIDZIIP');
    $datadb2['TerminateRemark']= $this->input->post('terminateremark');

    //DPS charges & Terms
    $datadb2['MonRenAmt']= $this->input->post('monrentamt');
    $datadb2['AdvMonths']= $this->input->post('advmts');
    $datadb2['ConPerMths']= $this->input->post('contraper');
    $datadb2['AmPackage']= $this->input->post('packtype');
    $datadb2['DirecDebit']= $this->input->post('dpsdd');
    $datadb2['ReceivedChqDate']= $this->input->post('dpschqdate');
    $datadb2['ReceivedChqTotal']= $this->input->post('dpschqamt');
    $datadb2['ReceivedChqNo']= $this->input->post('dpschqno');
    $datadb2['ReChqDate1']= $this->input->post('dpschqdate1');
    $datadb2['ReChqTotal1']= $this->input->post('dpschqamt1');
    $datadb2['ReChqNo1']= $this->input->post('dpschqno1');
    $datadb2['ReChqDate2']= $this->input->post('dpschqdate2');
    $datadb2['ReChqTotal2']= $this->input->post('dpschqamt2');
    $datadb2['ReChqNo2']= $this->input->post('dpschqno2');
    $datadb2['TotRental']= $this->input->post('totalren');
    $datadb2['Deposit']= $this->input->post('dpsdeposit');
    $datadb2['DPSProcess']= $this->input->post('dpsprocess');
    $datadb2['GrandTot']= $this->input->post('grandtt');
    $datadb2['DDRemark']= $this->input->post('dpsremark');


    $datadb2['Status']= $this->input->post('appstatus');
    $datadb2['DDFreq']= $this->input->post('dpsddfreq');

    $result = $this->Merchant_model->insert_merchantambank($datadb2);
    $this->trails($result,"Create New Merchant");
    
  }


  if($result){
    echo "<script type='text/javascript'>alert('Succesfully added !'); window.location.href = '".base_url().'merchantupdate/'.$result."';</script>";
  }else{
    echo "<script type='text/javascript'>alert('Failed !')</script>";
  }
}


public function submiteditmerchant()
{
  $id     = $this->input->post('merchantid');
  $statid = $this->input->post('stat');
  $btn    = $this->input->post('btn');
  $bank   = $this->session->userdata('bank');

  if($searchname){
    $searchname= $this->input->post('search');
  }else{
    $searchname= "null";
  }
 
  $data['bank'] = $bank;

if($btn == 'add'){
  if($bank == 'hlbb'){

    $datadb['RefNo']= $this->input->post('refno');
    $datadb['chain']= $this->input->post('chain');
    $datadb['StaffCode']= $this->input->post('staffcode');

    //   //
    $datadb['RegName']= $this->input->post('regname');
    $datadb['RegNo']= $this->input->post('regno');
    $datadb['RegAddress1']= $this->input->post('regadd1');
    $datadb['RegPostCode']= $this->input->post('regpostcode');
    $datadb['RegAddress2']= $this->input->post('regadd2');
    $datadb['RegCity']= $this->input->post('regcity');
    $datadb['RegState']= $this->input->post('regstate');
    $datadb['RegTel']= $this->input->post('regtel');
    $datadb['RegEmail']= $this->input->post('regemail');
    $datadb['RegFax']= $this->input->post('regfax');

    //   //trades info
    $datadb['TradeName']= $this->input->post('tradename');
    $datadb['TradeAddress1']= $this->input->post('tradeadd1');
    $datadb['TradePostCode']= $this->input->post('tradepostcode');
    $datadb['TradeAddress2']= $this->input->post('tradeadd2');
    $datadb['TradeCity']= $this->input->post('tradecity');
    $datadb['TradeState']= $this->input->post('tradestate');
    $datadb['TradeTel']= $this->input->post('tradetel');
    $datadb['TradeFax']= $this->input->post('tradefax');
    $datadb['IC']= $this->input->post('IC');
    $datadb['ContactPerson']= $this->input->post('contactperson');
    $datadb['TradeHP1']= $this->input->post('tradehp1');
    $datadb['Designation']= $this->input->post('designation');
    $datadb['TradeHP2']= $this->input->post('tradehp2');

    //   //biz info
    $datadb['BizType']= $this->input->post('biztype');
    $datadb['BizYear']= $this->input->post('bizyear');
    $datadb['BizTypeOther']= $this->input->post('biztypeother');
    $datadb['BizHourStart']= $this->input->post('bizstart');
    $datadb['BizHourEnd']= $this->input->post('bizend');
    $datadb['SalesTurnover']= $this->input->post('bizsales');
    $datadb['TotalOutlet']= $this->input->post('bizoutlet');
    $datadb['CardTurnover']= $this->input->post('bizcard');
    $datadb['TotalTerminal']= $this->input->post('bizterminal');
    $datadb['BizNature']= $this->input->post('biznature');
    $datadb['BizHotSpot']= $this->input->post('bizhotspot');
    $datadb['BizRemark']= $this->input->post('bizremark');

    //   //Payment details
    $datadb['Bank']= $this->input->post('paymethod');
    $datadb['Branch']= $this->input->post('paybranch');
    $datadb['BankOther']= $this->input->post('payotherbank');
    $datadb['AccountNo']= $this->input->post('payaccount');
    $datadb['PayRemark']= $this->input->post('payremark');

    //   //Recommendation
    $datadb['RecMonth6']= $this->input->post('RecMonth6');
    $datadb['RecMonthRate6']= $this->input->post('RecMonthRate6');
    $datadb['RecMonth12']= $this->input->post('RecMonth12');
    $datadb['RecMonthRate12']= $this->input->post('RecMonthRate12');
    $datadb['RecMonth18']= $this->input->post('RecMonth18');
    $datadb['RecMonthRate18']= $this->input->post('RecMonthRate18');
    $datadb['RecMonth24']= $this->input->post('RecMonth24');
    $datadb['RecMonthRate24']= $this->input->post('RecMonthRate24');
    $datadb['RecMonth36']= $this->input->post('RecMonth36');
    $datadb['RecMonthRate36']= $this->input->post('RecMonthRate36');
    $datadb['RecRemark']= $this->input->post('recRemark');
    $datadb['Rental']= $this->input->post('recrental');
    $datadb['VisaMaster']= $this->input->post('recvisamc');
    $datadb['Debit']= $this->input->post('recdebit');


    //   //Competitor info
    $datadb['RivalBank']= $this->input->post('combank');
    $datadb['RivalRental']= $this->input->post('comrental');
    $datadb['RivalRemark']= $this->input->post('comremark');
    $datadb['RivalMDR']= $this->input->post('commdr');

    //   //Bank offer term
    $datadb['MIDCredit']= $this->input->post('bankcredit');
    $datadb['BankVisaMaster']= $this->input->post('bankVisaMaster');
    $datadb['MIDZIIP']= $this->input->post('MIDZIIP');
    $datadb['BankDebit']= $this->input->post('bankmdrdebit');
    $datadb['MIDDebit']= $this->input->post('bankdebit');
    $datadb['MIDOTSR']= $this->input->post('MIDOTSR');
    $datadb['Tlimit']= $this->input->post('tlimit');
    $datadb['TerminalID']= $this->input->post('bankterminal');
    $datadb['Mlimit']= $this->input->post('mlimit');
    $datadb['TerminalIDZIIP']= $this->input->post('TerminalIDZIIP');
    $datadb['TIDOTSR']= $this->input->post('terminalotsr');
    $datadb['TIDDebit']= $this->input->post('terminaldebit');
    $datadb['MCC']= $this->input->post('bankMCC');
    $datadb['BankRemark']= $this->input->post('bankremark');


    //   //Equipment & installation
    $datadb['EDCType']= $this->input->post('edctype');
    $datadb['DateOpenCall']= $this->input->post('opencalldate');
    $datadb['EDCSerialNo']= $this->input->post('edcsno');
    $datadb['SiteID']= $this->input->post('siteid');
    $datadb['SAMSerialNo']= $this->input->post('samsno');
    $datadb['DateInstall']= $this->input->post('installdate');
    $datadb['InstallRemark']= $this->input->post('edcremark');


    //   //Termination
    $datadb['DateTerminated']= $this->input->post('terminatedate');
    $datadb['Refund']= $this->input->post('refund');
    $datadb['DateNotice']= $this->input->post('noticedate');
    $datadb['HandTo']= $this->input->post('handto');
    $datadb['TerminateReason']= $this->input->post('terminatereason');
    $datadb['ChqNo']= $this->input->post('chqno');
    $datadb['TerminateRemark']= $this->input->post('terminateremark');


    //   //DPS Charges
    $datadb['DPSRental']= $this->input->post('dpsrental');
    $datadb['DateRental']= $this->input->post('dpsrentaldate');
    $datadb['Deposit']= $this->input->post('dpsdeposit');
    $datadb['Rebate']= $this->input->post('dpsrebate');
    $datadb['dpsProcess']= $this->input->post('dpsprocess');
    $datadb['DirecDebit']= $this->input->post('dpsdd');
    $datadb['ReceivedChqDate']= $this->input->post('dpschqdate');
    $datadb['DDDate']= $this->input->post('dpsdddate');
    $datadb['ReceivedChqNo']= $this->input->post('dpschqno');
    $datadb['CurrentAcNo']= $this->input->post('dpsddaccount');
    $datadb['ReceivedChqTotal']= $this->input->post('dpschqamt');
    $datadb['DDTotal']= $this->input->post('dpsddamt');
    $datadb['DDRemark']= $this->input->post('dpsremark');
    $datadb['DDFreq']= $this->input->post('dpsddfreq');
    $datadb['DDEPICDate']= $this->input->post('dpsddepic');

    // //Application Status
    $datadb['DateSubmit']= $this->input->post('appsubmit');
    $datadb['DateDue']= $this->input->post('appdue');
    $datadb['DateReceivedStatus']= $this->input->post('appreceivestatus');
    $datadb['DateAppeal']= $this->input->post('appappeal');
    $datadb['DateReceivedLO']= $this->input->post('appreceivelo');
    $datadb['StatusRemark']= $this->input->post('appremark');
    $datadb['Status']= $this->input->post('appstatus');
    $status = $this->input->post('appstatus');

    $result = $this->Merchant_model->update_merchant($datadb,$id);
    $this->trails($result,"Update(".$status.")");

  }else if($bank == 'ghl'){

    $datadb['RefNo']= $this->input->post('refno');
    $datadb['chain']= $this->input->post('chain');
    $datadb['StaffCode']= $this->input->post('staffcode');
  
    //
    $datadb['RegName']= $this->input->post('regname');
    $datadb['RegNo']= $this->input->post('regno');
    $datadb['RegAddress1']= $this->input->post('regadd1');
    $datadb['RegPostCode']= $this->input->post('regpostcode');
    $datadb['RegAddress2']= $this->input->post('regadd2');
    $datadb['RegCity']= $this->input->post('regcity');
    $datadb['RegState']= $this->input->post('regstate');
    $datadb['RegTel']= $this->input->post('regtel');
    $datadb['RegEmail']= $this->input->post('regemail');
    $datadb['RegFax']= $this->input->post('regfax');
  
    //   //trades info
    $datadb['TradeName']= $this->input->post('tradename');
    $datadb['TradeAddress1']= $this->input->post('tradeadd1');
    $datadb['TradePostCode']= $this->input->post('tradepostcode');
    $datadb['TradeAddress2']= $this->input->post('tradeadd2');
    $datadb['TradeCity']= $this->input->post('tradecity');
    $datadb['TradeState']= $this->input->post('tradestate');
    $datadb['TradeTel']= $this->input->post('tradetel');
    $datadb['TradeFax']= $this->input->post('tradefax');
    $datadb['IC']= $this->input->post('IC');
    $datadb['ContactPerson']= $this->input->post('contactperson');
    $datadb['TradeHP1']= $this->input->post('tradehp1');
    $datadb['Designation']= $this->input->post('designation');
    $datadb['TradeHP2']= $this->input->post('tradehp2');
  
    //   //biz info
    $datadb['BizType']= $this->input->post('biztype');
    $datadb['BizYear']= $this->input->post('bizyear');
    $datadb['BizTypeOther']= $this->input->post('biztypeother');
    $datadb['BizHourStart']= $this->input->post('bizstart');
    $datadb['BizHourEnd']= $this->input->post('bizend');
    $datadb['SalesTurnover']= $this->input->post('bizsales');
    $datadb['TotalOutlet']= $this->input->post('bizoutlet');
    $datadb['CardTurnover']= $this->input->post('bizcard');
    $datadb['TotalTerminal']= $this->input->post('bizterminal');
    $datadb['BizNature']= $this->input->post('biznature');
    $datadb['BizHotSpot']= $this->input->post('bizhotspot');
    $datadb['BizRemark']= $this->input->post('bizremark');
  
    //   //Payment details
    $datadb['Bank']= $this->input->post('paymethod');
    $datadb['Branch']= $this->input->post('paybranch');
    $datadb['BankOther']= $this->input->post('payotherbank');
    $datadb['AccountNo']= $this->input->post('payaccount');
    $datadb['PayRemark']= $this->input->post('payremark');
  
    //   //Recommendation
    $datadb['RecMonth6']= $this->input->post('RecMonth6');
    $datadb['RecMonthRate6']= $this->input->post('RecMonthRate6');
    $datadb['RecMonth12']= $this->input->post('RecMonth12');
    $datadb['RecMonthRate12']= $this->input->post('RecMonthRate12');
    $datadb['RecMonth18']= $this->input->post('RecMonth18');
    $datadb['RecMonthRate18']= $this->input->post('RecMonthRate18');
    $datadb['RecMonth24']= $this->input->post('RecMonth24');
    $datadb['RecMonthRate24']= $this->input->post('RecMonthRate24');
    $datadb['RecMonth36']= $this->input->post('RecMonth36');
    $datadb['RecMonthRate36']= $this->input->post('RecMonthRate36');
    $datadb['RecRemark']= $this->input->post('recRemark');
    $datadb['Rental']= $this->input->post('recrental');
    $datadb['VisaMaster']= $this->input->post('recvisamc');
    $datadb['Debit']= $this->input->post('recdebit');
  
  
     //Competitor info
    $datadb['RivalBank']= $this->input->post('combank');
    $datadb['RivalRental']= $this->input->post('comrental');
    $datadb['RivalRemark']= $this->input->post('comremark');
    $datadb['RivalMDR']= $this->input->post('commdr');
  
     //Bank offer term
    $datadb['MIDCredit']= $this->input->post('bankcredit');
    $datadb['BankVisaMaster']= $this->input->post('bankVisaMaster');
    $datadb['MIDZIIP']= $this->input->post('MIDZIIP');
    $datadb['BankDebit']= $this->input->post('bankmdrdebit');
    $datadb['MIDDebit']= $this->input->post('bankdebit');
    $datadb['MIDOTSR']= $this->input->post('MIDOTSR');
    $datadb['Tlimit']= $this->input->post('tlimit');
    $datadb['TerminalID']= $this->input->post('bankterminal');
    $datadb['Mlimit']= $this->input->post('mlimit');
    $datadb['TerminalIDZIIP']= $this->input->post('TerminalIDZIIP');
    $datadb['TIDOTSR']= $this->input->post('terminalotsr');
    $datadb['TIDDebit']= $this->input->post('terminaldebit');
    $datadb['MCC']= $this->input->post('bankMCC');
    $datadb['BankRemark']= $this->input->post('bankremark');
  
    
     //Equipment & installation
    $datadb['EDCType']= $this->input->post('edctype');
    $datadb['DateOpenCall']= $this->input->post('opencalldate');
    $datadb['EDCSerialNo']= $this->input->post('edcsno');
    $datadb['SiteID']= $this->input->post('siteid');
    $datadb['SAMSerialNo']= $this->input->post('samsno');
    $datadb['DateInstall']= $this->input->post('installdate');
    $datadb['InstallRemark']= $this->input->post('edcremark');
  
  
     //Termination
    $datadb['DateTerminated']= $this->input->post('terminatedate');
    $datadb['Refund']= $this->input->post('refund');
    $datadb['DateNotice']= $this->input->post('noticedate');
    $datadb['HandTo']= $this->input->post('handto');
    $datadb['TerminateReason']= $this->input->post('terminatereason');
    $datadb['ChqNo']= $this->input->post('chqno');
    $datadb['TerminateRemark']= $this->input->post('terminateremark');
  
  
     //DPS Charges
    $datadb['DPSRental']= $this->input->post('dpsrental');
    $datadb['DateRental']= $this->input->post('dpsrentaldate');
    $datadb['Deposit']= $this->input->post('dpsdeposit');
    $datadb['Rebate']= $this->input->post('dpsrebate');
    $datadb['dpsProcess']= $this->input->post('dpsprocess');
    $datadb['DirecDebit']= $this->input->post('dpsdd');
    $datadb['ReceivedChqDate']= $this->input->post('dpschqdate');
    $datadb['DDDate']= $this->input->post('dpsdddate');
    $datadb['ReceivedChqNo']= $this->input->post('dpschqno');
    $datadb['CurrentAcNo']= $this->input->post('dpsddaccount');
    $datadb['ReceivedChqTotal']= $this->input->post('dpschqamt');
    $datadb['DDTotal']= $this->input->post('dpsddamt');
    $datadb['DDRemark']= $this->input->post('dpsremark');
    $datadb['DDFreq']= $this->input->post('dpsddfreq');
    $datadb['DDEPICDate']= $this->input->post('dpsddepic');
  
   //Application Status
    $datadb['DateSubmit']= $this->input->post('appsubmit');
    $datadb['DateDue']= $this->input->post('appdue');
    $datadb['DateReceivedStatus']= $this->input->post('appreceivestatus');
    $datadb['DateAppeal']= $this->input->post('appappeal');
    $datadb['DateReceivedLO']= $this->input->post('appreceivelo');
    $datadb['StatusRemark']= $this->input->post('appremark');
    $datadb['Status']= $this->input->post('appstatus');
    $status = $this->input->post('appstatus');
  
    $result = $this->Merchant_model->update_merchantghl($datadb,$id);
    $this->trails($result,"Update(".$status.")");
  
    }else if($bank == 'rhb'){

    //application info
    $datadb2['RefNo']= $this->input->post('refno');
    $datadb2['StaffCode']= $this->input->post('staffcode');
    $datadb2['DateSubmit']= $this->input->post('appsubmit');
    $datadb2['DateDue']= $this->input->post('appdue');
    $datadb2['DateAppeal']= $this->input->post('appappeal');
    $datadb2['DateReceivedLO']= $this->input->post('appreceivelo');

    //business info
    $datadb2['RegName']= $this->input->post('regname');
    $datadb2['RegNo']= $this->input->post('regno');
    $datadb2['RegAddress1']= $this->input->post('regadd1');
    $datadb2['BizType']= $this->input->post('biztype');
    $datadb2['RegAddress2']= $this->input->post('regadd2');
    $datadb2['BizNature']= $this->input->post('biznature');
    $datadb2['RegCity']= $this->input->post('regcity');
    $datadb2['MCC']= $this->input->post('bankMCC');
    $datadb2['RegState']= $this->input->post('regstate');
    $datadb2['RegPostCode']= $this->input->post('regpostcode');
    $datadb2['BizYear']= $this->input->post('bizyear');
    $datadb2['RegTel']= $this->input->post('regtel');
    $datadb2['RegFax']= $this->input->post('regfax');
    $datadb2['BizHourStart']= $this->input->post('bizstart');
    $datadb2['BizHourEnd']= $this->input->post('bizend');
    $datadb2['RegEmail']= $this->input->post('regemail');
    $datadb2['ContactPerson']= $this->input->post('contactperson');
    $datadb2['BizTypeOther']= $this->input->post('biztypeother');


    //trade info
    $datadb2['TradeName']= $this->input->post('tradename');
    $datadb2['Designation']= $this->input->post('designation');
    $datadb2['TradeAddress1']= $this->input->post('tradeadd1');
    $datadb2['IC']= $this->input->post('IC');
    $datadb2['TradeAddress2']= $this->input->post('tradeadd2');
    $datadb2['TradeHP1']= $this->input->post('tradehp1');
    $datadb2['TradeCity']= $this->input->post('tradecity');
    $datadb2['TradeState']= $this->input->post('tradestate');
    $datadb2['TradePostCode']= $this->input->post('tradepostcode');
    $datadb2['BizHotSpot']= $this->input->post('bizhotspot');
    $datadb2['TradeTel']= $this->input->post('tradetel');
    $datadb2['TradeHP2']= $this->input->post('tradehp2');
    $datadb2['TradeFax']= $this->input->post('tradefax');
    $datadb2['BizRemark']= $this->input->post('bizremark');


    //trasaction details
    $datadb2['Bank']= $this->input->post('paymethod');
    $datadb2['Branch']= $this->input->post('paybranch');
    $datadb['BankOther']= $this->input->post('payotherbank');
    $datadb2['AccountNo']= $this->input->post('payaccount');
    $datadb2['PayRemark']= $this->input->post('payremark');


    //Bank facilities
    $datadb2['VisaMaster']= $this->input->post('recvisamc');
    $datadb2['Debit']= $this->input->post('recdebit');
    $datadb2['Rental']= $this->input->post('recrental');
    $datadb2['RecMonthRate3']= $this->input->post('RecMonthRate3');
    $datadb2['RecMonthRate6']= $this->input->post('RecMonthRate6');
    $datadb2['RecMonthRate12']= $this->input->post('RecMonthRate12');
    $datadb2['RecMonthRate18']= $this->input->post('RecMonthRate18');
    $datadb2['RecMonthRate24']= $this->input->post('RecMonthRate24');
    $datadb2['RecMonthRate30']= $this->input->post('RecMonthRate30');
    $datadb2['RecMonthRate36']= $this->input->post('RecMonthRate36');
    $datadb2['RecMonthRate48']= $this->input->post('RecMonthRate48');
    $datadb2['MIDCredit']= $this->input->post('bankcredit');
    $datadb2['TerminalID']= $this->input->post('bankterminal');
    $datadb2['MIDDebit']= $this->input->post('bankdebit');
    $datadb2['TIDDebit']= $this->input->post('terminaldebit');
    $datadb2['EPP3Mid']= $this->input->post('mid3epp');
    $datadb2['EPP6Mid']= $this->input->post('mid6epp');
    $datadb2['EPP12Mid']= $this->input->post('mid12epp');
    $datadb2['EPP18Mid']= $this->input->post('mid18epp');
    $datadb2['EPP24Mid']= $this->input->post('mid24epp');
    $datadb2['EPP30Mid']= $this->input->post('mid30epp');
    $datadb2['EPP36Mid']= $this->input->post('mid36epp');
    $datadb2['EPP48Mid']= $this->input->post('mid48epp');
    $datadb2['EPP3Tid']= $this->input->post('tid3epp');
    $datadb2['EPP6Tid']= $this->input->post('tid6epp');
    $datadb2['EPP12Tid']= $this->input->post('tid12epp');
    $datadb2['EPP18Tid']= $this->input->post('tid18epp');
    $datadb2['EPP24Tid']= $this->input->post('tid24epp');
    $datadb2['EPP30Tid']= $this->input->post('tid30epp');
    $datadb2['EPP36Tid']= $this->input->post('tid36epp');
    $datadb2['EPP48Tid']= $this->input->post('tid48epp');
    $datadb2['Tipadj']= $this->input->post('tipsadj');
    $datadb2['ONthered']= $this->input->post('onthespot');
    $datadb2['CVV2']= $this->input->post('cvv2fun');
    $datadb2['MagS']= $this->input->post('magstripe');
    $datadb2['OnlineReg']= $this->input->post('onlinereg');
    $datadb2['AMEX']= $this->input->post('amex');
    $datadb2['RecRemark']= $this->input->post('recRemark');
    

    //Equipment
    $datadb2['EDCType']= $this->input->post('edctype');
    $datadb2['DateOpenCall']= $this->input->post('opencalldate');
    $datadb2['EDCSerialNo']= $this->input->post('edcsno');
    $datadb2['SiteID']= $this->input->post('siteid');
    $datadb2['SAMSerialNo']= $this->input->post('samsno');
    $datadb2['DateInstall']= $this->input->post('installdate');
    $datadb2['StatusRemark']= $this->input->post('appremark');
    $datadb2['InstallRemark']= $this->input->post('edcremark');


    //Terminal setting
    $datadb2['TerConMode']= $this->input->post('terconmode');
    $datadb2['PANMas']= $this->input->post('panmask');
    $datadb2['ManSettle']= $this->input->post('manialset');
    $datadb2['AutoSett']= $this->input->post('autoset');
    $datadb2['PreAuthF']= $this->input->post('preauth');
    $datadb2['OffFun']= $this->input->post('offlinef');
    $datadb2['ManEnFun']= $this->input->post('manentryf');
    $datadb2['CashAdvF']= $this->input->post('cashadvf');
    $datadb2['TermMode']= $this->input->post('termodel');
    $datadb2['ForceS']= $this->input->post('forceset');
    $datadb2['NACloc']= $this->input->post('nacloc');
    $datadb2['PrNACNo']= $this->input->post('primnac');
    $datadb2['TerSofApp']= $this->input->post('termsofapp');
    $datadb2['SeNACNo']= $this->input->post('secnac');

    //Terminal setting

    $datadb2['SWdate']= $this->input->post('SWdate');
    $datadb2['SAMsn']= $this->input->post('SAMsn');
    $datadb2['EDCsn']= $this->input->post('EDCsn');
    $datadb2['SWjs']= $this->input->post('SWjs');
    $datadb2['SWdate1']= $this->input->post('SWdate1');
    $datadb2['SAMsn1']= $this->input->post('SAMsn1');
    $datadb2['EDCsn1']= $this->input->post('EDCsn1');
    $datadb2['SWjs1']= $this->input->post('SWjs1');
    $datadb2['SWdate2']= $this->input->post('SWdate2');
    $datadb2['SAMsn2']= $this->input->post('SAMsn2');
    $datadb2['EDCsn2']= $this->input->post('EDCsn2');
    $datadb2['SWjs2']= $this->input->post('SWjs2');
    $datadb2['SWdate3']= $this->input->post('SWdate3');
    $datadb2['SAMsn3']= $this->input->post('SAMsn3');
    $datadb2['EDCsn3']= $this->input->post('EDCsn3');
    $datadb2['SWjs3']= $this->input->post('SWjs3');
    $datadb2['SWdate4']= $this->input->post('SWdate4');
    $datadb2['SAMsn4']= $this->input->post('SAMsn4');
    $datadb2['EDCsn4']= $this->input->post('EDCsn4');
    $datadb2['SWjs4']= $this->input->post('SWjs4');
    $datadb2['SWdate5']= $this->input->post('SWdate5');
    $datadb2['SAMsn5']= $this->input->post('SAMsn5');
    $datadb2['EDCsn5']= $this->input->post('EDCsn5');
    $datadb2['SWjs5']= $this->input->post('SWjs5');
    $datadb2['SWdate6']= $this->input->post('SWdate6');
    $datadb2['SAMsn6']= $this->input->post('SAMsn6');
    $datadb2['EDCsn6']= $this->input->post('EDCsn6');
    $datadb2['SWjs6']= $this->input->post('SWjs6');
    $datadb2['SWdate7']= $this->input->post('SWdate7');
    $datadb2['SAMsn7']= $this->input->post('SAMsn7');
    $datadb2['EDCsn7']= $this->input->post('EDCsn7');
    $datadb2['SWjs7']= $this->input->post('SWjs7');
    $datadb2['SWdate8']= $this->input->post('SWdate8');
    $datadb2['SAMsn8']= $this->input->post('SAMsn8');
    $datadb2['EDCsn8']= $this->input->post('EDCsn8');
    $datadb2['SWjs8']= $this->input->post('SWjs8');
    $datadb2['SWdate9']= $this->input->post('SWdate9');
    $datadb2['SAMsn9']= $this->input->post('SAMsn9');
    $datadb2['EDCsn9']= $this->input->post('EDCsn9');
    $datadb2['SWjs9']= $this->input->post('SWjs9');


    //terminal & retrieval
    $datadb2['DateNotice']= $this->input->post('noticedate');
    $datadb2['DateTerminated']= $this->input->post('terminatedate');
    $datadb2['Refund']= $this->input->post('refund');
    $datadb2['RetDate']= $this->input->post('retdate');
    $datadb2['TerminateReason']= $this->input->post('terminatereason');
    $datadb2['HandTo']= $this->input->post('handto');
    $datadb2['BankRemark']= $this->input->post('bankremark');
    $datadb2['ChqNo']= $this->input->post('chqno');
    $datadb2['TerminalIDZIIP']= $this->input->post('TerminalIDZIIP');
    $datadb2['TerminateRemark']= $this->input->post('terminateremark');

    //DPS charges & Terms
    $datadb2['MonRenAmt']= $this->input->post('monrentamt');
    $datadb2['AdvMonths']= $this->input->post('advmts');
    $datadb2['ConPerMths']= $this->input->post('contraper');
    $datadb2['AmPackage']= $this->input->post('packtype');
    $datadb2['DirecDebit']= $this->input->post('dpsdd');
    $datadb2['ReceivedChqDate']= $this->input->post('dpschqdate');
    $datadb2['ReceivedChqTotal']= $this->input->post('dpschqamt');
    $datadb2['ReceivedChqNo']= $this->input->post('dpschqno');
    $datadb2['ReChqDate1']= $this->input->post('dpschqdate1');
    $datadb2['ReChqTotal1']= $this->input->post('dpschqamt1');
    $datadb2['ReChqNo1']= $this->input->post('dpschqno1');
    $datadb2['ReChqDate2']= $this->input->post('dpschqdate2');
    $datadb2['ReChqTotal2']= $this->input->post('dpschqamt2');
    $datadb2['ReChqNo2']= $this->input->post('dpschqno2');
    $datadb2['TotRental']= $this->input->post('totalren');
    $datadb2['Deposit']= $this->input->post('dpsdeposit');
    $datadb2['DPSProcess']= $this->input->post('dpsprocess');
    $datadb2['GrandTot']= $this->input->post('grandtt');
    $datadb2['DDRemark']= $this->input->post('dpsremark');


    $datadb2['Status']= $this->input->post('appstatus');
    $datadb2['DDFreq']= $this->input->post('dpsddfreq');

    $result = $this->Merchant_model->update_merchantrhb($datadb2,$id);
    $this->trails($result,"Update(".$datadb2['Status'].")");

    
  }else if($bank == 'ambank'){

    //application info
    $datadb2['RefNo']= $this->input->post('refno');
    $datadb2['StaffCode']= $this->input->post('staffcode');
    $datadb2['DateSubmit']= $this->input->post('appsubmit');
    $datadb2['DateDue']= $this->input->post('appdue');
    $datadb2['DateAppeal']= $this->input->post('appappeal');
    $datadb2['DateReceivedLO']= $this->input->post('appreceivelo');

    //business info
    $datadb2['RegName']= $this->input->post('regname');
    $datadb2['RegNo']= $this->input->post('regno');
    $datadb2['RegAddress1']= $this->input->post('regadd1');
    $datadb2['BizType']= $this->input->post('biztype');
    $datadb2['RegAddress2']= $this->input->post('regadd2');
    $datadb2['BizNature']= $this->input->post('biznature');
    $datadb2['RegCity']= $this->input->post('regcity');
    $datadb2['MCC']= $this->input->post('bankMCC');
    $datadb2['RegState']= $this->input->post('regstate');
    $datadb2['RegPostCode']= $this->input->post('regpostcode');
    $datadb2['BizYear']= $this->input->post('bizyear');
    $datadb2['RegTel']= $this->input->post('regtel');
    $datadb2['RegFax']= $this->input->post('regfax');
    $datadb2['BizHourStart']= $this->input->post('bizstart');
    $datadb2['BizHourEnd']= $this->input->post('bizend');
    $datadb2['RegEmail']= $this->input->post('regemail');
    $datadb2['ContactPerson']= $this->input->post('contactperson');
    $datadb2['BizTypeOther']= $this->input->post('biztypeother');


    //trade info
    $datadb2['TradeName']= $this->input->post('tradename');
    $datadb2['Designation']= $this->input->post('designation');
    $datadb2['TradeAddress1']= $this->input->post('tradeadd1');
    $datadb2['IC']= $this->input->post('IC');
    $datadb2['TradeAddress2']= $this->input->post('tradeadd2');
    $datadb2['TradeHP1']= $this->input->post('tradehp1');
    $datadb2['TradeCity']= $this->input->post('tradecity');
    $datadb2['TradeState']= $this->input->post('tradestate');
    $datadb2['TradePostCode']= $this->input->post('tradepostcode');
    $datadb2['BizHotSpot']= $this->input->post('bizhotspot');
    $datadb2['TradeTel']= $this->input->post('tradetel');
    $datadb2['TradeHP2']= $this->input->post('tradehp2');
    $datadb2['TradeFax']= $this->input->post('tradefax');
    $datadb2['BizRemark']= $this->input->post('bizremark');


    //trasaction details
    $datadb2['Bank']= $this->input->post('paymethod');
    $datadb2['Branch']= $this->input->post('paybranch');
    $datadb2['AccountNo']= $this->input->post('payaccount');
    $datadb['BankOther']= $this->input->post('payotherbank');
    $datadb2['PayRemark']= $this->input->post('payremark');


    //Bank facilities
    $datadb2['VisaMaster']= $this->input->post('recvisamc');
    $datadb2['Debit']= $this->input->post('recdebit');
    $datadb2['Rental']= $this->input->post('recrental');
    $datadb2['RecMonthRate3']= $this->input->post('RecMonthRate3');
    $datadb2['RecMonthRate6']= $this->input->post('RecMonthRate6');
    $datadb2['RecMonthRate12']= $this->input->post('RecMonthRate12');
    $datadb2['RecMonthRate18']= $this->input->post('RecMonthRate18');
    $datadb2['RecMonthRate24']= $this->input->post('RecMonthRate24');
    $datadb2['RecMonthRate30']= $this->input->post('RecMonthRate30');
    $datadb2['RecMonthRate36']= $this->input->post('RecMonthRate36');
    $datadb2['RecMonthRate48']= $this->input->post('RecMonthRate48');
    $datadb2['MIDCredit']= $this->input->post('bankcredit');
    $datadb2['TerminalID']= $this->input->post('bankterminal');
    $datadb2['MIDDebit']= $this->input->post('bankdebit');
    $datadb2['TIDDebit']= $this->input->post('terminaldebit');
    $datadb2['EPP3Mid']= $this->input->post('mid3epp');
    $datadb2['EPP6Mid']= $this->input->post('mid6epp');
    $datadb2['EPP12Mid']= $this->input->post('mid12epp');
    $datadb2['EPP18Mid']= $this->input->post('mid18epp');
    $datadb2['EPP24Mid']= $this->input->post('mid24epp');
    $datadb2['EPP30Mid']= $this->input->post('mid30epp');
    $datadb2['EPP36Mid']= $this->input->post('mid36epp');
    $datadb2['EPP48Mid']= $this->input->post('mid48epp');
    $datadb2['EPP3Tid']= $this->input->post('tid3epp');
    $datadb2['EPP6Tid']= $this->input->post('tid6epp');
    $datadb2['EPP12Tid']= $this->input->post('tid12epp');
    $datadb2['EPP18Tid']= $this->input->post('tid18epp');
    $datadb2['EPP24Tid']= $this->input->post('tid24epp');
    $datadb2['EPP30Tid']= $this->input->post('tid30epp');
    $datadb2['EPP36Tid']= $this->input->post('tid36epp');
    $datadb2['EPP48Tid']= $this->input->post('tid48epp');
    $datadb2['Tipadj']= $this->input->post('tipsadj');
    $datadb2['ONthered']= $this->input->post('onthespot');
    $datadb2['CVV2']= $this->input->post('cvv2fun');
    $datadb2['MagS']= $this->input->post('magstripe');
    $datadb2['OnlineReg']= $this->input->post('onlinereg');
    $datadb2['AMEX']= $this->input->post('amex');
    $datadb2['RecRemark']= $this->input->post('recRemark');
    

    //Equipment
    $datadb2['EDCType']= $this->input->post('edctype');
    $datadb2['DateOpenCall']= $this->input->post('opencalldate');
    $datadb2['EDCSerialNo']= $this->input->post('edcsno');
    $datadb2['SiteID']= $this->input->post('siteid');
    $datadb2['SAMSerialNo']= $this->input->post('samsno');
    $datadb2['DateInstall']= $this->input->post('installdate');
    $datadb2['StatusRemark']= $this->input->post('appremark');
    $datadb2['InstallRemark']= $this->input->post('edcremark');


    //Terminal setting
    $datadb2['TerConMode']= $this->input->post('terconmode');
    $datadb2['PANMas']= $this->input->post('panmask');
    $datadb2['ManSettle']= $this->input->post('manialset');
    $datadb2['AutoSett']= $this->input->post('autoset');
    $datadb2['PreAuthF']= $this->input->post('preauth');
    $datadb2['OffFun']= $this->input->post('offlinef');
    $datadb2['ManEnFun']= $this->input->post('manentryf');
    $datadb2['CashAdvF']= $this->input->post('cashadvf');
    $datadb2['TermMode']= $this->input->post('termodel');
    $datadb2['ForceS']= $this->input->post('forceset');
    $datadb2['NACloc']= $this->input->post('nacloc');
    $datadb2['PrNACNo']= $this->input->post('primnac');
    $datadb2['TerSofApp']= $this->input->post('termsofapp');
    $datadb2['SeNACNo']= $this->input->post('secnac');

    //Terminal setting

    $datadb2['SWdate']= $this->input->post('SWdate');
    $datadb2['SAMsn']= $this->input->post('SAMsn');
    $datadb2['EDCsn']= $this->input->post('EDCsn');
    $datadb2['SWjs']= $this->input->post('SWjs');
    $datadb2['SWdate1']= $this->input->post('SWdate1');
    $datadb2['SAMsn1']= $this->input->post('SAMsn1');
    $datadb2['EDCsn1']= $this->input->post('EDCsn1');
    $datadb2['SWjs1']= $this->input->post('SWjs1');
    $datadb2['SWdate2']= $this->input->post('SWdate2');
    $datadb2['SAMsn2']= $this->input->post('SAMsn2');
    $datadb2['EDCsn2']= $this->input->post('EDCsn2');
    $datadb2['SWjs2']= $this->input->post('SWjs2');
    $datadb2['SWdate3']= $this->input->post('SWdate3');
    $datadb2['SAMsn3']= $this->input->post('SAMsn3');
    $datadb2['EDCsn3']= $this->input->post('EDCsn3');
    $datadb2['SWjs3']= $this->input->post('SWjs3');
    $datadb2['SWdate4']= $this->input->post('SWdate4');
    $datadb2['SAMsn4']= $this->input->post('SAMsn4');
    $datadb2['EDCsn4']= $this->input->post('EDCsn4');
    $datadb2['SWjs4']= $this->input->post('SWjs4');
    $datadb2['SWdate5']= $this->input->post('SWdate5');
    $datadb2['SAMsn5']= $this->input->post('SAMsn5');
    $datadb2['EDCsn5']= $this->input->post('EDCsn5');
    $datadb2['SWjs5']= $this->input->post('SWjs5');
    $datadb2['SWdate6']= $this->input->post('SWdate6');
    $datadb2['SAMsn6']= $this->input->post('SAMsn6');
    $datadb2['EDCsn6']= $this->input->post('EDCsn6');
    $datadb2['SWjs6']= $this->input->post('SWjs6');
    $datadb2['SWdate7']= $this->input->post('SWdate7');
    $datadb2['SAMsn7']= $this->input->post('SAMsn7');
    $datadb2['EDCsn7']= $this->input->post('EDCsn7');
    $datadb2['SWjs7']= $this->input->post('SWjs7');
    $datadb2['SWdate8']= $this->input->post('SWdate8');
    $datadb2['SAMsn8']= $this->input->post('SAMsn8');
    $datadb2['EDCsn8']= $this->input->post('EDCsn8');
    $datadb2['SWjs8']= $this->input->post('SWjs8');
    $datadb2['SWdate9']= $this->input->post('SWdate9');
    $datadb2['SAMsn9']= $this->input->post('SAMsn9');
    $datadb2['EDCsn9']= $this->input->post('EDCsn9');
    $datadb2['SWjs9']= $this->input->post('SWjs9');


    //terminal & retrieval
    $datadb2['DateNotice']= $this->input->post('noticedate');
    $datadb2['DateTerminated']= $this->input->post('terminatedate');
    $datadb2['Refund']= $this->input->post('refund');
    $datadb2['RetDate']= $this->input->post('retdate');
    $datadb2['TerminateReason']= $this->input->post('terminatereason');
    $datadb2['HandTo']= $this->input->post('handto');
    $datadb2['BankRemark']= $this->input->post('bankremark');
    $datadb2['ChqNo']= $this->input->post('chqno');
    $datadb2['TerminalIDZIIP']= $this->input->post('TerminalIDZIIP');
    $datadb2['TerminateRemark']= $this->input->post('terminateremark');

    //DPS charges & Terms
    $datadb2['MonRenAmt']= $this->input->post('monrentamt');
    $datadb2['AdvMonths']= $this->input->post('advmts');
    $datadb2['ConPerMths']= $this->input->post('contraper');
    $datadb2['AmPackage']= $this->input->post('packtype');
    $datadb2['DirecDebit']= $this->input->post('dpsdd');
    $datadb2['ReceivedChqDate']= $this->input->post('dpschqdate');
    $datadb2['ReceivedChqTotal']= $this->input->post('dpschqamt');
    $datadb2['ReceivedChqNo']= $this->input->post('dpschqno');
    $datadb2['ReChqDate1']= $this->input->post('dpschqdate1');
    $datadb2['ReChqTotal1']= $this->input->post('dpschqamt1');
    $datadb2['ReChqNo1']= $this->input->post('dpschqno1');
    $datadb2['ReChqDate2']= $this->input->post('dpschqdate2');
    $datadb2['ReChqTotal2']= $this->input->post('dpschqamt2');
    $datadb2['ReChqNo2']= $this->input->post('dpschqno2');
    $datadb2['TotRental']= $this->input->post('totalren');
    $datadb2['Deposit']= $this->input->post('dpsdeposit');
    $datadb2['DPSProcess']= $this->input->post('dpsprocess');
    $datadb2['GrandTot']= $this->input->post('grandtt');
    $datadb2['DDRemark']= $this->input->post('dpsremark');


    $datadb2['Status']= $this->input->post('appstatus');
    $datadb2['DDFreq']= $this->input->post('dpsddfreq');

    $result = $this->Merchant_model->update_merchantambank($datadb2,$id);
    $this->trails($result,"Update(".$datadb2['Status'].")");

    
  }


  if($result){
    echo "<script type='text/javascript'>alert('Update Succesfully !'); window.location.href = '".base_url().'merchantupdate/'.$id."/".$statid."/".$searchname."';</script>";
  }else{
    echo "<script type='text/javascript'> window.location.href = '".base_url().'merchantupdate/'.$id."/".$statid."/".$searchname."';</script>";
  }
}else{

  $query = 'a.ID,DateSubmit,RegName,RegState,b.name';
  if($searchname == 'null'){
    $searchname = '';
  }

  redirect ('Merchant/merchantInfo/'.$statid."/".$searchname.'');

  $data['search'] = $searchname;
  $data['state'] = $this->Merchant_model->liststate();

  $data['menuname'] = "Merchant Information";
  if($bank == "hlbb"){
    $data['listmerchant'] = $this->Merchant_model->getmerchantgroup($query,$statid,$searchname);
  }else if($bank == "rhb"){

    $data['listmerchant'] = $this->Merchant_model->getmerchantgrouprhb($query,$statid,$searchname);
  }
  
	$data['main_content'] = 'Merchant/merchant_view';
    $this->load->view('mainPage',$data);
}
}


public function trails($mid,$action){

  $bank= $this->session->userdata('bank');
  $datadb['mID']= $mid;
  $datadb['aDate']= date('Y-m-d H:m:s');
  $datadb['aUser']= $this->session->userdata('username');;
  $datadb['aRemark']= $action;

  if($bank == "hlbb"){
     $this->Merchant_model->insertrail($datadb);
  }else if($bank == "ghl"){
    $this->Merchant_model->insertrailghl($datadb);
 }else if($bank == "rhb"){
     $this->Merchant_model->insertrailrhb($datadb);
  }else if($bank == "ambank"){
    $this->Merchant_model->insertrailambank($datadb);
 }

}


}
