<?php
class Setting extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('menu_model');
    $this->load->model('dashboard_model');
    $this->load->model('Merchant_model');
    $this->load->model('Setting_model');
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }

  

public function salesmanIndex()
{
  $key="setting";
  $mid = $this->menu_model->getid($key);
  $query = '*';
  $roleid= $this->session->userdata('role');
  $bank= $this->session->userdata('bank');
  $data['search'] = '';
  $data['menuid'] = $mid;
  $data['bank'] = $bank;
  $data['menuname'] = 'Salesman Information';
  $data['access'] = $this->menu_model->get_access($roleid,$mid);

  if($bank == 'hlbb'){
    $data['listsaleman'] = $this->Merchant_model->liststaff();
  }else if($bank == 'rhb'){
    $data['listsaleman'] = $this->Merchant_model->liststaffrhb();
  }else if($bank == 'ghl'){
    $data['listsaleman'] = $this->Merchant_model->liststaffghl();
  }else if($bank == 'ambank'){
    $data['listsaleman'] = $this->Merchant_model->liststaffambank();
  }

  $data['main_content'] = 'Setting/setting_view';
  $this->load->view('mainPage',$data);
}


public function addsalesman()
{
  $bank= $this->session->userdata('bank');
  $data['bank'] = $bank;

  $datadb['CN']= $this->input->post('staffcode');
  $datadb['Name']= $this->input->post('staffname');
  $datadb['Contact']= $this->input->post('contactno');  
  $datadb['status']= $this->input->post('staffstatus');   

  $result = $this->Setting_model->insert_salesman($bank,$datadb);

  if($result){
    echo "<script type='text/javascript'>alert('Succesfully added !'); window.location.href = '".base_url().'salesman'."';</script>";
  }else{
    echo "<script type='text/javascript'>alert('Failed !')</script>";
  }
}


public function editSalesman($id)
{
  $key              = "setting";
  $bank             = $this->session->userdata('bank');
  $mid              = $this->menu_model->getid($key);
  $query            = '*';
  $roleid           = $this->session->userdata('role');
  $data['search']   = '';
  $data['menuid']   = $mid;
  $data['menuname'] = 'Merchant Information';
  $data['access']   = $this->menu_model->get_access($roleid,$mid);
  $data['salesmandata'] = $this->Setting_model->getsalemaninfo($bank,$id);
//   $data['listmerchant'] = $this->Merchant_model->getmerchant($query);
  $data['main_content'] = 'Setting/editsalesman_view';
  $this->load->view('mainPage',$data);
}


public function submiteditsalesman()
{
  $id               = $this->input->post('saleid');
  $bank             = $this->session->userdata('bank');
  $datadb['CN']     = $this->input->post('staffcode');
  $datadb['Name']   = $this->input->post('staffname');
  $datadb['Contact']= $this->input->post('contactno');  
  $datadb['status'] = $this->input->post('staffstatus'); 

  $result = $this->Setting_model->update_salesman($bank,$datadb,$id);
 
  if($result){
    echo "<script type='text/javascript'>alert('Update Succesfully !'); window.location.href = '".base_url().'salesman/'."';</script>";
  }else{
    echo "<script type='text/javascript'> window.location.href = '".base_url().'salesmanupdate/'.$id."';</script>";
  }
}




public function salesmanInfo()
{

  $btn= $this->input->post('btn');
  $statid= $this->input->post('statid');
  $searchname= $this->input->post('search');
  $bank= $this->session->userdata('bank');
  $data['search'] = $searchname;
  $data['bank'] = $bank;

  if($btn == "search"){
	$data['menuname'] = 'Salesman Information';
	$query = '*';
	if($statid){
		$data['statid'] = $statid;
	}else{
		$data['statid'] = 0;
  }
  	
  if($bank == "hlbb"){
    $data['listsaleman'] = $this->Setting_model->getsalesman($query,$statid,$searchname);
  }else if($bank == "rhb"){
    $data['listsaleman'] = $this->Setting_model->getsalesmanrhb($query,$statid,$searchname);
  }else if($bank == "ghl"){
    $data['listsaleman'] = $this->Setting_model->getsalesmanghl($query,$statid,$searchname);
  }
  
	$data['main_content'] = 'Setting/setting_view';
  $this->load->view('mainPage',$data);

  }else if($btn == "add"){
    $data['menuname'] = 'New Salesman';
    $data['main_content'] = 'Setting/addsalesman_view';
    $this->load->view('mainPage',$data);

  }else if($btn == "csv"){
    
  
      // output headers so that the file is downloaded rather than displayed
      header('Content-type: text/csv');
      header('Content-Disposition: attachment; filename= salesman'.date('Ymd').".csv");

      // do not cache the file
      header('Pragma: no-cache');
      header('Expires: 0');

      // create a file pointer connected to the output stream
      $file = fopen('php://output', 'w');

      // send the column headers
      fputcsv($file, array('Salesman Code', 'Salesname Name', 'Contact', 'Status'));

      // Sample data. This can be fetched from mysql too
      $data['menuname'] = 'Merchant Information';
      $query = 'CN,Name,Contact,status';

      if($bank == "hlbb"){
        $data = $this->Setting_model->getsalesman($query,$statid,$searchname);
      }else if($bank == "ghl"){
        $data = $this->Setting_model->getsalesmanghl($query,$statid,$searchname);
      }else if($bank == "rhb"){
        $data = $this->Setting_model->getsalesmanrhb($query,$statid,$searchname);
      }

      // output each row of the data
      foreach ($data as $row)
      {
        if( is_object($row) )
        $row = (array) $row;
        fputcsv($file, $row);

      }

      exit();

  }else if($btn == "pdf"){

      $data['menuname'] = 'Salesman Information';
      $query = '*';

      if($bank == "hlbb"){
        $data['listsaleman'] = $this->Setting_model->getsalesman($query,$statid,$searchname);
      }else if($bank == "ghl"){
        $data['listsaleman'] = $this->Setting_model->getsalesmanghl($query,$statid,$searchname);
      }else if($bank == "rhb"){
        $data['listsaleman'] = $this->Setting_model->getsalesmanrhb($query,$statid,$searchname);
      }

      $loaded_html = $this->load->view('Setting/salesmantemplate_view', $data, true);
      $filename='mechantreport'.date('Ymd').'.pdf';
      $this->load->library('pdf'); 
      $this->dompdf->loadHtml($loaded_html);
      $this->dompdf->setPaper('A4', 'potrait');
      $this->dompdf->render();
      $this->dompdf->stream($filename);

}else{
  redirect('merchant');
}

}


}
