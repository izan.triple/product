<?php
class HomestayMaintenance extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function listhomestaymaintenance($hid,$oid){
    $homestaymaintenanceModel  = GAMMA::getModel('homestaymaintenance');
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');
    
    $key                ="report";
    $data['homestayid'] = $hid;
    $data['ownerid']    = $oid;
    $data['setting']    = $siteModel->all();
    $roleid             =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'Homestay Maintenance Information';
    $data['homestaymaintenance'] = $homestaymaintenanceModel->listhomestaymaintenance($hid);
    $data['main_content'] = 'Homestay/homestaymaintenance_view';
    $this->load->view('mainPage',$data);
  }

  public function maintenanceaction(){
    $homestaymaintenanceModel  = GAMMA::getModel('homestaymaintenance');

    $oid                  = $this->input->post('ownerid');
    $hid                  = $this->input->post('homestayid');
    $datadb['homestayid'] = $hid;
    $datadb['ownerid']    = $oid;
    $datadb['date']       = $this->input->post('date');
    $datadb['description']= $this->input->post('desc');
    $datadb['amount']     = $this->input->post('amount');
    $datadb['status']     = $this->input->post('status');
    $datadb['remarks']    = $this->input->post('remarks');
    if (strpos($_SERVER['HTTP_HOST'], 'mastersentral.com') !== false) {
      $datadb['sync']     = 1;
    }
    $result               = $homestaymaintenanceModel->add($datadb);
    

    if($result > 0){
      redirect('homestaymaintenance/'.$hid.'/'.$oid);
    }
      
  }

  public function deleteitm($id){
      $homestaymaintenanceModel  = GAMMA::getModel('homestaymaintenance');
      $result = $homestaymaintenanceModel->delete(array('id' => $id));
      if($result){
        echo "Successfully deleted !";
      }else{
        echo "Failed !";
      }
  }
  





}
