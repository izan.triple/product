<?php
class Product extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function index(){
    $proddispModel  = GAMMA::getModel('productdisplay');
    $siteModel      = GAMMA::getModel('site');
    $menuModel      = GAMMA::getModel('menu');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'Product Information';
    $data['proddisp']   = $proddispModel->all();
    $data['main_content'] = 'Product/product_view';
    $this->load->view('mainPage',$data);
  }

  public function updateprod($id){
    $proddispModel  = GAMMA::getModel('productdisplay');
    $siteModel      = GAMMA::getModel('site');
    $menuModel      = GAMMA::getModel('menu');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'Product Information';
    $data['proddisp']   = $proddispModel->selectOne(array('id'=>$id));
    $data['main_content'] = 'Product/editproduct_view';
    $this->load->view('mainPage',$data);
  }

  public function prodaction(){
    $proddispModel  = GAMMA::getModel('productdisplay');
    
    $btn= $this->input->post('btn');
    $id= $this->input->post('prodid');

    if($btn == "update"){
      $datadb['name']   = $this->input->post('name');
      $datadb['price']  = $this->input->post('price');
      $datadb['stock']  = $this->input->post('stock');
      $proddispModel->update($datadb,array('id'=>$id));
      redirect('proddisp');
    }
  }


}
