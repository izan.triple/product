<?php
class Owner extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function index(){
    $ownerModel     = GAMMA::getModel('owner');
    $siteModel      = GAMMA::getModel('site');
    $menuModel      = GAMMA::getModel('menu');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['menuname']   = 'Owner Information';
    $data['homestay']   = $ownerModel->all();
    $data['main_content'] = 'Owner/owner_view';
    $this->load->view('mainPage',$data);
  }

  public function owneraction($btn=NULL,$statid=NULL,$searchname=NULL)
  {
    
    $ownerModel     = GAMMA::getModel('owner');

    if($statid == NULL){
      $btn= $this->input->post('btn');
      $statid= $this->input->post('statid');
    }

    if($searchname == NULL){
      $searchname= $this->input->post('search');
    }else if($searchname == 'null'){
      $searchname= '';
    }
  
      $data['menuid'] = 12;

      if($btn == "add"){
        $data['menuname']     = 'New Owner';
        $data['main_content'] = 'Owner/addowner_view';
        $this->load->view('mainPage',$data);

      }else if($btn == "submit"){
        $datadb['name']         = $this->input->post('name');
        $datadb['contactno ']   = $this->input->post('contact');
        $result                 = $ownerModel->add($datadb);
        
        if($result > 0){
          redirect('owner');
        }else{
          redirect('owner');
        }

      }else if($btn == "csv"){
        // output headers so that the file is downloaded rather than displayed
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename= mechantreport'.date('Ymd').".csv");

        // do not cache the file
        header('Pragma: no-cache');
        header('Expires: 0');

        // create a file pointer connected to the output stream
        $file = fopen('php://output', 'w');

        // send the column headers
        fputcsv($file, array('Submission Date', 'Merchant Name', 'Location', 'Status'));

        // Sample data. This can be fetched from mysql too
        $data['menuname'] = 'Merchant Information';
        $query = 'DateSubmit,RegName,RegState,b.name';

        if($bank == "hlbb"){
          $data = $this->Merchant_model->getmerchantgroup($query,$statid,$searchname);
        }else if($bank == "rhb"){
          $data = $this->Merchant_model->getmerchantgrouprhb($query,$statid,$searchname);
        }else if($bank == "ambank"){
          $data = $this->Merchant_model->getmerchantgroupambank($query,$statid,$searchname);
        }

        // output each row of the data
        foreach ($data as $row)
        {
          if( is_object($row) )
          $row = (array) $row;
          fputcsv($file, $row);

        }

        exit();

      }else if($btn == "pdf"){
        $data['menuname'] = 'Merchant Information';
        $query = 'DateSubmit,RegName,RegState,b.name';
        $data['liststatus'] = $this->Merchant_model->getstatus();

        if($bank == "hlbb"){
          $data['listmerchant'] = $this->Merchant_model->getmerchantgroup($query,$statid,$searchname);
        }else if($bank == "rhb"){
          $data['listmerchant'] = $this->Merchant_model->getmerchantgrouprhb($query,$statid,$searchname);
        }else if($bank == "ambank"){
          $data['listmerchant'] = $this->Merchant_model->getmerchantgroupambank($query,$statid,$searchname);
        }

        $loaded_html = $this->load->view('Merchant/merchanttemplate_view', $data, true);
        $filename='mechantreport'.date('Ymd').'.pdf';
        $this->load->library('pdf'); 
        $this->dompdf->loadHtml($loaded_html);
        $this->dompdf->setPaper('A4', 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream($filename);

      }else{
        echo $btn;
      }
      
} 

public function editowner($id)
{
  $ownerModel     = GAMMA::getModel('owner');
  $siteModel      = GAMMA::getModel('site');
  $menuModel      = GAMMA::getModel('menu');
  
  $key             ="merchant";
  $data['setting'] = $siteModel->all();
  $roleid          =  GAMMA::getSession('role');
  $data['menuid']     = $menuModel->getid($key);
  $data['key']        = $key;
  $data['menuname']   = 'Owner Information';
  $data['ownerdata']  = $ownerModel->selectOne(array('id' => $id));

  print_r($data['ownerdata']);exit;
  $data['main_content'] = 'Owner/editmerchant_view';

  $this->load->view('mainPage',$data);
}

public function deleteitm($id){

  $ownerModel     = GAMMA::getModel('owner');
  $result         = $ownerModel->delete(array('id' => $id));
  
	if($result){
    echo "Successfully deleted !";
  }else{
    echo "Failed !";
  }
}





}
