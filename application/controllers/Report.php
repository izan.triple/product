<?php
class Report extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }

  public function index()
  {
    $userModel          = GAMMA::getModel('user');
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');
    $yearModel          = GAMMA::getModel('year');
    $ownerhomestayModel = GAMMA::getModel('ownerhomestay');
    $homestayuserModel  = GAMMA::getModel('homestayuser');
    
    $key                ="report";
    $data['setting']    = $siteModel->all();
    $roleid             =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['month']      = date('m');
    $data['year']       = date('Y');
    $data['type']       = NULL;
    $uid                = GAMMA::getSession('id');
    $oid                = $homestayuserModel->selectOne(array('userid' => $uid));
    $data['homestay']   = $ownerhomestayModel->listuownerhomestay($oid['ownerid'],$uid);
    $data['listyear']   = $yearModel->all();
    $data['menuname']   = 'Report Information'; 
    $data['main_content'] = 'Report/report_view';
    $data['type']       = NULL; 
    $this->load->view('mainPage',$data);
  }

  public function generate($btn=NULL)
  {
    $siteModel          = GAMMA::getModel('site');
    $menuModel          = GAMMA::getModel('menu');
    $reportModel        = GAMMA::getModel('report');
    $yearModel          = GAMMA::getModel('year');
    $ownerhomestayModel = GAMMA::getModel('ownerhomestay');
    $homestayuserModel  = GAMMA::getModel('homestayuser');

    $btn                = $this->input->post('btn');
    $year               = $this->input->post('year');
    $month              = $this->input->post('month');
    $type               = $this->input->post('type');
    $homestay           = $this->input->post('homestay');

    
   
    $key                ="report";
    $data['setting']    = $siteModel->all();
    $roleid             =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['listyear']   = $yearModel->all();
    $data['key']        = $key;
    $data['month']      = $month;
    $data['year']       = $year;
    $data['menuname']   = 'Report Information';
    $data['type']       = $type; 
    $data['hstay']      = $homestay; 
    $uid                = GAMMA::getSession('id');
    $oid                = $homestayuserModel->selectOne(array('userid' => $uid));
    $data['homestay']   = $ownerhomestayModel->listuownerhomestay($oid['ownerid'],$uid);
    $data['info']       = $reportModel->info($year,$month,$type,$homestay);
    $data['hs']         = NULL;

    foreach($data['homestay'] as $hs){
      if($hs['id'] == $homestay){
        $data['hs']     = $hs['name'];
        // return;
      }
    }

    if($btn == "search"){
    }else if($btn == "pdf"){
        $this->load->library('pdf');
        $html = $this->load->view('Report/GeneratePdfView', $data, true);
        $this->pdf->downloadPDF($html, $data['type'].$data['month'].$data['year'],true);
    }
    $data['main_content'] = 'Report/report_view';
    $this->load->view('mainPage',$data);
  }



}
