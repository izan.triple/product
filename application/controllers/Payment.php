<?php
class Payment extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }


  public function listpayment($id,$hid){
    $paymentModel   = GAMMA::getModel('payment');
    $bookingModel   = GAMMA::getModel('booking');
    $siteModel      = GAMMA::getModel('site');
    $menuModel      = GAMMA::getModel('menu');
    $statusModel    = GAMMA::getModel('status');
    
    $key             ="report";
    $data['setting'] = $siteModel->all();
    $roleid          =  GAMMA::getSession('role');
    $data['menuid']     = $menuModel->getid($key);
    $data['key']        = $key;
    $data['bookingid']  = $id;
    $data['homestayid'] = $hid;
    $data['menuname']   = 'Payment Information';
    $data['booking']    = $bookingModel->selectAll(array('id' => $id));
    $data['paystatus']  = $statusModel->selectOne(array('id' => $data['booking'][0]['status']));;
    $data['payment']    = $paymentModel->selectAll(array('bookingid' => $id));
    $data['main_content'] = 'Payment/payment_view';
    $this->load->view('mainPage',$data);
  }

  public function addpayment(){
    $paymentModel   = GAMMA::getModel('payment');
    $bookingModel   = GAMMA::getModel('booking');
    $siteModel      = GAMMA::getModel('site');
    $menuModel      = GAMMA::getModel('menu');
    
    $datadb['description']  = $this->input->post('paydescrtiption');
    $datadb['bookingid']    = $this->input->post('bookid');
    $datadb['homestayid']   = $this->input->post('homestayid');
    $datadb['date']         = $this->input->post('paydate');
    $datadb['amount']       = $this->input->post('payamount');
    $result                 = $paymentModel->add($datadb);
    
    if($result > 0){
      $booking    = $bookingModel->selectOne(array('id' => $datadb['bookingid']));
      $datadb2['current_payment']  = $booking['current_payment']+$datadb['amount'];
      $datadb2['balance_payment']  = $booking['total'] - ($booking['current_payment']+$datadb['amount']);

      if($datadb2['balance_payment'] == 0){
        $datadb2['status']  = 2;
      }
      $bookingModel->update($datadb2,array('id' => $datadb['bookingid']));
      
      redirect('listpayment/'.$datadb['bookingid'].'/'.$datadb['homestayid']);
    }
  }

}
