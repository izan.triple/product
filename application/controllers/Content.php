<?php
class Content extends CI_Controller
{
	 public function __construct() {
	 	parent::__construct();
    	$this->load->model('login_model');
	 }

	function index(){
		$contentModel   = GAMMA::getModel('content');
		
		$listsetting = $this->login_model->listsetting();
		$data['siteSetting'] 	= $listsetting;
		$data['menuname'] 		= "Content";
		$data['content']		= $contentModel->all();
		$data['main_content'] 	= 'content/content_view';

		// print_r($data['content']);exit;
		$this->load->view('mainPage',$data);
	}

	public function contentaction($btn=NULL)
  	{
    
		$contentModel     = GAMMA::getModel('content');
		$btn			  = $this->input->post('btn');

		if($btn == "add"){
			$data['menuname']     = 'New Page';
			$data['main_content'] = 'content/addcontent_view';
			$this->load->view('mainPage',$data);
		}elseif($btn == "addpage"){
			$datadb['menu'] = $this->input->post('name');
			$datadb['url'] 	= $this->input->post('url');
			$result   = $contentModel->add($datadb);
			redirect('content');
		}else{
		echo $btn;
		}
	} 

	function page($ind){
		$contentModel   = GAMMA::getModel('content');
		$productModel   = GAMMA::getModel('product');

		$listsetting = $this->login_model->listsetting();
		$data['siteSetting'] 	= $listsetting;
		$data['menu']			= $contentModel->all();
		$data['product']		= $productModel->all();
		$data['ind']			= $ind;
		$data['content']		= $contentModel->selectOne(array('menu' => $ind));

		// print_r($data['content']);exit;
		// $this->load->view('view_data', $data);
		
		$this->load->view('home',$data);
	 }

	 public function editcontent($id)
	{
		$contentModel   = GAMMA::getModel('content');
		$siteModel      = GAMMA::getModel('site');
		$menuModel      = GAMMA::getModel('menu');
		
		$key             ="merchant";
		$data['setting'] = $siteModel->all();
		$roleid          =  GAMMA::getSession('role');
		$data['menuid']     = $menuModel->getid($key);
		$data['key']        = $key;
		$data['id']        	= $id;
		$data['menuname']   = 'Content Information';
		$data['content']  	= $contentModel->selectOne(array('id' => $id));

		// print_r($data['ownerdata']);exit;
		$data['main_content'] = 'content/content';

		$this->load->view('mainPage',$data);
	}

	function content(){
		$contentModel   = GAMMA::getModel('content');

		$listsetting 			= $this->login_model->listsetting();
		$btn					= $this->input->post('btn');
		$data['siteSetting'] 	= $listsetting;
		$data['content']		= $contentModel->selectOne(array('menu' => 'home'));
		$id						= $this->input->post('id');
		$datadb['content'] 		= $this->input->post('long_desc');
		$datadb['menu'] 		= $this->input->post('menu');

		if($btn == "save"){
			$result         		= $contentModel->update($datadb,array('id' => $id));
			
			if($result){
				echo "Successfully update !";
			}else{
				echo "Failed !";
			}

			$data['menuname'] 		= "Content";
			redirect('editcontent/'.$id);	
		}else{
			redirect('content');	
		}
			
	}

	
}
