<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends MY_Model {
    protected static $tablename = 'product';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

}

/* End of file Aboutus_model.php */
