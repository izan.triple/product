<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Homestayexpense_model extends MY_Model {
    protected static $tablename = 'expense';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function listexpensehomestay($oid){
        $this->db->select('e.*,s.name');
        $this->db->join('homestay h', 'e.homestayid = h.id', 'left');
        $this->db->join('status s', 'e.status = s.id', 'left');
        $this->db->where('e.homestayid', $oid);
        return $this->db->get('expense e')->result_array();
    }

    public function grepdata(){
        $this->db->select('*');
        $this->db->where('sync', 1);
        return $this->db->get('expense')->result_array();
    }

    public function totalexpense($hid){
        $this->db->select('sum(amount) as total');
        $this->db->where('homestayid', $hid);
        return $this->db->get('expense')->row('total');
	}

}



/* End of file Aboutus_model.php */
