<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Homestayuser_model extends MY_Model {
    protected static $tablename = 'homestay_user';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function listuserhomestay($hid){
        $this->db->select('*');
        $this->db->join('tbl_users u', 'o.userid = u.user_id', 'left');
        $this->db->where('o.homestayid', $hid);
        return $this->db->get('homestay_user o')->result_array();
	}

    public function selectUser($hid,$oid,$uid){
        $this->db->select('*');
        $this->db->where('homestayid', $hid);
        $this->db->where('ownerid', $oid);
        $this->db->where('userid', $uid);
        return $this->db->get('homestay_user')->result_array();
	}

    
}

/* End of file Aboutus_model.php */
