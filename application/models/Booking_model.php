<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_model extends MY_Model {
    protected static $tablename = 'booking';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function bookinfo($hid){
        if(GAMMA::getSession('role') == 3){
            $this->db->select('CONCAT(booking_id) as title,start,DATE_ADD(end_date, INTERVAL 1 DAY) as end');
        }else{
            $this->db->select('CONCAT(customer_name,"(",customer_phone,")") as title,start,DATE_ADD(end_date, INTERVAL 1 DAY) as end');
        }
        $this->db->where('homestayid', $hid);
        $this->db->where('status !=', 4);
        return $this->db->get('booking')->result_array();
	}

    public function listbook($hid){
        $this->db->select('b.*,s.name,u.name as username');
        $this->db->where('b.homestayid', $hid);
        if(GAMMA::getSession('role') == 3){
            $this->db->where('b.created_by', GAMMA::getSession('id'));
        }
        $this->db->join('status as s', 'b.status=s.id');
        $this->db->join('tbl_users as u', 'b.created_by=u.user_id','left');
        $this->db->order_by('b.status', 'DESC');
        return $this->db->get('booking as b')->result_array();
	}

    public function checkbooking($hid,$check_in,$check_out){
        $this->db->select('id');
        $this->db->where('homestayid', $hid);
        $this->db->where('status', 0);
        $this->db->where('start <="'.$check_in.'" AND end_date >="'.$check_in.'" AND status != 4');
        $this->db->or_where('start >="'.$check_in.'" AND end_date <="'.$check_in.'" AND status != 4');
        $this->db->or_where('start <="'.$check_out.'" AND end_date >="'.$check_out.'" AND status != 4');
        $this->db->or_where('start >="'.$check_out.'" AND end_date <="'.$check_out.'" AND status != 4');
        
        return $this->db->get('booking')->result_array();
    }

    public function totalbook($hid){
        $this->db->select('count(id) as total');
        $this->db->where('homestayid', $hid);
        return $this->db->get('booking')->row('total');
	}

    public function grepdata(){
        $this->db->select('*');
        $this->db->where('sync', 1);
        return $this->db->get('booking')->result_array();
    }


}

/* End of file Aboutus_model.php */
