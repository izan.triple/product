<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rndcost_model extends MY_Model {
    protected static $tablename = 'rnd_costing';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function listrnditem($rid){
        $this->db->select('*');
        $this->db->where('o.rndid', $rid);
        return $this->db->get('rnd_costing o')->result_array();
	}

    public function listrndprod($pid){
        $this->db->select('*');
        $this->db->where('o.prodid', $pid);
        return $this->db->get('rnd_costing o')->result_array();
	}

    public function totalcost($hid){
        $this->db->select('sum(total) as total');
        $this->db->where('prodid', $hid);
        return $this->db->get('rnd_costing')->row('total');
	}

    
}

/* End of file Aboutus_model.php */
