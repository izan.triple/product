<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends MY_Model {
    protected static $tablename = 'payment';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function totalpayment($hid){
        $this->db->select('sum(amount) as sum');
        $this->db->where('homestayid', $hid);
        return $this->db->get('payment')->row('sum');
	}

    public function grepdata(){
        $this->db->select('*');
        $this->db->where('sync', 1);
        return $this->db->get('payment')->result_array();
    }

}

/* End of file Aboutus_model.php */
