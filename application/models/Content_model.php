<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Content_model extends MY_Model {
    protected static $tablename = 'content';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function contentAll(){
        $this->db->select('*');
        $this->db->where('status', 0);
        return $this->db->get('content')->result_array();
    }

}

/* End of file Aboutus_model.php */
