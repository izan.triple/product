<?php
class Merchant_model extends CI_Model{

//1. Get Info
  function getmerchant($query,$where){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select($query);
    $DB2->from('merchants as a');
    if($where !=' '){
      $DB2->where("".$where."");
    }
   
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  function liststaff(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('salesman');
    $DB2->order_by('Name','ASC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function liststate(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('state');
    $DB2->order_by('statename','ASC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function  getmerchantinfo($id){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('a.ID',$id);
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

 

  function getmerchantgroup($query,$statid,$searchname){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select($query);
    if($statid == "ba" || $statid == "aa" ){
      $DB2->where('b.ind',$statid);
    }else if($statid != "all"){
      $DB2->where('a.status',$statid);
    }else{}

    if($searchname != ''){
      $DB2->like('RegName', $searchname, 'both'); 
      $DB2->or_like('tradename', $searchname, 'both'); 
      $DB2->or_like('RefNo', $searchname, 'both'); 
      $DB2->or_like('TerminalID', $searchname, 'both'); 
      $DB2->or_like('EDCSerialNo', $searchname, 'both');
      $DB2->or_like('c.Name', $searchname, 'both');
      $DB2->or_like('TIDDebit', $searchname, 'both');
      $DB2->or_like('ContactPerson', $searchname, 'both');
      $DB2->or_like('MIDCredit', $searchname, 'both');
      $DB2->or_like('MIDDebit', $searchname, 'both');
      
    }

        
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->join('salesman as c','a.StaffCode=c.CN','left');
    $DB2->order_by('a.DateSubmit','desc');
    $DB2->group_by('a.RefNo');
    
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getstatus(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('status');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  function get_dashboard(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('b.name,count(*) as total,status');
    $DB2->from('homestay as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->group_by('status');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }
  

  function insert_merchant($data)
  {
    $result = null;
    $DB2 = $this->load->database('db', TRUE);
    $DB2->insert("merchants", $data);
    $result= $DB2->insert_id();
    return  $result;
  }

  
  public function update_merchant($info, $id)
  {
    $DB2 = $this->load->database('db', TRUE);
    $DB2->where('ID',$id);
    $DB2->update('merchants',$info);
    return  $DB2->affected_rows();
  }

  function get_history($id){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('mID',$id);
    $DB2->from('trails');
    $DB2->order_by('aDate','DESC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

    function insertrail($data)
  {
    $result = null;
    $DB2 = $this->load->database('db', TRUE);
    $DB2->insert("trails", $data);
    $result= $DB2->insert_id();
    return  $result;
  }

  //------------------------------------GHL----------------------------
  function get_dashboardghl(){
    $DB2 = $this->load->database('db4', TRUE);
    $result = null;
    $DB2->select('b.name,count(*) as total,status');
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->group_by('status');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function liststaffghl(){
    $DB2 = $this->load->database('db4', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('salesman');
    $DB2->order_by('Name','ASC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getmerchantgroupghl($query,$statid,$searchname){
    $DB2 = $this->load->database('db4', TRUE);
    $result = null;
    $DB2->select($query);
    if($statid == "ba" || $statid == "aa" ){
      $DB2->where('b.ind',$statid);
    }else if($statid != "all"){
      $DB2->where('a.status',$statid);
    }else{}

    if($searchname != ''){
      $DB2->like('RegName', $searchname, 'both'); 
      $DB2->or_like('tradename', $searchname, 'both'); 
      $DB2->or_like('RefNo', $searchname, 'both'); 
      $DB2->or_like('TerminalID', $searchname, 'both'); 
      $DB2->or_like('EDCSerialNo', $searchname, 'both');
      $DB2->or_like('c.Name', $searchname, 'both');
      $DB2->or_like('TIDDebit', $searchname, 'both');
      $DB2->or_like('ContactPerson', $searchname, 'both');
      $DB2->or_like('MIDCredit', $searchname, 'both');
      $DB2->or_like('MIDDebit', $searchname, 'both');
      
    }

        
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->join('salesman as c','a.StaffCode=c.CN','left');
    $DB2->order_by('a.DateSubmit','desc');
    $DB2->group_by('a.RefNo');
    
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function get_historyghl($id){
    $DB2 = $this->load->database('db4', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('mID',$id);
    $DB2->from('trails');
    $DB2->order_by('aDate','DESC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function  getmerchantinfoghl($id){
    $DB2 = $this->load->database('db4', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('a.ID',$id);
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function insert_merchantghl($data)
  {
    $result = null;
    $DB2 = $this->load->database('db4', TRUE);
    $DB2->insert("merchants", $data);
    $result= $DB2->insert_id();
    return  $result;
  }

  public function update_merchantghl($info, $id)
  {
    $DB2 = $this->load->database('db4', TRUE);
    $DB2->where('ID',$id);
    $DB2->update('merchants',$info);
    return  $DB2->affected_rows();
  }

  function getmerchantghl($query,$where){
    $DB2 = $this->load->database('db4', TRUE);
    $result = null;
    $DB2->select($query);
    $DB2->from('merchants as a');
    if($where !=' '){
      $DB2->where("".$where."");
    }
   
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function insertrailghl($data)
  {
    $result = null;
    $DB2 = $this->load->database('db4', TRUE);
    $DB2->insert("trails", $data);
    $result= $DB2->insert_id();
    return  $result;
  }


  //------------------------------------RHB----------------------------
  function get_dashboardrhb(){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select('b.name,count(*) as total,status');
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->group_by('status');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function get_paymentrhb($id){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('mID',$id);
    $DB2->from('userspayment');
    $DB2->order_by('adate','DESC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function get_historyrhb($id){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('mID',$id);
    $DB2->from('trails');
    $DB2->order_by('aDate','DESC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  function getmerchantrhb($query,$where){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select($query);
    $DB2->from('merchants as a');
    if($where !=' '){
      $DB2->where("".$where."");
    }
   
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  function getmerchantgrouprhb($query,$statid,$searchname){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select($query);

    if($statid == "ba" || $statid == "aa" ){
      $DB2->where('b.ind',$statid);
    }else if($statid != "all"){
      $DB2->where('a.status',$statid);
    }else{}

    if($searchname != ''){
      $DB2->like('RegName', $searchname, 'both'); 
      $DB2->or_like('tradename', $searchname, 'both'); 
      $DB2->or_like('RefNo', $searchname, 'both'); 
      $DB2->or_like('TerminalID', $searchname, 'both'); 
      $DB2->or_like('EDCSerialNo', $searchname, 'both');
      $DB2->or_like('c.Name', $searchname, 'both');
      $DB2->or_like('MIDCredit', $searchname, 'both');
    }

    $DB2->group_by('a.RefNo');
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->join('salesman as c','a.StaffCode=c.CN','left');
    $DB2->order_by('a.DateSubmit','desc');
    
    
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function insert_merchantrhb($data)
  {
    $result = null;
    $DB2 = $this->load->database('db2', TRUE);
    $DB2->insert("merchants", $data);
    $result= $DB2->insert_id();
    return  $result;
  }

  public function update_merchantrhb($info, $id)
  {
    $DB2 = $this->load->database('db2', TRUE);
    $DB2->where('ID',$id);
    $DB2->update('merchants',$info);
    return  $DB2->affected_rows();
  }


  function  getmerchantinforhb($id){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('a.ID',$id);
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  function liststaffrhb(){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('salesman');
    $DB2->order_by('Name','ASC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function insertrailrhb($data)
  {
    $result = null;
    $DB2 = $this->load->database('db2', TRUE);
    $DB2->insert("trails", $data);
    $result= $DB2->insert_id();
    return  $result;
  }


  //------------------------------------Ambank----------------------------
  function get_dashboardambank(){
    $DB2 = $this->load->database('db3', TRUE);
    $result = null;
    $DB2->select('b.name,count(*) as total,status');
    $DB2->from('merchant as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->group_by('status');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function liststaffambank(){
    $DB2 = $this->load->database('db3', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('salesman');
    $DB2->order_by('Name','ASC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getmerchantgroupambank($query,$statid,$searchname){
    $DB2 = $this->load->database('db3', TRUE);
    $result = null;
    $DB2->select($query);

    if($statid == "ba" || $statid == "aa" ){
      $DB2->where('b.ind',$statid);
    }else if($statid != "all"){
      $DB2->where('a.status',$statid);
    }else{}

    if($searchname != ''){
      $DB2->like('RegName', $searchname, 'both'); 
      $DB2->or_like('tradename', $searchname, 'both'); 
      $DB2->or_like('RefNo', $searchname, 'both'); 
      $DB2->or_like('TerminalID', $searchname, 'both'); 
      $DB2->or_like('EDCSerialNo', $searchname, 'both');
      $DB2->or_like('c.Name', $searchname, 'both');
    }

        
    $DB2->from('merchant as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->join('salesman as c','a.StaffCode=c.CN','left');
    $DB2->order_by('a.DateSubmit','desc');
    
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }
  
  function insert_merchantambank($data)
  {
    $result = null;
    $DB2 = $this->load->database('db3', TRUE);
    $DB2->insert("merchant", $data);
    $result= $DB2->insert_id();
    return  $result;
  }


  function insertrailambank($data)
  {
    $result = null;
    $DB2 = $this->load->database('db3', TRUE);
    $DB2->insert("trails", $data);
    $result= $DB2->insert_id();
    return  $result;
  }

  function  getmerchantinfoambank($id){
    $DB2 = $this->load->database('db3', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('a.ID',$id);
    $DB2->from('merchant as a');
    $DB2->join('status as b','a.status=b.id','left');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  public function update_merchantambank($info, $id)
  {
    $DB2 = $this->load->database('db3', TRUE);
    $DB2->where('ID',$id);
    $DB2->update('merchant',$info);
    return  $DB2->affected_rows();
  }


  function get_historyambank($id){
    $DB2 = $this->load->database('db3', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('mID',$id);
    $DB2->from('trails');
    $DB2->order_by('aDate','DESC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  function getmerchantambank($query,$where){
    $DB2 = $this->load->database('db3', TRUE);
    $result = null;
    $DB2->select($query);
    $DB2->from('merchant as a');
    if($where !=' '){
      $DB2->where("".$where."");
    }
   
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

}
