<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment_model extends MY_Model {
    protected static $tablename = 'appointment';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function listcountry(){
        $this->db->select('country');
        $this->db->distinct();
        return $this->db->get('appointment')->result_array();
    }

    public function listappmt($ind){
        $this->db->select('*');
        if($ind != 'all'){
            $this->db->like('country', $ind, 'both'); 
        }
        
        return $this->db->get('appointment')->result_array();
    }

}

/* End of file Aboutus_model.php */
