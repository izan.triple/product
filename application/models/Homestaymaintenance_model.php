<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Homestaymaintenance_model extends MY_Model {
    protected static $tablename = 'maintenance';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function listhomestaymaintenance($oid){
        $this->db->select('m.*,s.name');
        $this->db->join('homestay h', 'm.homestayid = h.id', 'left');
        $this->db->join('status s', 'm.status = s.id', 'left');
        $this->db->where('m.homestayid', $oid);
        return $this->db->get('maintenance m')->result_array();
	}

    public function totalmaintenance($hid){
        $this->db->select('sum(amount) as total');
        $this->db->where('homestayid', $hid);
        return $this->db->get('maintenance')->row('total');
	}

    public function grepdata(){
        $this->db->select('*');
        $this->db->where('sync', 1);
        return $this->db->get('maintenance')->result_array();
    }

}

/* End of file Aboutus_model.php */
