<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Ownerhomestay_model extends MY_Model {
    protected static $tablename = 'owner_homestay';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function listownerhomestay($oid){
        $this->db->select('*');
        $this->db->join('homestay h', 'o.homestayid = h.id', 'left');
        $this->db->where('o.ownerid', $oid);
        return $this->db->get('owner_homestay o')->result_array();
	}

    public function listuownerhomestay($oid,$uid){
        $this->db->select('h.id,h.name,h.address1,h.postcode,h.city,h.pic,h.phone');
        $this->db->distinct();
        $this->db->join('homestay h', 'o.homestayid = h.id', 'left');
        $this->db->where('o.ownerid', $oid);
        if(GAMMA::getSession('role') == 3){
            $this->db->where('o.userid', $uid);
        }
        return $this->db->get('homestay_user o')->result_array();
	}

}

/* End of file Aboutus_model.php */
