<?php
class User_model extends MY_Model{

  protected static $tablename = 'tbl_users';  
  public function __construct() {
      parent::__construct();
      date_default_timezone_set("Asia/Kuala_Lumpur");
  }

//1. Get Info
  function getrole(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    // $DB2->where('roleid !=',1);
    $DB2->from('tbl_role');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function get_trails(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    // $DB2->where('roleid !=',1);
    $DB2->from('user_log');
    $DB2->order_by('datetime','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getroleinfo($roleid){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('roleid',$roleid);
    $DB2->from('tbl_role');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function checkroleinfo($roleid){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('fk_roleid',$roleid);
    $DB2->from('tbl_users');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function check_user($pwdnew,$username,$limit){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('username',$username);
    $DB2->from('tbl_passwordhistory');
    $DB2->order_by('datehistory','desc');
    $DB2->limit($limit);
    $query = $DB2->get();
    $result = $query->result();
    return $result;

  }

  function checkmenuid($mid){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('fk_menuid',$mid);
    $DB2->where('fk_submenuid',0);
    $DB2->from('tbl_role_access');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  
  function getarea($area){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    if($area == 0){ }else{$DB2->where('areaid',$area);}
    $DB2->from('tbl_area');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getmedia(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('tbl_media');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getstate(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('tbl_state');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  

  function getaccess($rid,$mid,$ind){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('accessid');
    $DB2->from('tbl_role_access');
    $DB2->where('fk_roleid',$rid);
    if($ind == 2){
      $DB2->where('fk_menuid',$mid);
    }else{
      $DB2->where('fk_submenuid',$mid);
    }

    $query = $DB2->get();
    return $query->row('accessid');
  }


  function getroleinfo2($roleid,$ind){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('a.roleid',$roleid);
      if($ind == 1){
        $DB2->where('b.fk_submenuid',0);
      }
      $DB2->from('tbl_role as a');
      $DB2->join('tbl_role_access as b','a.roleid=b.fk_roleid','left');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
    }

    function getuser($area){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      if($area == 0){ }else{$DB2->where('a.fk_areaid',$area);}
      $DB2->from('tbl_users as a');
      $DB2->join('tbl_role as b','b.roleid = a.fk_roleid','left');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
    }

    function getuserinfo($area,$id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      if($area == 0){ }else{$DB2->where('a.fk_areaid',$area);}
      $DB2->where('a.user_id',$id);
      $DB2->from('tbl_users as a');
      $DB2->join('tbl_role as b','b.roleid = a.fk_roleid','left');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
    }



//2. Insert Info-------------------------------------------------------------------------
function insert_tblrole($data)
  {
    $result = null;
    $DB2 = $this->load->database('db', TRUE);
    $DB2->insert("tbl_role", $data);
    $result= $DB2->insert_id();
    return  $result;
  }

  function insert_user($data)
  {
    $result = null;
    $DB2 = $this->load->database('db', TRUE);
    $DB2->insert("tbl_users", $data);
    $result= $DB2->insert_id();
    return  $result;
  }


  function user_trail($data)
  {
    $result = null;
    $DB2 = $this->load->database('db', TRUE);
    $DB2->insert("user_log", $data);
    $result= $DB2->insert_id();
    return  $result;
  }

  function password_history($data)
  {
    $result = null;
    $DB2 = $this->load->database('db', TRUE);
    $DB2->insert("tbl_passwordhistory", $data);
    $result= $DB2->insert_id();
    return  $result;
  }



  function insert_access($data)
    {
      $result = null;
      $DB2 = $this->load->database('db', TRUE);
      $DB2->insert("tbl_role_access", $data);
      $result= $DB2->insert_id();
      return  $result;
    }

    function insert_tblpersonnelprofile($data)
      {
        $result = null;
        $DB2 = $this->load->database('db', TRUE);
        $DB2->insert("tbl_personnel", $data);
        $result= $DB2->insert_id();
        return  $result;
      }



//3. Delete Info----------------------------------------------------------------------------
  function del_role($id){
    $DB2 = $this->load->database('db', TRUE);
    $DB2->where('roleid', $id);
    $DB2->delete('tbl_role');
    return  $DB2->affected_rows();
  }

  function del_access($id){
    $DB2 = $this->load->database('db', TRUE);
    $DB2->where('accessid', $id);
    $DB2->delete('tbl_role_access');
    return  $DB2->affected_rows();
  }

  function del_user($id){
    $DB2 = $this->load->database('db', TRUE);
    $DB2->where('user_id', $id);
    $DB2->delete('tbl_users');
    return  $DB2->affected_rows();
  }


  //4. Update Information--------------------------------------------------------------------
  public function update_access($info,$rid,$mid,$ind)
  {
    $DB2 = $this->load->database('db', TRUE);
    $DB2->where('fk_roleid',$rid);
    if($ind == 2){
      $DB2->where('fk_menuid',$mid);
    }else{
      $DB2->where('fk_submenuid',$mid);
    }
    $DB2->update('tbl_role_access',$info);
    return TRUE;
  }

  public function update_user($info, $id)
  {
    $DB2 = $this->load->database('db', TRUE);
    $DB2->where('user_id',$id);
    $DB2->update('tbl_users',$info);
    return  $DB2->affected_rows();
  }

}
