<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Sale_model extends MY_Model {
    protected static $tablename = 'sale';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function listsale($prodid){
        $this->db->select('b.*,s.name,u.name as username');
        $this->db->where('b.prodid', $prodid);
        $this->db->join('status as s', 'b.status=s.id');
        $this->db->join('tbl_users as u', 'b.created_by=u.user_id','left');
        $this->db->order_by('b.status', 'DESC');
        return $this->db->get('sale as b')->result_array();
	}

    public function listsalenew($prodid){
        $this->db->select('b.*,s.name,u.name as username');
        $this->db->where('b.prodid', $prodid);
        $this->db->where('b.status', 0);
        $this->db->join('status as s', 'b.status=s.id');
        $this->db->join('tbl_users as u', 'b.created_by=u.user_id','left');
        $this->db->order_by('b.status', 'DESC');
        return $this->db->get('sale as b')->result_array();
	}

}

/* End of file Aboutus_model.php */
