<?php
class Setting_model extends CI_Model{


      public function viewUser($id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->from('tbl_users as a');
      $DB2->join('tbl_company as b', 'a.compid = b.compid','LEFT');
      $DB2->join('tbl_role as c', 'a.user_level = c.roleid','LEFT');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function viewcomp(){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->from('tbl_company');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getrole(){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->from('tbl_role');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getaccess($id,$compid,$roleid){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('fk_pkgid', $id);
      $DB2->where('fk_roleid', $roleid);
      $DB2->where('compid', $compid);
      $DB2->from('tbl_role_access');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getaccessrole($compid,$roleid){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('fk_roleid', $roleid);
      $DB2->where('compid', $compid);
      $DB2->from('tbl_role_access');
      $DB2->join('tbl_menu as b', 'fk_menuid = b.menuid','LEFT');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getpkg($id,$compid,$roleid){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('a.compid', $compid);
      $DB2->from('tbl_package as a');
      $DB2->join('tbl_menu as b', 'a.fk_menuid = b.menuid','LEFT');
      $DB2->join('tbl_role_access as c', 'a.pkgid = c.fk_pkgid and c.fk_roleid='.$roleid,'LEFT');
      $DB2->order_by('b.menuid');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getpkgall(){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->from('tbl_package');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getlistall(){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->from('tbl_list');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getlistInfo($id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('listid', $id);
      $DB2->from('tbl_list');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getuserInfo($id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('user_id', $id);
      $DB2->from('tbl_users');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getuserEmailInfo($email,$compid){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('user_email', $email);
      $DB2->where('compid', $compid);
      $DB2->from('tbl_users');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getmenuall(){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->from('tbl_menu');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getSubMenu2($id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('menuid', $id);
      $DB2->from('tbl_submenu');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      
      public function inactive($day){
        $DB2 = $this->load->database('db', TRUE);
        $result = null;
          $DB2->select('*,DATEDIFF(CURDATE(), lastLogin) as day');
          $DB2->where('DATEDIFF(CURDATE(), lastLogin) >'.$day);
          $DB2->where('userstatus','Active');
          $DB2->or_where('lastLogin', '0000-00-00 00:00:00');
          $DB2->from('tbl_users');
          // return $DB2->get()->result_array();
          $query = $DB2->get();
          $result = $query->result();
          return $result;
        
      }


      public function getcountry($id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('listcat', $id);
      $DB2->from('tbl_list');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getpkginfo($id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('listcat', $id);
      $DB2->from('tbl_list as a');
      $DB2->join('tbl_package_info as b', 'a.listcode = b.fk_listcode' ,'LEFT');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getpkgmenu($id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('pkgcode', $id);
      $DB2->from('tbl_package as a');
      $DB2->join('tbl_menu as b', 'a.fk_menuid = b.menuid' ,'LEFT');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getcompInfo($id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('compid', $id);
      $DB2->from('tbl_company');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
      }

      public function getstateInfo($id){
        $DB2 = $this->load->database('db', TRUE);
        $result = null;
        $DB2->select('*');
        $DB2->from('tbl_list');
        $DB2->where('listmemberfk', $id);
        $query = $DB2->get();
        $result = $query->result();
        return $result;
        }

          

      //**************insert info Section****************
      public function insert_role($data){

        $result = null;
        $this->db->insert("tbl_role", $data);
        $result= $this->db->insert_id();
        return  $result;
      }

      public function insert_access($data){
        $result = null;
        $this->db->insert("tbl_role_access", $data);
        $result= $this->db->insert_id();
        return  $result;
      }

      public function insert_list($data){
        $result = null;
        $this->db->insert("tbl_list", $data);
        $result= $this->db->insert_id();
        return  $result;
      }

      public function insert_year($data){
        $result = null;
        $this->db->insert("tbl_year", $data);
        $result= $this->db->insert_id();
        return  $result;
      }

      public function insert_pkgmenu($data){
        $result = null;
        $this->db->insert("tbl_package", $data);
        $result= $this->db->insert_id();
        return  $result;
      }

      public function insert_menu($data){
        $result = null;
        $this->db->insert("tbl_menu", $data);
        $result= $this->db->insert_id();
        return  $result;
      }

      public function insert_submenu($data){
        $result = null;
        $this->db->insert("tbl_submenu", $data);
        $result= $this->db->insert_id();
        return  $result;
      }

      public function insert_user($data){
        $result = null;
        $this->db->insert("tbl_users", $data);
        $result= $this->db->insert_id();
        return  $result;
      }

      public function insert_comp($data){
        $result = null;
        $this->db->insert("tbl_company", $data);
        $result= $this->db->insert_id();
        return  $result;
      }

      //***********update Info Section**************
      function updatesitesetting($info, $id)
        {
            $DB2 = $this->load->database('db', TRUE);
            $DB2->where('id', $id);
            $DB2->update('tbl_sitesetting',$info);
            return $DB2->affected_rows();
        }

        function updatemenu($info, $id)
          {
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('menuid', $id);
              $DB2->update('tbl_menu',$info);
              return $DB2->affected_rows();
          }

          function updatelist($info, $id)
            {
                $DB2 = $this->load->database('db', TRUE);
                $DB2->where('listid', $id);
                $DB2->update('tbl_list',$info);
                return $DB2->affected_rows();
            }

        function updatesubmenu($info, $id)
          {
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('submenuid', $id);
              $DB2->update('tbl_submenu',$info);
              return $DB2->affected_rows();
          }

          function updaterole($info, $id)
            {
                $DB2 = $this->load->database('db', TRUE);
                $DB2->where('roleid', $id);
                $DB2->update('tbl_role',$info);
                return $DB2->affected_rows();
            }

            function updateuser($info, $id)
              {
                  $DB2 = $this->load->database('db', TRUE);
                  $DB2->where('user_id', $id);
                  $DB2->update('tbl_users',$info);
                  return $DB2->affected_rows();
              }


              function updatecomp($info, $id)
                {
                    $DB2 = $this->load->database('db', TRUE);
                    $DB2->where('compid', $id);
                    $DB2->update('tbl_company',$info);
                    return $DB2->affected_rows();
                }

        function update_access($info, $id, $compid,$roleid)
          {
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('fk_pkgid', $id);
              $DB2->where('fk_roleid', $roleid);
              $DB2->where('compid', $compid);
              $DB2->update('tbl_role_access',$info);
              return $DB2->affected_rows();
          }
      //********************Get info details Section********

          public function getsubInfo($id){
            $DB2 = $this->load->database('db', TRUE);
            $result = null;
            $DB2->select('*');
            $DB2->from('tbl_submenu');
            $DB2->where('submenuid', $id);
            return $DB2->get()->result_array();
            }

            public function getmenuInfo($id){
              $DB2 = $this->load->database('db', TRUE);
              $result = null;
              $DB2->select('*');
              $DB2->from('tbl_menu');
              $DB2->where('menuid', $id);
              return $DB2->get()->result_array();
              }

              public function getroleInfo($id){
                $DB2 = $this->load->database('db', TRUE);
                $result = null;
                $DB2->select('*');
                $DB2->from('tbl_role');
                $DB2->where('roleid', $id);
                return $DB2->get()->result_array();
                }


                public function getyearInfo($id){
                  $DB2 = $this->load->database('db', TRUE);
                  $result = null;
                  $DB2->select('*');
                  $DB2->from('tbl_year');
                  $DB2->where('yearname', $id);
                  return $DB2->get()->result_array();
                  }


                  public function get_cat(){
                    $DB2 = $this->load->database('db', TRUE);
                    $result = null;
                    $DB2->select('*');
                    $DB2->from('tbl_accountcategory');
                    $query = $DB2->get();
                    $result = $query->result();
                    return $result;
                    }

                  public function getyeardata($id){
                    $DB2 = $this->load->database('db', TRUE);
                    $result = null;
                    $DB2->select('*');
                    $DB2->from('tbl_accopen');
                    $DB2->where('year', $id);
                    return $DB2->get()->result_array();
                    }

                    public function getcatdata($id){
                      $DB2 = $this->load->database('db', TRUE);
                      $result = null;
                      $DB2->select('*');
                      $DB2->from('tbl_accitem');
                      $DB2->where('accItemCat', $id);
                      return $DB2->get()->result_array();
                      }





      //********************Delete Section ****************
            function delsubInfo($id){
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('submenuid', $id);
              $DB2->delete('tbl_submenu');
              return $DB2->affected_rows();
            }

            function delmenuInfo($id){
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('menuid', $id);
              $DB2->delete('tbl_menu');
              return $DB2->affected_rows();
            }

            function dellistInfo($id){
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('listid', $id);
              $DB2->delete('tbl_list');
              return $DB2->affected_rows();
            }

            function delyearInfo($id){
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('yearname', $id);
              $DB2->delete('tbl_year');
              return $DB2->affected_rows();
            }

            function delcatInfo($id){
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('acccatid', $id);
              $DB2->delete('tbl_accountcategory');
              return $DB2->affected_rows();
            }

            function delroleInfo($id){
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('roleid', $id);
              $DB2->delete('tbl_role');
              return $DB2->affected_rows();
            }

            function deluserInfo($id){
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('user_id', $id);
              $DB2->delete('tbl_users');
              return $DB2->affected_rows();
            }

            function delcompInfo($id){
              $DB2 = $this->load->database('db', TRUE);
              $DB2->where('compid', $id);
              $DB2->delete('tbl_company');
              return $DB2->affected_rows();
            }

            public function resetsaledata($ind){
              if($ind == 'sale'){
                $this->db->truncate('tbl_sale');
                $this->db->truncate('tbl_saledetails');
                $this->db->truncate('tbl_salepayment');
                $this->db->truncate('tbl_salepaymenthistory');
              }else if($ind == 'account'){
                $this->db->truncate('tbl_accitem');
                $this->db->truncate('tbl_accopen');
                $this->db->truncate('tbl_account');
                $this->db->truncate('tbl_accountmonth');
              }else if($ind == 'purchase'){
                $this->db->truncate('tbl_purchase');
                $this->db->truncate('tbl_purchasedetails');
                $this->db->truncate('tbl_purchasepayment');
                $this->db->truncate('tbl_purchasepaymenthistory');
              }else if($ind == 'stock'){
                $this->db->truncate('tbl_stock');
                $this->db->truncate('tbl_stockcategory');
                $this->db->truncate('tbl_stockhistory');
              }else if($ind == 'rno'){
                $this->db->truncate('tbl_running');
              }else if($ind == 'cust'){
                $this->db->truncate('tbl_cust');
              }else if($ind == 'all'){

                $this->db->where('supplierid !=', 1);
                $this->db->delete('tbl_supplier');
                $this->db->truncate('tbl_sale');
                $this->db->truncate('tbl_saledetails');
                $this->db->truncate('tbl_salepayment');
                $this->db->truncate('tbl_salepaymenthistory');
                $this->db->truncate('tbl_purchase');
                $this->db->truncate('tbl_purchasedetails');
                $this->db->truncate('tbl_purchasepayment');
                $this->db->truncate('tbl_purchasepaymenthistory');
                $this->db->truncate('tbl_stock');
                $this->db->truncate('tbl_stockcategory');
                $this->db->truncate('tbl_stockhistory');
                $this->db->truncate('tbl_running');
                $this->db->truncate('tbl_accitem');
                $this->db->truncate('tbl_accopen');
                $this->db->truncate('tbl_account');
                $this->db->truncate('tbl_accountmonth');
              }

            }
    
    function getsalesman($query,$statid,$searchname){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select($query);

      if($statid){
        $DB2->where('status',$statid);
      }
  
      if($searchname != ''){
        $DB2->like('CN', $searchname, 'both'); 
        $DB2->or_like('Name', $searchname, 'both'); 
      }     
      $DB2->from('salesman');
      $DB2->order_by('Name','desc');    
      $query = $DB2->get();
      $result = $query->result();
      return $result;
    }

    function  getsalemaninfo($bank,$id){
      if($bank == "ghl"){
        $DB2 = $this->load->database('db4', TRUE);
      }else{
        $DB2 = $this->load->database('db', TRUE);
      }
      $result = null;
      $DB2->select('*');
      $DB2->where('ID',$id);
      $DB2->from('salesman');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
    }


  function insert_salesman($bank,$data)
  {
    $result = null;
    if($bank == "ghl"){
      $DB2 = $this->load->database('db4', TRUE);
    }else{
      $DB2 = $this->load->database('db', TRUE);
    }
    $DB2->insert("salesman", $data);
    $result= $DB2->insert_id();
    return  $result;
  }


  public function update_salesman($bank,$info, $id)
  {
    if($bank == "ghl"){
      $DB2 = $this->load->database('db4', TRUE);
    }else{
      $DB2 = $this->load->database('db', TRUE);
    }
    $DB2->where('ID',$id);
    $DB2->update('salesman',$info);
    return  $DB2->affected_rows();
  }

  //-----------------------------------GHL--------------------------

  function getsalesmanghl($query,$statid,$searchname){
    $DB2 = $this->load->database('db4', TRUE);
    $result = null;
    $DB2->select($query);

    if($statid){
      $DB2->where('status',$statid);
    }

    if($searchname != ''){
      $DB2->like('CN', $searchname, 'both'); 
      $DB2->or_like('Name', $searchname, 'both'); 
    }     
    $DB2->from('salesman');
    $DB2->order_by('Name','desc');    
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  //-----------------------------------RHB--------------------------

  function getsalesmanrhb($query,$statid,$searchname){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select($query);

    if($statid){
      $DB2->where('status',$statid);
    }

    if($searchname != ''){
      $DB2->like('CN', $searchname, 'both'); 
      $DB2->or_like('Name', $searchname, 'both'); 
    }     
    $DB2->from('salesman');
    $DB2->order_by('Name','desc');    
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

}
