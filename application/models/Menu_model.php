<?php
class Menu_model extends MY_Model{

  protected static $tablename = 'menu';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

  public function getid($url){
    $this->db->select('*');
    $this->db->where('menuurl',$url);
    return $this->db->get('tbl_menu')->row('menuid');
  }

  public function getsitesetting(){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->from('tbl_sitesetting');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
    }

    public function displaymenu(){
        $DB2 = $this->load->database('db', TRUE);
        $result = null;
        $DB2->select('*');
        $DB2->where('menutype !=',1 );
        $DB2->from('tbl_menu');
        $query = $DB2->get();
        $result = $query->result();
        return $result;
      }

    public function getadmininfo(){
        $DB2 = $this->load->database('db', TRUE);
        $result = null;
        $DB2->select('*');
        $DB2->where('fk_roleid',3 );
        $DB2->from('tbl_users');
        $DB2->limit(1);
        $query = $DB2->get();
        $result = $query->result();
        return $result;
      }

    public function getqmenu(){
        $DB2 = $this->load->database('db', TRUE);
        $result = null;
        $DB2->select('*');
        $DB2->where('menuquick','Y');
        $DB2->where('submenustatus','Active');
        $DB2->from('tbl_submenu');
        $query = $DB2->get();
        $result = $query->result();
        return $result;
      }


  public function getMenu($id){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->where('c.menustate','Active');
      $DB2->where('a.fk_roleid',$id);
      $DB2->where('a.fk_submenuid',0);
      $DB2->from('tbl_role_access as a');
      $DB2->join('tbl_menu as c', 'a.fk_menuid = c.menuid','LEFT');
      $DB2->order_by('c.menuid',"ASC");
      $query = $DB2->get();
      $result = $query->result();
      return $result;
    }

    public function getSubMenu($id){
        $DB2 = $this->load->database('db', TRUE);
        $result = null;
        $DB2->select('*');
        $DB2->where('c.submenustatus','Active');
        $DB2->where('a.fk_roleid',$id);
        $DB2->where('a.fk_submenuid !=',0);
        $DB2->from('tbl_role_access as a');
        $DB2->join('tbl_submenu as c', 'a.fk_submenuid = c.submenuid','LEFT');
        $query = $DB2->get();
        $result = $query->result();
        return $result;
      }

      // public function getid($url){
      //     $DB2 = $this->load->database('db', TRUE);
      //     $result = null;
      //     $DB2->select('*');
      //     $DB2->from('tbl_menu');
      //     $DB2->where('menuurl',$url);
      //     $query = $DB2->get();
      //     return $query->row('menuid');
      //   }

//Get role access
    function get_access($roleid,$mid){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('access');
    $DB2->from('tbl_role_access');
    $DB2->where('fk_roleid',$roleid);
    $DB2->where('fk_menuid',$mid);
    $query = $DB2->get();
    return $query->row('access');
  }

  function get_submenu2(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('a.submenustatus','Active');
    $DB2->from('tbl_submenu as a');
    $DB2->join('tbl_menu as b','a.menuid = b.menuid','left');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function get_allmenu(){
  $DB2 = $this->load->database('db', TRUE);
  $result = null;
  $DB2->select('*');
  $DB2->where('menustate','Active');
  $DB2->where('menutype !=',1);
  $DB2->from('tbl_menu');
  $DB2->order_by('menuid','ASC');
  $query = $DB2->get();
  $result = $query->result();
  return $result;
}

function displaymenuitms($id){
$DB2 = $this->load->database('db', TRUE);
$result = null;
$DB2->select('*');
$DB2->where('menuid',$id);
$DB2->from('tbl_submenu');
$DB2->order_by('menuid','ASC');
$query = $DB2->get();
$result = $query->result();
return $result;
}



//2. Insert model
//*****Insert addmenu********************************
public function insert_addmenu($data){
 $result = null;
 $this->db->insert("tbl_menu", $data);
 $result= $this->db->insert_id();
 return  $result;
}

//*****Insert addsubmenu********************************
 public function insert_addsubmenu($data){
  $result = null;
  $this->db->insert("tbl_submenu", $data);
  $result= $this->db->insert_id();
  return  $result;
 }

}
