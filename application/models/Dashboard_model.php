<?php
class Dashboard_model extends CI_Model{

  function getform(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('tbl_form');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getfunction(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('tbl_function');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getitmform($id){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('formid',$id);
    $DB2->from('tbl_formitem');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }



  public function getcode(){
      $DB2 = $this->load->database('db', TRUE);
      $result = null;
      $DB2->select('*');
      $DB2->from('code');
      $query = $DB2->get();
      $result = $query->result();
      return $result;
    }

    public function getforminfo($id){
        $DB2 = $this->load->database('db', TRUE);
        $result = null;
        $DB2->select('*');
        $DB2->where('a.formid',$id);
        $DB2->from('tbl_form as a');
        $DB2->join('tbl_formitem as b','a.formid=b.formid','LEFT');
        $query = $DB2->get();
        $result = $query->result();
        return $result;
      }

      public function getfuncinfo($id){
          $DB2 = $this->load->database('db', TRUE);
          $result = null;
          $DB2->select('*');
          $DB2->where('functionid',$id);
          $DB2->from('tbl_function');
          $query = $DB2->get();
          $result = $query->result();
          return $result;
        }


    public function insert_form($data){
       $result = null;
       $this->db->insert("tbl_form", $data);
       $result= $this->db->insert_id();
       return  $result;
    }

    public function insert_itmform($data){
       $result = null;
       $this->db->insert("tbl_formitem", $data);
       $result= $this->db->insert_id();
       return  $result;
    }

    public function addfunction($data){
      $result = null;
      $this->db->insert(" tbl_function", $data);
      $result= $this->db->insert_id();
      return  $result;
    }

    //*****delete function********************************
      function delete_function($id){
        $DB2 = $this->load->database('db', TRUE);
        $DB2->where('functionid', $id);
        $DB2->delete('tbl_function');
        return TRUE;
      }

  function resetsaledata($ind){
    $DB2 = $this->load->database('db', TRUE);
     if($ind == 'all'){
    }else if($ind == 'booking'){
      $DB2->truncate('booking');
    }else if($ind == 'payment'){
      $DB2->truncate('payment');
    }else if($ind == 'customer'){
      $DB2->truncate('customer');
    }else if($ind == 'homestay'){
      $DB2->truncate('homestay');
    }else if($ind == 'owner'){
      $DB2->truncate('owner');
    }else if($ind == 'expense'){
      $DB2->truncate('expense');
    }else if($ind == 'maintenance'){
      $DB2->truncate('maintenance');
    }
  }

}
