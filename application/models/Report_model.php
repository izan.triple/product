<?php
class Report_model extends MY_Model{

  public function info($year,$month,$type,$homestay){
    $this->db->select('a.*,s.name,h.name as homestayname');
    $this->db->join('status as s','a.status=s.id');
    $this->db->join('homestay as h','a.homestayid=h.id');
    $this->db->where('MONTH(a.date)',$month);
    $this->db->where('MONTH(date)',$month);
    $this->db->where('a.homestayid',$homestay);
    return $this->db->get(''.$type.' as a')->result_array();
  }

}