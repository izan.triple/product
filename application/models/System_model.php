<?php
class System_model extends CI_Model{

//1. Get Info-------------------------------------------------------------------------
function liststate(){
  $DB2 = $this->load->database('db', TRUE);
  $result = null;
  $DB2->select('*');
  $DB2->from('tbl_state');
  $query = $DB2->get();
  $result = $query->result();
  return $result;
}

function listarea($area){
  $DB2 = $this->load->database('db', TRUE);
  $result = null;
  $DB2->select('*');
  if($area == 0){}else{$DB2->where('a.areaid',$area);}
  $DB2->from('tbl_area as a');
  $DB2->join('tbl_owner as b','a.fk_ownerid = b.ownerid','LEFT');
  $query = $DB2->get();
  $result = $query->result();
  return $result;
}

function listowner(){
  $DB2 = $this->load->database('db', TRUE);
  $result = null;
  $DB2->select('*');
  $DB2->from('tbl_owner as a');
  $DB2->join('tbl_area as b','a.ownerid = b.fk_ownerid','left');
  $query = $DB2->get();
  $result = $query->result();
  return $result;
}

function listcountry(){
  $DB2 = $this->load->database('db', TRUE);
  $result = null;
  $DB2->select('*');
  $DB2->from('tbl_country');
  $query = $DB2->get();
  $result = $query->result();
  return $result;
}


//2. Insert Info-------------------------------------------------------------------------
function insert_state($data)
{
  $result = null;
  $DB2 = $this->load->database('db', TRUE);
  $DB2->insert("tbl_state", $data);
  $result= $DB2->insert_id();
  return  $result;
}

function insert_area($data)
{
  $result = null;
  $DB2 = $this->load->database('db', TRUE);
  $DB2->insert("tbl_area", $data);
  $result= $DB2->insert_id();
  return  $result;
}

function insert_owner($data)
{
  $result = null;
  $DB2 = $this->load->database('db', TRUE);
  $DB2->insert("tbl_owner", $data);
  $result= $DB2->insert_id();
  return  $result;
}

//3. Delete Info----------------------------------------------------------------------------


//4. Update Information--------------------------------------------------------------------


}
