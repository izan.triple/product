<?php
class Merchant_model extends CI_Model{

//1. Get Info
  function getmerchant($query,$where){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select($query);
    $DB2->from('merchants as a');
    if($where !=' '){
      $DB2->where("".$where."");
    }
   
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  function liststaff(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('salesman');
    $DB2->order_by('Name','ASC');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function  getmerchantinfo($id){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->where('a.ID',$id);
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

 

  function getmerchantgroup($query,$statid,$searchname){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select($query);
    if($statid == "ba" || $statid == "aa" ){
      $DB2->where('b.ind',$statid);
    }else if($statid != "all"){
      $DB2->where('a.status',$statid);
    }else{}

    if($searchname != ''){
      $DB2->like('RegName', $searchname, 'both'); 
      $DB2->or_like('RefNo', $searchname, 'both'); 
      $DB2->or_like('TerminalID', $searchname, 'both'); 
      $DB2->or_like('EDCSerialNo', $searchname, 'both');
      $DB2->or_like('c.Name', $searchname, 'both');
    }

        
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->join('salesman as c','a.StaffCode=c.CN','left');
    $DB2->order_by('a.DateSubmit','desc');
    
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getstatus(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('*');
    $DB2->from('status');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }


  function get_dashboard(){
    $DB2 = $this->load->database('db', TRUE);
    $result = null;
    $DB2->select('b.name,count(*) as total,status');
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->group_by('status');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }
  

  function insert_merchant($data)
  {
    $result = null;
    $DB2 = $this->load->database('db', TRUE);
    $DB2->insert("merchants", $data);
    $result= $DB2->insert_id();
    return  $result;
  }

  public function update_merchant($info, $id)
  {
    $DB2 = $this->load->database('db', TRUE);
    $DB2->where('ID',$id);
    $DB2->update('merchants',$info);
    return  $DB2->affected_rows();
  }



  //------------------------------------RHB----------------------------
  function get_dashboardrhb(){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select('b.name,count(*) as total,status');
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->group_by('status');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  function getmerchantrhb($query,$where){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select($query);
    $DB2->from('merchants as a');
    if($where !=' '){
      $DB2->where("".$where."");
    }
   
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->order_by('a.DateSubmit','desc');
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }



  function getmerchantgrouprhb($query,$statid,$searchname){
    $DB2 = $this->load->database('db2', TRUE);
    $result = null;
    $DB2->select($query);
    if($statid != 0){
      $DB2->where('status',$statid);
    }

    if($searchname != ''){
      $DB2->like('RegName', $searchname, 'both'); 
      $DB2->or_like('RefNo', $searchname, 'both'); 
      $DB2->or_like('TerminalID', $searchname, 'both'); 
      $DB2->or_like('EDCSerialNo', $searchname, 'both');
      $DB2->or_like('c.Name', $searchname, 'both');
    }

        
    $DB2->from('merchants as a');
    $DB2->join('status as b','a.status=b.id','left');
    $DB2->join('salesman as c','a.StaffCode=c.CN','left');
    $DB2->order_by('a.DateSubmit','desc');
    
    $query = $DB2->get();
    $result = $query->result();
    return $result;
  }

  

}
