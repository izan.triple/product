<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rndstock_model extends MY_Model {
    protected static $tablename = 'rnd_stock';  
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function listrnditem($rid){
        $this->db->select('*');
        $this->db->where('o.rndid', $rid);
        return $this->db->get('rnd_item o')->result_array();
	}

    
}

/* End of file Aboutus_model.php */
